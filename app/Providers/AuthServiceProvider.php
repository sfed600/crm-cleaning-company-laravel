<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Models\Setting::class => \App\Policies\SettingPolicy::class,
        \App\Models\Order::class => \App\Policies\OrderPolicy::class,
        \App\Models\Tabel::class => \App\Policies\TabelPolicy::class,
        \App\Models\Sms::class => \App\Policies\SmsPolicy::class,
        \App\Models\Company::class => \App\Policies\CompanyPolicy::class,
        \App\Models\Auto::class => \App\Policies\AutoPolicy::class,
        \App\Models\Calendar::class => \App\Policies\CalendarPolicy::class,
        \App\Models\OrderContact::class => \App\Policies\OrderContactPolicy::class,
        \App\Models\OrderCompany::class => \App\Policies\OrderCompanyPolicy::class,
        \App\User::class => \App\Policies\UserPolicy::class,
        \App\Models\Service::class => \App\Policies\ServicePolicy::class,
        \App\Models\UserProfession::class => \App\Policies\UserProfessionPolicy::class,
        \App\Models\Offer::class => \App\Policies\OfferPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        //права на регистрацию пользователей
        $gate->define('register-user', function ($user) {
            return $user->group()->first()->name == 'Admin';
        });


    }

}
