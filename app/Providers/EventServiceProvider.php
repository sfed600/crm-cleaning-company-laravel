<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    /*protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];*/
    protected $listen = [
        'App\Events\onOrderAddEvent' => [
            'App\Listeners\AddOrderListener',
        ],
        'App\Events\onOrderUpdateEvent' => [
            'App\Listeners\UpdateOrderListener',
        ],
        'App\Events\onUserAddEvent' => [
            'App\Listeners\AddUserListener',
        ],
        'App\Events\onUserUpdateEvent' => [
            'App\Listeners\UpdateUserListener',
        ],
        'App\Events\onOrderContactAddEvent' => [
            'App\Listeners\AddOrderContactListener',
        ],
        'App\Events\onOrderContactUpdateEvent' => [
            'App\Listeners\UpdateOrderContactListener',
        ],
        'App\Events\onOrderCompanyAddEvent' => [
            'App\Listeners\AddOrderCompanyListener',
        ],
        'App\Events\onOrderCompanyUpdateEvent' => [
            'App\Listeners\UpdateOrderCompanyListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        /*$events->listen('onOrderUpdateEvent', function (\App\Models\Order $order, \App\User $user) {
            dd($order);
        });*/
    }
}
