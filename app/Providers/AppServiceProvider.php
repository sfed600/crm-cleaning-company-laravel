<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Validator;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function ($query){
            //Log::info($query->sql);
            //Log::info($query->bindings);
        });

        Validator::extend('sysname', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^[a-zA-Z0-9_-]+$/u', $value);
        });

        Validator::extend('youtube_code', function($attribute, $value, $parameters, $validator) {
            return preg_match('/^[a-zA-Z0-9_-]{11}$/u', $value);
        });

        Validator::extend('not_empty', function($attribute, $value, $parameters, $validator) {
            if(gettype($value) == 'array') {
                if(!empty($value) && !empty($value[0])) {
                    return true;
                }
            } elseif(!empty($value)) {
                return true;
            }
            return false;
        });

        Validator::extend('unique_with', function($attribute, $value, $parameters, $validator) {
            $result = DB::table($parameters[0])->where($attribute, $value)->where($parameters[1], $parameters[2]);
            if(!empty($parameters[3]) && $parameters[3]) {
                $result->where('id', '!=', $parameters[3]);
            }
            $result = $result->first();
            return $result ? false : true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
