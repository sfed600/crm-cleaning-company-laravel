<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use DB;

//class AuthNewController extends AuthController
class AuthNewController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            //'password' => 'required|min:6|confirmed',
            'password' => 'min:6',
            'phone' => 'required|min:11',
        ]);
    }

    public function registerCompany(Request $request)
    {
        //dd($request->all());
        if ($request->isMethod('get')) {
            return view('auth.register_company', ['request' => $request]);
        }
        else if ($request->isMethod('post'))
        {
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                $this->throwValidationException(
                    $request, $validator
                );
                //echo 'err';
            }
            else {
                //проверить номер уникальность
                $number = substr(preg_replace('|[^0-9]+|','',$request->phone), -10, 10);
                if( \App\Models\UserPhone::where('number', '=', $number)->count() > 0 )
                    return redirect()->back()->withErrors(['Номер телефона уже зарегистрирован!']);

                //create user
                $data = $request->all();

                $group = \App\Models\UserGroup::where('Name', 'Admin')->first();
                $data['group_id'] = $group->id;

                $data['name'] = $request->name;
                $data['status'] = 0;

                if($data['password']) {
                    $pasw = $data['password'];
                    $data['password'] = bcrypt($data['password']);
                } else {
                    $pasw = strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9));
                    $data['password'] = bcrypt($pasw);
                }

                $user = \App\User::create($data);

                //связь с телефонами
                if($request->has('phone')) {
                    $number = substr(preg_replace('|[^0-9]+|','',$data['phone']), -10, 10);
                    $user->phones()->create(['phone' => $data['phone'], 'number' => $number]);
                }

                //send email
                if( empty($request->password) ) {
                    $message = "Для заверщения регистрации пройдите по ссылке - " . route('auth.activate.email', ['user_id' => $user->id, 'inn' => $request->inn]) . "\r\nЛогин - ". $request->email . "\r\nПароль - " . $pasw;
                } else {
                    $message = "Для заверщения регистрации пройдите по ссылке - " . route('auth.activate.email', ['user_id' => $user->id, 'inn' => $request->inn]);
                }
                // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
                //$message = wordwrap($message, 70, "\r\n");

                // Отправляем
                if( mail($request->email, 'Регистрация', $message) ) {
                    //return redirect()->back()->withMessage('Вам отправлено письмо.');
                    echo 'success';
                }
            }
        }
    }
}
