<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use DB;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/main';

    protected $username = 'login';

    protected $smsLogin = 'oa@sph-cl.ru';
    protected $smsPsw = 'sp@5700S';
    protected $smsCode = 'sp@5700S';

    //protected $redirectAfterLogout = '/cleaner';
    //protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['only' => ['login']]);
        $this->redirectTo = route('admin.main');
    }

    public function logout()
    {
        if( !in_array(Auth::user()->group->name, ['Admin', 'Moderator', 'SuperAdmin']) ) {
            Auth::guard($this->getGuard())->logout();
            return redirect('/cleaner');
        }
        else {
            Auth::guard($this->getGuard())->logout();
            return redirect('/');
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|min:11',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $groupModerator = \App\Models\UserGroup::where('name', 'Moderator')->first();
        if(!$groupModerator) {
            return false;
        }

        return User::create([
            'group_id' => $groupModerator->id,
            'name' => $data['name'],
            'email' => $data['email'],
            'api_token' => str_random(60),
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        //Права на регистрацию
        $this->authorize('register-user');

        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }

        return view('auth.register');
    }

    //600 /*
    public function inn(Request $request)
    {
        if ($request->isMethod('get')) {
            //return view('auth.inn', ['request' => $request]);
            return view('auth_new.steps', ['request' => $request]);
        }
        else if ($request->isMethod('post')) {
            if(\App\Models\Company::where('inn', '=', $request->inn)->count() > 0) {
                //return redirect()->back()->withErrors(['Компания существует']);
                return \GuzzleHttp\json_encode(['err' => 'Компания существует']);
            }
            else {
                $dataCompany = \App\Models\Company::getCompanyDataByInn($request->inn);
                //dd($dataCompany);
                //return redirect()->action('Auth\AuthController@registrCompany', $dataCompany);
                return \GuzzleHttp\json_encode($dataCompany);
            }
        }        
    }
    
    public function registrCompany(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('auth.register_company', ['request' => $request]);
        }
        else if ($request->isMethod('post'))
        {
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                $this->throwValidationException(
                    $request, $validator
                );
            }

            //sms code
            $number = substr(preg_replace('|[^0-9]+|','',$request->phone), -10, 10);
            $smsCode = strval(rand(1000, 9999));
            $request->session()->flash('sms_code', $smsCode);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://smsc.ru/sys/send.php?login=$this->smsLogin&psw=$this->smsPsw&phones=7$number&mes=CODE:$smsCode&sender=cleancrm.ru",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err || strpos('ERROR', $response) > 0 ) {
                return redirect()->back()->withErrors(['Ошибка проверки номера телефона.']);
            } else {
                return view('auth.sms_code', ['request' => $request]);
            }

        }
        
    }

    public function smsVerify(Request $request){
        if( intval($request->sms_code) == intval($request->session()->get('sms_code')) ) {
            //$request->session()->forget('sms_code');

            //проверить номер уникальность
            $number = substr(preg_replace('|[^0-9]+|','',$request->phone), -10, 10);
            if( \App\Models\UserPhone::where('number', '=', $number)->count() > 0 )
                return redirect()->back()->withErrors(['Номер телефона уже зарегистрирован!']);
            
            //create user
            $data = $request->all();
            
            $group = \App\Models\UserGroup::where('Name', 'Admin')->first();
            $data['group_id'] = $group->id;

            $data['name'] = $request->user_name;
            $data['status'] = 0;
            
            if($data['password']) {
                $data['password'] = bcrypt($data['password']);
            }
            
            $user = \App\User::create($data);

            //связь с телефонами
            if($request->has('phone')) {
                $number = substr(preg_replace('|[^0-9]+|','',$data['phone']), -10, 10);
                $user->phones()->create(['phone' => $data['phone'], 'number' => $number]);
            }

            //send email
            //$message = "Line 1\r\nLine 2\r\nLine 3";
            $message = "Для заверщения регистрации пройдите по ссылке - ".route('auth.activate.email', ['user_id' => $user->id, 'inn' => $request->inn]);

            // На случай если какая-то строка письма длиннее 70 символов мы используем wordwrap()
            //$message = wordwrap($message, 70, "\r\n");

            // Отправляем
            //dd($request->email);
            if( mail($request->email, 'Регистрация', $message) )
                return redirect()->back()->withMessage('Вам отправлено письмо.');
            else
                return redirect()->back()->withErrors('Ошибка при отправлении письма!');
            //return view('auth.register_company', ['request' => $request]);
        } else {
            return redirect()->back()->withErrors(['Неправильный код!']);
        }
    }

    public function activateEmail(Request $request)
    {
        //dd($request->all());
        if( strlen($request->inn) != 10 && strlen($request->inn) != 12 ) {
            return view('auth.inn', ['request' => $request])->withErrors(['Неправильный ИНН']);
        }

        if(\App\Models\Company::where('inn', '=', $request->inn)->count() > 0) {
            return view('auth.inn', ['request' => $request])->withErrors(['Компания уже зарегистрирована.']);
        }
        else
        {
            $user = \App\User::findOrFail($request->user_id);

            $dataCompany = \App\Models\Company::getCompanyDataByInn($request->inn);
            if(is_array($dataCompany)) {
                //create company
                $dataCompany['name'] = $dataCompany['short_company_name'];
                $dataCompany['user_id'] = $user->id;
                //dd($dataCompany);
                $company = \App\Models\Company::create($dataCompany);
                if($company) {
                    $user->company_id = $company->id;
                    $user->status = 1;
                    $user->save();
                    //return redirect('/admin/users/'.$user->id)->withMessge('Компания и Администратор добавлены');
                    return redirect('/login');
                }
                else
                    return view('auth.inn', ['request' => $request])->withErrors(['Ошибка. Компания не зарегистрирована.']);
            }
        }
    }

    //600   */

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //Права на регистрацию
        $this->authorize('register-user');

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Auth::guard($this->getGuard())->login($this->create($request->all()));

        return redirect($this->redirectPath());
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        //ищем пользователя по Мылу или телефону
        $user = \App\User::where(function ($query) use ($request) {
            $query->where('email', $request->input('login'))
                ->orWhereHas('phones', function ($query) use ($request){
                    $phone = substr(preg_replace('|[^0-9]+|','',$request->input('login')), -10, 10);
                    $query->where('number', $phone);
                });
             })->where('status', 1)->first();
        
        if(!empty($user) && $user->password && password_verify($request->input('password'), $user->password)) {
            if (Auth::guard($this->getGuard())->login($user, $request->has('remember'))) {
                return $this->handleUserWasAuthenticated($request, $throttles);
            }
        }


        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

}
