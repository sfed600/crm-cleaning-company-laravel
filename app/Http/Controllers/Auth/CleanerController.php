<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\User;
use DB;
use Auth;
use Hash;

class CleanerController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $smsLogin = 'oa@sph-cl.ru';
    protected $smsPsw = 'sp@5700S';
    protected $smsCode = 'sp@5700S';

    //protected $redirectAfterLogout = '/cleaner';
    //protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['only' => ['login']]);
        $this->redirectTo = route('admin.main');
    }*/

    public function loginForm(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('auth.cleaner_login', ['request' => $request]);
        }
    }

    public function login(Request $request)
    {
        $number = substr(preg_replace('|[^0-9]+|','',$request->login), -10, 10);

        $user = User::whereHas('phones', function ($query) use ($number){
            $query->where('number', $number);
        })->where('status', 1)->first();

        /*dump(Hash::check($request->input('password'), $user->password));
        dd();*/

        if( !empty($user) ) {
            if( Hash::check($request->input('password'), $user->password) ) {
                Auth::login($user, true);

                return redirect()->route('cleaner.tabel');
            }
        }

        //return redirect()->back()->withErrors(['Неправильный номер или пароль']);
        return redirect()->route('cleaner.password.reset')->with([
            'phone' => $request->input('login'),
            'err' => 'Неправильный номер или пароль'
        ] );

        /*if(!empty($user) && $user->password && password_verify($request->input('password'), $user->password)) {
            if (Auth::guard($this->getGuard())->login($user, $request->has('remember'))) {
                return $this->handleUserWasAuthenticated($request, $throttles);
            }
        }*/

    }

    function password_reset(Request $request)
    {
        if($request->method() == 'GET')
        {
            if($request->has('phone'))
            {   
                //sms code
                $number = substr(preg_replace('|[^0-9]+|','',$request->phone), -10, 10);
                //$smsCode = strval(rand(1000, 9999));
                $smsCode = strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9)).strval(rand(0, 9));
                $request->session()->flash('sms_code', $smsCode);

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://smsc.ru/sys/send.php?login=$this->smsLogin&psw=$this->smsPsw&phones=7$number&mes=CODE:$smsCode&sender=cleancrm.ru",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                ));
                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                if ($err || strpos('ERROR', $response) > 0 ) {
                    //return redirect()->back()->withErrors(['Ошибка проверки номера телефона.']);
                    echo 'Ошибка проверки номера телефона.';
                }
                else {
                    $user = User::whereHas('phones', function ($query) use ($number){
                        $query->where('number', $number);
                    })->first();

                    $user->update(['password' => bcrypt($smsCode)]);
                }
            }
            else {
                return view('auth.cleaner_reset_pass');
            }
        }
        else if($request->method() == 'POST') {

        }
    }
    
    //600
    public function api(Request $request)
    {
        return \GuzzleHttp\json_encode([
            0=>["id"=>1,"time"=>1454166946000,"text"=>"rhoncus dui vel sem sed", "image"=>"img1"],
            1=>["id"=>2,"time"=>1454166946002,"text"=>"rhoncus dui vel sem sed2", "image"=>"img2"],
            2=>["id"=>3,"time"=>1454166946003,"text"=>"rhoncus dui vel sem sed3", "image"=>"img3"],
            3=>["id"=>4,"time"=>1454166946004,"text"=>"rhoncus dui vel sem sed4", "image"=>"img4"]  
        ]);
        
    }

}
