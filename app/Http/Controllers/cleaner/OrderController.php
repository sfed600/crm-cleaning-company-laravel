<?php

namespace App\Http\Controllers\cleaner;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        return view('cleaner.orders.index');
    }

    public function view()
    {
        return view('cleaner.orders.view');
    }
}
