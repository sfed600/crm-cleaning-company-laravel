<?php

namespace App\Http\Controllers\cleaner;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Carbon\Carbon;
use App\Models\UserPasport;
use App\Models\UserCard;

class AnketaController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function show($id)
    public function show()
    {
        $user = Auth::user();
        $card_sberbank = $user->cards()->where('bank', '=', 'sberbank')->first();
        if($card_sberbank) {
            $sber_field1 = substr($card_sberbank->number, 0, 4);
            $sber_field2 = substr($card_sberbank->number, 4, 4);
            $sber_field3 = substr($card_sberbank->number, 8, 4);
            $sber_field4 = substr($card_sberbank->number, 12);
        }

        $card_tinkoff = $user->cards()->where('bank', '=', 'tinkoff')->first();
        if($card_tinkoff) {
            $tinkoff_field1 = substr($card_tinkoff->number, 0, 4);
            $tinkoff_field2 = substr($card_tinkoff->number, 4, 4);
            $tinkoff_field3 = substr($card_tinkoff->number, 8, 4);
            $tinkoff_field4 = substr($card_tinkoff->number, 12);
        }
        
        return view('cleaner.anketa.view', [
            'user' => $user,
            'sber_field1' => @$sber_field1,
            'sber_field2' => @$sber_field2,
            'sber_field3' => @$sber_field3,
            'sber_field4' => @$sber_field4,
            'tinkoff_field1' => @$tinkoff_field1,
            'tinkoff_field2' => @$tinkoff_field2,
            'tinkoff_field3' => @$tinkoff_field3,
            'tinkoff_field4' => @$tinkoff_field4,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function edit($id)
    public function edit(Request $request)
    {
        $user = Auth::user();
        $card_sberbank = $user->cards()->where('bank', '=', 'sberbank')->first();
        if($card_sberbank) {
            $sber_field1 = substr($card_sberbank->number, 0, 4);
            $sber_field2 = substr($card_sberbank->number, 4, 4);
            $sber_field3 = substr($card_sberbank->number, 8, 4);
            $sber_field4 = substr($card_sberbank->number, 12);
        }
        
        $card_tinkoff = $user->cards()->where('bank', '=', 'tinkoff')->first();
        if($card_tinkoff) {
            $tinkoff_field1 = substr($card_tinkoff->number, 0, 4);
            $tinkoff_field2 = substr($card_tinkoff->number, 4, 4);
            $tinkoff_field3 = substr($card_tinkoff->number, 8, 4);
            $tinkoff_field4 = substr($card_tinkoff->number, 12);
        }

        if ($request->isMethod('get')) {
            return view('cleaner.anketa.form', [
                'user' => $user,
                'sber_field1' => @$sber_field1,
                'sber_field2' => @$sber_field2,
                'sber_field3' => @$sber_field3,
                'sber_field4' => @$sber_field4,
                'tinkoff_field1' => @$tinkoff_field1,
                'tinkoff_field2' => @$tinkoff_field2,
                'tinkoff_field3' => @$tinkoff_field3,
                'tinkoff_field4' => @$tinkoff_field4,
                
            ]);
        }
        else if ($request->isMethod('post')) 
        {
            $except = [
                'pasport-series',
                'pasport_number',
                'pasport_date',
                'pasport_issued',
                'sber-field-1',
                'sber-field-2',
                'sber-field-3',
                'sber-field-4',
                'tinkoff-field-1',
                'tinkoff-field-2',
                'tinkoff-field-3',
                'tinkoff-field-4'
            ];
            $data = $request->all();
            
            if($img = User::saveImg($request)) {
                $data['img'] = $img;
                $user->deleteImg();
            }
            
            if($request->has('bithdate')) {
                $data['bithdate'] = (new Carbon($data['bithdate']))->format('Y.m.d');
            }

            $user->update(array_except($data, $except));

            //passport
            $pasport = $user->pasport;

            if($pasport == null) {
                $user->pasport()->create([
                    'number' => $request->input('pasport_number'),
                    'series' => $request->input('pasport-series'),
                    //'date_issue' => $request->input('pasport_date'),
                    'date_issue' => (new Carbon($request->input('pasport_date')))->format('Y.m.d'),
                    'issued_by' => $request->input('pasport_issued')
                ]);
            }
            //else if( $pasport->number != $request->input('pasport_number') && $pasport->series != $request->input('pasport-series') ) {
            else {
                $user->pasport()->update([
                    'number' => $request->input('pasport_number'),
                    'series' => $request->input('pasport-series'),
                    //'date_issue' => $request->input('pasport_date'),
                    'date_issue' => (new Carbon($request->input('pasport_date')))->format('Y.m.d'),
                    'issued_by' => $request->input('pasport_issued')
                ]);
            }


            //cards
            //sber
            if( $request->has('sber-field-1') && $request->has('sber-field-2') && $request->has('sber-field-3') && $request->has('sber-field-4') ) {
                $sberNumber = $request->input('sber-field-1').$request->input('sber-field-2').$request->input('sber-field-3').$request->input('sber-field-4');
                $card = $user->cards()->where('bank', '=', 'sberbank')->first();
                if($card == null) {
                    $user->cards()->create(['bank' => 'sberbank', 'number' => $sberNumber]);
                } else if($card->number != $sberNumber) {
                    $card->update(['bank' => 'sberbank', 'number' => $sberNumber]);
                }
            }

            //tinkoff
            if( $request->has('tinkoff-field-1') && $request->has('tinkoff-field-2') && $request->has('tinkoff-field-3') && $request->has('tinkoff-field-4') ) {
                $tinkoffNumber = $request->input('tinkoff-field-1').$request->input('tinkoff-field-2').$request->input('tinkoff-field-3').$request->input('tinkoff-field-4');
                $card = $user->cards()->where('bank', '=', 'tinkoff')->first();
                if($card == null) {
                    $user->cards()->create(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);
                } else if($card->number != $tinkoffNumber) {
                    $card->update(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);
                }
            }

            return redirect()->route('cleaner.anketa.view');
        }
    }

    
}
