<?php

namespace App\Http\Controllers\cleaner;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommonController extends Controller
{
    function mutual_settlements() {
        return view('cleaner.common.mutual_settlements');
    }

    function calendar() {
        return view('cleaner.common.calendar');
    }
}
