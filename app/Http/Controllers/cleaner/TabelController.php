<?php

namespace App\Http\Controllers\cleaner;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;
use App\User;
use App\Models\Company;
use Auth;
use Gate;

class TabelController extends Controller
{
    function index(Request $request, $mobile = null) {
        $user = User::findOrFail(Auth::user()->id);

        if (Gate::denies('show', $user)) {
            abort(403);
        }

        $carbon = new Carbon();

        //компании
        $companies = null;
        //компании где работал данный сотрудник
        if (Auth::user()->can('mycompany', new User())) {
            $companies = Company::whereHas('orders', function($query) use ($user){
                $query->whereHas('users', function($query1) use ($user){
                    $query1->where('user_id', $user->id);
                });
            })->orderBy('name')->get();
        }
        $filters = $this->getFormFilter($request->input());
        if(empty($filters['date-range'])) {
            $filters['date-range'] = (new Carbon('first day of this month'))->format('d.m.Y').' - '.(new Carbon('last day of this month'))->format('d.m.Y');
        }
        //доступ к компаниям
        /*if (Auth::user()->can('mycompany', new User())) {
            if ((empty($filters['company']) || !$filters['company']) && !empty($companies) && $companies->count()){
                $filters['company'] = $companies->first()->id;
            }
        } else {
            $filters['company'] = Auth::user()->company_id;
        }*/

        //Выбираем все рабочие дни за выбранный перриод работника в выбранной компании
        $dates = \App\Models\OrderUser::where('user_id', Auth::user()->id);

        //фильтр по дате
        if(!empty($filters) && !empty($filters['date-range'])) {
            $f = explode('-', $filters['date-range']);
            $date_from = !empty($f[0]) ? $f[0] : '';
            $date_to = !empty($f[1]) ? $f[1] : '';
            $dates->whereHas('day', function ($query) use ($date_from, $date_to){
                $query->where('date', '>=', (new Carbon($date_from))->format('Y-m-d'))
                    ->where('date', '<=', (new Carbon($date_to))->format('Y-m-d'));
            });
        }
        //фильтр компании
        if (!empty($filters) && !empty($filters['company']) && $filters['company'] > 0) {
            $dates->whereHas('day.order', function ($query) use ($filters){
                $query->where('company_id', $filters['company']);
            });
        }
        $dates = $dates->with(['day' => function($query) {
            $query->orderBy('date');
        }])->get();

        if(!empty($dates) && $dates->count()) {
            $outcomes = \App\Models\UserIncome::where('user_id', Auth::user()->id)->where('action', 'outcome');
            if (!empty($filters['company']) && $filters['company']) {
                $outcomes->where('company_id', $filters['company']);
            }
            //фильтр по дате
            if(!empty($filters) && !empty($filters['date-range'])) {
                $f = explode('-', $filters['date-range']);
                $date_from = !empty($f[0]) ? $f[0] : '';
                $date_to = !empty($f[1]) ? $f[1] : '';
                $outcomes->where('date', '>=', (new Carbon($date_from))->format('Y-m-d'))->where('date', '<=', (new Carbon($date_to))->format('Y-m-d'));
            }
            $outcomes = $outcomes->orderBy('date', 'desc')->select('date', 'outcome', 'type', 'comment', 'created_at')->get();

            foreach($dates as $key => $date) {
                $date->date = $date->day->date;
                foreach($outcomes as $key2 => $outcome) {
                    if($date->date == $outcome->date && (empty($date->outcome))) {
                        $date->outcome = $outcome->outcome;
                        $date->type = $outcome->typeName();
                        $date->comment =  ($date->comment ?: $outcome->comment);
                        unset($outcomes[$key2]);
                    }
                }
            }
            if(!empty($outcomes)) {
                $outcomes = $outcomes->map(function ($item, $key) {
                    $item->type = $item->typeName();
                    return $item;
                });
                $dates = $dates->merge($outcomes);
            }
        }
        //вычисляем баланс
        $balance = 0;
        if(!empty($filters['company'])) {
            $balance = \App\Models\OrderUser::where('user_id', Auth::user()->id)->whereHas('day.order', function ($query) use ($filters) {
                $query->where('company_id', $filters['company']);
            })->sum('amount');
            $balance -= \App\Models\UserIncome::where('user_id', Auth::user()->id)->where('company_id', $filters['company'])->sum('outcome');
        }

        //dd($filters);
        //год/месяц фильтра
        $f = explode('.', $date_from);
        if( intval($f[1]) == 1 ) $month = 'Январь'; else if( intval($f[1]) == 2 ) $month = 'Февраль'; else if( intval($f[1]) == 3 ) $month = 'Март'; else if( intval($f[1]) == 4 ) $month = 'Апрель'; else if( intval($f[1]) == 5 ) $month = 'Май'; else if( intval($f[1]) == 6 ) $month = 'Июнь'; else if( intval($f[1]) == 7 ) $month = 'Июль'; else if( intval($f[1]) == 8 ) $month = 'Август'; else if( intval($f[1]) == 9 ) $month = 'Сентябрь'; else if( intval($f[1]) == 10 ) $month = 'Октябрь'; else if( intval($f[1]) == 11 ) $month = 'Ноябрь'; else if( intval($f[1]) == 12 ) $month = 'Декабрь';
        //dd($user, $user->toJson(), $dates, $dates->toJson(), $balance, $companies, $f[2], $month);
        if( !($mobile == 'mobile')) {
            return view('cleaner.tabel.index', [
                'user' => $user,
                'filters' => $filters,
                'dates' => $dates,
                'balance' => $balance,
                'companies' => $companies,
                'year' => $f[2],
                'month' => $month,
            ]);
        }
        else {
            return \GuzzleHttp\json_encode([
                0 => [
                        'id' => 1,
                        'time' => 1234567890,
                        'text' => 'zzz1',
                        'image' => ""
                    ],
                1 => [
                    'id' => 2,
                    'time' => 1234567890,
                    'text' => 'zzz2',
                    'image' => ""
                ],
                2 => [
                    'id' => 3,
                    'time' => 1234567890,
                    'text' => 'zzz3',
                    'image' => ""
                ]
            ]);
        }



    }
}
