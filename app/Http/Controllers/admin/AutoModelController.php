<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AutoModel;
use Auth;

class AutoModelController extends Controller
{
    public function __construct() {
        $this->authorize('index', new \App\Models\Auto());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFormFilter($request->input());

        $models = AutoModel::orderBy('id', 'desc');

        //доступ к компаниям
        $models->where('company_id', '=', Auth::user()->company_id)
            ->orWhere('company_id', null);

        $models = $models->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        return view('admin.auto.models.index', ['models' => $models, 'filters' => $filters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.auto.models.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\AutoModelRequest $request)
    {
        AutoModel::create($request->all());

        return redirect()->route('admin.auto_models.index')->withMessge('Модель автомобиля добавлена');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = AutoModel::find($id);
        return view('admin.auto.models.edit', ['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\AutoModelRequest $request, $id)
    {
        AutoModel::findOrFail($id)->update($request->all());

        return redirect()->route('admin.auto_models.index')->withMessge('Модель автомобиля изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AutoModel::destroy($id);
        return redirect()->route('admin.auto_models.index')->withMessage('Модель автомобиля удалена');
    }
}
