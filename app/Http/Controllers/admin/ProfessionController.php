<?php

namespace App\Http\Controllers\admin;

use App\Models\Service;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use \App\Models\UserProfession;
use Auth;

class ProfessionController extends Controller
{
    public function __construct() {
        $this->authorize('index', new \App\Models\UserProfession());
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professions = UserProfession::where('company_id', '=', Auth::user()->company_id)
            ->orWhere('company_id', null)
            ->orderBy('name')->get();

        return view('admin.professions.index', ['professions' => $professions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*if( !$this->authorize('index', new Service()) )
            return redirect()->back(403);*/

        return view('admin.professions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $profession = UserProfession::create([
            'name' => $data['name'],
            'company_id' => Auth::user()->company_id
        ]);

        //return redirect()->route('admin.profession.index')->withMessge('Добавлено');
        return redirect()->route('admin.settings.index', ['tab' => 'professions'])->withMessge('Добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profession = UserProfession::find($id);
        return view('admin.professions.edit', ['profession' => $profession]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profession = UserProfession::findOrFail($id)->update($request->all());
        //return redirect()->route('admin.profession.index')->withMessge('Професия изменена');
        return redirect()->route('admin.settings.index', ['tab' => 'professions'])->withMessge('Професия изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserProfession::destroy($id);
        //return redirect()->route('admin.profession.index')->withMessage('Професия удалена');
        return redirect()->route('admin.settings.index', ['tab' => 'professions'])->withMessge('Професия удалена');
    }
}
