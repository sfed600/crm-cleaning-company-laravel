<?php

namespace App\Http\Controllers\admin;

use App\Models\Invoice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Carbon\Carbon;
use PDF;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFormFilter($request->input());

        $invoices = Invoice::join('orders', 'order_id', '=', 'orders.id')
            ->select(DB::raw('invoices.*'))
            ->where('company_id', Auth::user()->company_id);

        $invoices = $invoices->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        $count_invoices = $invoices->count();
        //$sum_orders = $invoices->orders()->sum('amount');
        $sum_orders = 0;

        $sum_paginate = 0;
        foreach ($invoices as $invoice) {
            $sum_paginate = $sum_paginate + $invoice->order->amount;
        }

        return view('admin.invoice.index', [
            'invoices' => $invoices,
            'filters' => $filters,
            'count_invoices' => $count_invoices,
            'sum_orders' => $sum_orders,
            'sum_paginate' => $sum_paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($order_id)
    {   
        $order = \App\Models\Order::findOrFail($order_id);

        $invoice_number =  Invoice::join('orders', 'order_id', '=', 'orders.id')
            ->where('company_id', Auth::user()->company_id)->max('invoices.number');
        if($invoice_number > 0 )
            $invoice_number = $invoice_number + 1;
        else
            $invoice_number =  1;

        $products = \App\Models\Product::where('company_id', '=', Auth::user()->company_id)->get();
        
        return view('admin.invoice.create', [
            'order' => $order,
            'invoice_number' => $invoice_number,
            'products' => $products
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = \App\Models\Order::findOrFail($request->input('order_id'));

        $data = $request->all();
        $data['date'] = (new Carbon($data['date']))->format('Y.m.d');
        if($data['due_date'] != '')
            $data['due_date'] = (new Carbon($data['due_date']))->format('Y.m.d');

        if( $order->status != $data['status']) {
            $order->status = $data['status'];
            $order->save();
        }

        $invoice = Invoice::create($data);

        //связь products
        if($invoice) {
            if( $request->has('product_id') && intval($request->input('product_id')[0]) > 0 ) {
                $invoice->products()->sync($request->input('product_id'));
            }
        }

        return redirect()->route('admin.invoices.edit', [
            'id' => $invoice->id,
            'tab' => 'invoice',
        ])->withMessage('Счет добавлен  ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);
        $products = \App\Models\Product::where('company_id', '=', Auth::user()->company_id)->get();

        return view('admin.invoice.edit', [
            'invoice' => $invoice,
            'products' => $products
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::findOrFail($id);

        /*if (Gate::denies('edit', $order)) {
            abort(403);
        }*/

        $data = $request->input();

        $data['date'] = (new Carbon($data['date']))->format('Y.m.d');
        if($data['due_date'] != '')
            $data['due_date'] = (new Carbon($data['due_date']))->format('Y.m.d');

        if( $invoice->order->status != $data['status']) {
            $invoice->order->status = $data['status'];
            $invoice->order->save();
        }
        
        $invoice->update($data);

        //связь with products
        if( $request->has('product_id') && intval($request->input('product_id')[0]) > 0 ) {
            $invoice->products()->sync($request->input('product_id'));
        }

        return redirect()->route('admin.invoices.index')->withMessage('Счет изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Invoice::destroy($id);

        return redirect()->route('admin.invoices.index')->withMessage('Удалено');
    }

    public function downloadPDF($id){
        $invoice = Invoice::findOrFail($id);
        $company = \App\Models\Company::findOrFail(Auth::user()->company_id);

        $order_company = \App\Models\OrderCompany::find($invoice->order->company_id);

        //$pdf = PDF::loadView('admin.invoice.pdf', compact('invoice', 'company', 'order_company'));
        $pdf = PDF::loadView('admin.invoice.pdf', [
            'invoice' => $invoice, 
            'company' => $company, 
            'order_company' => $order_company,
            'sumNDS' => $invoice->sumNDS($invoice->id),
            'sumTotal' => $invoice->sumTotal($invoice->id)
        ]);
        
        //return $pdf->download('offer.pdf');
        return $pdf->stream();
    }
}
