<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Auto;
use App\Models\AutoModel;
use Carbon\Carbon;
use Auth;

class AutoController extends Controller
{
    public function __construct() {
        $this->authorize('index', new Auto());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $filters = $this->getFormFilter($request->input());
        //$autos = Auto::orderBy('id', 'desc')->with('model');

        //sort /*
        //dump($filters); dd();
        if ( isset($filters['order']) && $filters['order']!='')
        {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();

            switch ($arSort[0]) {
                case 'model':
                    $autos = Auto::join('auto_models', 'model_id', '=', 'auto_models.id')
                        ->orderBy('auto_models.name', $arSort[1]);
                    break;
                case 'number':
                    $autos = Auto::orderBy('number', $arSort[1])->with('model');
                    break;
                case 'sts':
                    $autos = Auto::orderBy('sts', $arSort[1])->with('model');
                    break;
                case 'date_registration':
                    $autos = Auto::orderBy('date_registration', $arSort[1])->with('model');
                    break;
                case 'year':
                    $autos = Auto::orderBy('year', $arSort[1])->with('model');
                    break;
                case 'vin':
                    $autos = Auto::orderBy('vin', $arSort[1])->with('model');
                    break;
                case 'date_insurance':
                    $autos = Auto::orderBy('date_insurance', $arSort[1])->with('model');
                    break;
            }
        }
        else
            $autos = Auto::orderBy('id', 'desc')->with('model');
        //sort */
        
        //автомашины только компании пользователя
        $autos = $autos->where('company_id', Auth::user()->company_id);
        $autos = $autos->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        return view('admin.auto.index', ['autos' => $autos, 'filters' => $filters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $models = AutoModel::orderBy('name')->get();
        return view('admin.auto.create', ['models' => $models]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\AutoRequest $request)
    {
        $data = $request->all();
        //компания
        $data['company_id'] = Auth::user()->company_id;

        $data['certificate'] = Auto::saveFile($request, 'certificate');
        $data['pts'] = Auto::saveFile($request, 'pts');
        $data['contract'] = Auto::saveFile($request, 'contract');
        if($request->has('date_registration')) {
            $data['date_registration'] = (new Carbon($data['date_registration']))->format('Y.m.d');
        }
        if($request->has('date_insurance')) {
            $data['date_insurance'] = (new Carbon($data['date_insurance']))->format('Y.m.d');
        }

        Auto::create($data);

        return redirect()->route('admin.auto.index')->withMessge('Автомобиль добавлен');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $auto = Auto::where('company_id', Auth::user()->company_id)->findOrFail($id);

        $models = AutoModel::orderBy('name')->get();
        return view('admin.auto.edit', ['auto' => $auto, 'models' => $models]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\AutoRequest $request, $id)
    {
        $data = $request->all();
        $auto = Auto::where('company_id', Auth::user()->company_id)->findOrFail($id);
        if($certificate = Auto::saveFile($request, 'certificate')) {
            $data['certificate'] = $certificate;
            $auto->deleteFile('certificate');
        }
        if($pts = Auto::saveFile($request, 'pts')) {
            $data['pts'] = $pts;
            $auto->deleteFile('pts');
        }
        if($contract = Auto::saveFile($request, 'contract')) {
            $data['contract'] = $contract;
            $auto->deleteFile('contract');
        }
        if($request->has('date_registration')) {
            $data['date_registration'] = (new Carbon($data['date_registration']))->format('Y.m.d');
        }
        if($request->has('date_insurance')) {
            $data['date_insurance'] = (new Carbon($data['date_insurance']))->format('Y.m.d');
        }
        $auto->update($data);

        return redirect()->route('admin.auto.index')->withMessge('Автомобиль изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auto::destroy($id);
        return redirect()->route('admin.auto.index')->withMessage('Автомобиль удален');
    }
}
