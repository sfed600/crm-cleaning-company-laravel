<?php

namespace App\Http\Controllers\admin;

use App\Models\Calendar;
use App\Models\Order;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;
use DB;

class CalendarController extends Controller
{
    public function __construct() {
        $this->authorize('index', new \App\Models\Calendar());
    }

    public function exclude_from_filter($filter) {
        session()->forget("admin.calendar.filters.$filter");
        return redirect()->route('admin.calendar');
    }

    public function header_info(Request $request) {
        //dd($request->all());
        $carbon = new Carbon();

        $search = $request->input('search');
        if( !empty($search) ) {    //period
            //$filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
            $arPeriod = explode('=', $search);
            $period_start = $carbon::createFromFormat('m.Y', trim($arPeriod[1]))->format('Y-m-d');
            $period_end = $carbon::createFromFormat('m.Y', trim($arPeriod[1]))->format('Y-m-d');

            //даты периода Ru format
            setlocale(LC_ALL, "ru_RU.UTF-8");
            $filters['period_start'] = strftime("%A %e %B %Y", strtotime(trim($arPeriod[0])));
            $filters['period_end'] = strftime("%A %e %B %Y", strtotime(trim($arPeriod[1])));
            //dd($filters['daterange']);
        }
    }

    public function index(Request $request)
    {
        $carbon = new Carbon();
        $filters = $this->getFormFilter($request->input());
        //dd($filters);

        //какая вкладка нажата
        if( isset($filters['tab']) && $filters['tab'] == 'kp-tabbox-calendar_fact')
            $filters['tab'] = $tab = 'fact';
        else
            $filters['tab'] = $tab = 'plan';

        /*if( empty($filters['date']) ) {
            $filters['date'] = $carbon->format('m.Y');
        }*/

        //dd($filters['daterange']);
        /*if(empty($filters['date'])) {
            $filters['date'] = $carbon->format('m.Y');
        }
        if(empty($filters['daterange'])) {
            $filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
        }*/

        if( !empty($filters['daterange']) ) {    //period
            //$filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
            $arPeriod = explode('-', $filters['daterange']);
            $period_start = $carbon::createFromFormat('d.m.Y', trim($arPeriod[0]))->format('Y-m-d');
            $period_end = $carbon::createFromFormat('d.m.Y', trim($arPeriod[1]))->format('Y-m-d');

            //даты периода Ru format
            setlocale(LC_ALL, "ru_RU.UTF-8");
            $filters['period_start'] = strftime("%A %e %B %Y", strtotime(trim($arPeriod[0])));
            $filters['period_end'] = strftime("%A %e %B %Y", strtotime(trim($arPeriod[1])));
            //dd($filters['daterange']);
        }
        else {
            if(empty($filters['date']) )
                $filters['date'] = $carbon->format('m.Y');

            $today = new Carbon('01.'.$filters['date']);
        }


        $where = '';
        if(isset($today)) {
            $where = $where. " WHERE (order_days.date >= '".$today->format('Y-m-d')."' AND order_days.date <= '".(new Carbon('last day of '.$today->format('F Y')))->format('Y-m-d')."') ";
        } else {
            $where = $where. " WHERE (order_days.date >= '".$period_start."' AND order_days.date <= '".$period_end."') ";
        }

        //filters
        //  $INNER_JOIN = '';
        if (!empty($filters) && (!empty($filters['user_id']) || @$filters['status'] == 'my' ) ) {
            //$INNER_JOIN .= ' INNER JOIN `order_user` ON orders.id = order_user.order_id ';
            if( @$filters['status'] == 'my' )
                $where = $where. " AND orders.id IN(SELECT DISTINCT `order_id` FROM `order_user` WHERE `user_id` =".Auth::user()->id.")";
            else
                $where = $where. " AND orders.id IN(SELECT DISTINCT `order_id` FROM `order_user` WHERE `user_id` = ".$filters['user_id'].")";

            $user_find = \App\User::find($filters['user_id']);
            $filters['fio'] = $user_find ? $user_find->fio() : '';
        }

        if (!empty($filters) && !empty($filters['status'])) {
            if( $filters['status'] == 'new' )
                $where = $where. " AND orders.date = '".$carbon->now()->format('Y-m-d')."' ";
            else if($filters['status'] != 'my')
                $where = $where. " AND orders.status = '".$filters['status']."' ";
        }

        //if (!empty($filters) && !empty($filters['typ'])) {
        if (!empty($filters) && intval(@$filters['typ']) > 0 ) {
            $where = $where. " AND json_contains(`calculator`, '{\"typ\" : \"".$filters['typ']."\"}') ";
            //select * from applications where json_contains(`typ`, '82')
        }

        if (!empty($filters) && !empty($filters['payment'])) {
            //$where = $where. " AND json_contains(`calculator`, '{\"payment\" : \"".$filters['payment']."\"}') ";
            if( $filters['payment'] == 600 )
                $where = $where. " AND json_contains(`calculator`, '{\"payment\" : \"0\"}') ";
            if( $filters['payment'] == 10 )
                $where = $where. " AND json_contains(`calculator`, '{\"payment\" : \"10\"}') ";
        }

        //sum filter
        if ( !empty($filters) && !empty($filters['sum_compare']) && ( !empty($filters['sum']) || (!empty($filters['sum_min']) && !empty($filters['sum_max'])) ) ) {
            switch ($filters['sum_compare']) {
                case 'equal':
                    //$turns = $turns->where('amount', '=', intval($filters['sum']));
                    $where = $where. " AND orders.amount = ".$filters['sum'];
                    break;
                case 'interval':
                    //$turns = $turns->where('amount', '>', intval($filters['sum_min']))->where('amount', '<', intval($filters['sum_max']));
                    $where = $where. " AND (orders.amount >= ".intval($filters['sum_min'])." AND orders.amount <= ".intval($filters['sum_max']).")";
                    break;
                case 'more':
                    //$turns = $turns->where('amount', '>', intval($filters['sum']));
                    $where = $where. " AND orders.amount > ".$filters['sum'];
                    break;
                case 'less':
                    //$turns = $turns->where('amount', '<', intval($filters['sum']));
                    $where = $where. " AND orders.amount < ".$filters['sum'];
                    break;
            }

        }

        //смены по датам
        $sql = "
            SELECT orders.number, order_days.date, order_days.time, `nigth`, orders.status, orders.address, orders.calculator, orders.id as order_id, cnt, order_days.id, metro.name as metro, otv.surname, otv.name, sas.cnt_mans 
            FROM `order_days` 
            
            INNER JOIN `orders`
                ON orders.id = order_days.order_id
                
            LEFT JOIN 
                ( SELECT `day_id`, order_users.user_id, `surname`, `name`, `main` 
                  FROM `order_users`
                  LEFT JOIN `users`
                      ON order_users.user_id = users.id
                  WHERE order_users.main = 1    
                ) AS otv ON order_days.id = otv.day_id AND otv.main = 1     
                
            LEFT JOIN `metro`
                ON order_days.metro_id = metro.id    
            
            LEFT JOIN 
                ( SELECT orders.id, COUNT('order_days.id') as cnt
                    FROM `orders` 
                    LEFT JOIN `order_days`
                    	ON orders.id = order_days.order_id
                    GROUP BY orders.id
                ) AS s ON s.id = order_days.order_id
                
            LEFT JOIN 
                ( SELECT day_id, COUNT('*') as cnt_mans
                    FROM `order_users` 
                    GROUP BY day_id
                ) AS sas ON sas.day_id = order_days.id    
            ".$where."
            ORDER BY order_days.date, order_days.created_at";
        //dd($sql);
        $query = \DB::select($sql);
        $turns = new \Illuminate\Support\Collection($query);
        //dd($turns);

        //кол-во дней в месяце
        if( empty($filters['daterange']) )
            $cnt_days = $carbon::createFromFormat('d.m.Y','01.'.$filters['date'])->daysInMonth;
        else {
            //$cnt_days = $carbon::createFromFormat('d.m.Y','01.'.$filters['date'])->daysInMonth;
            $cnt_days = $carbon::createFromFormat('Y-m-d', $period_end)->diffInDays($carbon::createFromFormat('Y-m-d', $period_start)) +1;
            //dump($period_start, $period_end);
        }

        $weeks = [];
        $week = 1;
        $details = [];  //детализация

        //заказы в этом месяце
        /*if(isset($today)) {
            $orders = \App\Models\Order::where('date', '>=', $today->format('Y-m-d'))->where('date', '<=', (new Carbon('last day of '.$today->format('F Y')))->format('Y-m-d'))
                ->whereIn('status', ['agree', 'agreed', 'confirmed', 'work', 'performed', 'final_positive'])
                ->whereNotIn('status', ['first'])
                ->where('company_id', Auth::user()->company_id);
        } else {    //за период
            $orders = \App\Models\Order::where('date', '>=', $period_start)->where('date', '<=', $period_end)
                ->whereIn('status', ['agree', 'agreed', 'confirmed', 'work', 'performed', 'final_positive'])
                ->whereNotIn('status', ['first'])
                ->where('company_id', Auth::user()->company_id);
        }*/
        if(isset($today)) {
            $orders = \App\Models\Order::where('date', '>=', $today->format('Y-m-d'))->where('date', '<=', (new Carbon('last day of '.$today->format('F Y')))->format('Y-m-d'))
                ->where('company_id', Auth::user()->company_id);
        } else {    //за период
            $orders = \App\Models\Order::where('date', '>=', $period_start)->where('date', '<=', $period_end)
                ->where('company_id', Auth::user()->company_id);
        }

        if($tab == 'fact') {
            $orders = $orders->whereIn('status', ['agree', 'agreed', 'confirmed', 'work', 'performed', 'final_positive'])
                ->whereNotIn('status', ['first'])
                ->has('days');
        }

        $orders = $orders->get();
        /*foreach ($orders as $order)
            dump($order->number);*/

        $periods = [];
        $cntDays = 0;   //кол-во уборок
        foreach ($orders as $order) {
            if($order->periods) {
                $arPeriods = \GuzzleHttp\json_decode($order->periods);
                foreach ($arPeriods as $period) {
                    //$periods[$period->date_start.'-'.$period->date_finish] = $order->id;
                    $periods[] = [
                        'period' => $period->date_start.'-'.$period->date_finish,
                        'order_id' => $order->id
                    ];
                    
                    //Order::getPeriodsDaysCount($order->periods);
                    $start = (new Carbon($period->date_start));
                    $finish = (new Carbon($period->date_finish));
                    $length = $finish->diffInDays($start) + 1;
                    $cntDays = $cntDays + $length;
                }
            } else if(intval($order->date) > 0 && intval($order->date_finish) > 0 ) {
                //$periods[date('d.m.Y', strtotime($order->date)).'-'.date('d.m.Y', strtotime($order->date_finish))] = $order->id;
                $periods[] = [
                    'period' => date('d.m.Y', strtotime($order->date)).'-'.date('d.m.Y', strtotime($order->date_finish)),
                    'order_id' => $order->id
                ];
            }
        }

        //dd($periods);

        for($i = 1; $i<= $cnt_days; $i++) {
            if( empty($filters['daterange']) )
                $date = $carbon::createFromFormat('d.m.Y', $i.'.'.$filters['date'], 'Europe/Moscow');
            else
                $date = $carbon::createFromFormat('Y-m-d', $period_start, 'Europe/Moscow')->addDays($i-1);
            $weeks[$week][$date->format('D')] = $date;
            
            if($tab == 'fact')
                $details[$week][$date->format('D')] = Calendar::detail_day_fact($date, $turns);
            else
                $details[$week][$date->format('D')] = Calendar::detail_day_plan($date, $orders);

            if($date->format('l') == 'Sunday') {
                $week++;
            }
        }
        //dd($details);

            return view('admin.calendar.index', [
            'weeks' => $weeks,
            'today' => @$today,
            'orders' => $orders,
            'turns' => $turns,
            'details' => $details,
            'filters' => $filters,
            //'count_orders' => sizeof(array_unique($orders_ids)),
            //'ordersPlanCnt' => $ordersPlanCnt,
            'cntDays' => $cntDays, 
            //'ordersPlan' => $ordersPlan,
            'periods' => $periods
        ]);
    }
}
