<?php

namespace App\Http\Controllers\admin;

use App\Models\Offer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use PDF;

class OfferController extends Controller
{
    public function __construct() {
        $this->authorize('index', new \App\Models\Offer());
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFormFilter($request->input());
        
        $offers = Offer::orderBy('id', 'desc');

        //sort
        /*if ( isset($filters['order']) && $filters['order']!='')
        {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();

            switch ($arSort[0]) {
                case 'number':
                    $orders = Order::orderBy('number', $arSort[1]);
                    break;
                case 'name':
                    $orders = Order::orderBy('name', $arSort[1]);
                    break;
                case 'status':
                    $orders = Order::orderBy('status', $arSort[1]);
                    break;
                case 'date':
                    $orders = Order::orderBy('date', $arSort[1]);
                    break;
                case 'days_count':
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `order_id`, COUNT(*) as cnt_days
                                FROM `order_days`
                                GROUP BY `order_id`
                                ORDER BY cnt_days DESC) `hz`"), 'hz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("hz.cnt_days $arSort[1]");
                    break;
                case 'users_count':
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT order_id, SUM(sz.u_cnt) user_cnt
                                FROM (
                                    SELECT order_days.id, order_id, u_cnt
                                    FROM `order_days`
                                    LEFT JOIN (
                                        SELECT day_id, COUNT(`user_id`) as u_cnt
                                        FROM `order_users`
                                        GROUP BY order_users.day_id
                                    ) hz
                                    ON hz.day_id = order_days.id ) as sz
                                GROUP BY order_id) `zz`"), 'zz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("zz.user_cnt $arSort[1]");
                    break;
                case 'sum':
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT order_id, SUM(sz.s_amount) sum_amount
                                FROM (
                                    SELECT order_days.id, order_id, s_amount
                                    FROM `order_days`
                                    LEFT JOIN (
                                        SELECT day_id, SUM(`amount`) as s_amount
                                        FROM `order_users`
                                        GROUP BY order_users.day_id
                                    ) hz
                                    ON hz.day_id = order_days.id ) as sz
                                GROUP BY order_id) `zz`"), 'zz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("zz.sum_amount $arSort[1]");
                    break;
            }
        }
        else
            $orders = Order::orderBy('id', 'desc');*/
        //sort

        //доступ к компаниям
        if (Auth::user()->can('index', new \App\Models\Offer())) {
            if (!empty($filters) && !empty($filters['company'])) {
                $offers->where('company_id', $filters['company']);
            }
            //только заказы твоей компании
        } else {
            $offers->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id)
                    ->orWhere('company_id', null);
            });
        }


        /*if (!empty($filters) && !empty($filters['number'])) {
            $orders->where('number', 'LIKE', '%'.$filters['number'].'%');
        }
        if (!empty($filters) && !empty($filters['date_from'])) {
            $orders->where('date', '>=', (new Carbon($filters['date_from']))->format('Y.m.d'));
        }
        if (!empty($filters) && !empty($filters['date_to'])) {
            $orders->where('date', '<=', (new Carbon($filters['date_to']))->format('Y.m.d'));
        }
        if (!empty($filters) && !empty($filters['status'])) {
            $orders->where('status', $filters['status']);
        }
        if (!empty($filters) && isset($filters['deleted']) && $filters['deleted']) {
            $orders->withTrashed();
        }
        $orders = $orders->with('days', 'users')->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);*/

        $companies = \App\Models\Company::orderBy('name')->get();
        
        //return view('admin.offers.index', ['offers' => $offers, 'filters' => $filters, 'companies' => $companies]);
        $offers = $offers->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        $count_offers = $offers->count();
        $sum_offers = $offers->sum('amount');

        $sum_paginate = 0;
        foreach ($offers as $offer) {
            $sum_paginate = $sum_paginate + $offer->amount;
        }

        return view('admin.offers.index', [
            'offers' => $offers,
            'filters' => $filters,
            'count_offers' => $count_offers,
            'sum_orders' => $sum_offers,
            'sum_paginate' => $sum_paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();

        $order_number =  Offer::where('company_id', Auth::user()->company_id)->max('number');
        //$companies = \App\Models\OrderCompany::orderBy('name')->get();
        $companies = \App\Models\OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();

        //return view('admin.orders.create', ['autos' => $autos, 'order_number' => ++$order_number, 'companies' => $companies, 'request' => $request]);

        $order_company_id = null; $contact_id = null;
        if($request->has('order_company_id') ) {
            $order_company_id = intval($request->input('order_company_id'));
        }
        else if($request->has('contact_id') ) {
            $contact_id = intval($request->input('contact_id'));
        }

        if($request->has('order_name'))
            $order_name = $request->input('order_name');

        $prices = [];
        $price = \App\Models\Price::where('company_id', '=', Auth::user()->company_id)->first();
        if($price) {
            $arPrice = \GuzzleHttp\json_decode($price->price);
            //var_dump(get_object_vars($arPrice)); dd();
            $prices = get_object_vars($arPrice);
        }

        $professions = \App\Models\UserProfession::where('company_id', '=', Auth::user()->company_id)
            ->orWhere('company_id', null)
            ->orderBy('name')->get();

        $products = \App\Models\Product::where('company_id', '=', Auth::user()->company_id)->get();

        return view('admin.offers.create', [
            'autos' => $autos,
            'order_number' => ++$order_number,
            'companies' => $companies,
            'request' => $request,
            'order_company_id' => $order_company_id,
            'contact_id' => ($contact_id > 0) ? $contact_id : null,
            'prices' => $prices,
            'order_name' => (isset($order_name)) ? $order_name : null,
            'professions' => $professions,
            'products' => $products
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\OfferRequest $request)
    {
        $data = $request->input();

        //компания
        $data['company_id'] = Auth::user()->company_id;

        $data['metro_id'] = (empty($data['metro_id']) || !$data['metro_id']) ? null : $data['metro_id'];
        $data['order_company_id'] = (empty($data['order_company_id']) || !$data['order_company_id']) ? null : $data['order_company_id'];

        //date / time
        $data['date_start'] = (new Carbon($data['date_start']))->format('Y.m.d');
        /*$data['timestart'] = (new Carbon($data['date'].' '.$request->input('timestart')))->timestamp;
        $data['timefinish'] = (new Carbon($data['date'].' '.$request->input('timefinish')))->timestamp;*/
        $data['date_finish'] = (new Carbon($data['date_finish']))->format('Y.m.d');

        //calculator
        /*$calculator = array();
        parse_str($data['calculator'], $calculator);*/
        //dump($calculator); dd();

        //контакты
        if( $request->has('contact_id') && intval($request->input('contact_id')[0]) > 0 )
        {
            $offer = Offer::create($data);
            $offer->contacts()->sync($request->input('contact_id'));
        }
        else {
            //return redirect()->back()->withErrors(['message', 'Добавьте контакты!'])->withInput($request->all());
            return redirect()->back()->withErrors(['Добавьте контакты']);
        }

        //products
        if($offer) {
            if( $request->has('product_id') && intval($request->input('product_id')[0]) > 0 ) {
                $offer->products()->sync($request->input('product_id'));
            }
        }

        return redirect()->route('admin.offers.edit', [
            'id' => $offer->id,
            'tab' => 'offer',
            //'calculator' => $calculator,
            //'prices' => $prices
        ])->withMessage('Предложение добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /*$order = Order::with(['days' => function($query){
            $query->orderBy('timestart', 'desc');
        }])->findOrFail($id);*/
        $offer = Offer::findOrFail($id);

        /*if (Gate::denies('edit', $offer)) {
            abort(403);
        }*/

        /*$sms_order = \App\Models\Setting::getVar('sms_order');
        $sms_order_cancel = \App\Models\Setting::getVar('sms_order_cancel');*/

        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();
        //$companies = \App\Models\OrderCompany::orderBy('name')->get();
        $companies = \App\Models\OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();

        $prices = [];
        $price = \App\Models\Price::where('company_id', '=', $offer->company_id)->first();
        if($price) {
            $arPrice = \GuzzleHttp\json_decode($price->price);
            //var_dump(get_object_vars($arPrice)); dd();
            $prices = get_object_vars($arPrice);
        }

        $calculator = null;
        if($request->has('calculator'))
            $calculator = $request->all()['calculator'];

        $professions = \App\Models\UserProfession::where('company_id', '=', Auth::user()->company_id)
            ->orWhere('company_id', null)
            ->orderBy('name')->get();

        $products = \App\Models\Product::where('company_id', '=', Auth::user()->company_id)->get();
        
        //dump($offer); dd();
        return view('admin.offers.edit', [
            'offer' => $offer,
            /*'sms_order' => $sms_order,
            'sms_order_cancel' => $sms_order_cancel,*/
            'autos' => $autos,
            'companies' => $companies,
            'request' => $request,
            'prices' => $prices,
            'calculator' => $calculator,
            'professions' => $professions,
            'products' => $products
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\OfferRequest $request, $id)
    {
        $offer = Offer::findOrFail($id);

        /*if (Gate::denies('edit', $order)) {
            abort(403);
        }*/

        //обновляем заказ
        $data = $request->input();
        $data['metro_id'] = (empty($data['metro_id']) || !$data['metro_id']) ? null : $data['metro_id'];
        $data['order_company_id'] = (empty($data['order_company_id']) || !$data['order_company_id']) ? null : $data['order_company_id'];

        //date/time
        //$data['date_start'] = (new Carbon($data['date']))->format('Y.m.d');
        /*$data['timestart'] = (new Carbon($data['date'].' '.$request->input('timestart')))->timestamp;
        $data['timefinish'] = (new Carbon($data['date'].' '.$request->input('timefinish')))->timestamp;*/
        //$data['date_finish'] = (new Carbon($data['date_finish']))->format('Y.m.d');

        $offer->update($data);
        //связь с контактами
        if($request->has('contact_id')) {
            $contacts = array_diff($request->input('contact_id'), array(''));
            if(count($contacts)) {
                $offer->contacts()->sync($contacts);
            }
        }

        //связь with products
        if( $request->has('product_id') && intval($request->input('product_id')[0]) > 0 ) {
            $offer->products()->sync($request->input('product_id'));
        }

        if(!empty($data['route']) &&  $data['route']=='back') {
            return redirect()->back()->withMessage('Изменения применены');
        } else {
            return redirect()->route('admin.offers.index')->withMessage('Предложение изменено');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::findOrFail($id);

        /*if (Gate::denies('delete', $order)) {
            abort(403);
        }*/

        Offer::destroy($id);

        return redirect()->route('admin.offers.index')->withMessage('Предложение удалено');
    }

    public function downloadPDF($id){
        $offer = Offer::findOrFail($id);
        $company = \App\Models\Company::findOrFail(Auth::user()->company_id);

        $order_company = \App\Models\OrderCompany::find($offer->order_company_id);
        
        /*$pdf = PDF::loadHTML('<h1>Test</h1><img style="width:100%" src="http://test.cleancrm.ru/assets/imgs/companies/stamp/1524066711_0k1At.jpg">');
        return $pdf->stream();*/

        //$pdf = PDF::loadView('admin.offers.pdf', compact('offer', 'company', 'order_company'));
        $pdf = PDF::loadView('admin.offers.pdf', [
            'offer' => $offer, 
            'company' => $company, 
            'order_company' => $order_company,
            'sumNDS' => $offer->sumNDS($offer->id),
            'sumTotal' => $offer->sumTotal($offer->id)
        ]);
        

        //return $pdf->download('offer.pdf');
        return $pdf->stream();
    }
}
