<?php

namespace App\Http\Controllers\admin;

use App\Events\onOrderAddEvent;
use App\Events\onOrderUpdateEvent;
use App\Models\OrderContact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Company;
use App\Models\OrderCompany;
use App\User;
use Carbon\Carbon;
use Auth;
use Gate;
use DB;
use Event;

class OrderController extends Controller
{
    public function __construct() {
        $this->authorize('index', new Order());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFormFilter($request->input());
        //dd($filters);
        //$orders = Order::orderBy('id', 'desc');

        //sort /*
        if ( isset($filters['order']) && $filters['order']!='')
        {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();

            switch ($arSort[0]) {
                case 'number':
                    $orders = Order::orderBy('number', $arSort[1]);
                    break;
                case 'name':
                    $orders = Order::orderBy('name', $arSort[1]);
                    break;
                case 'status':
                    $orders = Order::orderBy('status', $arSort[1]);
                    break;
                case 'date':
                    $orders = Order::orderBy('date', $arSort[1]);
                    break;
                case 'days_count':
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `order_id`, COUNT(*) as cnt_days
                                FROM `order_days`
                                GROUP BY `order_id`
                                ORDER BY cnt_days DESC) `hz`"), 'hz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("hz.cnt_days $arSort[1]");
                    //->get();
                    //->toSql();
                    break;
                case 'users_count':
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT order_id, SUM(sz.u_cnt) user_cnt
                                FROM (
                                    SELECT order_days.id, order_id, u_cnt
                                    FROM `order_days`
                                    LEFT JOIN (
                                        SELECT day_id, COUNT(`user_id`) as u_cnt
                                        FROM `order_users`
                                        GROUP BY order_users.day_id
                                    ) hz 
                                    ON hz.day_id = order_days.id ) as sz
                                GROUP BY order_id) `zz`"), 'zz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("zz.user_cnt $arSort[1]");
                    //->get();
                    /*->toSql();
                    dump($orders); dd();*/
                    break;
                case 'sum':
                    /*
                    SELECT * FROM `orders`
                    LEFT JOIN (
                        SELECT order_id, SUM(sz.s_amount) sum_amount
                        FROM (
                            SELECT order_days.id, order_id, s_amount
                            FROM `order_days`
                            LEFT JOIN (
                                SELECT day_id, SUM(`amount`) as s_amount
                                FROM `order_users`
                                GROUP BY order_users.day_id
                            ) hz
                            ON hz.day_id = order_days.id ) as sz
                        GROUP BY order_id
                    ) zz ON orders.id = zz.order_id
                    ORDER BY zz.sum_amount DESC
                    */
                    $orders = Order::select('orders.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT order_id, SUM(sz.s_amount) sum_amount
                                FROM (
                                    SELECT order_days.id, order_id, s_amount
                                    FROM `order_days`
                                    LEFT JOIN (
                                        SELECT day_id, SUM(`amount`) as s_amount
                                        FROM `order_users`
                                        GROUP BY order_users.day_id
                                    ) hz 
                                    ON hz.day_id = order_days.id ) as sz
                                GROUP BY order_id) `zz`"), 'zz.order_id', '=', 'orders.id'
                        )
                        ->orderByRaw("zz.sum_amount $arSort[1]");
                    //->get();
                    /*->toSql();
                    dump($orders); dd();*/
                    break;
            }
        }
        else
            $orders = Order::orderBy('id', 'desc');
        //sort */

        //доступ к компаниям
        if (Auth::user()->can('company', new Order())) {
            if (!empty($filters) && !empty($filters['company'])) {
                $orders->where('company_id', $filters['company']);
            }
        } else {    //только заказы твоей компании
            $orders->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id)
                    ->orWhere('company_id', null);
            });
        }

        //filter /*
        if (!empty($filters) && !empty($filters['number'])) {
            $orders->where('number', 'LIKE', '%'.$filters['number'].'%');
        }
        if (!empty($filters) && !empty($filters['status'])) {
            switch ($filters['status']) {
                case 'my':
                    //$orders->where('user_id', '=', Auth::user()->id);
                    $user_id = Auth::user()->id;
                    $orders->whereHas('responsible_users', function($q) use($user_id) {
                        $q->where('order_user.user_id', $user_id);
                    });
                    break;
                case 'new':
                    //dd(Carbon::today()->format('Y-m-d H:i:s'));
                    $orders->where('created_at', '>', Carbon::today()->format('Y-m-d H:i:s'));
                    break;
                default:
                    $orders->where('status', $filters['status']);
                    break;
            }
            
            
        }
        if (!empty($filters) && isset($filters['deleted']) && $filters['deleted']) {
            $orders->withTrashed();
        }
        if ( @$filters['user_id'] > 0 ) {
            $user_id = $filters['user_id'];
            $orders->whereHas('responsible_users', function($q) use($user_id) {
                $q->where('order_user.user_id', $user_id);
            });

            /*$user_find = \App\User::find($filters['user_id']);
            $filters['fio'] = $user_find ? $user_find->fio() : '';*/
        }

        if ( !empty($filters) && !empty($filters['sum_compare']) && ( !empty($filters['sum']) || (!empty($filters['sum_min']) && !empty($filters['sum_max'])) ) ) {
            switch ($filters['sum_compare']) {
                case 'equal':
                    $orders->where('amount', '=', intval($filters['sum']));
                    break;
                case 'interval':
                    $orders->where('amount', '>', intval($filters['sum_min']))->where('amount', '<', intval($filters['sum_max']));
                    break;
                case 'more':
                    $orders->where('amount', '>', intval($filters['sum']));
                    break;
                case 'less':
                    $orders->where('amount', '<', intval($filters['sum']));
                    break;
            }

        }
        if(isset($filters['date_start'])) {
            switch ($filters['date_start']) {
                case 'yesterday':
                    $orders->where('date', '=', Carbon::yesterday()->format('Y-m-d'));
                    break;
                case 'tomorrow':
                    $orders->where('date', '=', Carbon::tomorrow()->format('Y-m-d'));
                    break;
                case 'today':
                    $orders->where('date', '=', Carbon::today()->format('Y-m-d'));
                    break;
                case 'week':
                    $period_start = Carbon::now()->startOfWeek()->format('Y-m-d');
                    $period_end = Carbon::now()->endOfWeek()->format('Y-m-d');
                    $orders->where('date', '>=', $period_start)->where('date', '<=', $period_end);
                    break;
            }    
        }
        //dd(Carbon::now()->endOfWeek()->format('Y-m-d H:m:s'));
        if(isset($filters['date_created'])) {
            switch ($filters['date_created']) {
                case 'yesterday':
                    $orders->where('created_at', '>=', Carbon::yesterday()->startOfDay()->format('Y-m-d H:m:s'))
                        ->where('created_at', '<=', Carbon::yesterday()->endOfDay()->format('Y-m-d H:m:s'));
                    break;
                case 'today':
                    $orders->where('created_at', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:m:s'))
                        ->where('created_at', '<=', Carbon::now()->endOfDay()->format('Y-m-d H:m:s'));
                    break;
                case 'week':
                    $period_start = Carbon::now()->startOfWeek()->format('Y-m-d H:m:s');
                    $period_end = Carbon::now()->endOfWeek()->format('Y-m-d H:m:s');
                    $orders->where('created_at', '>=', $period_start)
                        ->where('created_at', '<=', $period_end);
                    break;
            }
        }

        if(isset($filters['date_update'])) {
            switch ($filters['date_update']) {
                case 'yesterday':
                    $orders->where('updated_at', '>=', Carbon::yesterday()->startOfDay()->format('Y-m-d H:m:s'))
                        ->where('updated_at', '<=', Carbon::yesterday()->endOfDay()->format('Y-m-d H:m:s'));
                    break;
                case 'today':
                    $orders->where('updated_at', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:m:s'))
                        ->where('updated_at', '<=', Carbon::now()->endOfDay()->format('Y-m-d H:m:s'));
                    break;
                case 'week':
                    $period_start = Carbon::now()->startOfWeek()->format('Y-m-d H:m:s');
                    $period_end = Carbon::now()->endOfWeek()->format('Y-m-d H:m:s');
                    $orders->where('updated_at', '>=', $period_start)
                        ->where('updated_at', '<=', $period_end);
                    break;
            }
        }

        if(isset($filters['date_finish'])) {
            switch ($filters['date_finish']) {
                case 'yesterday':
                    $orders->where('date_finish', '=', Carbon::yesterday()->format('Y-m-d'));
                    break;
                case 'tomorrow':
                    $orders->where('date_finish', '=', Carbon::tomorrow()->format('Y-m-d'));
                    break;
                case 'today':
                    $orders->where('date_finish', '=', Carbon::today()->format('Y-m-d'));
                    break;
                case 'week':
                    $period_start = Carbon::now()->startOfWeek()->format('Y-m-d');
                    $period_end = Carbon::now()->endOfWeek()->format('Y-m-d');
                    $orders->where('date_finish', '>=', $period_start)->where('date', '<=', $period_end);
                    break;
            }
        }
        
        if(isset($filters['typ']) && !empty($filters['typ'])) {
            $orders->whereRaw('json_contains(`calculator`, \'{"typ" : "'.$filters['typ'].'"}\')');
        }
        
        if(isset($filters['payment']) && $filters['payment'] != '' ) {
            $orders->whereRaw('json_contains(`calculator`, \'{"payment" : "'.$filters['payment'].'"}\')');
        }

        if(!empty(@$filters['pay_status']) ) {
            $pay_status = $filters['pay_status'];

            /*$orders->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id)
                    ->orWhere('company_id', null);
            });*/

            $orders->whereHas('invoice', function ($query) use ($pay_status){
                $query->where('status', $pay_status);
            })->orWhere('pay_status', $pay_status)
                ->where('company_id', Auth::user()->company_id)
                ->orWhere('company_id', null);
        }

        //filter */

        $orders = $orders->with('days', 'users')->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        //всего количество/сумма
        $count_orders = $orders->count();
        $sum_orders = $orders->sum('amount');

        $sum_paginate = 0;
        foreach ($orders as $order) {
            //dump($orders);
            $sum_paginate = $sum_paginate + $order->amount;
        }

        //dd($sum_paginate);
        $companies = Company::orderBy('name')->get();

        return view('admin.orders.index', [
            'orders' => $orders,
            'filters' => $filters,
            'companies' => $companies,
            'count_orders' => $count_orders,
            'sum_orders' => $sum_orders,
            'sum_paginate' => $sum_paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        //доступ на добавление
        $this->authorize('add', new Order());

        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();

        $order_number =  Order::where('company_id', Auth::user()->company_id)->max('number');
        //$companies = \App\Models\OrderCompany::orderBy('name')->get();
        $companies = OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();

        //return view('admin.orders.create', ['autos' => $autos, 'order_number' => ++$order_number, 'companies' => $companies, 'request' => $request]);

        $order_company_id = null; $contact_id = null;
        if($request->has('order_company_id') ) {
            $order_company_id = intval($request->input('order_company_id'));
        }
        else if($request->has('contact_id') ) {
            $contact_id = intval($request->input('contact_id'));
        }

        if($request->has('order_name'))
            $order_name = $request->input('order_name');
        
        $prices = [];
        $price = \App\Models\Price::where('company_id', '=', Auth::user()->company_id)->first();
        if($price) {
            $arPrice = \GuzzleHttp\json_decode($price->price);
            //var_dump(get_object_vars($arPrice)); dd();
            $prices = get_object_vars($arPrice);
        }

        //количество заказов на дату[сегодня +2мес]
        $countOrdersOnDate = \App\Models\Calendar::countOrdersOnDate();

        return view('admin.orders.create', [
            'autos' => $autos, 
            'order_number' => ++$order_number, 
            'companies' => $companies, 
            'request' => $request,
            'order_company_id' => $order_company_id,
            'contact_id' => ($contact_id > 0) ? $contact_id : null,
            'prices' => $prices,
            'order_name' => (isset($order_name)) ? $order_name : null,
            'turns' => [],
            'countOrdersOnDate' => $countOrdersOnDate
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\OrderRequest $request)
    {
        //dd($request->all());

        //доступ на добавление
        $this->authorize('add', new Order());

        //добавляем заказ
        $data = $request->input();
        //компания
        $data['company_id'] = Auth::user()->company_id;

        $data['metro_id'] = (empty($data['metro_id']) || !$data['metro_id']) ? null : $data['metro_id'];
        $data['order_company_id'] = (empty($data['order_company_id']) || !$data['order_company_id']) ? null : $data['order_company_id'];

        //dump($data); dd();
        if($data["company_id"] == null)
            $data["company_id"] = $data['order_company_id'];

        //date/time
        /*$data['date'] = (new Carbon($data['date']))->format('Y.m.d');
        $data['date_finish'] = (new Carbon($data['date_finish']))->format('Y.m.d');*/

        //order periods
        $periods = [];
        foreach ($request->input('date') as $key=>$val) {
            $periods[$key] = [
                "date_start" => @$request->input('date')[$key],
                "time_start" => @$request->input('time_start')[$key],
                "date_finish" => @$request->input('date_finish')[$key],
                "time_finish" => @$request->input('time_finish')[$key]
            ];
        }
        //dd($periods);
        $data['periods'] = \GuzzleHttp\json_encode($periods);
        //dd($data['periods']);
        if( count($periods) ) {
            $date_start = $periods[0]['date_start'];
            $date_finish = $periods[0]['date_finish'];
            foreach ($periods as $key=>$period)
            {
                //err
                if($period['date_start'] > $period['date_finish'])
                    return redirect()->back()->withErrors(['Дата начала больше даты окончания'])->withInput();

                if($period['date_start'] == $period['date_finish'] && $period['time_start'] >= $period['time_finish'])
                    return redirect()->back()->withErrors(['Время начала больше времени окончания'])->withInput();

                if($date_start > $period['date_start'])
                    $date_start = $period['date_start'];

                if($date_finish < $period['date_finish'])
                    $date_finish = $period['date_finish'];
            }
            $data['date'] = (new Carbon($date_start))->format('Y.m.d');
            $data['date_finish'] = (new Carbon($date_finish))->format('Y.m.d');
        }

        //calculator
        $calculator = array();
        parse_str($data['calculator'], $calculator);
        //dd($calculator);
        if(!isset($calculator["base_info"]) || empty($calculator["base_info"]) ) {
            //return redirect()->back()->withErrors(['Отсутствует базовая информация калькулятора'])->withInput();
            return redirect()->route('admin.orders.create', [
                'tab' => 'calculator',
                'calculator' => $calculator
            ])->withErrors(['Отсутствует базовая информация калькулятора'])->withInput();
        }
        else {
            $data['calculator'] = \GuzzleHttp\json_encode($calculator);
            $data['amount'] = intval($calculator['amount']);
        }

        //контакты
        if( $request->has('contact_ids') && intval($request->input('contact_ids')[0]) > 0 ) {
            $contacts = array_diff($request->input('contact_ids'), array(''));
            if(count($contacts)) {
                $order = @Order::create($data);
                $order->contacts()->sync($contacts);

                //связь с ответственными
                if($request->has('user_id')) {
                    $users = array_diff($request->input('user_id'), array(''));
                    if(count($users)) {
                        $order->responsible_users()->sync($users);
                    }
                }

                //logs
                Event::fire(new onOrderAddEvent($order, Auth::user()));

                return redirect()->route('admin.orders.edit', [
                    'id' => $order->id,
                    'tab' => 'order',
                    'calculator' => $calculator,
                    'date_periods' => \GuzzleHttp\json_decode($order->periods)
                    //'prices' => $prices
                ])->withMessage('Заказ добавлен');
            }
        }

        /*//связь с ответственными
        if($request->has('user_id')) {
            $users = array_diff($request->input('user_id'), array(''));
            if(count($users)) {
                $order->responsible_users()->sync($users);
            }
        }

        //logs
        Event::fire(new onOrderAddEvent($order, Auth::user()));

        return redirect()->route('admin.orders.edit', [
            'id' => $order->id,
            'tab' => 'order',
            'calculator' => $calculator,
            'date_periods' => \GuzzleHttp\json_decode($order->periods)
            //'prices' => $prices
        ])->withMessage('Заказ добавлен');*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $order = Order::with(['days' => function($query){
            $query->orderBy('timestart', 'desc');
        }, 'user', 'logs' => function($query){
            $query->orderBy('created_at', 'desc');
        }])->findOrFail($id);
        
        if (Gate::denies('edit', $order)) {
            abort(403);
        }

        $sms_order = \App\Models\Setting::getVar('sms_order');
        $sms_order_cancel = \App\Models\Setting::getVar('sms_order_cancel');

        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();

        /*$companies = OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();*/

        $prices = [];
        $price = \App\Models\Price::where('company_id', '=', $order->company_id)->first();
        if($price) {
            $arPrice = \GuzzleHttp\json_decode($price->price);
            //var_dump(get_object_vars($arPrice)); dd();
            $prices = get_object_vars($arPrice);
        }

        if($order->calculator) {
            $arCalculator = \GuzzleHttp\json_decode($order->calculator);
            //var_dump(get_object_vars($arCalculator)); dd();
            $calculator = @get_object_vars($arCalculator);
        }
        else
            $calculator = null;

        //logs
        $sql = "
        SELECT date_format(created_at, '%d.%m.%Y') as created_at, `user_id`, `note`, YEAR(created_at) as _year, MONTH(created_at) as _month
        FROM orders_logs
        WHERE `order_id` = $order->id
        GROUP BY YEAR(created_at) DESC, MONTH(created_at) DESC, created_at DESC, orders_logs.user_id, orders_logs.note
        ";

        $logs = DB::select($sql);
        $periods = [];
        foreach ($logs as $key=>$val)
            $periods[] = ['year' => $val->_year, 'month' => $val->_month];
        //dd(array_unique($periods, SORT_REGULAR));
        $periods = array_unique($periods, SORT_REGULAR);

        //staff
        $sql = "
        SELECT sq3.*
        FROM
            (SELECT *
            FROM
                (SELECT order_days.id as oday_id, `date`, `nigth`
                FROM `order_days` 
                WHERE `order_id` = $order->id) as sq1
            
            LEFT JOIN 
            
            (SELECT day_id, COUNT(`user_id`) as users_count, SUM(`amount`) as sum_amount
            FROM `order_users`
            WHERE day_id IN (SELECT `id` FROM `order_days` WHERE `order_id` = $order->id)       
            GROUP BY day_id) as sq2
              ON sq1.oday_id = sq2.day_id) as sq3
        ORDER BY sq3.date";

        $turns = DB::select($sql);
        //dd($order->periods);
        
        //order periods
        $date_periods = [];
        if($order->periods) {
            $date_periods = \GuzzleHttp\json_decode($order->periods);
            //dd(\GuzzleHttp\json_decode($order->periods));
        }

        //количество заказов на дату[сегодня +2мес]
        $countOrdersOnDate = \App\Models\Calendar::countOrdersOnDate(); 

        return view('admin.orders.edit', [
            'order' => $order,
            'sms_order' => $sms_order,
            'sms_order_cancel' => $sms_order_cancel,
            'autos' => $autos,
            //'companies' => $companies,
            'request' => $request,
            'prices' => $prices,
            'calculator' => $calculator,
            'logs' => $logs,
            'periods' => $periods,
            'turns' => $turns,
            'date_periods' => $date_periods,
            'countOrdersOnDate' => $countOrdersOnDate
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\OrderRequest $request, $id)
    {
        $order = Order::findOrFail($id);

        if (Gate::denies('edit', $order)) {
            abort(403);
        }

        //обновляем заказ
        $data = $request->input();
        //dd($data['date'][0]);
        $data['date'] = (new Carbon($data['date'][0]))->format('Y.m.d');
        $data['metro_id'] = (empty($data['metro_id']) || !$data['metro_id']) ? null : $data['metro_id'];
        $data['order_company_id'] = (empty($data['order_company_id']) || !$data['order_company_id']) ? null : $data['order_company_id'];

        //timestart(end)
        /*$data['timestart'] = (new Carbon($data['date'].' '.$request->input('timestart')))->timestamp;
        $data['timefinish'] = (new Carbon($data['date'].' '.$request->input('timefinish')))->timestamp;*/
        $data['date_finish'] = (new Carbon($data['date_finish'][0]))->format('Y.m.d');

        //calculator    /*
        $calculator = array();
        parse_str($data['calculator'], $calculator);

        if(sizeof($calculator) == 1) {
            $calculator = @\GuzzleHttp\json_decode($data['calculator']);

            if(!isset($calculator->base_info) || empty($calculator->base_info) ) {
                //return redirect()->back()->withErrors(['Отсутствует базовая информация калькулятора'])->withInput();
                return redirect()->route('admin.orders.edit', [
                    'tab' => 'calculator',
                    'calculator' => $calculator
                ])->withErrors(['Отсутствует базовая информация калькулятора'])->withInput();
            }
        }
        else {
            if(!isset($calculator["base_info"]) || empty($calculator["base_info"]) )
                return redirect()->back()->withErrors(['Отсутствует базовая информация калькулятора'])->withInput();
        }
        //dd($calculator);
        $data['calculator'] = \GuzzleHttp\json_encode($calculator);
        //  */

        $old_data = $order->getOriginal();
        /*$old_responsibles = $order->responsible_users;
        $old_contacts =  $order->contacts;*/

        $sql = "SELECT `contact_id` FROM `order_contact` WHERE `order_id` = $order->id";
        $old_contacts = DB::select($sql);
        //dump($order->contacts);

        $sql = "SELECT `user_id` FROM `order_user` WHERE `order_id` = $order->id";
        $old_responsibles = DB::select($sql);

        $order->update($data);

        //связь с контактами
        /*if($request->has('contact_id')) {
            $contacts = array_diff($request->input('contact_id'), array(''));
            if(count($contacts)) {
                $order->contacts()->sync($contacts);
            }
        }*/
        //dd($request->input('contact_id'));
        if($request->has('contact_ids')) {
            $contacts = array_diff($request->input('contact_ids'), array(''));
            //dd($contacts);
            if(count($contacts)) {
                $order->contacts()->sync($contacts);
            }
        }

        //связь с ответственными
        if($request->has('user_id')) {
            $users = array_diff($request->input('user_id'), array(''));
            if(count($users)) {
                $order->responsible_users()->sync($users);
            }
        }

        $new_data = $order->getAttributes();
        /*$new_responsibles = $order->responsible_users;
        $new_contacts =  $order->contacts;*/

        $sql = "SELECT `contact_id` FROM `order_contact` WHERE `order_id` = $order->id";
        $new_contacts = DB::select($sql);
        //dump($order->contacts); dd();

        $sql = "SELECT `user_id` FROM `order_user` WHERE `order_id` = $order->id";
        $new_responsibles = DB::select($sql);
        //dump($new_responsibles, $new_contacts); dd();

        //logs
        //Event::fire(new onOrderUpdateEvent($order, Auth::user()));
        Event::fire(new onOrderUpdateEvent($order, Auth::user(), $old_data, $new_data, $old_responsibles, $new_responsibles, $old_contacts, $new_contacts));
        
        //dd($data['route']);
        if(!empty($data['route']) &&  $data['route']=='back') {
            return redirect()->back()->withMessage('Изменения применены');
        } else {
            return redirect()->route('admin.orders.index')->withMessage('Заказ изменен');
        }
    }

    /**
     * Обновляем персонал
     * @param Request $request
     * @param $id
     */
    public function staff(Request $request, $id) {
        //dd($request->all());
        $order = Order::findOrFail($id);

        if (Gate::denies('edit', $order)) {
            abort(403);
        }
        
        //удаляем дни
        $order->days()->whereNotIn('id', $request->input('day_ids'))->delete();
        //$order->days()->whereIn('id', $request->input('day_ids'))->delete();

        //проходим дни
        if($request->has('dates')) {
            foreach($request->input('dates') as $day_id => $date)
            {
                if(!$date) {
                    continue;
                }
                /*dump($day_id);
                dd($request->input('metro_id')[$day_id-1]);*/
                //обновляем день
                $data_d = [
                    'date' => (new Carbon($date))->format('Y.m.d'),
                    'time' => $request->input('times.'.$day_id),
                    'ours' => $request->input('ours.'.$day_id) ?: 0,
                    'timestart' => (new Carbon($date.' '.$request->input('times.'.$day_id)))->timestamp,
                    'timefinish' => (new Carbon($date.' '.$request->input('times.'.$day_id)))->addHours($request->input('ours.'.$day_id))->timestamp,
                    'nigth' => $request->input('nigth.'.($day_id-1)) ?: 0,
                    'metro_id' => $request->input('metro_id')[$day_id-1] ?: null
                ];
                if($request->has('day_ids.'.$day_id)) {
                    $day = $order->days()->find($request->input('day_ids.'.$day_id));
                    $day->update($data_d);
                    //добавляем день
                } else {
                    $day = $order->days()->create($data_d);
                }
                //удаляем водителей
                //dd($request->input('user_ids_v.' . $day_id));
                if($request->has('user_id_v.'.$day_id) && $request->input('user_id_v.'.$day_id)) {
                    $day->users()->where('driver', true)->whereNotIn('id', $request->input('user_ids_v.' . $day_id))->delete();
                } else {
                    $day->users()->where('driver', true)->delete();
                }
                //$day->users()->where('driver', true)->delete();

                //проходим водителей
                if($request->has('user_id_v.'.$day_id)) {
                    foreach($request->input('user_id_v.'.$day_id) as $key => $user_id)
                    {
                        if(!$user_id) {
                            continue;
                        }

                        /*dump($key);
                        dump($request->input('fouls_v.'.$day_id.'.'.$key));*/
                        $data_u = ['user_id' => $user_id,
                            'cef' => 1,
                            'amount' => $request->has('amount_v.'.$day_id.'.'.$key) ? $request->input('amount_v.'.$day_id.'.'.$key) : '0',
                            'comment' => $request->has('comment_v.'.$day_id.'.'.$key) ? $request->input('comment_v.'.$day_id.'.'.$key) : '',
                            'driver' => true,
                            'auto_id' => $request->has('auto_id.'.$day_id.'.'.$key) ? $request->input('auto_id.'.$day_id.'.'.$key) : null,
                            //'main' =>  $request->has('main_v.'.$day_id.'.'.$key) ? 1 : 0
                            'main' =>  $request->input('main_v.'.$day_id) == $user_id ? 1 : 0,
                            'rating' => $request->input('brigadir_rating.'.$day_id.'.'.$key),
                            'late' =>  $request->input('fouls_v.'.$day_id.'.'.$key) == 'late' ? 1 : 0,
                            'nocome' =>  $request->input('fouls_v.'.$day_id.'.'.$key) == 'nocome' ? 1 : 0
                        ];

                        //обновляем пользователя
                        if($request->has('user_ids_v.'.$day_id.'.'.$key)){
                            $day->users()->find($request->input('user_ids_v.'.$day_id.'.'.$key))->update($data_u);
                            //добавляем пользователя
                        } else {
                            $day->users()->create($data_u);
                        }
                    }
                }

                //удаляем пользователей
                if($request->has('user_id.'.$day_id) && $request->input('user_id.'.$day_id)) {
                    $day->users()->where('driver', false)->whereNotIn('id', $request->input('user_ids.' . $day_id))->delete();
                } else {
                    $day->users()->where('driver', false)->delete();
                }

                //проходим пользователей
                if($request->has('user_id.'.$day_id)) {
                    foreach($request->input('user_id.'.$day_id) as $key => $user_id) {
                        if(!$user_id) {
                            continue;
                        }
                        $data_u = ['user_id' => $user_id,
                            'cef' => 1,
                            'amount' => $request->has('amount.'.$day_id.'.'.$key) ? $request->input('amount.'.$day_id.'.'.$key) : '0',
                            'comment' => $request->has('comment.'.$day_id.'.'.$key) ? $request->input('comment.'.$day_id.'.'.$key) : '',
                            'rating' => $request->input('sotrudnik_rating.'.$day_id.'.'.$key),
                            'late' =>  $request->input('fouls.'.$day_id.'.'.$key) == 'late' ? 1 : 0,
                            'nocome' =>  $request->input('fouls.'.$day_id.'.'.$key) == 'nocome' ? 1 : 0
                        ];

                        //обновляем пользователя
                        if($request->has('user_ids.'.$day_id.'.'.$key)) {
                            $day->users()->find($request->input('user_ids.'.$day_id.'.'.$key))->update($data_u);
                            //добавляем пользователя
                        } else {
                            $day->users()->create($data_u);
                        }
                    }
                }
            }
        }

        if($request->has('route') &&  $request->input('route')=='back') {
            return redirect()->route('admin.orders.edit', ['id' => $order->id, 'tab' => 'staff'])->withMessage('Заказ изменен');
        } else {
            return redirect()->route('admin.orders.index')->withMessage('Заказ изменен');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //доступ на удаление
        $order = Order::findOrFail($id);

        if (Gate::denies('delete', $order)) {
            abort(403);
        }

        Order::destroy($id);

        return redirect()->route('admin.orders.index')->withMessage('Заказ удален');
    }


    /**
     * Востановление мягко удаленной категории
     * @param $id
     * @return mixed
     */
    public function restore($id) {
        //доступ на удаление
        $order = Order::findOrFail($id);

        if (Gate::denies('delete', $order)) {
            abort(403);
        }

        Order::withTrashed()->find($id)->restore();
        return redirect()->route('admin.orders.index')->withMessage('Заказ востановлен');
    }

    /**
     * Отправка СМС по заказу
     * @param Requests\admin\SendSmsRequest $request
     */
    public function sendSms(Requests\admin\SendSmsRequest $request) {
        $data = $request->input();
        $data['order_id'] = $data['id'];

        $users = \App\User::whereIn('id', explode(',', $request->input('phones')))->with('phones')->get();
        foreach($users as $user) {
            $phones[] = '7'.$user->phones->first()->number;
        }

        $sms = new \App\Models\Sms();
        if(!$sms->send(implode(',', $phones), $data['text'])) {
            $data['result'] = '0';
            $data['error'] = $sms->getError();
        } else {
            $data['result'] = '1';
        }

        $sms = $sms->create($data);
        $sms->users()->saveMany($users);

        if($data['result']) {
            return redirect()->back()->withMessage('СМС успешно разосланы');
        } else {
            return redirect()->route('admin.sms.show', $sms->id)->withMessage('При отправке СМС произошла ошибка');
        }
    }

    /**
     * Отменяем день заказа
     * @param Requests\admin\OrderCancelRequest $request
     * @return mixed
     */
    public function cancel(Requests\admin\OrderCancelRequest $request) {
        $data = $request->input();
        $data['order_id'] = $data['id'];

        $phones = [];
        $day = \App\Models\OrderDay::findOrFail($data['day_id']);
        foreach($day->users as $user) {
            $phones[] = '7'.$user->user->phones->first()->number;
            //обнуляем рабочий день
            $user->update(['cef' => 0]);
        }

        if(count($phones)) {
            $sms = new \App\Models\Sms();
            if(!$sms->send(implode(',', $phones), $data['text'])) {
                $data['result'] = '0';
                $data['error'] = $sms->getError();
            } else {
                $data['result'] = '1';
            }

            $sms = $sms->create($data);
            foreach($day->users as $user) {
                $sms->users()->save($user->user);
            }
        }

        if($data['result']) {
            return redirect()->back()->withMessage('СМС успешно разосланы');
        } else {
            return redirect()->route('admin.sms.show', $sms->id)->withMessage('При отправке СМС произошла ошибка');
        }
    }

    /**
     * Поиск метро в автоподстановке
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchMetro(Request $request) {
        $data =[];
        if($request->has('search_param') && $request->input('search_param')) {
            $metro = \App\Models\Metro::where('name', 'LIKE', $request->input('search_param') . '%');
            $metro = $metro->orderBy('name')->take(10)->get();

            foreach ($metro as $item) {
                $data[] = ['id' => $item->id, 'name' => $item->name];
            }
        }

        return response()->json($data);
    }

    /**
     * Отметка о опоздании или не явке сотрудника на работу
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function late(Request $request) {
        $user = \App\Models\OrderUser::find($request->input('user_id'));
        if(empty($user)) {
            return response()->json(['result' => 'error']);
        }
        if($request->input('err') == 'late') {
            $data = ['late' => 1];
        } elseif($request->input('err') == 'nocome') {
            $data = ['nocome' => 1, 'amount' => 0];
        }

        $user->update($data);

        return response()->json(['result' => 'ok']);
    }

    /**
     * Исключить параметр из фильтра(нажали на крестик)
     * */
    public function exclude_from_filter($filter) {
        //dd(session()->get('admin.orders.index.filters'));
        if($filter == 'sum_compare') {
            session()->forget("admin.orders.index.filters.sum_compare");
            session()->forget("admin.orders.index.filters.sum_min");
            session()->forget("admin.orders.index.filters.sum_max");
        }
        else
            session()->forget("admin.orders.index.filters.$filter");

        return redirect()->route('admin.orders.index');
    }

    public function set_status(Request $request) {
        //dump($request->all());
        $order = Order::findOrFail($request->input('id'));

        $old_data = $order->getOriginal();

        if (Gate::denies('edit', $order)) {
            abort(403);
        }
        
        if( $request->has('typ') ) {    // статус оплаты
            if($order->invoice)
                $order->invoice()->update(['status' => $request->input('status')]);
            else
                $order->update(['pay_status' => $request->input('status')]);
        }
        else
            $order->update(['status' => $request->input('status')]);

        $new_data = $order->getAttributes();
        //dd($old_data, $new_data);
        $log = "\r\nСтатус: ".\App\Models\Order::$statuses[$old_data['status']]['name']." -> ".\App\Models\Order::$statuses[$new_data['status']]['name'];

        $data = [
            'order_id' => $order->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name.$log
        ];
        $orderLog = \App\Models\OrderLog::create($data);
    }

    /**
     * Карточка дня/ночи
     */
    public function turn($length, $id = null)
    {
        //dd($length);
        
        //$turn = \App\Models\OrderDay::findOrfail($id);
        $turn = \App\Models\OrderDay::find($id);
        $rand = rand(1, 5000);
        $returnHTML = view('admin.orders.staff_right', [
            'turn'=> $turn,
            'rand' => $rand,
            //'length' => $length + 1
            'length' => $length
        ])->render();

        $leftHtml = '<tr data-temp_id="'.$rand.'" onclick="turn_view('.$rand.');" data-btn-personal-loadr="" class="current"><td>'.($length+1).'</td><td>День</td><td>'.Carbon::now()->format('d.m.Y').'</td><td></td><td></td></tr>';

        echo json_encode( ['left' => $leftHtml, 'right'=>$returnHTML] );
    }

    //////////////////////////////////////////////////
    /**
     * удалить контакт
     */
    public function delete_contact(Request $request) {
        $order = Order::with('contacts')->where('id', '=', $request->input('order_id'))->first();
        if($order) {
            // Отсоединить одну контакт от заказа
            $order->contacts()->detach($request->input('contact_id'));
            //dd($order->contacts);

            //logs
            $data = [
                'order_id' => $order->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n-  удален контакт ".OrderContact::find($request->input('contact_id'))->name
            ];
            \App\Models\OrderLog::create($data);
        }
    }

    /**
     * удалить order_company
     */
    public function delete_order_company(Request $request) {
        $order = Order::find($request->input('id'));
        $order->order_company_id = null;
        $order->save();
    }

    /**
     * удалить/добавить/изменить период
     */
    public function save_periods(Request $request)
    {
        //dd($request->all());
        //$order = Order::findOrfail($request->input('order_id'));
        $order = Order::find($request->input('order_id'));
        if($order) {
            $periods = $request->input('periods');
            //dd($periods);
            if( count($periods) )
            {
                $date_start = $periods[0]['date_start'];
                $date_finish = $periods[0]['date_finish'];
                foreach ($periods as $key=>$period) {
                    //err
                    if($period['date_start'] > $period['date_finish'])
                        die('Дата начала больше даты окончания');

                    if($period['date_start'] == $period['date_finish'] && $period['time_start'] >= $period['time_finish'])
                        die('Время начала больше времени окончания');

                    if($date_start > $period['date_start'])
                        $date_start = $period['date_start'];

                    if($date_finish < $period['date_finish'])
                        $date_finish = $period['date_finish'];
                }

                $data = [];
                $data['periods'] = \GuzzleHttp\json_encode($request->input('periods'));
                $data['date'] = (new Carbon($date_start))->format('Y.m.d');
                $data['date_finish'] = (new Carbon($date_finish))->format('Y.m.d');

                $order->update($data);

                //logs
                $data = [
                    'order_id' => $order->id,
                    'user_id' => Auth::user()->id,
                    'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- период изменен"
                ];
                $orderLog = \App\Models\OrderLog::create($data);

            }
        }
    }

    public function update_ajax(Request $request) {
        //dd($request->input('field'), $request->input('val'));
        $order = Order::findOrfail($request->input('id'));

        //logs
        $old_val = $order->{$request->input('field')};
        //dd($order->order_company_id);

        $order->update([
            $request->input('field') => $request->input('val')
        ]);

        //logs
        //Event::fire(new onOrderUpdateEvent($order, Auth::user(), $old_data, $new_data, $old_responsibles, $new_responsibles, $old_contacts, $new_contacts));
        //Event::fire(new onOrderUpdateEvent($order, Auth::user(), $old_data, $new_data));
        if($request->input('field') == 'order_company_id') {
            if( $old_val > 0 ) {
                $data = [
                    'order_id' => $order->id,
                    'user_id' => Auth::user()->id,
                    //'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- компания: ".OrderCompany::find($old_val)->name.' -> '.$order->orderCompany->name
                    'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- компания: ".\App\Models\OrderCompany::find($old_val)->name.' -> '.$order->orderCompany->name
                ];
            } else {
                $data = [
                    'order_id' => $order->id,
                    'user_id' => Auth::user()->id,
                    //'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- компания: ".OrderCompany::find($old_val)->name.' -> '.$order->orderCompany->name
                    'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- добавлена компания: ".$order->orderCompany->name
                ];
            }
        } else {
            $data = [
                'order_id' => $order->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n- ".trans("order.".$request->input('field')).' '.$old_val.' -> '.$request->input('val')
            ];
        }

        \App\Models\OrderLog::create($data);
    }

    /**
     * добавить ответственного
     */
    public function attach_user(Request $request) {
        $order = Order::findOrFail($request->input('order_id'));
        $order->responsible_users()->attach($request->input('user_id'));

        //logs
        $data = [
            'order_id' => $order->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n-  добавлен ответственный ".User::find($request->input('user_id'))->name
        ];
        \App\Models\OrderLog::create($data);
    }

    /**
     * удалить ответственного
     */
    public function detach_user(Request $request) {
        $order = Order::findOrFail($request->input('order_id'));
        if($order) {
            // Отсоединить одну контакт от заказа
            $order->responsible_users()->detach($request->input('user_id'));
            //dd($order->contacts);

            //logs
            $data = [
                'order_id' => $order->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n-  удален ответственный ".User::find($request->input('user_id'))->name
            ];
            \App\Models\OrderLog::create($data);
        }
    }

    /**
     * добавить компанию
     */
    public function create_order_company(Request $request) {
        //dd($request->all());
        $order_company = OrderCompany::create([
            'name' => $request->input('name'),
            'user_id' => Auth::user()->id
        ]);

        if( $request->has('order_id') ) {
            $order = Order::findOrFail($request->input('order_id'));

            if($order) {
                $order->update([
                    'order_company_id' => $order_company->id
                ]);

                //подружить компанию с контактами заказа
                if( $request->has('contact_ids') ) {
                    foreach ($request->input('contact_ids') as $contact_id ) {
                        OrderContact::findOrFail($contact_id)->update([
                            'order_company_id' => $order_company->id
                        ]);
                    }

                }
            }
        } /*else {
            $contact = OrderContact::find($request->input('contact_ids')[0]);
            if($contact) {
                $contact->update([
                    'order_company_id' => $order_company->id
                ]);
            }
        }*/

        echo $order_company->id;

    }

    /**
     * ajax-редактирование калькулятора
     */
    public function update_calculator(Request $request)
    {
        $order = Order::findOrFail($request->input('order_id'));

        parse_str($request->input('calculator'), $calculator);
        //dd($calculator);

        //dd(\GuzzleHttp\json_encode($calculator));
        $order->update([
            'calculator' => \GuzzleHttp\json_encode($calculator),
            'amount' => intval($calculator['amount'])
        ]);


        //logs
        $data = $request->all();

        $calculator = [];
        parse_str($data['calculator'], $calculator);

        $calculator_old = [];
        parse_str($data['old_data'], $calculator_old);
        if( sizeof($calculator_old) == 1 )
            $calculator_old = get_object_vars(\GuzzleHttp\json_decode($data['old_data']));
        //dd($calculator_old, $calculator);

        $diff = [];
        foreach ($calculator as $key=>$val) {
            //dump($key);
            if( strpos($key, 'total') !== false || strpos($key, 'amount') !== false) {
                continue;
            }
            else if($key == 'base_info' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $diff[$key] = trans("calculator.".$calculator_old[$key]) . ' -> ' . trans("calculator.".$calculator[$key]);
            }
            else if($key == 'degree' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $degreeData = [0=>1, 5=>2, 10=>3, 15=>4, 20=>5];
                $diff[$key] = $degreeData[$calculator_old[$key]] . ' -> ' . $degreeData[$calculator[$key]];
            }
            else if($key == 'typ' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $typData = [82 => 'ген. уборка', 98 => 'послестрой'];
                $diff[$key] = $typData[$calculator_old[$key]] . ' -> ' . $typData[$calculator[$key]];
            }
            else if($key == 'furniture' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $furnitureData = [0 => 'без мебели',  10 => 'с мебелью'];
                $diff[$key] = $furnitureData[$calculator_old[$key]] . ' -> ' . $furnitureData[$calculator[$key]];
            }
            else if($key == 'ceiling_height' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $ceiling_height_data = [0 => 'меньше 3 м.', 5 => 'больше 3 м.'];
                $diff[$key] = $ceiling_height_data[$calculator_old[$key]] . ' -> ' . $ceiling_height_data[$calculator[$key]];
            }
            else if($key == 'himchistka_kovra' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $himchistka_kovra_data = [0 => 'короткий ворс', 30 => 'длинный ворс'];
                $diff[$key] = $himchistka_kovra_data[$calculator_old[$key]] . ' -> ' . $himchistka_kovra_data[$calculator[$key]];
            }
            else if($key == 'degree_kover' && (@$calculator[$key] != @$calculator_old[$key]) ) {
                $degree_kover_data = [0=>1, 5=>2, 10=>3, 15=>4, 20=>5];
                $diff[$key] = $degree_kover_data[$calculator_old[$key]] . ' -> ' . $degree_kover_data[$calculator[$key]];
            }
            else if( @$calculator[$key] != @$calculator_old[$key] ) {
                if( !is_array($calculator[$key]) )
                    $diff[$key] = $calculator_old[$key] . ' -> ' . $calculator[$key];
                else {
                    foreach ($calculator[$key] as $k=>$v) {
                        if( @$calculator[$key][$k] != @$calculator_old[$key][$k] ) {
                            if(null == @$calculator_old[$key][$k])
                                $diff[$key][] = 'добавлено ' . @$calculator[$key][$k];
                            else
                                $diff[$key][] = @$calculator_old[$key][$k] . ' -> ' . @$calculator[$key][$k];
                        }
                    }
                }

                //$diff[$key] = $key;
            }
        }
        //dd($diff);
        if(sizeof($diff) > 0) {
            $changed = true;
            $log_detail = '';

            foreach($diff as $key => $val)
            {
                if( !is_array($val) ) {
                    if($key == "payment") {
                        $old_pay = (intval(@$calculator[$key]) > 0) ? "безнал" : "наличные";
                        $new_pay = (intval(@$calculator[$key]) > 0) ? "безнал" : "наличные";
                        $log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": ".$old_pay." - ".$new_pay;
                    }
                    else {
                        //$log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": " . $calculator_old[$key]." - ".$calculator[$key];
                        $log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": " . $val;
                    }
                }
                elseif( is_array($val) ) {
                    foreach ($val as $key1=>$val1) {
                        $log_detail = $log_detail . "\r\n - " . trans("calculator." . $key) . ": " . $val1;
                    }
                    //dump($val);
                }
            }

        }

        if( @$changed === true ) {
            //Event::fire(new onOrderUpdateEvent($event->order, Auth::user()));

            $data = [
                'order_id' => $order->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name.$log_detail
            ];
            $orderLog = \App\Models\OrderLog::create($data);
        }
    }
}
