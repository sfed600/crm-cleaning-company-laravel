<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use App\Models\Tabel;
use App\Models\Company;
use Auth;

class TabelController extends Controller
{
    public function get_period_dates($period) {
        switch ($period) {
            case 'today':
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::today()->format('d.m.Y'),
                    'period_end' => Carbon::today()->format('d.m.Y')
                ]);
                break;
            case 'yesterday':
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::yesterday()->format('d.m.Y'),
                    'period_end' => Carbon::yesterday()->format('d.m.Y')
                ]);
                break;
            case 'current_week':
                //$period = Carbon::now()->startOfWeek()->format('d.m.Y').' - '.Carbon::now()->endOfWeek()->format('d.m.Y');
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::now()->startOfWeek()->format('d.m.Y'),
                    'period_end' => Carbon::now()->endOfWeek()->format('d.m.Y')
                ]);
                break;
            case 'last_week':
                //$period = Carbon::today()->subWeek()->startOfWeek()->format('d.m.Y').' - '.Carbon::today()->subWeek()->endOfWeek()->format('d.m.Y');
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::today()->subWeek()->startOfWeek()->format('d.m.Y'),
                    'period_end' => Carbon::today()->subWeek()->endOfWeek()->format('d.m.Y')
                ]);
                break;
            case 'current_month':
                //$period = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::now()->startOfMonth()->format('d.m.Y'),
                    'period_end' => Carbon::now()->endOfMonth()->format('d.m.Y')
                ]);
                break;
            case 'last_month':
                //$period = Carbon::today()->subMonth()->startOfMonth()->format('d.m.Y').' - '.Carbon::today()->subMonth()->endOfMonth()->format('d.m.Y');
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::today()->subMonth()->startOfMonth()->format('d.m.Y'),
                    'period_end' => Carbon::today()->subMonth()->endOfMonth()->format('d.m.Y')
                ]);
                break;
            default:
                //$period = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
                echo \GuzzleHttp\json_encode([
                    'period_start' => Carbon::now()->startOfMonth()->format('d.m.Y'),
                    'period_end' => Carbon::now()->endOfMonth()->format('d.m.Y')
                ]);
        }
    }

    public function exclude_from_filter($filter) {
        session()->forget("admin.tabel.filters.$filter");
        return redirect()->route('admin.tabel');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->group()->first()->name == 'User') {
            return redirect()->route('admin.users.show', Auth::user()->id);
        }
        $this->authorize('index', new Tabel());
        //компании
        $companies = Company::orderBy('name')->get();

        $carbon = new Carbon();
        
        $filters = $this->getFormFilter($request->input());
        //dd(@$filters['period']);
        //dd(session()->get('admin'));
        // начало/конец периода фильтра
        $period = null;
        if( isset($filters['period']) ) {
            switch ($filters['period']) {
                case 'today':
                    $period = Carbon::today()->format('d.m.Y');
                    break;
                case 'yesterday':
                    $period = Carbon::yesterday()->format('d.m.Y');
                    break;
                case 'current_week':
                    $period = Carbon::now()->startOfWeek()->format('d.m.Y').' - '.Carbon::now()->endOfWeek()->format('d.m.Y');
                    break;
                case 'last_week':
                    $period = Carbon::today()->subWeek()->startOfWeek()->format('d.m.Y').' - '.Carbon::today()->subWeek()->endOfWeek()->format('d.m.Y');
                    break;
                case 'current_month':
                    $period = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
                    break;
                case 'last_month':
                    $period = Carbon::today()->subMonth()->startOfMonth()->format('d.m.Y').' - '.Carbon::today()->subMonth()->endOfMonth()->format('d.m.Y');
                    break;
                default:
                    $period = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
            }
        }

        //доступ к компаниям
        if (Auth::user()->can('company', new Tabel())) {
            if (empty($filters['company']) || !$filters['company']) {
                $filters['company'] = $companies->first()->id;
            }
        } else {
            $filters['company'] = Auth::user()->company_id;
        }

        if(empty($filters['date'])) {
            $filters['date'] = $carbon->format('m.Y');
        }
        if(empty($filters['daterange'])) {
            $filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
        }

        //кол-во дней в месяце
        $cnt_days = $carbon::createFromFormat('d.m.Y','01.'.$filters['date'])->daysInMonth;
        $days = [];
        $weeks = ['Mon' => 'Пн', 'Tue' => 'Вт', 'Wed' => 'Ср', 'Thu' => 'Чт', 'Fri' => 'Пт', 'Sat' => 'Сб', 'Sun' => 'Вс'];
        for($i = 1; $i<= $cnt_days; $i++) {
            $week_name = $carbon::createFromFormat('d.m.Y', $i.'.'.$filters['date'])->format('D');
            $days[$i]['name'] = $weeks[$week_name];
            $days[$i]['weekday'] = (in_array($week_name, ['Sat', 'Sun']) ? 0 : 1);
            $days[$i]['date'] = $carbon::createFromFormat('d.m.Y', $i.'.'.$filters['date'])->format('Y-m-d');
        }

        //filters
        $where = '1';
        $innerJoin = '';
        if (!empty($filters) && !empty($filters['company'])) {
            $where .= ' AND o.company_id = '.$filters['company'];
        }
        if (!empty($filters) && !empty($filters['profession'])) {
            $where .= ' AND exists (select * from `user_professions` inner join `user_profession` on `user_professions`.`id` = `user_profession`.`profession_id` where `user_profession`.`user_id` = `u`.`id` and `profession_id` = \''.$filters['profession'].'\')';
        }
        if (!empty($filters) && !empty($filters['surname'])) {
            $where .= ' AND u.surname = \''.$filters['surname'].'\'';
        }
        if (!empty($filters) && !empty($filters['phone'])) {
            $number = substr(preg_replace('|[^0-9]+|','', $filters['phone']), -10, 10);
            $innerJoin .= 'INNER JOIN user_phones as uph ON uph.user_id = u.id AND uph.number = \''.$number.'\' ';
        } else {
            //$innerJoin .= 'LEFT JOIN user_phones as uph ON uph.user_id = u.id ';
            $innerJoin .= 'LEFT JOIN (SELECT MAX(`phone`) as phone1, user_id
                            FROM `user_phones`
                            GROUP BY `user_id`) as uph1 ON uph1.user_id = u.id  
                            
                            LEFT JOIN (SELECT MIN(`phone`) as phone2, user_id
                            FROM `user_phones`
                            GROUP BY `user_id`) as uph2 ON uph2.user_id = u.id ';
        }

        //period
        $arPeriod = explode('-', $filters['daterange']);
        $period_start = $carbon::createFromFormat('d.m.Y', trim($arPeriod[0]))->format('Y-m-d');
        $period_end = $carbon::createFromFormat('d.m.Y', trim($arPeriod[1]))->format('Y-m-d');
        /*$period_start = date('Y-m-d', strtotime("2018-04-01"));
        $period_end = date('Y-m-d', strtotime("2018-04-30"));*/

        $order_by_name = 'u.surname';
        $order_by_smen = null;
        $order_by_income = null;
        $order_by_ostatok = null;
        $order_by_date = null;
        $order_date = null;
        //if ( isset($filters['order']) && $filters['order'] != '' && strpos($filters['order'], 'date') == false ) {
        if ( isset($filters['order']) && $filters['order'] != '' ) {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            //dump($filters['order']); dd();
            if (strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();

            switch ($arSort[0]) {
                case 'name':
                    $order_by_name = 'u.surname ' . strtoupper($arSort[1]);
                    break;
                case 'smen':
                    $order_by_smen = strtoupper($arSort[1]);
                    break;
                case 'income':
                    $order_by_income = strtoupper($arSort[1]);
                    break;
                case 'ostatok':
                    $order_by_ostatok = strtoupper($arSort[1]);
                    break;
                case 'date':
                    $order_date = $filters['order_date'];
                    $order_by_date = strtoupper($arSort[1]);
                    break;
            }
        }
        //dump($innerJoin);
        $sql = 'SELECT DISTINCT d.date, ou.day_id, ou.user_id, ou.amount, ou.cef, u.surname, u.name, u.patronicname, uph1.phone1, uph2.phone2, o.number, u.deleted_at, d.nigth, o.id as order_id
                FROM order_days as d
                  INNER JOIN order_users as ou ON d.id = ou.day_id
                  INNER JOIN users as u ON ou.user_id = u.id
                  INNER JOIN orders as o ON d.order_id = o.id 
                  '.$innerJoin.'                 
                WHERE '.$where.' AND d.date >= \''.$period_start.'\' AND
                      d.date <= \''.$period_end.'\'
                      AND o.deleted_at is null
                ORDER BY '.$order_by_name;

        if( $order_by_date && $order_date ) {
            $sql = "
                SELECT d.date, ou.*, u.surname, u.name, u.patronicname, o.number, u.deleted_at, q.sum_cef, uph.phone, d.nigth, o.id as order_id
                FROM order_days as d
                  INNER JOIN order_users as ou ON d.id = ou.day_id
                  INNER JOIN users as u ON ou.user_id = u.id
                  INNER JOIN orders as o ON d.order_id = o.id '.$innerJoin.'
                  LEFT JOIN (SELECT `user_id`, sum(`cef`) as sum_cef
                    FROM `order_users` 
                    INNER JOIN order_days ON order_days.id = order_users.day_id
                    WHERE order_days.date = '".$order_date."'
                    GROUP BY `user_id`) q ON q.user_id = ou.user_id                  
                WHERE ".$where." AND d.date >= '".$period_start."' AND
                      d.date <= '".$period_end."'
                      AND o.deleted_at is null
                ORDER BY q.sum_cef ".$order_by_date;
        }

        $total_smen = 0;
        $total_income = 0;
        $total_outcome = 0;
        $dates = DB::select($sql);
        //dd($sql);
        //dump($dates);
        if(count($dates)) {
            $users = [];
            foreach($dates as $date) {
                $day = $carbon::createFromFormat('Y-m-d', $date->date)->format('j');
                $users[$date->user_id]['user'] = $date;

                $users[$date->user_id]['phones'] = \App\User::find($date->user_id)->phones;

                if(empty($users[$date->user_id]['sum'])) {
                    $users[$date->user_id]['sum'] = 0;
                }
                $users[$date->user_id]['sum'] += $date->cef;
                $users[$date->user_id]['days'][$day]['items'][] = $date;
                if(empty($users[$date->user_id]['days'][$day]['sum_cef'])) {
                    $users[$date->user_id]['days'][$day]['sum_cef'] = 0;
                    //кол-во работников в день
                    $days[$day]['cnt'] = (empty($days[$day]['cnt']) ? 1 : $days[$day]['cnt']+1);
                }
                $users[$date->user_id]['days'][$day]['sum_cef'] += $date->cef;
            }

            $incomes = \App\Models\UserIncome::whereIn('user_id', array_keys($users))
                ->where('date', '>=', $carbon::createFromFormat('d.m.Y', '01.'.$filters['date'])->format('Y-m-d'))
                ->where('date', '<=', $carbon::createFromFormat('d.m.Y', $cnt_days.'.'.$filters['date'])->format('Y-m-d'));

            if (!empty($filters) && !empty($filters['company'])) {
                $incomes->where('company_id', $filters['company']);
            }
            $incomes = $incomes->orderBy('date', 'desc')->get();

            //pagination
            $total_users = count($users);
            if($filters['perpage'] < $total_users) {
                $page = (isset($filters['page'])) ? $filters['page'] : 0;
                $users = array_slice($users, $page, $filters['perpage'], true);
            }

            foreach($users as $user_id => $user)
            {
                $users[$user_id]['income'] = 0;
                $users[$user_id]['outcome'] = 0;
                //проверям достаточно ли минимальной даты выплаты в этом месяце для данного сотрудника
                $date_min = $incomes->where('user_id', $user_id)->filter( function ($value, $key) {return  $value->outcome == 0; })->min('date');
                if(empty($date_min) || strtotime(key($user['days']).'.'.$filters['date']) < strtotime($date_min)) {
                    //ищем самую высокую выплату ниже текущего месяца
                    $income = \App\Models\UserIncome::where('user_id', $user_id)->where('outcome', 0)
                        ->where('date', '<', $carbon::createFromFormat('d.m.Y', '01.'.$filters['date'])->format('Y-m-d'));
                    if (!empty($filters) && !empty($filters['company'])) {
                        $income->where('company_id', $filters['company']);
                    }
                    $income = $income->orderBy('date', 'desc')->first();
                    if(!empty($income)) {
                        $incomes->prepend($income);
                    }
                }

                $income_n = null;
                foreach($user['days'] as $day_n => $day) {
                    foreach($incomes->where('user_id', $user_id)->filter( function ($value, $key) {return $value->amount > 0; }) as $income) {
                        if(strtotime($income->datePicker()) <= strtotime($day_n.'.'.$filters['date'])) {
                            $income_n = $income;
                        }
                    }

                    $sum_income = 0;
                    foreach($day['items'] as $day_item) {
                        $sum_income += $day_item->amount;
                    }
                    //$sum_income = $day['sum_cef'] * $income_n->amount;
                    $users[$user_id]['days'][$day_n]['income'] = $sum_income;
                    $users[$user_id]['income'] += $sum_income;
                    $total_income += $sum_income;

                }

                //суммируем выплаты
                foreach($incomes->where('user_id', $user_id)->filter( function ($value, $key) use ($filters) {return $value->outcome > 0 && strtotime($value->date) >= strtotime('01.'.$filters['date']); }) as $income) {
                    $users[$user_id]['outcome'] += $income->outcome;
                    $total_outcome += $income->outcome;
                }

                //суммируем смены
                $total_smen = $total_smen + intval($users[$user_id]['sum']);
            }
        }
        $professions = \App\Models\UserProfession::orderBy('name')->get();

        //dump($days); dd();
        if( $order_by_smen ) {
            foreach($users as $user_id => $user) {
                $sum[$user_id]  = $user['sum'];
            }

            if( $order_by_smen == 'DESC' )
                array_multisort($sum, SORT_DESC, $users);
            else
                array_multisort($sum, SORT_ASC, $users);
        }

        if( $order_by_income ) {
            foreach($users as $user_id => $user) {
                $ar_income[$user_id]  = $user['income'];
            }
            //dump($ar_income); dd();
            if( $order_by_income == 'DESC' )
                array_multisort($ar_income, SORT_DESC, $users);
            else
                array_multisort($ar_income, SORT_ASC, $users);
        }

        if( $order_by_ostatok ) {
            foreach($users as $user_id => $user) {
                $ostatok[$user_id]  = $user['income'] - $user['outcome'];
            }
            //dump($ar_income); dd();
            if( $order_by_ostatok == 'DESC' )
                array_multisort($ostatok, SORT_DESC, $users);
            else
                array_multisort($ostatok, SORT_ASC, $users);
        }
        //dump($users); dd();
        return view('admin.tabel.2index', [
            'filters' => $filters,
            'professions' => $professions,
            'days' => $days,
            'users' => (!empty($users) ? $users : null),
            'total_users' => (isset($total_users)) ? $total_users : (isset($users)) ? count($users) : 0,
            'total_smen' => $total_smen,
            'total_income' => $total_income,
            'total_outcome' => $total_outcome,
            'companies' => $companies,
            'aliases' => [
                'today' => 'сегодня',
                'current_week' => 'текущая неделя',
                'last_week' => 'прошлая неделя',
                'current_month' => 'текущий месяц',
                'last_month' => 'прошлый месяц'
            ],
            'period' => $period,
            //'daterange' => $daterange,
            'period_start' => $arPeriod[0],
            'period_end' => $arPeriod[1],
        ]);
    }
}
