<?php

namespace App\Http\Controllers\admin;

use App\Models\OrderDay;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderDayController extends Controller
{
    public function destroy($id)
    {
        OrderDay::destroy($id);

        //return redirect()->route('admin.invoices.index')->withMessage('Удалено');
    }
}
