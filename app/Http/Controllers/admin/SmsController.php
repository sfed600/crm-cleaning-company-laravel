<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sms;
use Auth;

class SmsController extends Controller
{
    public function __construct() {
        $this->authorize('index', new Sms());
    }

    public function index(Request $request)
    {
        $sms = Sms::orderBy('id', 'desc');
        //только смс заказов твоей компании
        if (Auth::user()->cannot('company', new \App\Models\Order())) {
            $sms->whereHas('order', function ($query) {
                $query->where('company_id', Auth::user()->company_id);
            });
        }


        $sms = $sms->with('users')->paginate(10);

        return view('admin.sms.index', ['sms' => $sms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->can('company', new \App\Models\Order())) {
            $sms = \App\Models\Sms::findOrFail($id);
            //только смс заказов твоей компании
        } else {
            $sms = \App\Models\Sms::whereHas('order', function ($query) {
                $query->where('company_id', Auth::user()->company_id);
            })->findOrFail($id);

        }


        return view('admin.sms.show', ['sms' => $sms]);
    }

}
