<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Company;
use Auth;

class CompanyController extends Controller
{
    public function __construct() {
        //$this->authorize('index', new Company());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( !$this->authorize('index', new Company()) )
            return redirect()->back(403);
        
        $filters = $this->getFormFilter($request->input());

        $companies = Company::orderBy('id', 'desc');
        if (!empty($filters) && !empty($filters['name'])) {
            $companies->where('name', 'LIKE', '%'.$filters['name'].'%');
        }
        if (!empty($filters) && !empty($filters['inn'])) {
            $companies->where('inn', 'LIKE', '%'.$filters['inn'].'%');
        }
        $companies = $companies->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        return view('admin.companies.index', ['companies' => $companies, 'filters' => $filters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( !$this->authorize('index', new Company()) )
            return redirect()->back(403);

        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\CompanyInsertRequest $request)
    {
        if( !$this->authorize('index', new Company()) )
            return redirect()->back(403);

        $data = $request->all();
        $company = Company::create(['name' => $data['company'], 'inn' => $data['inn']]);

        $group = \App\Models\UserGroup::where('Name', 'Admin')->first();

        $data['company_id'] = $company->id;
        $data['group_id'] = $group->id;
        $data['status'] = 1;
        if($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }
        $user = \App\User::create($data);

        //связь с телефонами
        if($request->has('phones')) {
            foreach($request->input('phones') as  $phone) {
                $number = substr(preg_replace('|[^0-9]+|','',$phone), -10, 10);
                $user->phones()->create(['phone' => $phone, 'number' => $number]);
            }
        }

        return redirect()->route('admin.companies.index')->withMessge('Компания и Администратор добавлены');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        /*if( !$this->authorize('update', new Company()) )
            return redirect()->back(403);*/
        if( !$this->authorize('index', new Company()) )
            return redirect()->back(403);

        $company = Company::find($id);
        return view('admin.companies.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\CompanyRequest $request, $id)
    {
        //dd($request->file('logo'));
        //dd($_FILES);
        if ( !$request->user()->can('index', new Company()) ) {
            if (!$this->authorize('update', new Company()) OR $request->user()->company_id != $id)
                return redirect()->back(403);
        }

        $data = $request->all();
        //$data = $request->except('logo');
        $company = Company::findOrFail($id);

        $arImgs = Company::saveImg($request);
        if( !empty($arImgs) ) {
            if(isset($arImgs['stamp'])) {
                $data['stamp'] = $arImgs['stamp'];
                $company->deleteImg('stamp');                
            }
            if(isset($arImgs['signature'])) {
                $data['signature'] = $arImgs['signature'];
                $company->deleteImg('signature');
            }
            if(isset($arImgs['logo'])) {
                //unset($data['logo']);
                $data['logo'] = $arImgs['logo'];
                $company->deleteImg('logo');
            }
        } else {
            if($request->file('logo') || $request->file('stamp') || $request->file('signature'))
                return redirect()->back()->withErrors(['Файл слишком большой!'])->withInput();
        }

        $company->update($data);

        //return redirect()->route('admin.companies.index')->withMessge('Компания изменена');
        return redirect()->back()->withMessge('Компания изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::destroy($id);
        return redirect()->route('admin.companies.index')->withMessage('Компания удалена');
    }
    
    //
    public function fill_by_inn(Request $request) {
        $dataCompany = Company::getCompanyDataByInn($request->inn, 'ajax');
        //dd($dataCompany);
        echo json_encode($dataCompany);
    }

    /**
     * search autocomplete
     */
    public function search(Request $request) {
        $data =[];
        if($request->has('term') && $request->input('term')) {
            if(Auth::user()->company) {
                /*$companies = Company::where('name', 'LIKE', $request->input('term') . '%')
                    ->where('id', Auth::user()->company_id)
                    ->orderBy('name')->take(10)->get();*/
                $companies = Company::where('id', Auth::user()->company_id)
                    ->where('name', 'LIKE', $request->input('term') . '%')
                    ->orderBy('name')->take(10)->get();
            } else {
                $companies = Company::where('name', 'LIKE', $request->input('term') . '%')
                    ->orderBy('name')->take(10)->get();
            }

            //$companies = $companies->orderBy('name')->take(10)->get();

            foreach ($companies as $company) {
                $item = [
                    'id' => $company->id,
                    'name' => $company->name,
                    'site' => $company->site,
                    'phone' => $company->phone,
                    'email' => $company->email,
                ];
                $data[] = $item;
            }
        }

        return response()->json($data);
    }

}
