<?php

namespace App\Http\Controllers\admin;

use App\Models\Company;
use App\Models\Price;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Auth;
use DB;

class SettingController extends Controller
{
    public function __construct() {
        $this->authorize('index', new Setting());
        $this->authorize('index', new \App\Models\UserProfession());
    }

    //set company price
    public function price(Request $request)
    {
        $this->authorize('price', new Setting());

        $params = array();
        parse_str($request->input('data'), $params);
        unset($params['_token']);
        unset($params['_method']);
        //dump($params); dd();

        $price = \App\Models\Price::updateOrCreate(
            ['company_id' => $request->user()->company_id],
            [
                'company_id' => $request->user()->company_id,
                'price' => \GuzzleHttp\json_encode($params["price"])
            ]
        );

        if($price)
            echo 'success';
        /*else
            echo 'error';*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters = $this->getFormFilter($request->input());

        $settings = Setting::orderBy('id', 'desc');
        if (!empty($filters) && !empty($filters['var'])) {
            $settings->where('var', 'LIKE', '%'.$filters['var'].'%');
        }
        $settings = $settings->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);
        //$company = \App\Models\Company::find($request->user()->company_id);

        $sql = "
        SELECT companies.*, `users`.`name` as user_name, `users`.`surname` as user_surname, `users`.`patronicname` as user_patronicname, `users`.`img` as user_avatar 
        FROM `companies` 
        LEFT JOIN `users`
          ON companies.user_id = users.id
        WHERE companies.id = ".Auth::user()->company_id;

        $query = \DB::select($sql);
        $collection = new \Illuminate\Support\Collection($query);
        //dd($collection[0]);
        $company = $collection[0];

        $prices = [];
        $price = Price::where('company_id', '=', $company->id)->first();
        if($price) {
            $arPrice = \GuzzleHttp\json_decode($price->price);
            //var_dump(get_object_vars($arPrice)); dd();
            $prices = get_object_vars($arPrice);
        }

        $professions = \App\Models\UserProfession::where('company_id', '=', Auth::user()->company_id)
            ->orWhere('company_id', null)
            ->orderBy('name')->get();

        return view('admin.settings.index', [
            'settings' => $settings,
            'filters' => $filters,
            'company' => $company,
            'prices' => $prices,
            'professions' => $professions,
            'request' => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //доступ на добавление
        $this->authorize('add', new Setting());

        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\SettingsRequest $request)
    {
        //доступ на добавление
        $this->authorize('add', new Setting());

        $data = $request->input();
        if($request->input('type') == 'array') {
            $data['value'] = json_encode(array_combine($request->input('keys'), $request->input('values')));
        }
        Setting::create($data);
        return redirect()->route('admin.settings.index')->withMessage('Настройка добавлена');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::find($id);
        $setting->getVarArray();
        return view('admin.settings.edit', ['setting' => $setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\SettingsRequest $request, $id)
    {
        $data = $request->input();
        if($request->input('type') == 'array') {
            $data['value'] = json_encode(array_combine($request->input('keys'), $request->input('values')));
        }

        $setting = Setting::findOrFail($id);
        //проверка прав на изменение маршрута
        if($request->user()->can('var', $setting)) {
            $setting->update($data);
        } else {
            $setting->update(array_except($data, ['var']));
        }
        return redirect()->route('admin.settings.index')->withMessage('Настройка изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //доступ на удаление
        $this->authorize('delete', new Setting());
        Setting::destroy($id);
        return redirect()->route('admin.settings.index')->withMessage('Настройка удалена');
    }
}

