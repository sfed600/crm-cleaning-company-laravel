<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OrderContact;
use App\Models\OrderContactEmail;
use App\Models\OrderContactPhone;
use App\Models\OrderCompany;
use App\Models\Order;
use DB;
use Auth;
use Event;
use App\Events\onOrderContactAddEvent;
use App\Events\onOrderContactUpdateEvent;

class OrderContactController extends Controller
{
    public function __construct() {
        $this->authorize('index', new \App\Models\OrderContact());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $filters = $this->getFormFilter($request->input());
        //$contacts = OrderContact::orderBy('id', 'desc');

        //sort /*
        if ( isset($filters['order']) && $filters['order']!='')
        {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();
            //dump($filters); dd();
            switch ($arSort[0]) {
                case 'name':
                    $contacts = OrderContact::orderBy('name', $arSort[1]);
                    break;
                case 'company':
                    $contacts = OrderContact::select('contacts.*')
                        ->leftJoin(
                            DB::raw("(SELECT `id`, `name` FROM `order_companies`) `zz`"), 'zz.id', '=', 'order_contacts.order_company_id'
                        )
                        ->orderByRaw("zz.name $arSort[1]");
                        //->toSql();
                    //dump($contacts); dd();
                    //$contacts = OrderContact::orderBy('id', 'desc');
                    break;
                case 'responsible':
                    $contacts = OrderContact::select('order_contacts.*')
                        ->leftJoin(
                            DB::raw("(SELECT `id`, `surname` FROM `users`) `zz`"), 'zz.id', '=', 'order_contacts.user_id'
                        )
                        ->orderByRaw("zz.surname $arSort[1]");
                    //->toSql();
                    //dump($contacts); dd();
                    break;
                case 'created':
                    $contacts = OrderContact::orderBy('created_at', $arSort[1]);
                    break;
            }
        }
        else
            $contacts = OrderContact::orderBy('id', 'desc');
        //sort */

        //доступ к компаниям
        if( Auth::user()->company != null) {
            $contacts->whereIn('user_id', function($query) {
                $query->select(DB::raw('id'))
                    ->from('users')
                    ->whereRaw("users.company_id = ".Auth::user()->company->id);
            })->orWhere('user_id', Auth::user()->id);
        }
        else
            return redirect()->back();

        if (!empty($filters) && !empty($filters['name'])) {
            $contacts->where('name', 'LIKE', '%'.$filters['name'].'%');
        }
        if (!empty($filters) && !empty($filters['company_id'])) {
            $contacts->where('order_company_id', $filters['company_id']);
        }

        $total_contacts_count = $contacts->count();

        $contacts = $contacts->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

        //всего количество/сумма
        $count_contacts = $contacts->count();
        //$sum_contacts = $contacts->orders->sum('amount');

        $sum_paginate = 0;
        foreach ($contacts as $contact) {
            //dump($orders);
            $sum_paginate = $sum_paginate + $contact->orders->sum('amount');
        }
        
        $companies = OrderCompany::orderBy('name')->get();

        return view('admin.order_contacts.index', [
            'contacts' => $contacts, 
            'filters' => $filters, 
            'companies' => $companies,
            'count_contacts' => $count_contacts,
            //'sum_contacts' => $sum_contacts,
            'sum_paginate' => $sum_paginate,
            'total_contacts_count' => $total_contacts_count
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //$companies = OrderCompany::orderBy('name')->get();
        $companies = OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();

        $order_company_id = 0;
        if($request->has('order_company_id') ) {
            $order_company_id = intval($request->input('order_company_id'));
        }
        
        return view('admin.order_contacts.create', [
            'companies' => $companies,
            'order_company_id' => $order_company_id,
            'request' => $request
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\OrderContactRequest $request)
    {
        $data = $request->all();
        //dd($data);
        
        $data['order_company_id'] = $request->input('order_company_id') ? $request->input('order_company_id') : null;

        //photo
        $arImgs = OrderContact::saveImg($request);
        if( !empty($arImgs) ) {
            if(isset($arImgs['photo'])) {
                $data['photo'] = $arImgs['photo'];
                //$contact->deleteImg();
            }
        } else {
            if($request->file('photo') )
                return redirect()->back()->withErrors(['Файл слишком большой!'])->withInput();
        }

        $contact = OrderContact::create($data);

        //logs
        Event::fire(new onOrderContactAddEvent($contact, Auth::user()));

        //связь с телефонами
        if($request->has('phones')) {
            foreach($request->input('phones') as $key => $phone) {
                if( !empty($phone) ) {
                    //$type = $request->input('types.'.$key);
                    $type = $key;
                    $contact->phones()->create(['phone' => $phone, 'type' => $type]);
                }
            }
        }

        //связь с мыльниками
        if($request->has('emails')) {
            foreach($request->input('emails') as $email) {
                $contact->emails()->create(['email' => $email]);
            }
        }

        if($request->has('route') && $request->input('route')=='order') {
            return redirect()->route('admin.orders.create', [
                'contact_id' => $contact->id,
                //'name' => @$request->input('order_name'),
                'order_name' => @$request->input('order_name'),
                'amount' => @$request->input('amount')
            ])->withMessge('Контакт добавлен');
        } else {
            if($request->ajax()) {
                if($request->has('order_id') && $request->input('order_id') > 0 ) {
                    //$order = Order::findOrfail($request->input('order_id'))->with('contacts')->first();
                    $order = Order::findOrfail($request->input('order_id'));
                    //dd($request->input('id'));
                    
                    //связь с контактами
                    //dd($order->contacts);
                    $order->contacts()->attach($contact->id);
                }

                echo $contact->id;
            }
            else
                return redirect()->route('admin.order_contacts.index')->withMessge('Контакт добавлен');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = OrderContact::find($id);
        //$companies = OrderCompany::orderBy('name')->get();
        $companies = OrderCompany::whereIn('user_id', function($query) {
            $query->select(DB::raw('id'))
                ->from('users')
                ->whereRaw("users.company_id = ".Auth::user()->company_id);
        })->orWhere('user_id', Auth::user()->id)
            ->orderBy('name', 'asc')
            ->get();

        $orders = Order::join('order_contact', 'orders.id', '=', 'order_contact.order_id')
            ->select('orders.*')
            //->getQuery() // Optional: array format
            ->where('order_contact.contact_id', $id)
            ->orderBy('id', 'desc')
            ->get();
        //dump($orders); dd();

        $offers = \App\Models\Offer::join('offer_contact', 'offers.id', '=', 'offer_contact.offer_id')
            ->select('offers.*')
            //->getQuery() // Optional: array format
            ->where('offer_contact.contact_id', $id)
            ->orderBy('id', 'desc')
            ->get();

        //invoices
        $invoices = \App\Models\Invoice::whereIn('order_id', function($query) use ($id) {
            $query->select('order_id')
                ->from('order_contact')
                ->where('contact_id', $id);
        })->get();
        //dd($invoices);

        //logs
        $sql = "
            SELECT date_format(created_at, '%d.%m.%Y') as created_at, `user_id`, `note`, YEAR(created_at) as _year, MONTH(created_at) as _month
            FROM order_contact_logs
            WHERE `contact_id` = $contact->id
            GROUP BY YEAR(created_at) ASC, MONTH(created_at) ASC, created_at ASC, order_contact_logs.user_id, order_contact_logs.note
        ";

        $logs = DB::select($sql);
        $periods = [];
        foreach ($logs as $key=>$val)
            $periods[] = ['year' => $val->_year, 'month' => $val->_month];
        //dd(array_unique($periods, SORT_REGULAR));
        $periods = array_unique($periods, SORT_REGULAR);

        return view('admin.order_contacts.edit', [
            'contact' => $contact, 
            'companies' => $companies,
            'orders' => $orders,
            'offers' => $offers,
            'invoices' => $invoices,
            'logs' => $logs,
            'periods' => $periods
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\OrderCompanyRequest $request, $id)
    {
        $data = $request->all();
        //dd($data);
        //$contact = OrderContact::findOrFail($id);
        //$contact = OrderContact::with(['phones', 'emails', 'company', 'user'])->where('id','=', $id)->get();
        $contact = OrderContact::with(['phones', 'emails', 'company', 'user'])->get()->find($id);
        
        //photo
        $arImgs = OrderContact::saveImg($request);
        //dump($arImgs); dd();
        if( !empty($arImgs) ) {
            if(isset($arImgs['photo'])) {
                $data['photo'] = $arImgs['photo'];
                $contact->deleteImg();
            }
        } else {
            if($request->file('photo') )
                return redirect()->back()->withErrors(['Файл слишком большой!'])->withInput();
        }
        
        $data['order_company_id'] = $request->input('order_company_id') ? $request->input('order_company_id') : null;

        //для логов
        $old_data = $contact->getOriginal();

        $contact->update($data);

        //$relations_log = "\r\n- ";
        $relations_log = "";

        //связь с телефонами
        //$contact->phones()->whereNotIn('id', $request->input('phone_ids'))->delete();
        $contact->phones()->delete();
        if($request->has('phones')) {
            foreach($request->input('phones') as $key => $phone) {
                $type = $key;

                if(!empty($phone))
                    $contact->phones()->create(['phone' => $phone, 'type' => $type]);

                //logs
                if($request->input('old_phone')[$key] != $phone) {
                    $relations_log = $relations_log."\r\n - телефон: ".$request->input('old_phone')[$key].' -> '.$phone;
                }

            }
        }

        //связь с E-mail
        $contact->emails()->whereNotIn('id', $request->input('email_ids'))->delete();
        /*if($request->has('emails')) {
            foreach($request->input('emails') as $key => $email) {
                if($request->input('email_ids')[$key]) {
                    $contact->emails()->find($request->input('email_ids')[$key])->update(['email' => $email]);
                } else {
                    $contact->emails()->create(['email' => $email]);
                }
            }
        }*/
        $email = @$contact->emails()->first()->email;
        $contact->emails()->delete();
        if( $email != $request->input('email') ) {
            if( !empty($request->input('email')) )
                $contact->emails()->create(['email' => $request->input('email')]);

            //logs
            $relations_log = $relations_log."\r\n - email: ".$email.' -> '.$request->input('email');
        }


        //logs
        $new_data = $contact->getAttributes();
        Event::fire(new onOrderContactUpdateEvent($contact, Auth::user(), $old_data, $new_data, $relations_log));

        //return redirect()->route('admin.order_contacts.index')->withMessge('Контакт изменен');
        return redirect()->back()->withMessge('Контакт изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //public function destroy(Request $request, $id)
    public function destroy($id)
    {
        //dd($request->all());
        //OrderContact::destroy($request->input('id'));
        OrderContact::destroy($id);

        return redirect()->route('admin.order_contacts.index')->withMessage('Контакт удален');
    }

    /**
     * Поиск по пользователям в автоподстановке
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request) {
        $data =[];
        if($request->has('term') && $request->input('term')) {
            //$contacts = OrderContact::where('name', 'LIKE', $request->input('term') . '%');
            $contacts = OrderContact::with('phones', 'emails')->where('order_contacts.name', 'LIKE', $request->input('term') . '%');
            $contacts = $contacts->orderBy('name')->take(10)->get();

            foreach ($contacts as $contact) {
                //$item = ['id' => $contact->id, 'name' => $contact->name];
                $item = [
                    'id' => $contact->id,
                    'name' => $contact->name,
                    'post' => $contact->post,
                    //'phone' => $contact->phones[0]->phone,
                    'tel' => @$contact->phones()->where('type', 'work')->first()->phone,
                    'mobile' => @$contact->phones()->where('type', '=', 'mobile')->first()->phone,
                    'email' => @$contact->emails[0]->email,
                ];
                $data[] = $item;
            }
        }

        return response()->json($data);
    }

    public function get_order_company($contact_id) {
        $order_contact = OrderContact::find($contact_id);
        //dump($order_contact->order_company_id); dd();
        return $order_contact->order_company_id;

    }

    public function attach_to_order(Request $request) {
        //dd($request->all());
        $order = Order::findOrFail($request->input('order_id'));
        $order->contacts()->attach($request->input('contact_id'));

        //logs
        $data = [
            'order_id' => $order->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$order->name."\r\n-  добавлен контакт ".OrderContact::find($request->input('contact_id'))->name
        ];
        \App\Models\OrderLog::create($data);
    }

    /**
     * update ajax query from order
     */
    public function update_ajax(Request $request) {
        //dd( isset($request->all()["file"]) );
        //dd( $request->file("photo") );
        $contact = OrderContact::findOrfail($request->input('id'));
        $log = null;

        if( $request->file("photo") ) {  //photo
            $arImgs = OrderContact::saveImg($request);
            //dd($arImgs);
            if( !empty($arImgs) ) {
                if(isset($arImgs['photo'])) {
                    $contact->deleteImg();
                    $contact->update([
                        'photo' => $arImgs['photo']
                    ]);

                    $log = 'фото';
                }
            } else {
                if($request->file('photo') )
                    echo 'Файл слишком большой!';
                    //return redirect()->route('admin.order_contacts.edit')->withErrors(['Файл слишком большой!'])->withInput();
            }
        }
        else if($request->has('phone_type')) {
            $phone = $contact->phones()->where('type', '=', $request->input('phone_type'))->first();

            if($phone) {
                $phone->update([
                    'phone' => $request->input('val')
                ]);

                if($request->input('phone_type') == 'work') {
                    if( strlen(str_replace('_', '', $request->input('old_tel'))) == 16  && strlen(str_replace('_', '', $request->input('val'))) == 16 )
                        $log = 'телефон изменен ' . $request->input('old_tel') . ' -> ' . $request->input('val');
                    else if( strlen(str_replace('_', '', $request->input('old_tel'))) == 16 && empty($request->input('val')) )
                        $log = 'телефон удален '.$request->input('old_tel');
                    else if( empty($request->input('old_tel')) && strlen(str_replace('_', '', $request->input('val'))) == 16 )
                        $log = 'телефон добавлен '.$request->input('val');
                }
                else {
                    if( strlen(str_replace('_', '', $request->input('old_mobile'))) == 16 && strlen(str_replace('_', '', $request->input('val'))) == 16 )
                        $log = 'телефон изменен ' . $request->input('old_mobile') . ' -> ' . $request->input('val');
                    else if( strlen(str_replace('_', '', $request->input('old_mobile'))) == 16 && empty($request->input('val')) )
                        $log = 'телефон удален '.$request->input('old_mobile');
                    else if( empty($request->input('old_mobile')) && strlen(str_replace('_', '', $request->input('val'))) == 16 )
                        $log = 'телефон добавлен '.$request->input('val');
                }
            } else {
                $phone = OrderContactPhone::create([
                    'contact_id' => $contact->id,
                    'phone' => $request->input('val'),
                    'type' => $request->input('phone_type')
                ]);

                $contact->phones()->attach($phone->id);

                $log = 'добавлен телефон '.$request->input('val');
            }

        }
        else if($request->input('field') == 'email') {
            $email = $contact->emails()->first();

            if($email) {
                $email->update([
                    'email' => $request->input('val')
                ]);

                $log = " email: ".$email->email.' -> '.$request->input('val');
            } else {
                $email = OrderContactEmail::create([
                    'contact_id' => $contact->id,
                    'email' => $request->input('val')
                ]);

                //$contact->emails()->attach($email->id);

                $log = " добавлен email: ".$email->email;
            }
        }
        else if($request->input('field') == 'contact_names') {
            $contact->update([
                'name' => $request->input('val')
            ]);
        }
        else {
            //dd($request->input('field'), $request->input('val'));
            $old_val = $contact->{$request->input('field')};

            if($request->input('field') == 'order_company_id') {
                $contact->update([
                    $request->input('field') => $request->input('val')
                ]);

                if( $old_val > 0 ) {
                    $log = " компания: ".\App\Models\OrderCompany::find($old_val)->name.' -> '.$contact->company->name;
                } else {
                    $log = " добавлена компания: ".$contact->company->name;
                }
            }
            else if($request->input('field') == 'user_id') {
                //dump($contact->user->fio());
                $contact->update([
                    $request->input('field') => $request->input('val')
                ]);

                if( $old_val > 0 ) {
                    $log = " ответственный: ".\App\User::find($old_val)->fio().' -> '.$contact->user->fio();
                } else {
                    $log = " добавлен ответственный: ".$contact->user->fio();
                }
            }
            else {  //other fields
                $contact->update([
                    $request->input('field') => $request->input('val')
                ]);

                $log = trans("order_contact.".$request->input('field')). ': '.$old_val.' -> '.$request->input('val');
            }
        }

        if(!empty($log)) {
            $data = [
                'contact_id' => $contact->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($contact->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил контакт '.$contact->name."\r\n- ".$log
            ];

            \App\Models\OrderContactLog::create($data);
        }

    }

    /**
     * удалить order_company
     */
    public function delete_order_company(Request $request) {
        $contact = OrderContact::find($request->input('id'));
        $contact->order_company_id = null;
        $contact->save();
    }
}
