<?php

namespace App\Http\Controllers\admin;

use App\Events\onOrderCompanyUpdateEvent;
use App\Models\OrderContact;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\OrderCompany;
use Carbon\Carbon;
use DB;
use Auth;
use Event;
use App\Events\onOrderCompanyAddEvent;


class OrderCompanyController extends Controller
{
	public function __construct() {
		$this->authorize('index', new \App\Models\OrderContact());
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$filters = $this->getFormFilter($request->input());
		//$companies = OrderCompany::orderBy('id', 'desc');

		//sort /*
		if ( isset($filters['order']) && $filters['order']!='')
		{
			//check sql-injection
			$arSort = explode('-', $filters['order']);
			if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
				return redirect()->back();
			//dump($filters); dd();
			switch ($arSort[0]) {
				case 'name':
					$companies = OrderCompany::orderBy('name', $arSort[1]);
					break;
				case 'site':
					$companies = OrderCompany::orderBy('site', $arSort[1]);
					break;
				case 'responsible':
					$companies = OrderCompany::select('order_companies.*')
						->leftJoin(
							DB::raw("(SELECT `id`, `surname` FROM `users`) `zz`"), 'zz.id', '=', 'order_companies.user_id'
						)
						->orderByRaw("zz.surname $arSort[1]");
					//->toSql();
					//dump($contacts); dd();
					break;
				case 'created':
					$companies = OrderCompany::orderBy('created_at', $arSort[1]);
					break;
			}
		}
		else
			$companies = OrderCompany::orderBy('id', 'desc');
		//sort */

		//доступ к компаниям
		if( Auth::user()->company != null) {
			$companies->whereIn('user_id', function($query) {
				$query->select(DB::raw('id'))
					->from('users')
					->whereRaw("users.company_id = ".Auth::user()->company->id);
			})->orWhere('user_id', Auth::user()->id);
		}
		else
			return redirect()->back();

		if (!empty($filters) && !empty($filters['name'])) {
			$companies->where('name', 'LIKE', '%'.$filters['name'].'%');
		}

		$total_companies_count = $companies->count();
		
		$companies = $companies->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);

		//всего количество/сумма
		$count_companies = $companies->count();
		
		$sum_paginate = 0;
		foreach ($companies as $company) {
			//dump($orders);
			$sum_paginate = $sum_paginate + $company->orders->sum('amount');
		}
		
		return view('admin.order_companies.index', [
			'companies' => $companies, 
			'filters' => $filters,
			'total_companies_count' => $total_companies_count,
			'count_companies' => $count_companies,
			'sum_paginate' => $sum_paginate
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Request $request)
	{
		return view('admin.order_companies.create', ['request' => $request]);
	}
	/*public function create()
	{
		return view('admin.order_companies.create');
	}*/

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(\App\Http\Requests\admin\OrderCompanyRequest $request)
	{
		$data = $request->all();
		//dump($request->input('route')); dd();

		//logo
		$arImgs = OrderCompany::saveImg($request);
		if( !empty($arImgs) ) {
			if(isset($arImgs['logo'])) {
				$data['logo'] = $arImgs['logo'];
				//$contact->deleteImg();
			}
		} else {
			if($request->file('logo') )
				return redirect()->back()->withErrors(['Файл слишком большой!'])->withInput();
		}

		$company = OrderCompany::create($data);

		//logs
		Event::fire(new onOrderCompanyAddEvent($company, Auth::user()));

		if($request->has('route') && $request->input('route')=='order') {
			return redirect()->route('admin.orders.create', [
				'order_company_id' => $company->id,
				'order_name' => @$request->input('order_name'),
				'amount' => @$request->input('amount')
			])->withMessge('Компания добавлена');
		}
		else if($request->has('route') && $request->input('route')=='contact') {
			return redirect()->route('admin.order_contacts.create', [
				'order_company_id' => $company->id,
				'order_name' => @$request->input('order_name'),
				'contact_name' => @$request->input('contact_name'),
				'route2' => 'order'
				//'amount' => @$request->input('amount')
			])->withMessge('Компания добавлена');
		}
		else {
			return redirect()->route('admin.order_companies.index')->withMessge('Компания добавлена');
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$company = OrderCompany::find($id);
		$company_contacts = OrderContact::where('order_company_id', '=', $id)->get();
		$company_orders = Order::where('order_company_id', '=', $id)->get();
		$offers = \App\Models\Offer::where('order_company_id', '=', $id)
			->orderBy('id', 'desc')
			->get();

		//invoices
		$invoices = \App\Models\Invoice::whereIn('order_id', function($query) use ($id) {
			$query->select('id')
				->from(with(new Order())->getTable())
				->where('order_company_id', $id);
		})->get();
		//dd($invoices);

		//logs
		$sql = "
            SELECT date_format(created_at, '%d.%m.%Y') as created_at, `user_id`, `note`, YEAR(created_at) as _year, MONTH(created_at) as _month
            FROM order_company_logs
            WHERE `order_company_id` = $company->id
            GROUP BY YEAR(created_at) ASC, MONTH(created_at) ASC, created_at ASC, order_company_logs.user_id, order_company_logs.note
        ";

		$logs = DB::select($sql);
		$periods = [];
		foreach ($logs as $key=>$val)
			$periods[] = ['year' => $val->_year, 'month' => $val->_month];
		//dd(array_unique($periods, SORT_REGULAR));
		$periods = array_unique($periods, SORT_REGULAR);

		return view('admin.order_companies.edit', [
			'company' => $company,
			'company_contacts' => $company_contacts,
			'company_orders' => $company_orders,
			'offers' => $offers,
			'invoices' => $invoices,
			'logs' => $logs,
			'periods' => $periods
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(\App\Http\Requests\admin\OrderCompanyRequest $request, $id)
	{
		$data = $request->all();

		if($request->has('date_register')) {
			$data['date_register'] = (new Carbon($data['date_register']))->format('Y.m.d');
		}

		//OrderCompany::findOrFail($id)->update($data);

		$company = OrderCompany::find($id);
		$arImgs = OrderCompany::saveImg($request);
		//dump($arImgs); dd();
		if( !empty($arImgs) ) {
			if(isset($arImgs['logo'])) {
				//unset($data['logo']);
				$data['logo'] = $arImgs['logo'];
				$company->deleteImg('logo');
			}
		} else {
			if($request->file('logo') )
				return redirect()->back()->withErrors(['Файл слишком большой!'])->withInput();
		}

		$old_data = $company->getOriginal();

		$company->update($data);

		//logs
		$new_data = $company->getAttributes();
		Event::fire(new onOrderCompanyUpdateEvent($company, Auth::user(), $old_data, $new_data));
		
		return redirect('/admin/order_companies/'.$id.'/edit')->with('message', 'Компания изменена');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		OrderCompany::destroy($id);
		return redirect()->route('admin.order_companies.index')->withMessage('Компания удалена');
	}

	/*public function search(Request $request) {
		$data =[];
		if($request->has('search_param') && $request->input('search_param')) {
			$companies = OrderCompany::whereIn('user_id', function($query) {
				$query->select(DB::raw('id'))
					->from('users')
					->whereRaw("users.company_id = ".Auth::user()->company_id);
			})->where('name', 'LIKE', $request->input('search_param') . '%');
			$companies = $companies->orderBy('name')->take(10)->get();

			foreach ($companies as $company) {
				$item = ['id' => $company->id, 'name' => $company->name];
				$data[] = $item;
			}
		}

		return response()->json($data);
	}*/

	public function search(Request $request) {
		$data =[];
		if($request->has('term') && $request->input('term')) {
			if(Auth::user()->company_id) {
				$companies = OrderCompany::whereIn('user_id', function($query) {
					$query->select(DB::raw('id'))
						->from('users')
						->whereRaw("users.company_id IS NULL OR users.company_id = ".Auth::user()->company_id);
				})->where('name', 'LIKE', $request->input('term') . '%');
			} else {
				$companies = OrderCompany::whereIn('user_id', function($query) {
					$query->select(DB::raw('id'))
						->from('users')
						->whereRaw("users.company_id IS NULL");
				})->where('name', 'LIKE', $request->input('term') . '%');
			}

			$companies = $companies->orderBy('name')->take(10)->get();

			foreach ($companies as $company) {
				$item = [
					'id' => $company->id,
					'name' => $company->name,
					'site' => $company->site,
					'phone' => $company->phone,
					'email' => $company->email,
				];
				$data[] = $item;
			}
		}

		return response()->json($data);
	}

	/**
	 * update ajax query from order
	 */
	public function update_ajax(Request $request) {
		//dd($request->all());
		$order_company = OrderCompany::findOrfail($request->input('id'));
		$log = null;

		if( $request->file("logo") ) {  //photo
			$arImgs = OrderCompany::saveImg($request);
			//dd($arImgs);
			if( !empty($arImgs) ) {
				if(isset($arImgs['logo'])) {
					$order_company->deleteImg();
					$order_company->update([
						'logo' => $arImgs['logo']
					]);

					$log = 'фото';
				}
			} else {
				if($request->file('logo') )
					echo 'Файл слишком большой!';
				//return redirect()->route('admin.order_contacts.edit')->withErrors(['Файл слишком большой!'])->withInput();
			}
		}
		else if($request->input('field') == 'user_id') {
			$old_val = $order_company->{$request->input('field')};

			//dump($contact->user->fio());
			$order_company->update([
				$request->input('field') => $request->input('val')
			]);

			if( $old_val > 0 ) {
				$log = " ответственный: ".\App\User::find($old_val)->fio().' -> '.$order_company->user->fio();
			} else {
				$log = " добавлен ответственный: ".$order_company->user->fio();
			}
		}
		else if($request->input('field') == 'phone') {
			$old_val = $order_company->{$request->input('field')};

			$order_company->update([
				$request->input('field') => $request->input('val')
			]);

			/*if( strlen(str_replace('_', '', $old_val)) == 16  && strlen(str_replace('_', '', $request->input('val'))) == 16 )
				$log = 'телефон изменен ' . $old_val . ' -> ' . $request->input('val');
			else if( strlen(str_replace('_', '', $old_val)) == 16 && empty($request->input('val')) )
				$log = 'телефон удален '.$old_val;
			else if( empty($old_val) && strlen(str_replace('_', '', $request->input('val'))) == 16 )
				$log = 'телефон добавлен '.$request->input('val');*/
			if( strlen(str_replace('_', '', $request->input('val'))) == 16 )
				$log = 'телефон изменен ' . $old_val . ' -> ' . $request->input('val');
			else if( !empty($old_val) && empty($request->input('val')) )
				$log = 'телефон удален '.$old_val;
			else if( empty($old_val) && strlen(str_replace('_', '', $request->input('val'))) == 16 )
				$log = 'телефон добавлен '.$request->input('val');
		}
		else {
			$old_val = $order_company->{$request->input('field')};

			$order_company->update([
				$request->input('field') => $request->input('val')
			]);

			$log = trans("order_company.".$request->input('field')). ': '.$old_val.' -> '.$request->input('val');
		}

		if(!empty($log)) {
			$data = [
				'order_company_id' => $order_company->id,
				'user_id' => Auth::user()->id,
				'note' => date('d.m.Y H:i', strtotime($order_company->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил контакт '.$order_company->name."\r\n- ".$log
			];

			\App\Models\OrderCompanyLog::create($data);
		}

	}
}
