<?php

namespace App\Http\Controllers\admin;

use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$products = Product::where(function ($query) {
            $query->where('company_id', Auth::user()->company_id)
                //->orWhere('company_id', null);
                ->orderBy('name', 'asc')->get();
        });*/
        $products = Product::where('company_id', '=', Auth::user()->company_id)
            ->orderBy('name', 'asc')
            ->get();
        
        return view('admin.products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.products.create', [
            'request' => $request
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\ProductRequest $request)
    {
        $data = $request->input();

        //компания
        //$data['company_id'] = Auth::user()->company_id;

        $product = Product::create($data);

        return redirect()->route('admin.products.edit', [
            'id' => $product->id,
        ])->withMessage('Добавлено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        return view('admin.products.edit', [
            'product' => $product,
            'request' => $request
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $data = $request->input();
        $product->update($data);

        return redirect()->route('admin.products.index')->withMessage('Изменено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        Product::destroy($id);

        return redirect()->route('admin.products.index')->withMessage('Удалено');
    }
}
