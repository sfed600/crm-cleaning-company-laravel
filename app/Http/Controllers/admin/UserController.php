<?php

namespace App\Http\Controllers\admin;

use App\Models\Order;
use App\Models\UserIncome;
use Illuminate\Http\Request;
use App\Events\onUserAddEvent;
use App\Events\onUserUpdateEvent;
use Event;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserGroup;
use App\Models\Company;
use App\Models\UserProfession;
use Carbon\Carbon;
use DB;
use Date;
use Gate;
use Auth;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $this->authorize('index', new User());
        
        $filters = $this->getFormFilter($request->input());
        //$users = User::orderBy('surname', 'asc');

        //sort
        //dump($filters['order']); dd();
        if ( isset($filters['order']) && $filters['order']!='')
        {
            //check sql-injection
            $arSort = explode('-', $filters['order']);
            if( strtoupper($arSort[1]) != 'ASC' && strtoupper($arSort[1]) != 'DESC')
                return redirect()->back();

            switch ($arSort[0]) {
                case 'name':
                    $users = User::orderBy('name', $arSort[1]);
                    break;
                case 'surname':
                    $users = User::orderBy('surname', $arSort[1]);
                    break;
                /*case 'phone':
                    //dump($users); dd();
                    break;*/
                case 'rating':
                    /*
                    select users.*
                    from `users`
                    LEFT JOIN (SELECT `user_id`, COUNT(*) as cnt, SUM(`nocome`) as sum_nocome, SUM(`late`) as sum_late,
                    (COUNT(*)*100 - SUM(`late`) - SUM(`nocome`) * 10) / COUNT(*) as rating
                    FROM `order_users`
                    GROUP BY `user_id`) as hz ON hz.user_id = users.id
                    where `users`.`deleted_at` is null
                    order by hz.rating desc
                    */

                    $users = User::select('users.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `user_id`, COUNT(*) as cnt, SUM(`nocome`) as sum_nocome, SUM(`late`) as sum_late,
                                    (COUNT(*)*100 - SUM(`late`) - SUM(`nocome`) * 10) / COUNT(*) as rating
                                FROM `order_users`
                                GROUP BY `user_id`) `hz`"), 'hz.user_id', '=', 'users.id'
                        )
                        //->whereRaw("users.deleted_at is null")
                        //->orderBy(DB::raw("hz.rating desc"));
                        ->orderByRaw("hz.rating $arSort[1]");
                        //->get();
                        //->toSql();
                    //dump($users); dd();
                    break;
                case 'orders_cnt':
                    $users = User::select('users.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `user_id`, COUNT(*) as cnt
                                FROM `order_users`
                                GROUP BY `user_id`) `hz`"), 'hz.user_id', '=', 'users.id'
                        )
                        //->whereRaw("users.deleted_at is null")
                        ->orderByRaw("hz.cnt $arSort[1]");
                    break;
                case 'late':
                    $users = User::select('users.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `user_id`, SUM(`late`) as sum_late
                                FROM `order_users`
                                GROUP BY `user_id`) `hz`"), 'hz.user_id', '=', 'users.id'
                        )
                        //->whereRaw("users.deleted_at is null")
                        //->orderByRaw('hz.sum_late ?', [$arSort[1]]);
                        ->orderByRaw("hz.sum_late $arSort[1]");
                    break;
                case 'nocome':
                    $users = User::select('users.*')
                        ->leftJoin(
                            DB::raw("
                                (SELECT `user_id`, SUM(`nocome`) as sum_nocome
                                FROM `order_users`
                                GROUP BY `user_id`) `hz`"), 'hz.user_id', '=', 'users.id'
                        )
                        //->whereRaw("users.deleted_at is null")
                        //->orderByRaw('hz.sum_late ?', [$arSort[1]]);
                        ->orderByRaw("hz.sum_nocome $arSort[1]");
                    break;
                case 'created_at':
                    $users = User::orderBy('created_at', $arSort[1]);
                    break;
            }
        }
        else {
            //$users = User::orderBy('surname', 'asc');
            $users = User::where(function ($query) {
                $query->where('company_id', '=', Auth::user()->company_id)
                    ->where('staff', '=', 1)
                    ->orWhereNull('company_id');
            })->orderBy('surname', 'asc');
            /*$users = User::orderBy('surname', 'asc')->where(function ($query) {
                $query->where('company_id', '=', Auth::user()->company_id)
                    ->where('staff', '=', 1)
                    ->orWhereNull('company_id');
            });*/
        }

        if (!empty($filters) && !empty($filters['bloc'])) {
            $users->whereHas('group', function ($query) use ($filters){
                $query->whereHas('block', function ($query1) use ($filters){
                    $query1->where('name', $filters['bloc']);
                });
            });
        }
        if (!empty($filters) && !empty($filters['group_id'])) {
            $users->where('group_id', $filters['group_id']);
        }

        //доступ к компаниям
        if (Auth::user()->can('company', new User())) {
            if (!empty($filters) && !empty($filters['company'])) {
                $users->where('company_id', $filters['company']);
            }
        //только пользовали твоей компании
        } /*else {
            $users->where(function ($query) {
                $query->where('company_id', Auth::user()->company_id)
                      ->orWhere('company_id', null);
            })->whereHas('group', function ($query){
                $query->where('name', '!=', 'SuperAdmin');
            });
        }*/
        else {
            //$users->where('company_id', Auth::user()->company_id);
            $users->where(function ($query) {
                $query->whereNull('company_id')
                    ->orWhere('company_id', '=', Auth::user()->company_id);
            });
        }

        if (!empty($filters) && !empty($filters['id'])) {
            //$users->where('id', $filters['id']);
            $user_find = \App\User::find($filters['id']);
            $filters['fio'] = $user_find ? $user_find->fio() : '';
        }
        
        if (!empty($filters) && !empty($filters['email'])) {
            $users->where('email', 'LIKE', '%'.$filters['email'].'%');
        }
        if (!empty($filters) && !empty($filters['name'])) {
            $users->where(function ($query) use ($filters) {
                $query->where('name', 'LIKE', '%'.$filters['name'].'%')
                    ->orWhere('surname', 'LIKE', '%'.$filters['name'].'%');
            });
        }

        if (!empty($filters) && !empty($filters['phone'])) {
            $users->whereHas('phones', function ($query) use ($filters){
                $query->where('phone', $filters['phone']);
            });
        }

        if (!empty($filters) && !empty($filters['date'])) {
            $users->where('created_at', '>=', (new Carbon('01.'.$filters['date'].' 00:00'))->format('Y.m.d H:i'));
            $cnt_days = (new Carbon)->createFromFormat('d.m.Y','01.'.$filters['date'])->daysInMonth;
            $users->where('created_at', '<=', (new Carbon($cnt_days.'.'.$filters['date'].' 23:59'))->format('Y.m.d H:i'));
        }
        if (!empty($filters) && !empty($filters['passport'])) {
            $users->where('passport', 'LIKE', '%'.$filters['passport'].'%');
        }
        if (!empty($filters) && isset($filters['status']) && $filters['status']!='') {
            $users->where('status', $filters['status']);
        }

        /*if (!empty($filters) && !empty($filters['profession'])) {
            $users->whereHas('professions', function ($query) use ($filters) {
                $query->where('profession_id', $filters['profession']);
            });
        }*/

        if (!empty($filters) && isset($filters['deleted']) && $filters['deleted']) {
            $users->withTrashed();
        }

        //всего сотрудников
        $total_users_count = $users->count();

        if( @$filters['id'] > 0 ) { //если фильтр по сотруднику
            $paginate = 10;
            $page = 1;
            $offSet = ($page * $paginate) - $paginate;
            $itemsForCurrentPage = array_slice([0 => User::find($filters['id'])], $offSet, $paginate, true);
            $users = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, 1, $paginate, $page);
            //$users = $users->toArray();

        }
        else {
            $users = $users->with('group', 'group.block', 'orders', 'phones')
                ->paginate($filters['perpage'], null, 'page', !empty($filters['page']) ? $filters['page'] : null);
        }
        //dump($users); dd();
        $groups = UserGroup::orderBy('name_rus')->get();
        $companies = Company::orderBy('name')->get();
        $professions = UserProfession::orderBy('name')->get();

        return view('admin.users.index', [
            'users' => $users, 
            'groups' => $groups, 
            'companies' => $companies,
            'professions' => $professions, 
            'filters' => $filters,
            //'total_users' => (isset($total_users)) ? $total_users : (isset($users)) ? count($users) : 0,
            'total_users_count' => $total_users_count
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \InvalidArgumentException
     */
    public function show(Request $request, $id) 
    {
        $user = User::findOrFail($id);
        $user->with(['last_visit']);
         
        if (Gate::denies('show', $user)) {
            abort(403);
        }

        $carbon = new Carbon();

        //компании
        $companies = null;
        //компании где работал данный сотрудник
        if (Auth::user()->can('mycompany', new User())) {
            $companies = Company::whereHas('orders', function($query) use ($user){
                $query->whereHas('users', function($query1) use ($user){
                    $query1->where('user_id', $user->id);
                });
            })->orderBy('name')->get();
        }

        $filters = $this->getFormFilter($request->input());
        /*if(empty($filters['date-range'])) {
            $filters['date-range'] = (new Carbon('first day of this month'))->format('d.m.Y').' - '.(new Carbon('last day of this month'))->format('d.m.Y');
        }*/

        //фильтр по дате
        /*if(empty($filters['daterange'])) {
            $filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');
        } else {
            $f = explode('-', $filters['daterange']);
            $date_from = !empty($f[0]) ? $f[0] : '';
            $date_to = !empty($f[1]) ? $f[1] : '';
        }*/

        $filters['period'] = Input::get('period', null);
        $filters['daterange'] = Input::get('daterange', null);
        if(!$filters['daterange'])
            $filters['daterange'] = Carbon::now()->startOfMonth()->format('d.m.Y').' - '.Carbon::now()->endOfMonth()->format('d.m.Y');

        //доступ к компаниям
        /*if (Auth::user()->can('mycompany', new User())) {
            if ((empty($filters['company']) || !$filters['company']) && !empty($companies) && $companies->count()){
                $filters['company'] = $companies->first()->id;
            }
        } else {
            $filters['company'] = Auth::user()->company_id;
        }*/

        //Выбираем все рабочие дни за выбранный перриод работника в выбранной компании
        $dates = \App\Models\OrderUser::where('user_id', $id);

        //фильтр по дате
        if(!empty($filters) && !empty($filters['daterange'])) {
            $f = explode('-', $filters['daterange']);
            $date_from = !empty($f[0]) ? $f[0] : '';
            $date_to = !empty($f[1]) ? $f[1] : '';
            $dates->whereHas('day', function ($query) use ($date_from, $date_to){
                $query->where('date', '>=', (new Carbon($date_from))->format('Y-m-d'))
                      ->where('date', '<=', (new Carbon($date_to))->format('Y-m-d'));
            });
        }
        //фильтр компании
        if (!empty($filters) && !empty($filters['company'])) {
            $dates->whereHas('day.order', function ($query) use ($filters){
                $query->where('company_id', $filters['company']);
            });
        }
        $dates = $dates->with(['day' => function($query) {
                $query->orderBy('date');
            }])->get();

        $outcomes = \App\Models\UserIncome::where('user_id', $id);
        if (!empty($filters['company']) && $filters['company']) {
            $outcomes->where('company_id', $filters['company']);
        }

        $outcomes->where('date', '>=', (new Carbon($date_from))->format('Y-m-d'))->where('date', '<=', (new Carbon($date_to))->format('Y-m-d'));

        /*if(!empty($filters) && !empty($filters['date-range'])) {
            $f = explode('-', $filters['date-range']);
            $date_from = !empty($f[0]) ? $f[0] : '';
            $date_to = !empty($f[1]) ? $f[1] : '';

            $outcomes->where('date', '>=', (new Carbon($date_from))->format('Y-m-d'))->where('date', '<=', (new Carbon($date_to))->format('Y-m-d'));
        }*/
        $outcomes = $outcomes->orderBy('date', 'desc')->select('id', 'date', 'amount', 'outcome', 'type', 'comment', 'created_at')->get();

        //dd($dates, $outcomes);
        foreach($dates as $key => $date) {
            $date->date = $date->day->date;
            $date->order_id = $date->day['order_id'];
            $date->order_number = $date->day->order->number;
            foreach($outcomes as $key2 => $outcome) {
                if( $date->date == $outcome->date && $outcome->type == 'wages' ) {
                    $date->id = $outcome->id;
                    $date->outcome = $outcome->outcome;
                    //$date->type = $outcome->typeName();
                    $date->type = $outcome->type;
                    $date->comment =  ($date->comment ?: $outcome->comment);
                    unset($outcomes[$key2]);
                }
            }
        }

        //объеденить $dates & $outcomes
        $arDate = [];

        foreach($dates as $key => $date) {
            $row = [
                'id' => $date->id,
                'date' => $date->date,
                'order_id' => $date->order_id,
                'order_number' => $date->order_number,
                'amount' => $date->amount,
                'outcome' => $date->outcome,
                'type' => $date->type,
                'comment' => $date->comment
            ];
            $arDate[] = $row;
        }

        foreach($outcomes as $key => $outcome) {
            $row = [
                'id' => $outcome->id,
                'date' => $outcome->date,
                'amount' => $outcome->amount,
                'outcome' => $outcome->outcome,
                'type' => $outcome->type,
                'comment' => $outcome->comment
            ];
            $arDate[] = $row;
        }

        $dates = $this->customMultiSort($arDate, "date");
        $total_amount = 0;
        $total_outcome = 0;
        foreach ($dates as $key => $value)
        {
            $total_amount += $value['amount'];
            $total_outcome += $value['outcome'];
                
            $row = new \stdClass;
            foreach ($value as $k=>$val) {
                $row->$k = $val;
            }

            $dates[$key] = $row;
        }

        //dd($dates);
        
        //вычисляем баланс
        $balance = 0;
        /*if(!empty($filters['company'])) {
            $total_from_orders = \App\Models\OrderUser::where('user_id', $id)->whereHas('day.order', function ($query) use ($filters) {
                $query->where('company_id', $filters['company']);
            })->sum('amount');

            $sum_amount = UserIncome::where('user_id', $id)->where('company_id', $filters['company'])->where('action', 'outcome')->where('type', '!=', 'wages')->sum('amount') - UserIncome::where('user_id', $id)->where('company_id', $filters['company'])->where('action', 'income')->where('type', '!=', 'wages')->sum('amount');
            $sum_outcome = UserIncome::where('user_id', $id)->where('company_id', $filters['company'])->where('action', 'outcome')->sum('outcome') - UserIncome::where('user_id', $id)->where('company_id', $filters['company'])->where('action', 'income')->sum('outcome');
            //dd($total_from_orders, $sum_amount, $sum_outcome);
            $balance = $total_from_orders + $sum_amount - $sum_outcome;
        }*/
        $total_from_orders = \App\Models\OrderUser::where('user_id', $id)->sum('amount');
        $sum_amount = UserIncome::where('user_id', $id)->where('action', 'outcome')->where('type', '!=', 'wages')->sum('amount') - UserIncome::where('user_id', $id)->where('action', 'income')->where('type', '!=', 'wages')->sum('amount');
        $sum_outcome = UserIncome::where('user_id', $id)->where('action', 'outcome')->sum('outcome') - UserIncome::where('user_id', $id)->where('action', 'income')->sum('outcome');
        //dd($total_from_orders, $sum_amount, $sum_outcome);
        $balance = $total_from_orders + $sum_amount - $sum_outcome;

        $sql = "
            SELECT `id`, `region_id` as region, `name`, null as district FROM `geo_city` 
            UNION 
            SELECT id, null, name, null FROM `geo_district` 
            UNION 
            SELECT id, null, name, district_id FROM `geo_regions`
        ";

        $work_cities = DB::select($sql);
        //dd($work_cities);

        $profs = [];    
        foreach (UserProfession::orderBy('name')->get() as $profession)
            $profs[$profession->name] = $profession->id;

        $groups = [];
        foreach (UserGroup::orderBy('name')->get() as $group)
            $groups[$group->name_rus] = $group->id;

        //logs
        $sql = "
        SELECT date_format(created_at, '%d.%m.%Y') as created_at, `responsible`, `note`, YEAR(created_at) as _year, MONTH(created_at) as _month
        FROM user_logs
        WHERE `user_id` = $user->id
        GROUP BY YEAR(created_at) DESC, MONTH(created_at) DESC, created_at DESC, user_logs.user_id, user_logs.note
        ";

        $logs = DB::select($sql);
        //dd($logs);

        //periods logs
        $periods = [];
        foreach ($logs as $key=>$val)
            $periods[] = ['year' => $val->_year, 'month' => $val->_month];
        //dd(array_unique($periods, SORT_REGULAR));
        $periods = array_unique($periods, SORT_REGULAR);

        return view('admin.users.show', [
            'user' => $user,
            'filters' => $filters,
            'dates' => $dates,
            'balance' => $balance,
            //'total_accrued' => $total_accrued,
            'total_amount' => $total_amount,
            'total_outcome' => $total_outcome,
            'companies' => $companies,
            'cnt_turns_u_company' => (isset($outcomes)) ? @$outcomes->where('company_id', Auth::user()->company_id)->count : 0,
            'work_cities' => $work_cities,
            'sberbank' => $user->cards()->where('bank', '=', 'sberbank')->first(),
            'tinkoff' => $user->cards()->where('bank', '=', 'tinkoff')->first(),
            'action' => 'show',
            'professions' => $profs,
            'logs' => $logs,
            'periods' => $periods,
            'groups' => $groups,
            'cities_work' => (isset($user->work_regions)) ? \GuzzleHttp\json_decode($user->work_regions) : null,
            'reviews' => \App\Models\UserReview::where('user_id', $id)->with(['author', 'order'])->orderBy('updated_at', 'desc')->get()
        ]);
    }

    /**
     * добавить отзыв
     */
    public function review_add(Request $request) {
        $user = User::findOrFail($request->input('user_id'));
        $author = Auth::user();

        if (Gate::denies('review', $user)) {
            //abort(403);
            exit('Доступ запрещен.');
        }

        $order = Order::where('number', $request->input('order_id'))->with(['company'])->first();
        if( !$order )
            exit(\GuzzleHttp\json_encode(['error' => 'Заказ не найден.']));

        $review = new \App\Models\UserReview();
        $review->order_id = $order->id;
        $review->user_id = $request->input('user_id');
        $review->review = $request->input('review');
        $review->author_id = $author->id;
        if( $review->save() ) {
            $data = [];
            $data['photo'] = $author->getImgPreviewPath().$author->img;
            $data['name'] = $author->fio();
            $data['group'] = $author->group->name;
            $data['company'] = $author->company->name;
            $data['date'] = date('d.m.Y H:i');
            //$data['rating'] = ;
            $data['message'] = $review->review;
            $data['order_id'] = $order->number;
            //$data['order_url'] = route('order.show', ['id' => $order->id]);
            $data['order_url'] = "/admin/orders/$order->id/edit";
            echo \GuzzleHttp\json_encode($data);
        }
        else
            exit(\GuzzleHttp\json_encode(['error' => 'Ошибка. Обратитесь к администратору.']));

    }

    public function review_update(Request $request) {
        //dd($request->all());
        $review = \App\Models\UserReview::find($request->input('id'));
        if(!$review)
            exit('Коментарий не найден.');

        if($review->author_id != Auth::user()->id)
            exit('Коментарий может быть изменен только автором.');

        if( $review->update(['review' => $request->input('review')]) )
            echo 'success';

    }

    public function review_delete(Request $request) {
        //dd($request->all());
        $review = \App\Models\UserReview::find($request->input('id'));
        if(!$review)
            exit('Коментарий не найден.');

        if($review->author_id != Auth::user()->id)
            exit('Коментарий может быть удален только автором.');

        if( $review->delete() )
            echo 'success';
    }
    
    /**
     * начисления/штрафы
     */
    public function accruals(Request $request)
    {
        //dd($request->input('data'));
        $new_outcomes = [];
        foreach ($request->input('data') as $item) {
            if( @$item['id'] > 0 ) {
                $outcome = \App\Models\UserIncome::find($item['id']);
                $outcome->outcome = $item['outcome'];
                $outcome->save();
            }
            else {
                $new_outcomes[] = [
                    //"date" => $item['date'],
                    "date" => (new Carbon($item['date']))->format('Y-m-d'),
                    "amount" => $item['amount'],
                    "outcome" => $item['outcome'],
                    "type" => $item['type'],
                    "action" => $item['action'],
                    "company_id" => ($item['company_id'] > 0) ? $item['company_id'] : null,
                    "user_id" => $item['user_id']
                ];
            }
        }
        //dd($new_outcomes);

        $rows = UserIncome::insert($new_outcomes);
        if($rows) echo 'success!';
    }
    
    /**
     * сортировка многомерного массива $arr по $field
     */
    function customMultiSort($arr, $field) {
        $sortArr = array();
        foreach($arr as $key=>$val){
            $sortArr[$key] = $val[$field];
        }

        array_multisort($sortArr, SORT_DESC, $arr, SORT_DESC);

        return $arr;
    }

    /**
     * кнопка "применить"
     */
    public function apply_changes(Request $request)
    {
        $arData = [];
        $data = $request->all();
        parse_str($data['data'], $arData);
        //dd($arData);
        $user = User::find($arData['user_self_id']);
        if( !$user ) {
            $this->authorize('index', new User());
            $user = User::create($arData);
        } else if (Gate::denies('edit', $user)) {
            abort(403);
        }

        //parse fio
        if( empty($arData['fio']) )
            return redirect()->back()->withErrors('Имя обязательно.');
        else {
            $arrFio = explode(' ', $arData['fio']);
            $arData['surname'] = (null == @$arrFio[0]) ? '' : $arrFio[0];
            $arData['name'] = (null == @$arrFio[1]) ? '' : $arrFio[1];
            $arData['patronicname'] = (null == @$arrFio[2]) ? '' : $arrFio[2];
        }

        //cities_work
        $cities_work = array();
        parse_str($arData['str_city_work'], $cities_work);
        $arData['work_regions'] = json_encode($cities_work);

        //если метро не выбрано
        if( empty($arData['metro_id']) )
            $arData['metro_id'] = null;

        //Штатный сотрудник
        //dd($arData['staff']);
        //if($request->has('staff')) {
        if( @$arData['staff'] == 'on')
            $arData['staff'] = 1;


        //photo
        //dd($request->file("photo"));
        if(null !== $request->file("img") ) {
            if($img = User::saveImg($request)) {
                $arData['img'] = $img;
                $user->deleteImg();
            }    
        }
                
        $except = [];
        //проверка прав на изменение маршрута
        /*if($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        } else {
            $except[] = 'password';
        }*/
        //dd($arData['bithdate']);
        if( !empty($arData['bithdate'])) {
            $arData['bithdate'] = Carbon::createFromFormat('d.m.Y', $arData['bithdate'])->format('Y-m-d');
        }
        //компания если есть
        /*if(!$request->has('company_id') || !$request->input('company_id')) {
            $except[] = 'company_id';
        }

        $data['auto_id'] = (empty($data['auto_id']) || !$data['auto_id'] ? null : $data['auto_id']);*/

        $old_data = $user->getOriginal(); //dd($old_data);
        $user->update(array_except($arData, $except));

        //связь с телефонами
        //dd($request->input('old_phone'));
        //$user->phones()->whereNotIn('id', $request->input('phone_ids'))->delete();
        $user->phones()->delete();
        if(is_array($arData['phone'])) {
            $phones_logs = '';
            foreach($arData['phone'] as $key => $phone) {
                $changed = true;    //for logs
                $number = substr(preg_replace('|[^0-9]+|','',$phone), -10, 10);
                if(null !== @$request->input('phone_ids')[$key]) {
                    $user->phones()->find($request->input('phone_ids')[$key])->update(['phone' => $phone, 'number' => $number]);
                } else {
                    $user->phones()->create(['phone' => $phone, 'number' => $number]);

                    //logs
                    foreach($arData['old_phone'] as $old_phone) {
                        if($old_phone == $phone) {
                            $changed = false;
                            break;
                        }
                    }
                    if($changed == true) {
                        $phones_logs = $phones_logs."\r\n - добавлен телефон: ".$phone.' вместо '.$arData['old_phone'][$key];
                    }
                }
            }
        }

        //связь с тегами
        /*if($request->has('professions')) {
            $user->professions()->sync($request->input('professions'));
        } else {
            $user->professions()->detach();
        }*/

        //cards
        //for logs
        $old_cards = [];
        $cards_logs = '';
        foreach($user->cards as $card) {
            $old_cards[$card->bank] = $card->number;
        }

        //sber
        if( !empty($arData['sberbank']) ) {
            $sberNumber = $arData['sberbank'];
            $card = $user->cards()->where('bank', '=', 'sberbank')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'sberbank', 'number' => $sberNumber]);

                //logs
                $cards_logs = $cards_logs."\r\n - добавлена карта сбербанка: ". $sberNumber;
            } else if($card->number != $sberNumber) {
                $card->update(['bank' => 'sberbank', 'number' => $sberNumber]);

                //logs
                if( $old_cards['sberbank'] != $sberNumber)
                    $cards_logs = $cards_logs."\r\n - изменена карта сбербанка: ". $old_cards['sberbank'].' -> '.$sberNumber;
            }
        }

        //tinkoff
        if( !empty($arData['tinkoff']) ) {
            $tinkoffNumber = $arData['tinkoff'];
            $card = $user->cards()->where('bank', '=', 'tinkoff')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);

                //logs
                $cards_logs = $cards_logs."\r\n - добавлена карта tinkoff: ". $tinkoffNumber;
            } else if($card->number != $tinkoffNumber) {
                $card->update(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);

                //logs
                if( $old_cards['tinkoff'] != $tinkoffNumber)
                    $cards_logs = $cards_logs."\r\n - изменена карта tinkoff: ". $old_cards['tinkoff'].' -> '.$tinkoffNumber;
            }
        }

        //logs
        //$old_data = $user->getOriginal();
        $new_data = $user->getAttributes();
        //dd($old_data, $new_data);
        Event::fire(new onUserUpdateEvent($user, Auth::user(), $old_data, $new_data, $phones_logs, $cards_logs));

    }

    /**
     * записать последний визит пользователя
     */
    public function triggerOnline()
    {
        $sql = "REPLACE INTO online VALUES ('".Auth::user()->id."', '".date("Y-m-d H:i:s")."')";
        DB::statement($sql);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function income($id) {
        $this->authorize('income', new User());

        $incomes = \App\Models\UserIncome::where('user_id', $id)
            ->where('company_id', Auth::user()->company_id)
            ->where('action', 'income')->orderBy('date', 'desc')->get();

        $user = User::findOrFail($id);
        return view('admin.users.income', ['user' => $user, 'incomes' => $incomes]);
    }

    /**
     * @param Requests\admin\UserIncomeRequest $request
     * @param $id
     * @return mixed
     */
    public function incomeStore(\App\Http\Requests\admin\UserIncomeRequest $request, $id) {
        $this->authorize('income', new User());

        $data = $request->input();

        $data['user_id'] = $id;
        $data['action'] = 'income';
        $data['company_id'] = Auth::user()->company_id;
        $data['date'] = (new Carbon($data['date']))->format('Y.m.d');
        $data['amount'] = (!isset($data['amount']) || $data['amount']==='' ? null : $data['amount']);
        $data['amount_nigth'] = (!isset($data['amount_nigth']) || $data['amount_nigth']==='' ? null : $data['amount_nigth']);

        \App\Models\UserIncome::create($data);

        return redirect()->route('admin.users.income', $id)->withMessage('Ставка добавлена');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function incomeDelete($id) {
        $this->authorize('income', new User());

        $income = \App\Models\UserIncome::findOrFail($id);
        $income->delete();

        return redirect()->route('admin.users.income', $income->user_id)->withMessage('Ставка удалена');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function outcome($id) {
        $this->authorize('income', new User());

        $outcomes = \App\Models\UserIncome::where('user_id', $id)
            ->where('company_id', Auth::user()->company_id)
            ->where('action', 'outcome')->orderBy('date')->get();

        $user = User::findOrFail($id);
        return view('admin.users.outcome', ['user' => $user, 'outcomes' => $outcomes]);
    }

    /**
     * @param Requests\admin\UserIncomeRequest $request
     * @param $id
     * @return mixed
     */
    public function outcomeStore(\App\Http\Requests\admin\UserOutcomeRequest $request, $id) {
        $this->authorize('income', new User());

        $data = $request->input();
        $data['user_id'] = $id;
        $data['action'] = 'outcome';
        $data['company_id'] = Auth::user()->company_id;
        $data['date'] = (new Carbon($data['date']))->format('Y.m.d');
        \App\Models\UserIncome::create($data);

        return redirect()->route('admin.users.outcome', $id)->withMessage('Выплата добавлена');
    }

    /**
     * Массовое добавление выплат
     * @param $id
     */
    public function outcomeStoreMass(Request $request, $id) {
        $this->authorize('income', new User());

        if($request->has('dates')) {
            $data['company_id'] = Auth::user()->company_id;
            $data['user_id'] = $id;
            $data['action'] = 'outcome';
            $data['type'] = 'wages';
            foreach($request->input('dates') as $date => $dates) {
                $data['date'] = (new Carbon($date))->format('Y.m.d');
                foreach($dates as $amount) {
                    $data['outcome'] = $amount;
                    \App\Models\UserIncome::create($data);
                }
            }
        }
        return redirect()->route('admin.users.show', $id)->withMessage('Выплаты сделаны');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function outcomeDelete($id) {
        $this->authorize('income', new User());

        $income = \App\Models\UserIncome::findOrFail($id);
        $income->delete();

        return redirect()->route('admin.users.outcome', $income->user_id)->withMessage('Выплата удалена');
    }


    /**
     * Комментарии
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function comments($id) {
        $this->authorize('index', new User());

        $comments = \App\Models\UserComment::where('user_id', $id)->with('userAdd')->orderBy('id', 'desc')->get();

        $user = User::findOrFail($id);

        return view('admin.users.comments', ['user' => $user, 'comments' => $comments]);
    }


    public function commentsStore(Requests\admin\UserCommentRequest $request, $id) {
        \App\Models\UserComment::create(['user_id' => $id, 'user_add_id' => Auth::user()->id, 'comment' => $request->input('comment')]);
        return redirect()->route('admin.users.comments', $id)->withMessage('Комментарий добавлен');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('index', new User());

        $groups = UserGroup::orderBy('name_rus')->get();
        $companies = Company::orderBy('name')->get();
        //$professions = UserProfession::orderBy('name')->get();
        $profs = [];
        foreach (UserProfession::orderBy('name')->get() as $profession)
            $profs[$profession->name] = $profession->id;

        $groups = [];
        foreach (UserGroup::orderBy('name')->get() as $group)
            $groups[$group->name_rus] = $group->id;

        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();

        //все города/регионы
        $sql = "
            SELECT `id`, `region_id` as region, `name`, null as district FROM `geo_city` 
            UNION 
            SELECT id, null, name, null FROM `geo_district` 
            UNION 
            SELECT id, null, name, district_id FROM `geo_regions`
        ";
        $work_cities = DB::select($sql);

        //cities_work
        /*$cities_work = [];
        parse_str(old('cities_work'), $cities_work);
        dd($cities_work);*/

        return view('admin.users.create', [
            'groups' => $groups, 
            'companies' => $companies, 
            'professions' => $profs,
            'autos' => $autos,
            'action' => 'create',
            'work_cities' => $work_cities,
            //'cities_work' => \GuzzleHttp\json_decode($cities_work)
            //'cities_work' => $cities_work
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\admin\UserRequest $request)
    {
        $this->authorize('index', new User());

        $data = $request->all();
        //dd($data);

        //parse fio
        if( empty($data['fio']) )
            return redirect()->back()->withErrors('Имя обязательно.');
        else {
            $arrFio = explode(' ', $data['fio']);
            $data['surname'] = (null == @$arrFio[0]) ? '' : $arrFio[0];
            $data['name'] = (null == @$arrFio[1]) ? '' : $arrFio[1];
            $data['patronicname'] = (null == @$arrFio[2]) ? '' : $arrFio[2];
        }

        //cities_work
        //dd($data['city_work']);
        $cities_work = array();
        parse_str($data['str_city_work'], $cities_work);
        $data['work_regions'] = json_encode($cities_work);

        //если метро не выбрано
        if( empty($data['metro_id']) )
            $data['metro_id'] = null;

        //Штатный сотрудник
        if($request->has('staff')) {
            $data['staff'] = 1;
        }

        //dd($request->has('password'));
        if($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        }
        if($request->has('bithdate')) {
            $data['bithdate'] = Carbon::createFromFormat('d.m.Y', $data['bithdate'])->format('Y-m-d');
        }
        //компания если есть
        if($request->has('company_id') && !$request->input('company_id')) {
            $data['company_id'] = null;
        }

        //ставим компанию по владельцу
        /*if($request->has('staff')) {
            $data['company_id'] = User::find($request->input('user_id')->company->id);
        }*/

        //dd($data);
        $data['img'] = User::saveImg($request);
		$data['auto_id'] = (empty($data['auto_id']) || !$data['auto_id'] ? null : $data['auto_id']);
        $user = User::create($data);

        //logs
        Event::fire(new onUserAddEvent($user, Auth::user()));

        //связь с телефонами
        if($request->has('phone')) {
            foreach($request->input('phone') as  $phone) {
                $number = substr(preg_replace('|[^0-9]+|','',$phone), -10, 10);
                $user->phones()->create(['phone' => $phone, 'number' => $number]);
            }
        }

        //связь с тегами
        /*if($request->has('professions')) {
            $user->professions()->sync($request->input('professions'));
        }*/

        //cards
        //sber
        if( !empty($request->input('sberbank')) ) {
            $sberNumber = $request->input('sberbank');
            $card = $user->cards()->where('bank', '=', 'sberbank')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'sberbank', 'number' => $sberNumber]);
            } else if($card->number != $sberNumber) {
                $card->update(['bank' => 'sberbank', 'number' => $sberNumber]);
            }
        }

        //tinkoff
        if( !empty($request->input('tinkoff')) ) {
            $tinkoffNumber = $request->input('tinkoff');
            $card = $user->cards()->where('bank', '=', 'tinkoff')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);
            } else if($card->number != $tinkoffNumber) {
                $card->update(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);
            }
        }

        return redirect()->route('admin.users.show', $user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        if (Gate::denies('edit', $user)) {
            abort(403);
        }

        $groups = UserGroup::orderBy('name_rus')->get();
        $companies = Company::orderBy('name')->get();
        //$professions = UserProfession::orderBy('name')->get();
        $profs = [];
        foreach (UserProfession::orderBy('name')->get() as $profession)
            $profs[$profession->name] = $profession->id;

        $autos = \App\Models\Auto::orderBy('number')->with('model')->get();
        return view('admin.users.edit', [
            'user' => $user,
            'groups' => $groups,
            'companies' => $companies,
            'professions' => $profs,
            'autos' => $autos
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\admin\UserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        if (Gate::denies('edit', $user)) {
            abort(403);
        }

        $data = $request->all();
        //dd($data);

        //parse fio
        if( empty($data['fio']) )
            return redirect()->back()->withErrors('Имя обязательно.');
        else {
            $arrFio = explode(' ', $data['fio']);
            $data['surname'] = (null == @$arrFio[0]) ? '' : $arrFio[0];
            $data['name'] = (null == @$arrFio[1]) ? '' : $arrFio[1];
            $data['patronicname'] = (null == @$arrFio[2]) ? '' : $arrFio[2];
        }

        //cities_work
        //dd($data['city_work']);
        $cities_work = array();
        parse_str($data['str_city_work'], $cities_work);
        $data['work_regions'] = json_encode($cities_work);
        
        //если метро не выбрано
        if( empty($data['metro_id']) )
            $data['metro_id'] = null;

        //Штатный сотрудник
        $data['staff'] = ($request->has('staff'));

        //img
        if($img = User::saveImg($request)) {
            $data['img'] = $img;
            $user->deleteImg();
        }
        $except = [];
        //проверка прав на изменение маршрута
        if($request->has('password')) {
            $data['password'] = bcrypt($data['password']);
        } else {
            $except[] = 'password';
        }
        if($request->has('bithdate')) {
            $data['bithdate'] = Carbon::createFromFormat('d.m.Y', $data['bithdate'])->format('Y-m-d');
        }
        //компания если есть
        if(!$request->has('company_id') || !$request->input('company_id')) {
            $except[] = 'company_id';
        }

        //ставим компанию по владельцу
        /*if($request->has('staff')) {
            $data['company_id'] = User::find($request->input('user_id')->company->id);
        }*/
        
		$data['auto_id'] = (empty($data['auto_id']) || !$data['auto_id'] ? null : $data['auto_id']);

        //for logs
        $old_data = $user->getOriginal();
                
        $user->update(array_except($data, $except));

        //dd($old_metro_name, $user->metro->name);

        //связь с телефонами
        //$user->phones()->whereNotIn('id', $request->input('phone_ids'))->delete();
        $user->phones()->delete();
        if($request->has('phone')) {
            $phones_logs = '';
            if(is_array($request->input('phone'))) {
                foreach($request->input('phone') as $key => $phone) {
                    $number = substr(preg_replace('|[^0-9]+|','',$phone), -10, 10);
                    if(null !== @$request->input('phone_ids')[$key]) {
                        $user->phones()->find($request->input('phone_ids')[$key])->update(['phone' => $phone, 'number' => $number]);
                    } else {
                        $user->phones()->create(['phone' => $phone, 'number' => $number]);

                        //logs
                        if($request->input('old_phone')[$key] != $phone) {
                            $phones_logs = $phones_logs."\r\n - добавлен телефон: ".$phone.' вместо '.$request->input('old_phone')[$key];
                        }
                    }
                }
            }
        }

        //связь с тегами
        /*if($request->has('professions')) {
            $user->professions()->sync($request->input('professions'));
        } else {
            $user->professions()->detach();
        }*/


        //cards/////////////////////
        //for logs
        $old_cards = [];
        $cards_logs = '';
        foreach($user->cards as $card) {
            $old_cards[$card->bank] = $card->number;
        }

        //sber
        if( !empty($request->input('sberbank')) ) {
            $sberNumber = $request->input('sberbank');
            $card = $user->cards()->where('bank', '=', 'sberbank')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'sberbank', 'number' => $sberNumber]);

                //logs
                $cards_logs = $cards_logs."\r\n - добавлена карта сбербанка: ". $sberNumber;
            } else if($card->number != $sberNumber) {
                $card->update(['bank' => 'sberbank', 'number' => $sberNumber]);

                //logs
                if( $old_cards['sberbank'] != $sberNumber)
                    $cards_logs = $cards_logs."\r\n - изменена карта сбербанка: ". $old_cards['sberbank'].' -> '.$sberNumber;
            }
        }

        //tinkoff
        if( !empty($request->input('tinkoff')) ) {
            $tinkoffNumber = $request->input('tinkoff');
            $card = $user->cards()->where('bank', '=', 'tinkoff')->first();
            if($card == null) {
                $user->cards()->create(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);

                //logs
                $cards_logs = $cards_logs."\r\n - добавлена карта tinkoff: ". $tinkoffNumber;
            } else if($card->number != $tinkoffNumber) {
                $card->update(['bank' => 'tinkoff', 'number' => $tinkoffNumber]);

                //logs
                if( $old_cards['tinkoff'] != $tinkoffNumber)
                    $cards_logs = $cards_logs."\r\n - изменена карта tinkoff: ". $old_cards['tinkoff'].' -> '.$tinkoffNumber;
            }
        }


        //logs
        //dd($user->metro->id);
        
        //$old_data = $user->getOriginal();
        $new_data = $user->getAttributes();
        Event::fire(new onUserUpdateEvent($user, Auth::user(), $old_data, $new_data, $phones_logs, $cards_logs));

        return redirect()->route('admin.users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //доступ на удаление
        $this->authorize('delete', new User());

        User::destroy($id);
        return redirect()->route('admin.users.index')->withMessage('Пользователь удален');
    }


    /**
     * Востановление мягко удаленной категории
     * @param $id
     * @return mixed
     */
    public function restore($id) {
        //доступ на удаление
        $this->authorize('delete', new User());

        User::withTrashed()->find($id)->restore();
        return redirect()->route('admin.users.index')->withMessage('Пользователь востановлен');
    }

    /**
     * Удаление пользователя из компании
     * @param $id
     * @return mixed
     */
    public function fire($id) {
        $user = User::findOrFail($id);
        //проверка прав на удаление из компании
        if(Auth::user()->cannot('in_company', $user)) {
            abort(403);
        }
        $user->update(['company_id' => null]);

        return redirect()->route('admin.users.index')->withMessage('Пользователь удален из компании');
    }


    /**
     * Поиск по пользователям в автоподстановке
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request) {
        //dd($request->all());
        $data =[];
        if($request->has('search_param') && $request->input('search_param')) {
            $users = User::where('surname', 'LIKE', $request->input('search_param') . '%');
            if (Auth::user()->cannot('company', new User())) {
                //возможность добавить только своих или только штатных сотрудников
                $users->where(function ($query) {
                    $query->where('company_id', Auth::user()->company_id)
                        ->orWhere('company_id', null);
                });
            }
            if($request->has('type') && $request->input('type')== 'driver') {
                $users->whereHas('professions', function($query) {
                    $query->where('name', 'Водитель');
                });
            }

            /*if($request->has('block')) {
                $users->whereHas('group', function($query) use ($request) {
                    $query->whereHas('block', function($query2) use ($request) {
                        $query2->where('name', $request->input('block'));
                    });
                });
            }*/
            if($request->has('group')) {
                if( $request->input('group') == 'management' )
                    $users->whereIn('group_id', [1, 2]);
            }

            $users = $users->orderBy('surname')->take(10)->get();

            foreach ($users as $user) {
                $item = [
                    'id' => $user->id,
                    'name' => $user->fio(),
                    'auto_id' => $user->auto_id,
                    'avatar' => $user->img
                ];

                //проверяем занятость в этот день если он передан
                if($request->has('date') && $request->input('date')) {
                    $busy = \App\Models\OrderUser::where('user_id', $user->id)->whereHas('day', function($query) use ($request) {
                        $query->where('timestart', '<=', (new Carbon($request->input('date').' '.$request->input('time')))->timestamp)
                              ->where('timefinish', '>=', (new Carbon($request->input('date').' '.$request->input('time')))->timestamp);
                    })->count();
                    $item['busy'] = $busy;
                }
                //оплата труда для пользователя в этой компании за эту дату
                $income = \App\Models\UserIncome::where('user_id', $user->id)->where('action', 'income');
                if(Auth::user()->company_id) {
                    $income->where('company_id', Auth::user()->company_id);
                }
                if($request->has('date') && $request->input('date')) {
                    $income->where('date','<=', (new Carbon($request->input('date')))->format('Y.m.d'));
                }
                //оклад в день или ночь
                if($request->has('nigth') && $request->input('nigth') == 1) {
                    $income->whereNotNull('amount_nigth')->select('amount_nigth as amount');
                } else {
                    $income->whereNotNull('amount')->select('amount');
                }

                $income = $income->orderBy('date', 'desc')->first();
                $item['amount'] = empty($income) ? 0 : $income->amount;

                $data[] = $item;
            }
        }

        return response()->json($data);
    }

    /**
     * Исключить параметр из фильтра(нажали на крестик)
     * */
    public function exclude_from_filter($filter) {
        //dd(session()->get('admin.users.index.filters'));
        session()->forget("admin.users.index.filters.$filter");

        return redirect()->route('admin.users.index');
    }
}
