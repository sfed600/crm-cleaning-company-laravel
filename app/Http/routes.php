<?php
Route::get('/', ['as' => 'index', function(){     return redirect('login'); }]);
//Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

//api
Route::match(['get', 'post'], '/api/login/{user}/{pass}', ['as' => 'api.login', 'uses' => 'Auth\CleanerController@api_login']);
Route::match(['get', 'post'], '/api/tabel/{api_key}', ['as' => 'api.tabel', 'uses' => 'cleaner\TabelController@api']);

//Route::match(['get', 'post'], '/api/tabel', ['as' => 'cleaner.api', 'uses' => 'Auth\CleanerController@api']);
Route::group(['prefix' => 'api', 'middleware' => 'auth:api'], function () {
    Route::match(['get', 'post'], '/tabel', ['as' => 'cleaner.api', 'uses' => 'Auth\CleanerController@api']);
});


/*
* Админка
*/
Route::group(['prefix'=>'admin', 'middleware' => 'auth'], function()
{
    //Route::get('/main', ['as' => 'admin.main', 'uses' => 'admin\MainController@index']);
    Route::get('/main', ['as' => 'admin.main', function(){     return redirect()->route('admin.tabel'); }]);

    Route::get('/tabel', ['as' => 'admin.tabel', 'uses' => 'admin\TabelController@index']);
    Route::get('/calendar', ['as' => 'admin.calendar', 'uses' => 'admin\CalendarController@index']);
    Route::get('/calendar/exclude_from_filter/{filter}', ['uses' => 'admin\CalendarController@exclude_from_filter']);
    Route::get('/calendar/header_info', ['uses' => 'admin\CalendarController@header_info']);

    Route::get('/tabel/get_period_dates/{period}', ['uses' => 'admin\TabelController@get_period_dates']);
    Route::get('/tabel/exclude_from_filter/{filter}', ['uses' => 'admin\TabelController@exclude_from_filter']);

    Route::get('/users/trigger_online/', ['uses' => 'admin\UserController@triggerOnline']);
    Route::resource('users', 'admin\UserController');
    Route::put('/users/restore/{id}', ['as' => 'admin.users.restore', 'uses' => 'admin\UserController@restore'])->where(['id' => '[0-9]+']);
    Route::get('/users/income/{id}', ['as' => 'admin.users.income', 'uses' => 'admin\UserController@income'])->where(['id' => '[0-9]+']);
    Route::post('/users/income/{id}/store', ['as' => 'admin.users.income.store', 'uses' => 'admin\UserController@incomeStore'])->where(['id' => '[0-9]+']);
    Route::delete('/users/income/{id}/delete', ['as' => 'admin.users.income.delete', 'uses' => 'admin\UserController@incomeDelete'])->where(['id' => '[0-9]+']);
    Route::get('/users/outcome/{id}', ['as' => 'admin.users.outcome', 'uses' => 'admin\UserController@outcome'])->where(['id' => '[0-9]+']);
    Route::post('/users/outcome/{id}/store', ['as' => 'admin.users.outcome.store', 'uses' => 'admin\UserController@outcomeStore'])->where(['id' => '[0-9]+']);
    Route::delete('/users/outcome/{id}/delete', ['as' => 'admin.users.outcome.delete', 'uses' => 'admin\UserController@outcomeDelete'])->where(['id' => '[0-9]+']);
    Route::post('/users/outcome/{id}/mass', ['as' => 'admin.users.outcome.mass', 'uses' => 'admin\UserController@outcomeStoreMass'])->where(['id' => '[0-9]+']);
    Route::post('/users/search', ['as' => 'admin.users.search', 'uses' => 'admin\UserController@search']);
    Route::get('/users/fire/{id}', ['as' => 'admin.users.fire', 'uses' => 'admin\UserController@fire'])->where(['id' => '[0-9]+']);
    Route::get('/users/comments/{id}', ['as' => 'admin.users.comments', 'uses' => 'admin\UserController@comments'])->where(['id' => '[0-9]+']);
    Route::post('/users/comments/{id}/store', ['as' => 'admin.users.comments.store', 'uses' => 'admin\UserController@commentsStore'])->where(['id' => '[0-9]+']);
    Route::get('/users/exclude_from_filter/{filter}', ['uses' => 'admin\UserController@exclude_from_filter']);
    Route::post('/users/apply_changes/', ['uses' => 'admin\UserController@apply_changes']);
    Route::post('/users/accruals', ['uses' => 'admin\UserController@accruals']);
    Route::post('/users/review_add', ['uses' => 'admin\UserController@review_add']);
    Route::post('/users/review_update', ['uses' => 'admin\UserController@review_update']);
    Route::post('/users/review_delete', ['uses' => 'admin\UserController@review_delete']);

    Route::get('/company/fill_by_inn', ['uses' => 'admin\CompanyController@fill_by_inn']);
    Route::get('/companies/search', ['as' => 'admin.companies.search', 'uses' => 'admin\CompanyController@search']);
    Route::resource('companies', 'admin\CompanyController');

    Route::resource('services', 'admin\ServiceController');

    Route::resource('profession', 'admin\ProfessionController');

    Route::resource('products', 'admin\ProductController');

    Route::get('/offers/downloadPDF/{id}', ['as' => 'admin.offers.pdf', 'uses' => 'admin\OfferController@downloadPDF']);
    Route::resource('offers', 'admin\OfferController');

    Route::get('/invoices/downloadPDF/{id}', ['as' => 'admin.invoices.pdf', 'uses' => 'admin\InvoiceController@downloadPDF']);
    Route::get('/invoices/create/{order_id}', ['as' => 'admin.invoices.create', 'uses' => 'admin\InvoiceController@create']);
    Route::resource('invoices', 'admin\InvoiceController', ['except' => ['create']]);

    Route::resource('auto', 'admin\AutoController');
    Route::resource('auto_models', 'admin\AutoModelController');

    Route::get('/sms', ['as' => 'admin.sms.index', 'uses' => 'admin\SmsController@index']);
    Route::get('/sms/{id}', ['as' => 'admin.sms.show', 'uses' => 'admin\SmsController@show'])->where(['id' => '[0-9]+']);

    Route::get('/admin/orders/create/{?company_id}', ['uses' => 'admin\OrderController@create']);
    Route::get('/admin/orders/create/{?contact_id}', ['uses' => 'admin\OrderController@create']);

    Route::resource('orders', 'admin\OrderController', ['except' => ['show']]);

    Route::put('/orders/restore/{id}', ['as' => 'admin.orders.restore', 'uses' => 'admin\OrderController@restore'])->where(['id' => '[0-9]+']);
    Route::put('/orders/staff/{id}', ['as' => 'admin.orders.staff', 'uses' => 'admin\OrderController@staff'])->where(['id' => '[0-9]+']);
    Route::post('/orders/sms', ['as' => 'admin.orders.sms', 'uses' => 'admin\OrderController@sendSms']);
    Route::post('/orders/cancel', ['as' => 'admin.orders.cancel', 'uses' => 'admin\OrderController@cancel']);
    Route::post('/orders/late', ['as' => 'admin.orders.late', 'uses' => 'admin\OrderController@late']);
    Route::get('/orders/exclude_from_filter/{filter}', ['uses' => 'admin\OrderController@exclude_from_filter']);
    Route::post('/orders/set_status', ['as' => 'admin.orders.set_status', 'uses' => 'admin\OrderController@set_status']);
    Route::match(['get', 'post'], '/orders/turn/{length}/{id?}', ['as' => 'admin.orders.turn', 'uses' => 'admin\OrderController@turn']);
    Route::post('/orders/delete_contact', ['uses' => 'admin\OrderController@delete_contact']);
    Route::post('/orders/save_periods', ['uses' => 'admin\OrderController@save_periods']);
    Route::post('/orders/update_ajax', ['uses' => 'admin\OrderController@update_ajax']);
    Route::post('/orders/attach_user', ['uses' => 'admin\OrderController@attach_user']);
    Route::post('/orders/detach_user', ['uses' => 'admin\OrderController@detach_user']);
    Route::post('/orders/create_order_company', ['uses' => 'admin\OrderController@create_order_company']);
    Route::post('/orders/update_calculator', ['uses' => 'admin\OrderController@update_calculator']);
    Route::get('/orders/allCountOfDay', ['uses' => 'admin\OrderController@allCountOfDay']);
    Route::post('/orders/delete_order_company', ['uses' => 'admin\OrderController@delete_order_company']);
    
    Route::post('/metro', ['as' => 'admin.metro.search', 'uses' => 'admin\OrderController@searchMetro']);

    Route::post('/order_companies/update_ajax', ['uses' => 'admin\OrderCompanyController@update_ajax']);
    Route::get('/order_companies/search', ['as' => 'admin.order_companies.search', 'uses' => 'admin\OrderCompanyController@search']);
    Route::resource('order_companies', 'admin\OrderCompanyController');

    Route::post('/order_contacts/update_ajax', ['uses' => 'admin\OrderContactController@update_ajax']);
    Route::post('/order_contacts/attach_to_order', ['uses' => 'admin\OrderContactController@attach_to_order']);
    Route::get('/order_contacts/search', ['as' => 'admin.order_contacts.search', 'uses' => 'admin\OrderContactController@search']);
    Route::resource('order_contacts', 'admin\OrderContactController');
    Route::post('/order_contacts/search', ['as' => 'admin.order_contacts.search', 'uses' => 'admin\OrderContactController@search']);
    Route::get('/order_contacts/get_order_company/{contact_id}', ['uses' => 'admin\OrderContactController@get_order_company']);
    Route::post('/order_contacts/delete_order_company', ['uses' => 'admin\OrderContactController@delete_order_company']);
    
    Route::resource('pages', 'admin\PageController', ['except' => ['show']]);
    Route::get('/pages/{sysname}/edit_sysname', ['as' => 'admin.pages.edit_sysname', 'uses' => 'admin\PageController@editSysname'])->where(['sysname' => '[a-zA-Z0-9_-]+']);
    Route::put('/pages/{sysname}/content', ['as' => 'admin.pages.update_content', 'uses' => 'admin\PageController@updateContent'])->where(['sysname' => '[a-zA-Z0-9_-]+']);

    Route::resource('metatags', 'admin\MetatagsController', ['except' => ['show']]);
    Route::get('/metatags/{route}/edit_route', ['as' => 'admin.metatags.edit_route', 'uses' => 'admin\MetatagsController@editRoute'])->where(['route' => '[\.a-zA-Z0-9_-]+']);

    Route::post('/settings/price', ['as' => 'admin.settings.price', 'uses' => 'admin\SettingController@price']);
    Route::resource('settings', 'admin\SettingController', ['except' => ['show']]);

    Route::get('/image/crop', ['as' => 'admin.image.crop', 'uses' => 'admin\MainController@crop']);
    Route::post('/image/crop', ['as' => 'admin.image.crop.save', 'uses' => 'admin\MainController@cropUpdate']);

    Route::post('/editor/upload', ['as' => 'editor.upload', 'uses' => 'admin\MainController@uploadFileCKeditor'] );

});

// auth
Route::auth();
Route::match(['get', 'head'], '/login', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@showLoginForm']);

//Route::match(['get', 'head'], '/register', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::match(['get', 'post'], '/inn', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@inn']);
Route::match(['get', 'post'], '/smsVerify', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@smsVerify']);
//Route::match(['get', 'post'], '/register_company', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@registrCompany']);
Route::match(['get', 'post'], '/register_company', ['middleware' => 'guest', 'uses' => 'Auth\AuthNewController@registerCompany']);
Route::match(['get', 'post'], '/activateEmail', ['middleware' => 'guest', 'uses' => 'Auth\AuthController@activateEmail', 'as' => 'auth.activate.email']);

//Route::match(['get', 'post'], '/resetPass', ['middleware' => 'guest', 'uses' => 'Auth\CleanerController@getPassFromSms']);
Route::match(['get', 'post'], '/cleaner/password/reset', ['middleware' => 'guest', 'as' => 'cleaner.password.reset', 'uses' => 'Auth\CleanerController@password_reset']);
//Route::match(['get', 'post'], '/cleaner/api', ['middleware' => 'guest', 'as' => 'cleaner.api', 'uses' => 'Auth\CleanerController@api']);


/*
* Cleaner
*/

Route::match(['get', 'head'], '/cleaner', ['as' => 'cleaner', 'middleware' => 'guest', 'uses' => 'Auth\CleanerController@loginForm']);
Route::post('/cleaner/login', ['as' => 'cleaner.login', 'uses' => 'Auth\CleanerController@login']);

Route::group(['prefix'=>'cleaner', 'middleware' => 'auth'], function()
{
    Route::get('/main', ['middleware' => 'auth', 'as' => 'cleaner.main', function(){ return redirect()->route('cleaner.tabel'); }]);
    //Route::get('/tabel', ['middleware' => 'auth', 'as' => 'cleaner.tabel', 'uses' => 'cleaner\TabelController@index']);
    Route::get('/tabel/{mobile?}', ['middleware' => 'auth', 'as' => 'cleaner.tabel', 'uses' => 'cleaner\TabelController@index']);
    //Route::get('/mtabel', ['middleware' => 'auth', 'as' => 'cleaner.mtabel', 'uses' => 'cleaner\TabelController@index']);
    
    Route::get('/mutual_settlements', ['as' => 'cleaner.mutual_settlements', 'uses' => 'cleaner\CommonController@mutual_settlements']);
    Route::get('/calendar', ['as' => 'cleaner.calendar', 'uses' => 'cleaner\CommonController@calendar']);

    Route::match(['get', 'post'], '/anketa/edit', ['middleware' => 'auth', 'as' => 'cleaner.anketa.edit', 'uses' => 'cleaner\AnketaController@edit']);
    Route::get('/anketa/show', ['as' => 'cleaner.anketa.view', 'uses' => 'cleaner\AnketaController@show']);

    Route::get('/departures', ['as' => 'cleaner.departures', 'uses' => 'cleaner\DepartureController@index']);
    Route::get('/departures/current', ['as' => 'cleaner.departures.current', 'uses' => 'cleaner\DepartureController@current']);

    Route::get('/orders', ['as' => 'cleaner.orders', 'uses' => 'cleaner\OrderController@index']);
    Route::get('/orders/view', ['as' => 'cleaner.orders.view', 'uses' => 'cleaner\OrderController@view']);
});