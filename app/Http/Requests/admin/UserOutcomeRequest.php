<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

class UserOutcomeRequest extends Request
{
    private $rules = [
        'date' => 'required|date',
        'outcome' => 'required|numeric',
        'type' => 'required',
    ];

    public function validate() {
        $this->prepareForValidation();
        parent::validate();
    }


    protected function prepareForValidation() {
        $this->rules['type'] = 'required|in:'.implode(',', array_keys(\App\Models\UserIncome::$types));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }
}
