<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

use Auth;

class OfferRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|unique:offers,number,'.$this->route('offers').',id,company_id,'.Auth::user()->company_id,
            //'date_start' => 'required|date',
            'name' => 'required',
            'area' => 'integer',
            'amount' => 'integer',
            'user_id' => 'required|exists:users,id',
            'status' => 'required',

            //'company_id' => 'required|exists:companies,id',
            'order_company_id' => 'exists:order_companies,id',
            /*'timestart' => 'required|max:5',
            'timefinish' => 'required|max:5',*/
            'date_start' => 'date',
            'date_finish' => 'date',
            'metro_id' => 'exists:metro,id',
            'address' => 'max:255',
            'apartment' => 'max:4',
            'porch' => 'max:4',
            'floor' => 'max:3',
            'domofon' => 'max:5',
        ];
    }
}
