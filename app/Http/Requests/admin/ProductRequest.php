<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

class ProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tip' => 'required|in:"product", "service"',
            'name' => 'required|max:255',
            'full_name' => 'max:255',
            'articul' => 'string|max:35',
            'measure' => 'string|max:10',
            'nds' => 'string|max:10',
            'price' => 'required',
            'company_id' => 'required|exists:companies,id',
            'comment' => 'string|max:350',
        ];
    }
}
