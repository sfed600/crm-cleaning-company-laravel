<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

class UserIncomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'amount' => 'required_without:amount_nigth|numeric',
            'amount_nigth' => 'required_without:amount|numeric',
        ];
    }
}
