<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;
use Auth;

class OrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|unique:orders,number,'.$this->route('orders').',id,company_id,'.Auth::user()->company_id,
            //'date' => 'required|date',
            'name' => 'required',
            'amount' => 'integer',
            'user_id' => 'required|exists:users,id',
            'status' => 'required',
            'contact_ids' => 'required',
        ];
    }

    public function messages() {
        return [
            'user_id.required' => 'Поле "ответственный" обязательно для заполнения.',
            'contact_ids.required' => 'Введите ФИО контакта.',
        ];
    }
}
