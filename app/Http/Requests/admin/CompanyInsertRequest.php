<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

class CompanyInsertRequest extends Request
{

    private $rules = [
        'company' => 'required',
        'surname' => 'required|max:255',
        'email' => 'email|max:255',
        'password' => 'min:6|confirmed',

    ];

    public function validate() {
        $this->prepareForValidation();
        parent::validate();
    }


    protected function prepareForValidation() {
        $this->rules['name'] = 'required|max:255|unique_with:users,surname,'.Request::input('surname').','.$this->route('users');
        $this->rules['inn'] = 'required|regex:/^\d{10,12}$/|unique:companies,inn,'.$this->route('companies');
        $this->rules['phones.*'] = 'required|unique:user_phones,phone';
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages() {
        return [
            'name.unique_with' => 'Фамилия и Имя должны быть уникальны'
        ];
    }

}
