<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;
use Auth;

class UserRequest extends Request
{
    private $rules = [
        'group_id' => 'required|exists:user_groups,id',
        'company_id' => 'sometimes|required|exists:companies,id',
        //'surname' => 'required|max:255',
        //'email' => 'email|max:255',
        'password' => 'min:6|confirmed',
        'bithdate' => 'date',
        'img' => 'image',
        //'passport' => 'regex:/^[0-9]{10}$/',
    ];

    public function validate() {
        $this->prepareForValidation();
        parent::validate();
    }


    protected function prepareForValidation() {
        //$this->rules['name'] = 'required|max:255|unique_with:users,surname,'.Request::input('surname').','.$this->route('users');
        $this->rules['email'] = 'required|max:255|unique_with:users,email,'.Request::input('email').','.$this->route('users');
        if(!$this->route('users')) {
            $this->rules['phones.*'] = 'required|unique:user_phones,phone';
        }
        //если отмечен сотрудник штатный, то устанавливаем компанию
        if(!$this->request->has('company_id') || !$this->request->get('company_id')) {
            if($this->request->has('staff') && $this->request->get('staff')) {
                $this->request->set('company_id', Auth::user()->company_id);
                //определяем блок польователей
            } elseif($this->request->has('group_id') && $this->request->get('group_id')) {
                //если это Менеджемент, то проставляем компанию автоматом
                $group = \App\Models\UserGroup::findOrFail($this->request->get('group_id'));
                if($group->block->name == 'Management') {
                    $this->request->set('company_id', Auth::user()->company_id);
                //если Персонал то удаляем компанию совсем
                } elseif ($group->block->name == 'Workers' || $group->block->name == 'SuperAdmin') {
                    $this->request->remove('company_id');
                }
            }
        }

        if (Auth::user()->cannot('superAdmin', new \App\User())) {
            $groups = \App\Models\UserGroup::where('name', '!=', 'superAdmin')->get();
            $this->rules['group_id'] = 'required|in:'.$groups->implode('id', ',');
        }

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public function messages() {
        return [
            'name.unique_with' => 'Фамилия и Имя должны быть уникальны',
            'email.unique_with' => 'Email уже существует'
        ];
    }



}
