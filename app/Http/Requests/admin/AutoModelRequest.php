<?php

namespace App\Http\Requests\admin;

use App\Http\Requests\Request;

class AutoModelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' => 'required|unique:auto_models,name,'.$this->route('auto_models')
        ];
    }
}
