<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Image;
use Date;
use Lang;
use Carbon\Carbon;

class User extends Authenticatable
{
    use SoftDeletes;

    public static $img_path = 'assets/imgs/users/';
    public static $img_path_preview = 'assets/imgs/users/preview/';

    protected static $size_preview_width = 180;
    protected static $size_preview_height = 200;

    protected $dates = ['deleted_at'];

    protected $with = ['group'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'group_id', 'name', 'surname', 'patronicname', 'email', 'password', 'gender', 'img',
        'card', 'citizenship', 'bithdate', 'status', 'passport', 'auto_id', 'address', 'staff', 'work_regions', 'apartment',
        'porch', 'floor', 'domofon', 'note', 'metro_id', 'api_token', 'profession_id', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public static $citizenships = ['Россия' , 'Украина', 'Молдова', 'Кыргыстан', 'Узбекистан', 'Другое'];

    public function group()
    {
        return $this->belongsTo('App\Models\UserGroup', 'group_id');
    }

    public function metro() {
        return $this->belongsTo('App\Models\Metro');
    }

    /*public function responsible()
    {
        return $this->belongsTo('App\User', 'user_id');
    }*/

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public static function saveImg($request = null, $url = null) {
        //URL в приоритете
        if(!empty($url)) {
            if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                return '';
            }
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            $filename = time().'_'.str_random(5).'.'.$ext;

            Storage::disk('public')->put(self::$img_path.$filename, file_get_contents($url));
            $sizes = getimagesize(public_path(self::$img_path).$filename);
        } else {
            if(!$request->hasFile('img') || !$request->file('img')->isValid()) {
                return '';
            }

            $sizes = getimagesize($request->file('img')->getRealPath());
            $filename = time().'_'.str_random(5).'.'.$request->file('img')->getClientOriginalExtension();
            $request->file('img')->move(public_path(self::$img_path), $filename);
        }
        //preview
        Image::make(public_path(self::$img_path).$filename)
            ->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
                $constraint->upsize();
            })
            ->save(public_path(self::$img_path_preview).$filename);
        return $filename;
    }

    public function deleteImg() {
        if(!$this->img) {
            return false;
        }

        Storage::disk('public')->delete([
            $this->getImgPath().$this->img,
            $this->getImgPreviewPath().$this->img,
        ]);
        return true;
    }

    public function getImgPath(){
        return self::$img_path;
    }

    public function getImgPreviewPath(){
        return self::$img_path_preview;
    }

    public function getPreviewSize($attribute = 'width'){
        $attribute = 'size_preview_'.$attribute;
        return self::$$attribute;
    }

    public function datePicker() {
        if($this->bithdate && $this->bithdate!='0000-00-00') {
            return (new Date($this->bithdate))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function reviews() {
        return $this->hasMany('App\Models\UserReview', 'user_id');
    }
    
    public function phones() {
        return $this->hasMany('App\Models\UserPhone', 'user_id');
    }

    public function cards() {
        return $this->hasMany('App\Models\UserCard', 'user_id');
    }

    public function pasport() {
        return $this->hasOne('App\Models\UserPasport', 'user_id');
    }

    public function last_visit() {
        return $this->hasOne('App\Models\UserOnline', 'user_id');
    }

    public function orders() {
        return $this->hasMany('App\Models\OrderUser', 'user_id');
    }

    public function days() {
        return $this->belongsToMany('App\Models\OrderDay', 'order_users', 'user_id', 'day_id')->withPivot('cef', 'amount');
    }

    public function comments() {
        return $this->hasMany('App\Models\UserComment', 'user_id');
    }

    public function professions()
    {
        return $this->belongsToMany('App\Models\UserProfession', 'user_profession', 'user_id', 'profession_id');
    }

    /*public function profession(){
        return $this->belongsTo('App\Models\UserProfession', 'profession_id');
    }*/

    public function auto(){
        return $this->belongsTo('App\Models\Auto', 'auto_id');
    }

    public function fio() {
        return $this->surname.($this->name ? ' '.$this->name : '').($this->patronicname ? ' '.$this->patronicname : '');
    }

    public function genderName() {
        return $this->gender == 'male' ? 'Мужчина' : 'Женщина';
    }

    public function age() {
        if(!$this->bithdate || $this->bithdate == '0000-00-00') {
            return '';
        }
        $age = date('Y') - (new Carbon($this->bithdate))->year;
        if ($age) {
            return $age . ' ' . Lang::choice('год|года|лет', $age, [], 'ru');
        }
        return '';
    }

    public function rating() {
        $this->orders_cnt = $this->orders->count();
        $this->orders_late = $this->orders->sum('late');
        $this->orders_nocome = $this->orders->sum('nocome');
        $this->rating = $this->orders_cnt ? round(($this->orders_cnt*100 - $this->orders_late - ($this->orders_nocome * 10)) / $this->orders_cnt, 2) : 0;

        return $this->rating;
    }

    //вычисляем баланс
    public function balance($filters)
    {
        $balance = 0;
        /*if(!empty($filters['company'])) {
            $balance = \App\Models\OrderUser::where('user_id', $this->id)->whereHas('day.order', function ($query) use ($filters) {
                $query->where('company_id', $filters['company']);
            })->sum('amount');
            $balance -= \App\Models\UserIncome::where('user_id', $this->id)->where('company_id', $filters['company'])->sum('outcome');
        }*/
        $balance = \App\Models\OrderUser::where('user_id', $this->id)->whereHas('day.order', function ($query) use ($filters) {
            //$query->where('company_id', $filters['company']);
        })->sum('amount');
        $balance -= \App\Models\UserIncome::where('user_id', $this->id)->sum('outcome');
        
        return $balance;
    }

}