<?php

namespace App\Policies;

use App\Models\Offer;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use Auth;

class OfferPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        //
    }

    public function before(User $user, $ability) {
        return ( in_array($user->group->name, ['Admin', 'Moderator']) && Auth::user()->company_id > 0  );
    }

}
