<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function index(User $user) {
        return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
    }

    public function delete(User $user) {
        return in_array($user->group->name, ['Admin', 'SuperAdmin']);
    }

    public function show(User $user, User $usernow) {
        if($user->group->block->name == 'Workers') {
            return $usernow->id == $user->id;
        } elseif(in_array($user->group->name, ['Admin', 'Moderator'])) {
            //return $user->company_id == $usernow->company_id || ($usernow->company_id === null && $usernow->group->block->name == 'Workers');
            return ($user->company_id == $usernow->company_id) || $usernow->company_id == null || $usernow->staff != 1;
        } elseif($user->group->block->name == 'SuperAdmin') {
            return true;
        }
        return false;
    }

    public function review(User $author, User $user) {
        if(in_array($author->group->name, ['Admin', 'Moderator'])) {
            return ($user->company_id == $author->company_id) || $user->company_id == null || $user->staff != 1;
        } 
        
        return false;
    }

    public function edit(User $user, User $usernow) {
        if(in_array($user->group->name, ['Admin', 'Moderator'])) {
            //return $user->company_id == $usernow->company_id || ($usernow->company_id === null && $usernow->group->block->name == 'Workers');
            return ($user->company_id == $usernow->company_id) || $usernow->company_id === null || $usernow->staff != 1;
        } elseif($user->group->block->name == 'SuperAdmin') {
            return true;
        }
        return false;
    }

    public function income(User $user) {
        return in_array($user->group->name, ['Admin']);
    }

    public function company(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function superAdmin(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function in_company(User $user, User $usernow) {
        if($user->group->block->name == 'Management' && $usernow->group->block->name == 'Workers') {
            return  $user->company_id && $user->company_id == $usernow->company_id;
        }
        return false;
    }

    public function management(User $user) {
        return $user->group->block->name == 'Management' ;
    }

    public function mycompany(User $user) {
        return in_array($user->group->block->name, ['SuperAdmin', 'Workers']);
    }



}
