<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class SmsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user) {
    }

    public function before(User $user, $ability) {
        return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
    }

}
