<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /*public function index(User $user) {}

    public function before(User $user, $ability) {
        return in_array($user->group->name, ['SuperAdmin']);
    }*/

    public function index(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function update(User $user, \App\Models\Company $company)
    {   //dump($company->id); dd();
        return (in_array($user->group->name, ['Admin', 'Moderator']) OR in_array($user->group->name, ['SuperAdmin']) );
    }
    /*public function setting(User $user) {
        return ( ($user->company_id > 0) && in_array($user->group->name, ['Admin', 'Moderator']) );
    }*/

}
