<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class UserProfessionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /*public function index(User $user) {
        return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
    }*/

    public function before(User $user, $ability) {
        //return in_array($user->group->name, ['SuperAdmin']);
        return in_array($user->group->name, ['Admin', 'Moderator']);
    }

}
