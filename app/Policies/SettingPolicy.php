<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class SettingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user) {
        //return in_array($user->group->name, ['SuperAdmin']);
        return (in_array($user->group->name, ['Admin', 'Moderator']) && $user->company_id > 0 );
    }

    public function add(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function delete(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function name(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

    public function price(User $user) {
        //return in_array($user->group->name, ['SuperAdmin']);
        return (in_array($user->group->name, ['Admin', 'Moderator']) && $user->company_id > 0 );
    }
}
