<?php

namespace App\Policies;

use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user) {
        return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
    }

    public function add(User $user) {
        return in_array($user->group->name, ['Admin', 'Moderator']);
        //return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
    }

    public function edit(User $user, Order $order) {
        if(in_array($user->group->name, ['Admin', 'Moderator'])) {
            return $user->company_id == $order->company_id;
        } elseif($user->group->block->name == 'SuperAdmin') {
            return true;
        }
        return false;
    }

    public function delete(User $user, Order $order) {
        if($user->group->block->name == 'Admin') {
            return $user->company_id == $order->company_id;
        } elseif($user->group->block->name == 'SuperAdmin') {
            return true;
        }
        return false;
    }

    public function company(User $user) {
        return in_array($user->group->name, ['SuperAdmin']);
    }

}
