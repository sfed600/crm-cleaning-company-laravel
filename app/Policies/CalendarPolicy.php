<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class CalendarPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user) {
        //return in_array($user->group->name, ['Admin', 'Moderator', 'SuperAdmin']);
        return in_array($user->group->name, ['Admin', 'Moderator']);
    }
}
