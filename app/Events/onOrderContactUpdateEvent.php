<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class onOrderContactUpdateEvent extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\OrderContact $contact, \App\User $user, $old_data, $new_data, $relations_log)
    {
        $this->contact = $contact;

        $this->old_data = $old_data;
        $this->new_data = $new_data;
        $this->relations_log = $relations_log;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
