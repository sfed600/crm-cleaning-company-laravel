<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class onUserUpdateEvent extends Event
{
    use SerializesModels;
    
    public $user;
    public $responsible;
    public $old_data;
    public $new_data;
    public $phones_logs;
    public $cards_logs;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\User $user, \App\User $responsible, $old_data, $new_data, $phones_logs='', $cards_logs='')
    {
        $this->user = $user;
        $this->responsible = $responsible;

        $this->old_data = $old_data;
        $this->new_data = $new_data;

        /*$this->old_phones = $old_phones;
        $this->new_phones = $new_phones;*/
        $this->phones_logs = $phones_logs;

        $this->cards_logs = $cards_logs;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
