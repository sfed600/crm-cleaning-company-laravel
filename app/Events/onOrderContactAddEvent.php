<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class onOrderContactAddEvent extends Event
{
    use SerializesModels;

    public $contact;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\OrderContact $contact, \App\User $user)
    {
        $this->contact = $contact;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
