<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class onOrderUpdateEvent extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Order $order, \App\User $user, $old_data, $new_data, $old_responsibles, $new_responsibles, $old_contacts, $new_contacts)
    {
        $this->order = $order;

        $this->old_data = $old_data;
        $this->new_data = $new_data; 
        $this->old_responsibles = $old_responsibles;
        $this->new_responsibles = $new_responsibles;
        $this->old_contacts = $old_contacts;
        $this->new_contacts = $new_contacts;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
