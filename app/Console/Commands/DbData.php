<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;


class DbData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //проставляем всем пользователям с пустым паролем, пароль 1111 по умолчанию

        $password = '1111';
        $users = User::where('password', '')->get();
        if($users->count()) {
            foreach($users as $user) {
                $user->update(['password' => bcrypt($password)]);
            }
        }

        \App\Models\UserIncome::where('amount', '>', 0)->update(['action' => 'income']);
        \App\Models\UserIncome::where('amount', '=', 0)->where('outcome', '=', 0)->update(['action' => 'income']);


        //проставляем заработки за день, в пересчете из ЗП и кэфа
        $shifts = \App\Models\OrderUser::where(function ($query){
            $query->where('amount', 0)->orWhere('amount', null);
        })->get();

        foreach($shifts as $shift) {
            $income = \App\Models\UserIncome::where('user_id', $shift->user_id)
                        ->where('action','income')
                        ->where('date','<=', $shift->day->date)
                        ->orderBy('date', 'desc')
                        ->first();
            if(!empty($income)) {
                $amount = intval($shift->cef * $income->amount);

                $shift->update(['amount' => $amount]);
            }
        }
    }
}
