<?php

namespace App\Listeners;

use App\Events\onUserUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use Lang;

class UpdateUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onUserUpdateEvent  $event
     * @return void
     */
    public function handle(onUserUpdateEvent $event)
    {
        $changed = false;
        $log_detail = '';
        $diff = [];
        //dd($event->old_data, $event->new_data);
        foreach ($event->new_data as $key => $val)
        {
            if($key == 'updated_at')
                continue;
            else if($key == 'bithdate') {
                if(empty($event->new_data[$key]))
                    continue;
            }
            else if($key == 'metro_id' && ( $event->new_data[$key] != $event->old_data[$key]) ) {
                $changed = true;

                $diff[$key] = \App\Models\Metro::find($event->old_data[$key])->name.' -> '.\App\Models\Metro::find($event->new_data[$key])->name;
            }
            else if($key == 'group_id' && ( $event->new_data[$key] != $event->old_data[$key]) ) {
                $changed = true;

                $diff[$key] = \App\Models\UserGroup::find($event->old_data[$key])->name.' -> '.\App\Models\UserGroup::find($event->new_data[$key])->name;
            }
            else if($key == 'profession_id' && ( $event->new_data[$key] != $event->old_data[$key]) ) {
                $changed = true;

                $diff[$key] = \App\Models\UserProfession::find($event->old_data[$key])->name.' -> '.\App\Models\UserProfession::find($event->new_data[$key])->name;
            }
            else if($key == 'company_id' && ( $event->new_data[$key] != $event->old_data[$key]) ) {
                $changed = true;

                $diff[$key] = @\App\Models\Company::find($event->old_data[$key])->name.' -> '.@\App\Models\Company::find($event->new_data[$key])->name;
            }
            else if($key == 'staff' && ( intval($event->new_data[$key]) != intval($event->old_data[$key])) ) {
                $changed = true;

                if( intval($event->new_data[$key]) > 0 ) $diff[$key] = ' штатный';
                else $diff[$key] = ' внештатный';
            }
            else if($key == 'gender' && ( $event->new_data[$key] != $event->old_data[$key]) ) {
                $changed = true;

                if( $event->new_data[$key] == 'female' ) $diff[$key] = ' женский';
                else $diff[$key] = ' мужской';
            }
            else if($key == 'work_regions') {
                if($event->old_data[$key] == '[]') {
                    $old_city_work = [];
                    $old_region_work = [];
                } else {
                    $old_city_work = @\GuzzleHttp\json_decode($event->old_data[$key], true)['city_work'];
                    $old_region_work = @\GuzzleHttp\json_decode($event->old_data[$key], true)['region_work'];
                }

                if($event->new_data[$key] == '[]') {
                    $new_city_work = [];
                    $new_region_work = [];
                } else {
                    $new_city_work = @\GuzzleHttp\json_decode($event->new_data[$key], true)['city_work'];
                    $new_region_work = @\GuzzleHttp\json_decode($event->new_data[$key], true)['region_work'];
                }

                $old_city_work = ($old_city_work) ? $old_city_work : [];
                $new_city_work = ($new_city_work) ? $new_city_work : [];
                $old_region_work = ($old_region_work) ? $old_region_work : [];
                $new_region_work = ($new_region_work) ? $new_region_work : [];

                if( !empty(array_diff($new_city_work, $old_city_work))
                    || !empty(array_diff($old_city_work, $new_city_work))
                    || !empty(array_diff($old_region_work, $new_region_work))
                    || !empty(array_diff($new_region_work, $old_region_work)))
                {
                    $changed = true;
                    $diff[$key] = 'изменены';
                }
            }
            else if( $event->new_data[$key] != $event->old_data[$key] )
            {
                $changed = true;

                if($key == 'img') $diff[$key] = '';
                else $diff[$key] = $event->old_data[$key]." -> ".$event->new_data[$key];
            }
        }

        //dump($diff); dd(sizeof($diff));
        //dump($event->new_data, $event->old_data); dd(sizeof($diff));
        if(sizeof($diff) > 0) {
            foreach($diff as $key=>$val) {
                $log_detail = $log_detail."\r\n - ".Lang::get('user.'.$key).": ".$val;
            }
        }

        //phones
        if(!empty($event->phones_logs)) {
            $log_detail = $log_detail.$event->phones_logs;
            $changed = true;
        }

        //cards
        if(!empty($event->cards_logs)) {
            $log_detail = $log_detail . $event->cards_logs;
            $changed = true;
        }

        /*$phone_changes = false;
        foreach($event->new_phones as $new_phone) {
            $phone_changes = true;
            foreach($event->old_phones as $old_phone) {
                if($old_phone->number == $new_phone->number) {
                    $phone_changes = false;
                    break;
                }
            }
            if($phone_changes) {
                $log_detail = $log_detail."\r\n - добавлен телефон - ".$new_phone->phone;
            }
        }*/

        if( $changed === true) {
            $data = [
                'user_id' => $event->user->id,
                'responsible' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($event->user->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил сотрудника: '.$event->user->fio().$log_detail
            ];
            $userLog = \App\Models\UserLog::create($data);
        }
    }
}
