<?php

namespace App\Listeners;

use App\Events\onOrderContactAddEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class AddOrderContactListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderContactAddEvent  $event
     * @return void
     */
    public function handle(onOrderContactAddEvent $event)
    {
        $data = [
            'contact_id' => $event->contact->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($event->contact->created_at)).' <b>'.strtoupper(Auth::user()->fio()).'</b> Контакт создан: '.$event->contact->name
        ];
        \App\Models\OrderContactLog::create($data);
    }
}
