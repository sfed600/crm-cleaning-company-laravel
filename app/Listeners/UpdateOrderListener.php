<?php

namespace App\Listeners;

use App\Events\onOrderUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class UpdateOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderUpdateEvent  $event
     * @return void
     */
    public function handle(onOrderUpdateEvent $event)
    {   
        $changed = false;
        $log_detail = '';
        $diff_main = $diff_calculator = [];
        //dd($event->new_data);
        foreach ($event->new_data as $key=>$val)
        {
            //dump($key); //dd();
            if($key == 'updated_at')
                continue;
            else if( $event->new_data[$key] != $event->old_data[$key] )
            {
                if( ($key == 'date' || $key == 'date_finish') ) {
                    //dump($event->new_data['date']); //dd();
                    if( str_replace(['.', '-'], '', $event->old_data[$key]) != str_replace(['.', '-'], '', $event->new_data[$key]) ) {
                        $changed = true;
                        $diff_main[$key] = $event->new_data[$key];
                        //dd($event->old_data['date_finish']);
                        $log_detail = $log_detail."\r\n - ".trans("order.".$key).": ".$event->old_data[$key]." - ".$event->new_data[$key];
                    }
                }elseif($key == 'calculator') {
                    $calculator_old = @\GuzzleHttp\json_decode($event->old_data[$key]);
                    $calculator_new = @\GuzzleHttp\json_decode($event->new_data[$key]);
                    $calculator_old = @get_object_vars($calculator_old);
                    $calculator_new = @get_object_vars($calculator_new);

                    //dump($calculator_new, $calculator_old); dd();
                    $diff_calculator = $this->compare_calculator_data($calculator_old, $calculator_new);
                    //dd($diff_calculator);
                }
                else {
                    $changed = true;
                    $diff_main[$key] = $event->new_data[$key];
                    /*if(!is_array($event->old_data[$key]) && !is_array($event->new_data[$key])) {
                        $log_detail = $log_detail."\r\n - $key: @$event->old_data[$key] - @$event->new_data[$key]";
                    }
                    else {
                        if( sizeof($event->new_contacts[$key]) >= sizeof($event->old_contacts[$key])  ) {
                            $log_detail = $log_detail."\r\n - $key: @$event->old_data[$key] - @$event->new_data[$key]";
                        }
                    }*/
                    //dd($key);
                    if($key == 'status') {
                        $log_detail = $log_detail."\r\n - ".trans("order.".$key).": ".\App\Models\Order::$statuses[$event->old_data[$key]]['name']." -> ".\App\Models\Order::$statuses[$event->new_data[$key]]['name'];
                    }
                    else if($key == 'order_company_id') {
                        $log_detail = $log_detail."\r\n - ".trans("order.".$key).": ".@\App\Models\OrderCompany::find($event->old_data[$key])->name." -> ".@\App\Models\OrderCompany::find($event->new_data[$key])->name;
                    }
                    else {
                        $log_detail = $log_detail."\r\n - ".trans("order.".$key).": ".$event->old_data[$key]." - ".$event->new_data[$key];
                    }
                    

                }
            }
        }
        //dd($diff_main, $diff_calculator);
        /*if(sizeof($diff_main) > 0) {
            foreach($diff_main as $key=>$val) {
                $log_detail = $log_detail."\r\n - $key - $val";
            }

        }*/
        //dd($diff_calculator);
        //dd($calculator_old, $calculator_new);
        if(sizeof($diff_calculator) > 0) {
            $changed = true;
            foreach($diff_calculator as $key=>$val)
            {
                if(!is_array($val) && (strpos($key, 'total') === false) ) {
                    if($key == "payment") {
                        $old_pay = (intval($calculator_old[$key]) > 0) ? "безнал" : "наличные";
                        $new_pay = (intval($calculator_new[$key]) > 0) ? "безнал" : "наличные";
                        $log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": ".$old_pay." - ".$new_pay;
                    }
                    else
                        $log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": ".$calculator_old[$key]." - ".$calculator_new[$key];
                }
                elseif(is_array($val)) {
                    foreach ($val as $key1=>$val1) {
                        //$log_detail = $log_detail."\r\n - ".trans("calculator.".$key).": ".trans("calculator.".$calculator_old[$key][$key1])." - ".trans("calculator.".$calculator_new[$key][$key1]);
                        $log_detail = $log_detail . "\r\n - " . trans("calculator." . $key) . ": " . trans("calculator.".$val1);
                    }
                }
            }

        }
        //dd();
        //responsibles///////////////////
        $arr_new_responsible = [];
        foreach( $event->new_responsibles as $new_responsible) {
            //dump($new_responsible->user_id);
            $arr_new_responsible[] = $new_responsible->user_id;
        }

        $arr_old_responsible = [];
        foreach( $event->old_responsibles as $old_responsible) {
            $arr_old_responsible[] = $old_responsible->user_id;
        }
        //dd($arr_old_responsible, $arr_new_responsible);
        $diff_old = array_diff($arr_old_responsible, $arr_new_responsible);
        if(sizeof($diff_old) > 0 ) {
            $changed = true;
            foreach($diff_old as $user_id) {
                $log_detail = $log_detail . "\r\n - удален ответственный " . \App\User::find($user_id)->fio();
            }
        }

        $diff_new = array_diff($arr_new_responsible, $arr_old_responsible);
        if(sizeof($diff_new) > 0) {
            $changed = true;
            foreach($diff_new as $user_id) {
                $log_detail = $log_detail . "\r\n - добавлен ответственный " . \App\User::find($user_id)->fio();
            }
        }


        //contacts////////////////////////////////////////
        $arr_new_contacts = [];
        foreach( $event->new_contacts as $new_contact) {
            //dump($new_contact); dd();
            $arr_new_contacts[] = $new_contact->contact_id;
        }

        $arr_old_contacts = [];
        foreach( $event->old_contacts as $old_contact) {
            //dump(old_contact); dd();
            $arr_old_contacts[] = $old_contact->contact_id;
        }
        //dd($arr_old_contacts, $arr_new_contacts);
        //$diff = array_diff($arr_old_contacts, $arr_new_contacts);
        $diff = array_diff($arr_new_contacts, $arr_old_contacts);
        //dump($arr_old_contacts, $arr_new_contacts); dd(sizeof($diff));
        if(sizeof($diff) > 0) {
            $changed = true;

            foreach($diff as $id) {
                $log_detail = $log_detail . "\r\n - новый контакт " . \App\Models\OrderContact::find($id)->name;
            }
        }

        if( $changed === true ) {
            //Event::fire(new onOrderUpdateEvent($event->order, Auth::user()));

            $data = [
                'order_id' => $event->order->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($event->order->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил заказ: '.$event->order->name.$log_detail
            ];
            $orderLog = \App\Models\OrderLog::create($data);
        }

        
    }

    public function compare_calculator_data($calculator_old, $calculator_new) {
        //dd($calculator_old, $calculator_new);
        $old_diff = $new_diff = [];
        foreach ($calculator_new as $key=>$val) {
            if( @$calculator_new[$key] != @$calculator_old[$key] ) {
                $new_diff[$key] = $calculator_new[$key];
            }
        }

        /*return [
            'added' => $new_diff,
            //'deleted' => $old_diff
        ];*/

        return $new_diff;
    }
}
