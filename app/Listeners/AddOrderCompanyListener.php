<?php

namespace App\Listeners;

use App\Events\onOrderCompanyAddEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class AddOrderCompanyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderCompanyAddEvent  $event
     * @return void
     */
    public function handle(onOrderCompanyAddEvent $event)
    {
        $data = [
            'order_company_id' => $event->company->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($event->company->created_at)).' <b>'.strtoupper(Auth::user()->fio()).'</b> Компания создана: '.$event->company->name
        ];
        \App\Models\OrderCompanyLog::create($data);
    }
}
