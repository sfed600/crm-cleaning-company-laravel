<?php

namespace App\Listeners;

use App\Events\onOrderAddEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Auth;

class AddOrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderAddEvent  $event
     * @return void
     */
    public function handle(onOrderAddEvent $event)
    {
        //Log::info('Order was added'/*, $event->order->name*/);
        //dd($event->order);
        $data = [
            'order_id' => $event->order->id,
            'user_id' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($event->order->created_at)).' <b>'.strtoupper(Auth::user()->fio()).'</b> Заказ создан: '.$event->order->name
        ];
        $orderLog = \App\Models\OrderLog::create($data);
    }
}
