<?php

namespace App\Listeners;

use App\Events\onOrderCompanyUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class UpdateOrderCompanyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderCompanyUpdateEvent  $event
     * @return void
     */
    public function handle(onOrderCompanyUpdateEvent $event)
    {
        $changed = false;
        $log = '';
        $diff = [];
        //dd($event->old_data, $event->new_data);
        foreach ($event->new_data as $key=>$val)
        {
            //dump($key); //dd();
            if($key == 'updated_at' || $key == 'date_register')
                continue;
            else if( $event->new_data[$key] != $event->old_data[$key] )
            {
                if($key == 'user_id') {
                    $diff[$key] = \App\User::find($event->old_data[$key])->fio().' -> '.$event->company->user->fio();
                }
                else if($key == 'logo') {
                    $diff[$key] = '';
                }
                else
                    $diff[$key] = $event->old_data[$key].' -> '.$event->new_data[$key];
            }
        }
        //dd($diff);
        if(sizeof($diff) > 0) {
            $changed = true;

            foreach($diff as $key=>$val) {
                $log = $log . "\r\n - ".trans("order_company.".$key).' ' . $val;
            }
        }

        if($changed === true && !empty($log)) {
            $data = [
                'order_company_id' => $event->company->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($event->company->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил компанию '.$event->company->name.":".$log
                //'note' => 'qwerty'
            ];

            \App\Models\OrderCompanyLog::create($data);
        }
    }
}
