<?php

namespace App\Listeners;

use App\Events\onUserAddEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class AddUserListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onUserAddEvent  $event
     * @return void
     */
    public function handle(onUserAddEvent $event)
    {
        $data = [
            'user_id' => $event->user->id,
            'responsible' => Auth::user()->id,
            'note' => date('d.m.Y H:i', strtotime($event->user->created_at)).' <b>'.strtoupper(Auth::user()->fio()).'</b> Пользователь создан: '.$event->user->fio()
        ];
        $userLog = \App\Models\UserLog::create($data);
    }
}
