<?php

namespace App\Listeners;

use App\Events\onOrderContactUpdateEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;

class UpdateOrderContactListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onOrderContactUpdateEvent  $event
     * @return void
     */
    public function handle(onOrderContactUpdateEvent $event)
    {
        $changed = false;
        $log = '';
        $diff = [];

        //dd($event->old_data, $event->new_data);
  
        foreach ($event->new_data as $key=>$val)
        {
            //dump($key); //dd();
            if($key == 'updated_at')
                continue;
            else if( $event->new_data[$key] != $event->old_data[$key] )
            {
                if( $key == 'user_id' && $event->old_data[$key] != $event->new_data[$key] ) {
                    $diff[$key] = \App\User::find($event->old_data[$key])->fio().' -> '.\App\User::find($event->new_data[$key])->fio();
                }
                else if( $key == 'order_company_id' && $event->old_data[$key] != $event->new_data[$key] ) {
                    if(isset($event->old_data[$key]))
                        $diff[$key] = \App\Models\OrderCompany::find($event->old_data[$key])->name.' -> '.\App\Models\OrderCompany::find($event->new_data[$key])->name;
                    else
                        $diff[$key] = ' добавлено '.\App\Models\OrderCompany::find($event->new_data[$key])->name;
                }
                else if($key == 'photo') {
                    $diff[$key] = '';
                }
                else
                    $diff[$key] = $event->old_data[$key].' -> '.$event->new_data[$key];
            }
        }
        //dd($diff);
        if(sizeof($diff) > 0) {
            $changed = true;

            foreach($diff as $key=>$val) {
                $log = $log . "\r\n - ".trans("order_contact.".$key).' ' . $val;
            }
        }

        if( ($changed === true && !empty($log)) || !empty($event->relations_log) ) {
            $data = [
                'contact_id' => $event->contact->id,
                'user_id' => Auth::user()->id,
                'note' => date('d.m.Y H:i', strtotime($event->contact->updated_at)).' <b>'.(Auth::user()->fio()).'</b> изменил контакт '.$event->contact->name.":".$log.$event->relations_log
                //'note' => 'qwerty'
            ];
            //dd($data);
            \App\Models\OrderContactLog::create($data);
        }
    }
}
