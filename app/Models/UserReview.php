<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    protected $table = 'user_reviews';

    protected $fillable = ['user_id', 'author_id', 'order_id', 'review'];

    public function author() {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}
