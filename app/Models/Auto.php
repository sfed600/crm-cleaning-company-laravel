<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use Date;

class Auto extends Model
{
    protected $table = 'autos';

    protected $fillable = ['company_id', 'model_id', 'number', 'date_registration', 'year', 'vin', 'certificate', 'pts', 'contract', 'date_insurance', 'sts'];

    public static $file_path = 'assets/files/autos/';

    public function model() {
        return $this->belongsTo('App\Models\AutoModel', 'model_id');
    }

    public static function saveFile($request = null, $name = 'certificate') {
        if(!$request->hasFile($name) || !$request->file($name)->isValid()) {
            return '';
        }
        $filename = time().'_'.str_random(5).'.'.$request->file($name)->getClientOriginalExtension();
        $request->file($name)->move(public_path(self::$file_path), $filename);
        return $filename;
    }


    public function deleteFile($name) {
        if(!$this->$name){
            return false;
        }

        Storage::disk('public')->delete([
            $this->getFilePath().$this->$name,
            $this->getFilePreviewPath().$this->$name,
        ]);
        return true;
    }

    public function getFilePath(){
        return self::$file_path;
    }

    public function dateRegFormat() {
        if($this->date_registration && $this->date_registration!='0000-00-00') {
            return (new Date($this->date_registration))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function dateInsuranceFormat() {
        if($this->date_insurance && $this->date_insurance!='0000-00-00') {
            return (new Date($this->date_insurance))->format('d.m.Y');
        } else {
            return '';
        }
    }

}
