<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBlock extends Model
{
    protected $table = 'user_blocks';

    protected $fillable = ['name', 'name_rus'];

    public function groups()
    {
        return $this->hasMany('App\Models\UserGroup', 'block_id');
    }
}
