<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Image;
use Storage;

class OrderContact extends Model
{
    use SoftDeletes;

    protected $table = 'order_contacts';

    protected $fillable = [
        'order_company_id', 
        'name', 
        'post', 
        'comments', 
        'user_id',
        'typ',
        'photo',
        'address',
        'apartment',
        'porch',
        'floor',
        'domofon'
    ];

    protected $dates = ['deleted_at'];

    public static $photo_path = 'assets/imgs/contacts/';
    public static $photo_path_preview = 'assets/imgs/contacts/preview/';

    protected static $size_preview_width = 180;
    protected static $size_preview_height = 200;


    public function orders() {
        return $this->belongsToMany('App\Models\Order', 'order_contact', 'contact_id', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\OrderCompany', 'order_company_id');
    }

    public function phones() {
        return $this->hasMany('App\Models\OrderContactPhone', 'contact_id');
    }

    public function emails() {
        return $this->hasMany('App\Models\OrderContactEmail', 'contact_id');
    }


    public static function saveImg($request = null, $url = null)
    {
        $arFiles = [];
        //URL в приоритете
        if(!empty($url)) {
        }
        else {
            if( $request->hasFile('photo') && $request->file('photo')->isValid() ) {
                $sizes = getimagesize($request->file('photo')->getRealPath());
                $filename = time().'_'.str_random(5).'.'.$request->file('photo')->getClientOriginalExtension();
                $request->file('photo')->move(public_path(self::$photo_path), $filename);

                //preview
                Image::make(public_path(self::$photo_path).$filename)
                    ->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save(public_path(self::$photo_path_preview).$filename);

                $arFiles['photo'] = $filename;
            }

        }

        return $arFiles;
    }

    public function deleteImg() {
        if(!$this->photo) {
            return false;
        }

        Storage::disk('public')->delete([
            $this->getImgPath('photo').$this->photo,
            $this->getImgPreviewPath('photo').$this->photo,
        ]);

        return true;
    }

    public function getImgPath(){
        return self::$photo_path;

    }

    public function getImgPreviewPath(){
        return self::$photo_path_preview;
    }

    public function getPreviewSize($attribute = 'width'){
        $attribute = 'size_preview_'.$attribute;
        return self::$$attribute;
    }
    
}
