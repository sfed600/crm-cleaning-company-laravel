<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Date;
use DB;

class OrderDay extends Model
{
    protected $table = 'order_days';

    protected $fillable = ['order_id', 'date', 'time', 'ours', 'timestart', 'timefinish', 'nigth', 'metro_id'];

    /*public function metro() {
        return $this->hasOne('App\Models\Metro');
    }*/
    public function metro() {
        return $this->belongsTo('App\Models\Metro');
    }

    public function users() {
        return $this->hasMany('App\Models\OrderUser', 'day_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function drivers() {
        return $this->users()->where('driver', 1);
    }

    public function workers() {
        return $this->users()->where('driver', 0);
    }

    public function datePicker() {
        return (new Date($this->date))->format('d.m.Y');
    }

    /*
     * № смены по порядку внутри заказа
     * */
    public static function row_number($id) {
        $sql = "
            SELECT @i := @i + 1 AS row_number, od.id
            FROM `order_days`as od, (select @i:=0) as hz
            WHERE od.order_id = ".OrderDay::find($id)->order->id."
            ORDER BY od.date, od.created_at";

        $res = DB::select($sql);
        //dd($res);
        foreach ($res as $row) {
            if($id == $row->id)
                return $row->row_number;
        }

    }

}
