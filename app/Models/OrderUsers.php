<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderUsers extends Model
{
    protected $table = 'order_user';

    //public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
