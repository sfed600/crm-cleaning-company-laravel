<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfession extends Model
{
    protected $table = 'user_professions';

    protected $fillable = ['name', 'company_id'];
}
