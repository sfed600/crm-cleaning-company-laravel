<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Calendar extends Model
{
    /**
     * кол-во дней заказа
     */
    public static function count_order_days($order) {
        $count = 0;

        if($order->periods)
        {
            $periods = \GuzzleHttp\json_decode($order->periods);

            foreach ($periods as $key=>$period)
            {
                if(!empty($period->date_start) && !empty($period->date_finish) ) {
                    $cur_date = Carbon::createFromFormat('d.m.Y', $period->date_start);
                    do {
                        $count++;
                        $cur_date = $cur_date->addDay();
                    } while ($cur_date <= Carbon::createFromFormat('d.m.Y', $period->date_finish));
                }
            }

        } else {
            if($order->date != '0000-00-00' &&  $order->date_finish != '0000-00-00') {
                $cur_date = Carbon::createFromFormat('Y-m-d', $order->date);
                do {
                    $count++;
                    $cur_date = $cur_date->addDay();
                } while ($cur_date <= Carbon::createFromFormat('Y-m-d', $order->date_finish));
            }
        }
        return $count;
    }

    /**
     * общее кол-во заказов на дату
     */
    public static function countOrdersOnDate() {
        $now = Carbon::now();
        //echo $now->year;
        $endInterval = Carbon::now()->addMonths(2);

        $dataIntervals = [];
        $n = 0;
        for ($i=$now->month; $i <= $endInterval->month; $i++) 
        {
            //$dataIntervals[$i] = [];
            $date = Carbon::now()->addMonths($n);
            //кол-во дней в месяце
            $cnt_days = $date->daysInMonth;
            //$cnt_days = Carbon::createFromFormat('d.m.Y','01.'.$i.''.$filters['date'])->daysInMonth;
            //dump($cnt_days);
            for($j = 1; $j<= $cnt_days; $j++) {
                $date_day = Carbon::createFromFormat('d.m.Y', $j.'.'.$date->month.'.'.$date->year, 'Europe/Moscow');
                $count = Order::where('date', '<=', $date_day)
                    ->where('date_finish', '>=', $date_day)
                    ->count();

                if($count > 0)
                    $dataIntervals[$i][$j] = $count;
            }

            $n++;
        }
        //dd($dataIntervals);
        return $dataIntervals;
    }

    /**
     * Кол-во плановых заказов на дату
     */
    public function count_orders_of_day($date, $orders) {
        $count = 0;

        foreach ($orders as $order) {
            if($order->periods)
            {
                $periods = \GuzzleHttp\json_decode($order->periods);

                foreach ($periods as $key=>$period)
                {
                    if($period->date_start <= $date->format('d.m.Y') && $date->format('d.m.Y') <= $period->date_finish)
                        $count++;
                }

            } else {
                if($order->date != '0000-00-00' &&  $order->date_finish != '0000-00-00') {
                    //if($order->date == $date || $order->date_finish == $date) {
                    if($order->date <= $date->format('Y-m-d') && $date->format('Y-m-d') <= $order->date_finish) {
                        $count++;
                    }
                }
            }
        }

        return $count;
    }

    /**
     * Данные калькулятора для заказа
     */
    public function order_calculator_data($order)
    {
        $calculator_str = '';

        if($order->calculator) {
            $calculator = \GuzzleHttp\json_decode($order->calculator);
            /*$calculator = [];
            parse_str($turn->calculator, $calculator);*/
            //dd($calculator->dry_cleaning);

            if( 'apartment' == $calculator->base_info)
                $calculator_str = $calculator_str.' Квартира';
            else if( 'house' == $calculator->base_info)
                $calculator_str = $calculator_str.' Дом/коттедж';
            else if( 'office' == $calculator->base_info)
                $calculator_str = $calculator_str.' Офис';
            else if( 'shop' == $calculator->base_info)
                $calculator_str = $calculator_str.' Производственное помещение';

            if( @$calculator->area > 0 )
                $calculator_str = $calculator_str.' '.@$calculator->area.'м.';

            if( isset($calculator->dry_cleaning) )
                $calculator_str = $calculator_str.', химчистка';

            if( isset($calculator->front))
                $calculator_str = $calculator_str.', мойка окон, фасада';

            if(isset($calculator->other_work))
                $calculator_str = $calculator_str.', прочие работы';

        }

        return $calculator_str;
    }

    /**
     * время дня в заказе
     */
    public function time_of_day($order, $date)
    {
        if($order->periods)
        {
            $periods = \GuzzleHttp\json_decode($order->periods);
            foreach ($periods as $period)
            {
                if(!empty($period->date_start) && !empty($period->date_finish) ) {
                    $cur_date = Carbon::createFromFormat('d.m.Y', $period->date_start);
                    if($cur_date == $date)
                        return $period->time_start;

                    //while ($date != $cur_date && $cur_date <= Carbon::createFromFormat('d.m.Y', $period->date_finish) ) {
                    while ($cur_date <= Carbon::createFromFormat('d.m.Y', $period->date_finish) ) {
                        if($cur_date == $date)
                            return $period->time_start;

                        $cur_date = $cur_date->addDay();
                    }
                }
            }
        } else {
            if($order->date != '0000-00-00' &&  $order->date_finish != '0000-00-00') {
                $cur_date = Carbon::createFromFormat('Y-m-d', $order->date);
                if($cur_date == $date)
                    return $order->timestart;

                while($cur_date <= Carbon::createFromFormat('Y-m-d', $order->date_finish)) {
                    if($cur_date == $date)
                        return $order->timestart;

                    $cur_date = $cur_date->addDay();
                }
            }
        }

        return '09:00 ';
    }

    /**
     * Номер дня в заказе
     */
    public static function ordinal_day_number($order, $date)
    {
        $i = 1;
        $days_of_periods = [];

        if($order->periods)
        {
            $periods = \GuzzleHttp\json_decode($order->periods);
            foreach ($periods as $key=>$period)
            {
                if(!empty($period->date_start) && !empty($period->date_finish) ) {
                    $cur_date = Carbon::createFromFormat('d.m.Y', $period->date_start);
                    if($cur_date == $date)
                        break;

                    while ($date != $cur_date && $cur_date <= Carbon::createFromFormat('d.m.Y', $period->date_finish) ) {
                        $days_of_periods[] = $cur_date;
                        $cur_date = $cur_date->addDay();
                        //dump($cur_date);
                        $i++;
                    }
                }
            }
        } else {
            if($order->date != '0000-00-00' &&  $order->date_finish != '0000-00-00') {
                $cur_date = Carbon::createFromFormat('Y-m-d', $order->date);
                if( $cur_date != $cur_date) {
                    while ($date != $cur_date && $cur_date <= Carbon::createFromFormat('Y-m-d', $order->date_finish)) {
                        $days_of_periods[] = $cur_date;
                        $cur_date = $cur_date->addDay();
                        //dump($cur_date);
                        $i++;
                    }
                }
            }
        }

        return $i;
    }

    /**
     * детальная инфа на дату календаря
     */
    public static function detail_day_plan($date, $orders)
    {
        $calendar = new Calendar();

        setlocale(LC_ALL, "ru_RU.UTF-8");

        $html = '<div class="calendar-day-item-date"><span>'.strftime("%A %e %B %Y", strtotime($date->format('d.m.Y'))).' - '.$calendar->count_orders_of_day($date, $orders).' заказов в этот день</span></div>';
        foreach ($orders as $order)
        {
            //парсим периоды, сравниваем с $date
            if($order->periods)
            {
                $periods = \GuzzleHttp\json_decode($order->periods);

                foreach ($periods as $key=>$period)
                {
                    if(empty($period->date_start) || empty($period->date_finish))
                        continue;

                    if($period->date_start <= $date->format('d.m.Y') && $date->format('d.m.Y') <= $period->date_finish)
                    {
                        $html = $html. '<div class="calendar-day-entry">
                                            <div class="calendar-day-entry-heads">
                                                <a href="#"><span class="calendar-clr-blue">Заказ №'.$order->number.' - день '.self::ordinal_day_number($order, $date).' из '.$calendar->count_order_days($order).'</span></a>
                                            </div>
                                        </div>';

                        //$html = $html. '<div class="calendar-day-entry-text"> Уборка в '.$calculator->base_info[0].' в Мелихино, 60м2, окна </div>';
                        $html = $html. '<div class="calendar-day-entry-text"> '.$calendar->order_calculator_data($order).' </div>';

                        $html = $html. '<div class="calendar-day-entry-cur">'.$calendar->time_of_day($order, $date).' '.$order->address.'</div>';

                        //personal data
                        //self::order_personal_data($order);

                    }
                }

            } else {
                if($order->date != '0000-00-00' &&  $order->date_finish != '0000-00-00') {
                    //if($order->date == $date || $order->date_finish == $date) {
                    if($order->date <= $date->format('Y-m-d') && $date->format('Y-m-d') <= $order->date_finish) {
                        $html = $html. '<div class="calendar-day-entry">
                                            <div class="calendar-day-entry-heads">
                                                <a href="#"><span class="calendar-clr-blue">Заказ №'.$order->number.' - день '.self::ordinal_day_number($order, $date).' из '.$order->cnt.'</span></a>
                                            </div>';

                        //$html = $html. '<div class="calendar-day-entry-text"> Уборка в '.$calculator->base_info[0].' в Мелихино, 60м2, окна </div>';
                        $html = $html. '<div class="calendar-day-entry-text"> '.$calendar->order_calculator_data($order).' </div></div>';

                        $html = $html. '<div class="calendar-day-entry-cur">'.$calendar->time_of_day($order, $date).' '.$order->address.'</div>';

                        //personal data
                        //self::order_personal_data($order);
                    }
                }
            }
        }

        return $html;
    }

    public static function detail_day_fact($date, $_turns)
    {
        $calendar = new Calendar();

        setlocale(LC_ALL, "ru_RU.UTF-8");

        $turns = $_turns->where('date', $date->format('Y-m-d'));

        $html = '<div class="calendar-day-item-date"><span>'.strftime("%A %e %B %Y", strtotime($date->format('d.m.Y'))).' - '.$turns->count().' уборок в этот день</span></div>';

        foreach ($turns as $turn)
        {
            $html = $html. '<div class="calendar-day-entry">
                                    <div class="calendar-day-entry-heads">
                                        <a href="#"><span class="calendar-clr-blue">Заказ №'.$turn->number.' - день '.\App\Models\OrderDay::row_number($turn->id).' из '.$turn->cnt.'</span></a>
                                    </div>';

            //$html = $html. '<div class="calendar-day-entry-text"> Уборка в '.$calculator->base_info[0].' в Мелихино, 60м2, окна </div>';
            $html = $html. '<div class="calendar-day-entry-text"> '.$calendar->order_calculator_data($turn).' </div>';

            $html = $html. '<div class="calendar-day-entry-text"> '.$calendar->staff($turn).' </div>';
            
            if($turn->nigth == 1)
                $html = $html. '<div class="calendar-day-entry-cur">Ночь, '.$turn->time.' - '.$turn->metro.'</div></div>';
            else
                $html = $html. '<div class="calendar-day-entry-cur">День, '.$turn->time.' - '.$turn->metro.'</div></div>';
        }

        return $html;
    }

    public function staff($turn) {
        $staff = '';
        $staff = $staff. ' Бригадир - '.$turn->surname.' '.$turn->name.'<br>';
        $staff = $staff.'Человек - '. $turn->cnt_mans;

        return $staff;
        //return dump($turn);
    }
}
