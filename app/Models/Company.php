<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Image;

class Company extends Model
{
    use SoftDeletes;

    protected $table = 'companies';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'site',
        'comments',
        'inn',
        'short_company_name',
        'full_company_name',
        'address',
        'ogrn',
        'kpp',
        'date_register',
        'okpo',
        'oktmo',
        'director',
        'buhgalter',
        'company_dir_fio',
        'user_id',
        'bank_name',
        'bik',
        'checking_account',
        'cor_account',
        'stamp',
        'signature',
        'logo',
        'typ',
        'user_id',
        'building',
        'housing',
        'office'
        //'note'
    ];

    protected $dates = ['deleted_at'];

    public static $stamp_path = 'assets/imgs/companies/stamp/';
    public static $stamp_path_preview = 'assets/imgs/companies/stamp/preview/';

    public static $signature_path = 'assets/imgs/companies/signature/';
    public static $signature_path_preview = 'assets/imgs/companies/signature/preview/';

    public static $logo_path = 'assets/imgs/companies/logo/';
    public static $logo_path_preview = 'assets/imgs/companies/logo/preview/';

    protected static $size_preview_width = 180;
    protected static $size_preview_height = 200;

    /*public function user()
    {
        return $this->hasOne('App\User', 'company_id');
    }*/

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'company_id');
    }

    public function datePicker() {
        if($this->date_register && $this->date_register!='0000-00-00') {
            return (new \Date($this->date_register))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public static function saveImg($request = null, $url = null)
    {
        $arFiles = [];
        //URL в приоритете
        if(!empty($url)) {
            /*if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                return '';
            }
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            $filename = time().'_'.str_random(5).'.'.$ext;

            Storage::disk('public')->put(self::$img_path.$filename, file_get_contents($url));
            $sizes = getimagesize(public_path(self::$img_path).$filename);*/
        } else {
            /*if(!$request->hasFile('img') || !$request->file('img')->isValid()) {
                return '';
            }*/
            if( $request->hasFile('stamp') && $request->file('stamp')->isValid() ) {
                $sizes = getimagesize($request->file('stamp')->getRealPath());
                $filename = time().'_'.str_random(5).'.'.$request->file('stamp')->getClientOriginalExtension();
                //dump($filename); dd();
                $request->file('stamp')->move(public_path(self::$stamp_path), $filename);

                //preview
                Image::make(public_path(self::$stamp_path).$filename)
                    ->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save(public_path(self::$stamp_path_preview).$filename);

                $arFiles['stamp'] = $filename;
            }

            if( $request->hasFile('signature') && $request->file('signature')->isValid() ) {
                $sizes = getimagesize($request->file('signature')->getRealPath());
                $filename = time().'_'.str_random(5).'.'.$request->file('signature')->getClientOriginalExtension();
                $request->file('signature')->move(public_path(self::$signature_path), $filename);

                //preview
                Image::make(public_path(self::$signature_path).$filename)
                    ->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save(public_path(self::$signature_path_preview).$filename);

                $arFiles['signature'] = $filename;
            }

            //dd($request->all());
            if( $request->hasFile('logo') && $request->file('logo')->isValid() ) {
                $sizes = getimagesize($request->file('logo')->getRealPath());
                $filename = time().'_'.str_random(5).'.'.$request->file('logo')->getClientOriginalExtension();
                $request->file('logo')->move(public_path(self::$logo_path), $filename);

                //preview
                Image::make(public_path(self::$logo_path).$filename)
                    ->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save(public_path(self::$logo_path_preview).$filename);

                $arFiles['logo'] = $filename;
            }
        }

        return $arFiles;
    }

    public function deleteImg($typ) {
        switch ($typ) {
            case 'stamp':
                if(!$this->stamp) {
                    return false;
                }

                Storage::disk('public')->delete([
                    $this->getImgPath('stamp').$this->stamp,
                    $this->getImgPreviewPath('stamp').$this->stamp,
                ]);
                
                break;
            case 'signature':
                if(!$this->signature) {
                    return false;
                }

                Storage::disk('public')->delete([
                    $this->getImgPath('signature').$this->signature,
                    $this->getImgPreviewPath('signature').$this->signature,
                ]);
                
                break;
            case 'logo':
                if(!$this->logo) {
                    return false;
                }

                Storage::disk('public')->delete([
                    $this->getImgPath('logo').$this->logo,
                    $this->getImgPreviewPath('logo').$this->logo,
                ]);
                
                break;
        }
        
        return true;
    }

    public function getImgPath($typ){
        switch ($typ) {
            case 'stamp':
                return self::$stamp_path;
                break;
            case 'signature':
                return self::$signature_path;
                break;
            case 'logo':
                return self::$logo_path;
                break;
        }
        
    }

    public function getImgPreviewPath($typ){
        switch ($typ) {
            case 'stamp':
                return self::$stamp_path_preview;
                break;
            case 'signature':
                return self::$signature_path_preview;
                break;
            case 'logo':
                return self::$logo_path_preview;
                break;
        }
    }

    public function getPreviewSize($attribute = 'width'){
        $attribute = 'size_preview_'.$attribute;
        return self::$$attribute;
    }
    
    //
    public static function getCompanyDataByInn($inn, $ajax=null)
    {
        $data = [
            "query" => $inn,
            "branch_type" => "MAIN"
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://suggestions.dadata.ru/suggestions/api/4_1/rs/findById/party",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: application/json",
                //"accept-language: en-US,en;q=0.8",
                "content-type: application/json",
                "authorization: Token df6d903c4017b2ff53aa0fab126b2aaaaf05be46"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        //dd($response);
        if ($err) {
            return false;
        } else {
            $ar_suggestions = get_object_vars(json_decode($response));
            //dd($ar_suggestions['suggestions']);
            /*$arData = get_object_vars($ar_suggestions['suggestions'][0]);
            $company_name = $arData["value"];*/

            try {
                $arData = @get_object_vars($ar_suggestions['suggestions'][0]);
                $company_name = $arData["value"];
            } catch (Exception $e) {
                return [];
            }

            if($arData == null) return [];

            $data = $arData["data"];

            return [
                'company_name' => $company_name,
                'inn' => $inn,
                'kpp' => @$data->kpp,
                'okpo' => ($data->okpo) ? $data->okpo : '',
                'full_company_name' => $data->name->full_with_opf,
                'short_company_name' => $data->name->short_with_opf,
                'ogrn' => $data->ogrn,
                'address' => $data->address->value,
                //'date_register' => date('d.m.Y', $data->ogrn_date / 1000),
                //'date_register' => date('Y-m-d', $data->ogrn_date / 1000),
                'date_register' => ($ajax) ? date('d.m.Y', $data->ogrn_date / 1000) : date('Y-m-d', $data->ogrn_date / 1000),
                'director' => (@$data->management->name) ? @$data->management->name : ''
            ];
        }
    } 
}
