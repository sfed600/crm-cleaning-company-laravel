<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Date;
use Carbon\Carbon;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'company_id',
        'number',
        'name',
        'amount',
        'order_company_id',
        'date',
        'metro_id',
        'address',
        'apartment',
        'porch',
        'floor',
        'domofon',
        'note',
        'status',
        'pay_status',
        //'user_id',
        'timestart',
        'date_finish',
        'timefinish',
        'calculator',
        'periods'
    ];

    static public $dates_filter = [
        'yesterday' => 'вчера',
        'tomorrow' => 'завтра',
        'today' => 'сегодня',
        'week' => 'неделя',
    ];

    static public $months = [
        1=>'Январь',
        2=>'Февраль',
        3=>'Март',
        4=>'Апрель',
        5=>'Май',
        6=>'Июнь',
        7=>'Июль',
        8=>'Август',
        9=>'Сентябрь',
        10=>'Октябрь',
        11=>'Ноябрь',
        12=>'Декабрь'
    ];


    static public $statuses = ['first' => ['name' => 'Первичный контакт', 'color' => '#fbbc2f'],
        'new' => ['name' => 'Новые', 'color' => '#555'],
        'my' => ['name' => 'Мои', 'color' => '#36bef8'],
        'agree' => ['name' => 'Согласовать', 'color' => '#f7ab31'],
        'agreed' => ['name' => 'Согласованно', 'color' => '#78e79b'],
        'confirmed' => ['name' => 'Подтверждено', 'color' => '#36bef8'],
        'work' => ['name' => 'В работе', 'color' => '#2ba1e9'],
        'performed' => ['name' => 'Работы выполнены', 'color' => '#935892'],
        'final_positive' => ['name' => 'Успешно реализовано', 'color' => '#935892'],
        'final_negative' => ['name' => 'Не реализовано', 'color' => '#d02b29'],
    ];

    static public $payStatuses = [
        'payed' => 'Оплачен',
        'nopayed' => 'Не плачен',
        'partpay' => 'Частично оплачен'
    ];

    public function statusName() {
        return $this->status ? self::$statuses[$this->status]['name'] : '';
    }

    public function statusColor() {
        return $this->status ? self::$statuses[$this->status]['color'] : '';
    }

    public function days() {
        return $this->hasMany('App\Models\OrderDay', 'order_id');
    }

    public function users() {
        return $this->hasManyThrough('App\Models\OrderUser', 'App\Models\OrderDay', 'order_id', 'day_id');
    }

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function orderCompany() {
        return $this->belongsTo('App\Models\OrderCompany', 'order_company_id');
    }

    public function metro() {
        return $this->belongsTo('App\Models\Metro');
    }

    public function sms(){
        return $this->hasMany('App\Models\Sms', 'order_id');
    }

    public function logs(){
        return $this->hasMany('App\Models\OrderLog', 'order_id');
    }

    public function datePicker() {
        if($this->date) {
            return (new Date($this->date))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function dateFinishPicker() {
        if($this->date_finish) {
            return (new Date($this->date_finish))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function contacts() {
        return $this->belongsToMany('App\Models\OrderContact', 'order_contact', 'order_id', 'contact_id');
    }

    public function responsible_users() {
        return $this->belongsToMany('App\User', 'order_user', 'order_id', 'user_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Models\Invoice');
    }

    ////////////////
    /*public static function getPeriodsDaysCount($periods)
    {
        $date_periods = [];
        if($periods) {
            $date_periods = \GuzzleHttp\json_decode($periods);
            //dd($date_periods);
        }

        $cnt = 0;
        foreach ($date_periods as $date_period)
        {
            $start = (new Carbon($date_period->date_start));
            $finish = (new Carbon($date_period->date_finish));
            $length = $finish->diffInDays($start) + 1;
            $cnt = $cnt + $length;
            //dump($start, $finish); dd($length);
        }

        return $cnt;
    }*/

}
