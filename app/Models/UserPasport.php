<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPasport extends Model
{
    protected $table = 'user_pasports';
    protected $fillable = ['user_id', 'number', 'series', 'date_issue', 'issued_by'];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
