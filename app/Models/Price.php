<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';

    protected $fillable = ['company_id', 'price'];

    public $timestamps = false;

    public function company()
    {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    /*public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }*/
}
