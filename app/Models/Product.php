<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'full_name',
        'tip',
        'company_id',
        'price',
        'measure',
        'nds',
        'articul',
        'comment'
    ];

    static public $tips = [
        'product' => ['name' => 'товар', 'color' => '#d0b2ff'],
        'service' => ['name' => 'услуга', 'color' => '#ffffff'],
    ];

    static public $measures = [
        'service' => ['name' => 'услуга', 'color' => '#ffffff'],
        'unit' => ['name' => 'шт.', 'color' => '#d0b2ff'],
        'metr' => ['name' => 'м.кв', 'color' => '#a774f7'],
        'box' => ['name' => 'упак.', 'color' => '#6a11f5'],
    ];

    static public $nds = [
        'no_nds' => ['name' => 'Без НДС', 'color' => '#ffffff'],
        'nds18' => ['name' => 'НДС18%', 'color' => '#d0b2ff'],
        'nds10' => ['name' => 'НДС10%', 'color' => '#6a11f5'],
    ];

    public function measureName() {
        return $this->measure ? self::$measures[$this->measure]['name'] : '';
    }

    public function ndsName() {
        return $this->nds ? self::$nds[$this->nds]['name'] : '';
    }

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    /*public function contacts() {
        return $this->belongsToMany('App\Models\OrderContact', 'order_contact', 'order_id', 'contact_id');
    }*/

}
