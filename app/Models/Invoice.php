<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Date;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'number',
        'order_id',
        'date',
        'due_date',
        'note'
    ];

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function datePicker() {
        if($this->date) {
            return (new Date($this->date))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function dueDatePicker() {
        if($this->due_date && $this->due_date != "0000-00-00" && $this->due_date != '') {
            return (new Date($this->due_date))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'invoice_product', 'invoice_id', 'product_id');
    }

    public function sumNDS($id)
    {
        $invoice = Invoice::findOrFail($id);

        $sumNDS = 0;
        foreach($invoice->products as $product) {
            if($product->nds == 'nds18')
                $sumNDS = $sumNDS + $product->price * 18 / 100;
            else if($product->nds == 'nds10')
                $sumNDS = $sumNDS + $product->price * 10 / 100;
        }

        return $sumNDS;
    }

    public function sumTotal($id)
    {
        $invoice = Invoice::findOrFail($id);

        $sumTotal = 0;
        foreach($invoice->products as $product) {
            $sumTotal = $sumTotal + $product->price;
        }

        return $sumTotal;
    }

    function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $this->morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.$this->morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }

}
