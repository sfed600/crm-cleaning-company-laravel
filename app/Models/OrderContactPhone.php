<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderContactPhone extends Model
{
    protected $table = 'order_contact_phones';

    protected $fillable = ['contact_id', 'phone', 'type'];

    static public $types = ['mobile' => 'Мобильный', 'work' => 'Рабочий'];

    public function typeName() {
        return self::$types[$this->type];
    }

}
