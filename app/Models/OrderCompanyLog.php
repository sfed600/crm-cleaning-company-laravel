<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCompanyLog extends Model
{
    protected $table = 'order_company_logs';

    protected $fillable = [
        'order_company_id',
        'user_id',
        'note',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
