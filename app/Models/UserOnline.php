<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOnline extends Model
{
    protected $table = 'online';
}
