<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    protected $table = 'orders_logs';

    protected $fillable = [
        'order_id',
        'user_id',
        'note',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
}
