<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderContactEmail extends Model
{
    protected $table = 'order_contact_emails';

    protected $fillable = ['contact_id', 'email'];
}
