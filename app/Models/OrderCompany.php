<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Image;

class OrderCompany extends Model
{
	use SoftDeletes;

	protected $table = 'order_companies';

	protected $fillable = [
		'name',
		'phone',
		'email',
		'site',
		'comments',
		'inn',
		'short_company_name',
		'full_company_name',
		'address',
		'ogrn',
		'kpp',
		'date_register',
		'okpo',
		'oktmo',
		'director',
		'buhgalter',
		'company_dir_fio',
		'user_id',
		'bank_name',
		'bik',
		'checking_account',
		'cor_account',
		'typ', 
		'logo',
		'building',
		'housing',
		'office'
	];

	protected $dates = ['deleted_at'];

	public static $logo_path = 'assets/imgs/order_companies/';
	public static $logo_path_preview = 'assets/imgs/order_companies/preview/';

	protected static $size_preview_width = 180;
	protected static $size_preview_height = 200;


	public function orders() {
		return $this->hasMany('App\Models\Order', 'order_company_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function datePicker() {
		if($this->date_register && $this->date_register!='0000-00-00') {
			return (new \Date($this->date_register))->format('d.m.Y');
		} else {
			return '';
		}
	}
	
	//logo
	public static function saveImg($request = null, $url = null)
	{
		$arFiles = [];
		//URL в приоритете
		if(!empty($url)) {
			/*if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
                return '';
            }
            $ext = pathinfo($url, PATHINFO_EXTENSION);
            $filename = time().'_'.str_random(5).'.'.$ext;

            Storage::disk('public')->put(self::$img_path.$filename, file_get_contents($url));
            $sizes = getimagesize(public_path(self::$img_path).$filename);*/
		} else {
			//dd($request->all());
			if( $request->hasFile('logo') && $request->file('logo')->isValid() ) {
				$sizes = getimagesize($request->file('logo')->getRealPath());
				$filename = time().'_'.str_random(5).'.'.$request->file('logo')->getClientOriginalExtension();
				$request->file('logo')->move(public_path(self::$logo_path), $filename);

				//preview
				Image::make(public_path(self::$logo_path).$filename)
					->fit(min(self::$size_preview_width, $sizes[0]), min(self::$size_preview_height, $sizes[1]), function ($constraint) {
						$constraint->upsize();
					})
					->save(public_path(self::$logo_path_preview).$filename);

				$arFiles['logo'] = $filename;
			}
		}

		return $arFiles;
	}

	public function deleteImg() {
		if(!$this->logo) {
			return false;
		}

		Storage::disk('public')->delete([
			$this->getImgPath('logo').$this->logo,
			$this->getImgPreviewPath('logo').$this->logo,
		]);

		return true;
	}

	public function getImgPath($typ){
		switch ($typ) {
			case 'logo':
				return self::$logo_path;
				break;
		}

	}

	public function getImgPreviewPath($typ){
		switch ($typ) {
			case 'logo':
				return self::$logo_path_preview;
				break;
		}
	}

	public function getPreviewSize($attribute = 'width'){
		$attribute = 'size_preview_'.$attribute;
		return self::$$attribute;
	}
}
