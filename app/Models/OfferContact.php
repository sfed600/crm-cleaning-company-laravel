<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OfferContact extends Model
{
    //use SoftDeletes;

    //protected $table = 'offer_contact';
    protected $table = 'order_contacts';

    /*protected $fillable = ['order_company_id', 'name', 'post', 'comments', 'user_id'];

    protected $dates = ['deleted_at']; */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\OrderCompany', 'order_company_id');
    }

    public function phones() {
        return $this->hasMany('App\Models\OrderContactPhone', 'contact_id');
    }

    public function emails() {
        return $this->hasMany('App\Models\OrderContactEmail', 'contact_id');
    }


}
