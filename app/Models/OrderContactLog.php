<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderContactLog extends Model
{
    protected $table = 'order_contact_logs';

    protected $fillable = [
        'contact_id',
        'user_id',
        'note',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
