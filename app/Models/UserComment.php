<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserComment extends Model
{
    protected $table = 'user_comments';

    protected $fillable = ['user_id', 'user_add_id', 'comment'];

    public function dateHuman() {
        return  $this->created_at->diffForHumans();
    }

    public function userAdd() {
        return $this->belongsTo('App\User', 'user_add_id');
    }

}
