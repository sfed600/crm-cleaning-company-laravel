<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Date;

class Offer extends Model
{
    //use SoftDeletes;

    protected $table = 'offers';

    //protected $dates = ['deleted_at'];

    protected $fillable = [
        'company_id',
        'number',
        'name',
        'area',
        'amount',
        'order_company_id',
        'date_start',
        'timestart',
        'date_finish',
        'timefinish',
        'metro_id',
        'address',
        'apartment',
        'porch',
        'floor',
        'domofon',
        'note',
        'status',
        'user_id'
    ];

    static public $statuses = ['first' => ['name' => 'Первичный контакт', 'color' => '#fbbc2f'],
        'agree' => ['name' => 'Согласовать', 'color' => '#f7ab31'],
        'agreed' => ['name' => 'Согласованно', 'color' => '#78e79b'],
        'confirmed' => ['name' => 'Подтверждено', 'color' => '#36bef8'],
        'work' => ['name' => 'В работе', 'color' => '#2ba1e9'],
        'performed' => ['name' => 'Работы выполнены', 'color' => '#935892'],
        'final_positive' => ['name' => 'Успешно реализовано', 'color' => '#935892'],
        'final_negative' => ['name' => 'Не реализовано', 'color' => '#d02b29'],
    ];

    public function statusName() {
        return $this->status ? self::$statuses[$this->status]['name'] : '';
    }

    public function statusColor() {
        return $this->status ? self::$statuses[$this->status]['color'] : '';
    }

    /*public function days() {
        return $this->hasMany('App\Models\OrderDay', 'order_id');
    }

    public function users() {
        return $this->hasManyThrough('App\Models\OrderUser', 'App\Models\OrderDay', 'order_id', 'day_id');
    }*/

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function orderCompany() {
        return $this->belongsTo('App\Models\OrderCompany', 'order_company_id');
    }

    public function metro() {
        return $this->belongsTo('App\Models\Metro');
    }

    /*public function sms(){
        return $this->hasMany('App\Models\Sms', 'order_id');
    }*/

    public function datePicker() {
        if($this->date_start) {
            return (new Date($this->date_start))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function dateFinishPicker() {
        if($this->date_finish) {
            return (new Date($this->date_finish))->format('d.m.Y');
        } else {
            return '';
        }
    }

    public function contacts() {
        return $this->belongsToMany('App\Models\OfferContact', 'offer_contact', 'offer_id', 'contact_id');
    }


    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'offer_product', 'offer_id', 'product_id');
    }

    public function sumNDS($id)
    {
        $offer = Offer::findOrFail($id);

        $sumNDS = 0;
        foreach($offer->products as $product) {
            if($product->nds == 'nds18')
                $sumNDS = $sumNDS + $product->price * 18 / 100;
            else if($product->nds == 'nds10')
                $sumNDS = $sumNDS + $product->price * 10 / 100;
        }
        
        return $sumNDS;
    }

    public function sumTotal($id)
    {
        $offer = Offer::findOrFail($id);

        $sumTotal = 0;
        foreach($offer->products as $product) {
            $sumTotal = $sumTotal + $product->price;
        }

        return $sumTotal;
    }

    function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $this->morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = $this->morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.$this->morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }
    
}
