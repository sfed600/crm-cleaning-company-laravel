<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table = 'user_logs';

    protected $fillable = [
        'responsible',
        'user_id',
        'note',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function responsible() {
        return $this->belongsTo('App\User', 'responsible');
    }
}
