<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AutoModel extends Model
{
    protected $table = 'auto_models';

    //protected $fillable = ['name'];
    protected $fillable = ['name', 'company_id'];
}
