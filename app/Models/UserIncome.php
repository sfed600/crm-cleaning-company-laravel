<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Date;

class UserIncome extends Model
{
    protected $table = 'user_incomes';

    protected $fillable = ['company_id', 'user_id', 'date', 'action', 'amount', 'amount_nigth', 'outcome', 'type', 'comment'];

    //static public $types = ['advance' => 'Аванс', 'wages' => 'Зарплата', 'avance' => 'Премия'];
    static public $types = ['penalty' => 'Штраф', 'wages' => 'Зарплата', 'bonus' => 'Премия', 'advance' => 'Аванс'];

    static public $actions = ['income' => 'Приход', 'outcome' => 'Расход'];

    public function typeName() {
        return self::$types[$this->type];
    }

    public function datePicker() {
        return (new Date($this->date))->format('d.m.Y');
    }

}
