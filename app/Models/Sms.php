<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $table = 'sms';

    protected $fillable = ['phones', 'text', 'result', 'error', 'order_id'];

    protected $url = 'http://smsc.ru/sys/send.php?login=<login>&psw=<password>&phones=<phones>&mes=<message>&charset=utf-8';

    protected $login = 'oa@sph-cl.ru';

    protected $password = 'sp@5700S';

    protected $error = '';

    public function send($phones, $message) {
        $data = [
            '<login>' => $this->login,
            '<password>' => $this->password,
            '<phones>' => $phones,
            '<message>' => urlencode($message),
        ];

        $url = str_replace(array_keys($data), array_values($data), $this->url);
        $result = file_get_contents($url);

        //dump($url);
        //dump($result);

        if(strpos($result, 'ERROR')!==false) {
            $res = explode('=', $result);
            if(!empty($res[1])) {
               $this->setError($res[1]);
            }
            return false;
        }
        return true;
    }

    protected function setError($error) {
        $this->error = $error;
    }

    public function getError() {
        return $this->error;
    }

    public function users() {
        return $this->belongsToMany('App\User', 'sms_user', 'sms_id', 'user_id');
    }

    public function order(){
        return $this->belongsTo('App\Models\Order', 'order_id');
    }


}
