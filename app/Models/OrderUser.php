<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{
    protected $table = 'order_users';

    protected $fillable = ['day_id', 'user_id', 'cef', 'amount', 'comment', 'driver', 'auto_id', 'main', 'late', 'nocome', 'rating'];

    public function day() {
        return $this->belongsTo('App\Models\OrderDay', 'day_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
