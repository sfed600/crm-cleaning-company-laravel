/*$(function(){

})*/

function getPassFromSms() {
    var phone = $('input[name="login"]').val();

    $.get(
        '/cleaner/password/reset',
        {
            'phone': phone
        }/*,
        function (data) {
            console.log(data);
            if(data) {
                alert(data);
            }
        }*/
    );
}


//////////////////////////////////////////////////////////////////////////////////
var months = {
    'Январь': 0,
    'Февраль': 1,
    'Март': 2,
    'Апрель': 3,
    'Май': 4,
    'Июнь': 5,
    'Июль': 6,
    'Август': 7,
    'Сентябрь': 8,
    'Октябрь': 9,
    'Ноябрь': 10,
    'Декабрь': 11,
};

function set_period_month(elm) {
    var year = $('.filter_year > div').text();
    var month = $(elm).text();
    var last_day = getLastDayOfMonth(year, months[month]);
    var period = '01.' + (months[month]+1) + '.' + year + '-' + last_day + '.' + (months[month]+1) + '.' + year;
    $('[name="f[date-range]"]').val(period);

    $('form#cleaner-tabel-filter').submit();
}

function set_period_year(elm) {
    var year = $(elm).text();
    var month = $('.filter_month > div').text();
    var last_day = getLastDayOfMonth(year, months[month]);
    var period = '01.' + (months[month]+1) + '.' + year + '-' + last_day + '.' + (months[month]+1) + '.' + year;
    $('[name="f[date-range]"]').val(period);
    //console.log(period);

    $('form#cleaner-tabel-filter').submit();
}

function getLastDayOfMonth(year, month) {
    var date = new Date(year, month + 1, 0);
    return date.getDate();
}

function set_company_filter(company_id) {
    $('[name="f[company]"]').val(company_id);

    $('form#cleaner-tabel-filter').submit();
}