$(document).ready(function() {
    var $document = $(document);
    var $body = $('body');
    var $dashboardContent = $('#dashboard-content');


    /* Add KP pesron template */
    $document.on('click', '[data-kp-addtemplate]', function () {
        var $this = $(this);
        var $addTemplate = $this.data('kp-addtemplate');
        var $getTemplate = $($addTemplate).html();

        $getTemplate = $getTemplate.replace(/{id}/g, Date.now());

        var $elTemplace = $($getTemplate);
        $elTemplace.hide();

        if ($this.data('kp-addtemplate-append')) {
            $elTemplace.appendTo($($this.data('kp-addtemplate-append'))).fadeIn(500);
        }
        else if ($this.data('kp-addtemplate-prepend')) {
            $elTemplace.prependTo($($this.data('kp-addtemplate-append'))).fadeIn(500);
        }
        $document.trigger('init-chosen');
    });

    $document.on('click', '[data-kp-row-remove="button"]', function () {
       var $this = $(this);
       var $item = $this.parents('[data-kp-row-remove="item"]');
        $item.fadeOut(500, function () {
            $item.remove();
        });
    });




    /* CONTACTS :: Order autocomplete add */
    var availablePerson = [
        "Осипов Павел",
        "Осипов Павел 2",
        "Осипов Павел 3",
        "Клоков Василий Петрович",
        "Клоков Василий Петрович 2",
        "Клоков Василий Петрович 3"
    ];

    $( ".js-personal-person" ).autocomplete({
        source: availablePerson,
        select: function( event, ui ) {
            $( this ).val(ui['item']['value']).trigger('op-input-change');
            return false;
        }
    });


    $document.on('change', '[data-brigadier-driver="check"]', function () {
        var $checked = $(this);
        var $wrap = $checked.parents('[data-brigadier-driver="wrap"]');
        if($checked.is(':checked'))
        {
            $wrap.removeClass('type-disabled');
        }
        else
        {
            $wrap.addClass('type-disabled');
        }
    });


    $document.on('click', '[data-btn-personal-loadr]', function () {
        var $item = $(this);
        $item.addClass('current').siblings('[data-btn-personal-loadr]').removeClass('current');
    });


    
    $document.on('blur', '.js-ajax-valid', function () {
        var $input = $(this);
        var $value = $input.val();
        var $name = $input.attr('name');

        $input.addClass('input-request');
    });


    // Calendar insert ////////////////////////////////////////////////////////////
    // Массив дней, возможно его надо грузить с помощью ajax либо сразу принтовать в html
    /*var $plannedDateArray = {
        '2018-8-14' : {'counter' : '5', 'color' : '#e92020'},
        '2018-8-15' : {'counter' : '6', 'color' : '#e92020'},
        '2018-8-16' : {'counter' : '7', 'color' : '#e92020'},
        '2018-8-17' : {'counter' : '8', 'color' : '#e92020'},
        '2018-8-18' : {'counter' : '9', 'color' : '#e92020'},
        '2018-8-24' : {'counter' : '3', 'color' : '#278845'},
        '2018-8-25' : {'counter' : '3', 'color' : '#278845'},
        '2018-8-26' : {'counter' : '3', 'color' : '#278845'},
        '2018-8-27' : {'counter' : '3', 'color' : '#ffc924'},
        '2018-8-28' : {'counter' : '3', 'color' : '#ffc924'},
        '2018-8-29' : {'counter' : '3', 'color' : '#ffc924'}
    };*/

    $document.on('focus', '[data-kpcalendar="date"]', function () {
        //console.log('zz');
        var $this = $(this);
        if($this.hasClass('init-script')) return;
        $this.addClass('init-script');

        var $datepickerBottom = $('<div/>').addClass('datepicker--bottom');
        var $datepickerBottomInfo = $('<div/>').addClass('datepicker--bottom-info');
        var $datepickerBottomBtn = $('<div/>').addClass('datepicker--bottom-btn').html('<span class="o-btn o-hvr bgs-green" data-btn-clndr="choise">Выбрать</span><span class="o-btn o-hvr bgs-orange" data-btn-clndr="cancel">Отмена</span>');
        $datepickerBottomInfo.appendTo($datepickerBottom);
        $datepickerBottomBtn.appendTo($datepickerBottom);

        var $options = {
            navTitles: {
                days: 'MM yyyy'
            },
            prevHtml: '<span class="datepicker--nav-btn-prev"><span></span></span>',
            nextHtml: '<span class="datepicker--nav-btn-next"><span></span></span>',
            moveToOtherYearsOnSelect: false,
            dateFormat: '',
            toggleSelected: false,
            showOtherYears: false,
            selectOtherYears: false,
            autoClose: false,
            onRenderCell: function(date, cellType) {
                //console.log(cellType);
                if(cellType == 'day')
                {
                    var $currentDate = false;
                    if(date) {
                        $currentDate = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();
                    }

                    //ajax
                    /*$.ajax({
                     type: "GET",
                     url: '/admin/orders/allCountOfDay',
                     async: false,
                     data: {
                     year: date.getFullYear(),
                     month: date.getMonth()+1,
                     day: date.getDate()
                     },
                     //datatype: "json",
                     success: function(data){
                     counter = data;
                     //console.log("Inside ajax: "+result);
                     }
                     });*/

                    //console.log(date, $currentDate);
                    /*if (date && $currentDate && $currentDate in $plannedDateArray) {
                     return {
                     html: '<span class="day-value"><span class="day-number" style="color:' + $plannedDateArray[$currentDate]['color'] + ';">' + $plannedDateArray[$currentDate]['counter'] + '</span><span class="day-date">' + date.getDate() + '</span></span>',
                     };
                     }
                     if(date)
                     {
                     return {html : '<span class="day-value"><span class="day-date">' + date.getDate() + '</span></span>'};
                     }*/

                    //console.log(plannedDateArray[date.getMonth()+1][date.getDate()]);
                    if (typeof plannedDateArray[date.getMonth()+1] != 'undefined') {
                        if (typeof plannedDateArray[date.getMonth()+1][date.getDate()] != 'undefined') {
                            return {
                                html: '<span class="day-value"><span class="day-number" style="color:red;">' + plannedDateArray[date.getMonth()+1][date.getDate()] + '</span><span class="day-date">' + date.getDate() + '</span></span>',
                            };
                        }
                    }

                    if(date)
                    {
                        return {html : '<span class="day-value"><span class="day-date">' + date.getDate() + '</span></span>'};
                    }

                }
            },
            onShow: function (inst, animationCompleted) {
                //console.log('onShow');
                if(!animationCompleted)
                {
                    $this.addClass('input-focused');
                }
                if(!animationCompleted && !inst.$content.find('.datepicker--bottom').length)
                {
                    $datepickerBottom.appendTo(inst.$content);
                    $datepickerBottomBtn.on('click', '[data-btn-clndr="choise"]', function () {
                        $this.val($this.attr('data-choised'));
                        $this.blur();
                    });
                    $datepickerBottomBtn.on('click', '[data-btn-clndr="cancel"]', function () {
                        $this.blur();
                    });
                }
            },
            onHide: function (inst, animationCompleted) {
                if(animationCompleted)
                {
                    $this.removeClass('input-focused');
                }
            },
            onSelect: function onSelect(fd, date) {
                //console.log($this.val());

                //600 при заполнении даты начала заказа дата конца = дата начала
                var parent = $this.parents('[data-kporder-date="clone"]');
                $(parent).find('input[name="date_finish[]"]').val($this.val());

                var $currentDate = false;
                if(date)
                {
                    $currentDate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();

                    //если вкладка "Основное"
                    if( $('[data-findtab="kp-tabbox-main"]').hasClass('current') )
                        save_order_periods($this, 'change');
                }

                //если вкладка "Основное"
                if( $('[data-findtab="kp-tabbox-main"]').hasClass('current') ) {
                    if (typeof plannedDateArray[date.getMonth()+1] != 'undefined') {
                        if (typeof plannedDateArray[date.getMonth()+1][date.getDate()] != 'undefined') {
                            /*return {
                             html: '<span class="day-value"><span class="day-number" style="color:red;">' + plannedDateArray[date.getMonth()+1][date.getDate()] + '</span><span class="day-date">' + date.getDate() + '</span></span>',
                             };*/
                            $datepickerBottomInfo.html('<span style="color:red;">' + plannedDateArray[date.getMonth()+1][date.getDate()] + ' уборок запланировано в этот день</span>');
                        } else {
                            $datepickerBottomInfo.html('<span>В этот день нечего не запланировано</span>');
                        }
                    } else {
                        $datepickerBottomInfo.html('<span>В этот день нечего не запланировано</span>');
                    }
                }

                $this.val('').attr('data-choised', fd);

            }
        };
        var $datepicker = $this.datepicker($options).data('datepicker');
        //$datepicker.selectDate(new Date(2018, 3, 11));
        //$datepicker.date = new Date(2018, 3, 11);
    });


    $document.on('focus', '[data-kpcalendar="time"]', function () {
        //console.log('data-kpcalendar');
        var $this = $(this);
        if($this.hasClass('init-script')) return;
        $this.addClass('init-script');

        var $options = {
            timepicker: true,
            dateFormat: ' ',
            timeFormat: 'hh:ii',
            classes: 'only-timepicker',
            onShow: function (inst, animationCompleted) {
                //console.log(inst, animationCompleted);
                if(!animationCompleted) $this.addClass('input-focused');
            },
            onHide: function (inst, animationCompleted) {
                //console.log(inst, animationCompleted);
                if(animationCompleted) $this.removeClass('input-focused');
            }
        };
        var $datepicker = $this.datepicker($options).data('datepicker');
    });


    /* END */

});