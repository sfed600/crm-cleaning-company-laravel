$(document).ready(function() {
    
    //при закрытии страницы/браузера если данные не сохранены
    $(window).on("beforeunload", function() {
        //если нажали "Сохранить" то без предупреждения
        if (userClickedSave)
            return;

        //console.log($('.page-kp-form-bottom-button').css('display'));
        if( $('.page-kp-form-bottom-button').is(":visible") )
            return "Данные не сохранены. Точно перейти?";
    })
    
    var $document = $(document);

    $document.on('click', '[data-contact-block-photo="remove"]', function () {
        var $this = $(this);
        var $wrap = $('[data-contact-block-photo="wrap"]');
        $wrap.removeClass('type-uploads');
    });

    $document.on('change', '[data-contact-block-photo="upload"]', function () {
        var $this = $(this);
        var $wrap = $('[data-contact-block-photo="wrap"]');


        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var $elImage = $wrap.find('[data-contact-block-photo="image"]');
                $elImage.attr({'href' : e.target.result}).css({'background-image' : 'url(' + e.target.result + ')'});
                $wrap.addClass('type-uploads');
            };
            reader.readAsDataURL(this.files[0]);
        };
    });


    //////////////////////////////////
    
    /* сохр имя(name) */
    //console.log(window.location.pathname);
    if( (window.location.pathname.indexOf('/admin/order_contacts/') + 1) > 0 ) {
        var link = $('input#order_contact_heads_label');
        link.on('change', function () {
            $('#order-contact-form').find('input[name="name"]').val(this.value);
        });

        //при изменениии сделать видимой кнопку "Сохранить"
        link.on('keyup', function() {
            if( $('.page-kp-form-bottom-button').is(":hidden") )
                $('.page-kp-form-bottom-button').show();
        });
    }


    //изменение данных ч/з ajax//////////////////////////////////
    //photo
    /*var link = $('#order-contact-form').find('input[upload-ajax]');
    link.on('change', function () {
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('photo', file_data);
        form_data.append('_token', $('[name="_token"]').val());
        form_data.append('id', $('[name="contact_id"]').val());
        //alert(form_data);
        $.ajax({
            url: '/admin/order_contacts/update_ajax',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(response){
                //alert(response);
                if(response.length > 0 ) {
                    //console.log(response);
                    $.notifye('error', response, 10000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notifye('error', 'Файл слишком большой.', 10000);
                //alert(thrownError);
            }
        });
    });

    //comments
    var link = $('#order-contact-form').find('textarea[name="comments"]');
    link.on('change', function(){
        //console.log($(this).val());
        update_ajax('comments', 'order_contacts');
    });

    var link = $('#order-contact-form').find('[update-ajax]');
    //console.log(link.prop('name'));
    link.on('change', function(){
        if( $(this).prop('id') == 'order_contact_heads_label')
            update_ajax('name', 'order_contacts');
        else
            update_ajax($(this).prop('name'), 'order_contacts');
    });

    //address
    var link = $('#order-contact-form').find('input#suggest');
    link.focusout(function() {
        //console.log(link.val());
        update_ajax('address', 'order_contacts');
    });*/

    //при изменениии сделать видимой кнопку "Сохранить" /*
    //img
    var link = $('#order-contact-form').find('input[upload-ajax]');
    link.on('change', function () {
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });

    var link = $('#order-contact-form').find('[update-ajax]');
    link.on('keyup', function() {
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });

    //comments
    var link = $('#order-contact-form').find('textarea[name="comments"]');
    link.on('keyup', function(){
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });
    //  */
});