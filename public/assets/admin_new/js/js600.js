$(function(){
    //переключение табов в календаре
    /*var link = $('[data-tab="dash-kp"]');
    link.on('click', function(){
        console.log( $(this).attr('data-findtab') );
    });*/


    ////////////////////////////
    var link = $('[data-findtab="kp-tabbox-staff"]');
    link.on('click', function(){
        $('.order-navbar-btn').text('+ Добавить смену');
        $('.order-navbar-btn').attr('onclick', 'add_turn();');
        //$('.dash-panel-dropdown-drop').hide();

        $('.dash-wrap').addClass('tbs-kp-tabbox-personal');
    });

    var link = $('[data-findtab="kp-tabbox-offers"]');
    link.on('click', function(){
        $('.order-navbar-btn').text('+ Добавить КП');
        $('.dash-panel-dropdown-drop').show();

        $('.dash-wrap').removeClass('tbs-kp-tabbox-personal');
    });

    link = $('[data-findtab="kp-tabbox-calc"]');
    link.on('click', function(){
        $('.order-navbar-btn').text('+ Добавить Заказ');
        $('.order-navbar-btn').attr('onclick', 'window.location.href = \'/admin/orders/create\';');
        $('.dash-panel-dropdown-drop').hide();

        $('.dash-wrap').removeClass('tbs-kp-tabbox-personal');
    });

    link = $('[data-findtab="kp-tabbox-main"]');
    link.on('click', function(){
        $('.order-navbar-btn').text('+ Добавить Заказ');
        $('.order-navbar-btn').attr('onclick', 'window.location.href = \'/admin/orders/create\';');
        $('.dash-panel-dropdown-drop').hide();

        $('.dash-wrap').removeClass('tbs-kp-tabbox-personal');
    });

    
    //////////////////////////////////////////////////////////////////////
    if( (window.location.pathname.indexOf('/admin/orders/') + 1) > 0 ) {
        var link = $('input#order-header-name');
        link.on('change', function() {
            $('input[name="name"]').val(this.value);
        });
        
        //back link
        /*console.log($('.dash-panel-heads-label:after').length);
        $('.dash-panel-heads-label:after').on('click', function(){
            alert();
        });*/
    }

    //orders filter /*///////////////////////////////////////////////////
    var link = $('select[name="f[sum_compare]"]');
    var filter_sum_block = link.parents('.o-form-row-sum');
    //console.log(filter_sum_block);
    link.on('change', function() {
        $(filter_sum_block).children().last().remove();

        if( $(this).val() == 'equal' || $(this).val() == 'more' || $(this).val() == 'less' ) {
            filter_sum_block.append('<div class="o-form-row-sum-col col-value"><div class="o-form-row-input"><input type="text" name="f[sum]" /></div></div>');
        } else if( $(this).val() == 'interval') {
            filter_sum_block.append('<div class="o-form-row-sum-col col-value"><div class="o-form-row-input" style="display: flex;"><input type="text" name="f[sum_min]" style="width: 48%;" /> <div class="interval-dash">&nbsp;-&nbsp;</div> <input name="f[sum_max]" type="text" style="width: 48%;" /></div></div>');
        }
    });
    //orders filter */
})

///////////////////////////////////////////////////////////////////////////
function clear_input(elm) {
    //console.log('zzz');
    var $this = $(elm);
    var $wrap = $this.parents('.o-form-row-input');
    $wrap.find('input[type]').val('').trigger('op-input-change');
}

function tabel_paginator_init(items, perpage, currentPage) {
    $('.js-paginator-report').pagination({
        items: items,
        itemsOnPage: perpage,
        displayedPages: 5,
        currentPage: currentPage,
        prevText : '&laquo;',
        nextText : '&raquo;',
        cssStyle: 'pag-theme',
        //hrefTextPrefix: '?page='
        hrefTextPrefix: window.location.href + '&page='
    });
}

function perpage_pagination(perpage, entity) {
    $('[name="f[perpage]"]').val(perpage);
    
    $('#'+entity+'_filter_form').submit();
}

//filters   /*
function set_orders_filter_status(status, elm) {
    $('[name="f[status]"]').val(status);

    $('ul.dash-search-drop-list > li').removeClass('current');
    $(elm).addClass('current');
}

function reset_filter(entity) {
    $('form#' + entity + '_filter_form input').val('');
    $('form#' + entity + '_filter_form select').val('');
    $('ul.dash-search-drop-list > li').removeClass('current');

    $('.dash-search-option').html('');

    //$('form#tabel-filter-form').submit();
    window.location.href = '/admin/' + entity + '?refresh=1';
}

function exclude_from_filter(elm, filter, entity) {
    $(elm).parent().remove();

    /*var count_hidden = $('.filters_btns>button').length - 2;
     if( $('.filters_btns>button').length > 2 ) {
     for (var i = 0; i <= $('.filters_btns>button').length; i++) {
     //console.log(i);
     $('.filters_btns>button').eq(i).remove();
     }

     $('.filters_btns').append('<button class="btn btn-info btn-xs" type="button" data-filter="date">Еще '+ String(count_hidden+1) +' <i class="ace-icon fa fa-times" onclick="update_filters_result(this);"></i></button>');
     }*/

    window.location.href = '/admin/' + entity + '/exclude_from_filter/' + filter;
}

function set_tabel_filter_period(period, elm) {
    //$('[name="f[period]"]').val(period);
    $('a[onclick^="set_tabel_filter_period"]').removeClass('current');
    $(elm).addClass('current');

    $.get(
        '/admin/tabel/get_period_dates/' + period,
        function(data) {
            //console.log(JSON.parse(data).period_start);
            $('[name="f[daterange]"]').val(JSON.parse(data).period_start + ' - ' + JSON.parse(data).period_end);
        }
    );
}
//filters   */

function set_order_status($this) {
    //console.log($($this).data('value'));
    var $option = $(this);
    var $optionClass = $option.data('class');

    // Добавляем класс фона от опции
    if ($optionClass) {
        $title.removeClass(function (index, className) {
            return (className.match(/(^|\s)bgs-\S+/g) || []).join(' ');
        });
        $title.addClass($optionClass);
    }

    $title.text($option.text());

    $option.addClass('current').siblings('[data-value]').removeClass('current');
    $selectWrap.removeClass('open-drop');
}


/*
function setPrice(btn) {
    $.post(
        '/admin/settings/price',
        {
            '_token': $('[name="_token"]').val(),
            'data': $(btn).parents('form').serialize()
        },
        function(data) {
            //console.log(data);
            if(data == 'success')
                alert('Сохранено.');
        }
    );
}*/
