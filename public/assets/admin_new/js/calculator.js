$(function(){
    var link = $('input[calculator]');
    link.on('change', function() {
        //console.log('qaz');
        calculate(this);
        total();

        /*$('input[name="calculator"]').val('');
        $('input[name="calculator"]').val( $('form#calculator-form').serialize() );*/
    });

    var link = $('input[name="discount"]');
    link.on('change', function() {
        total();
        /*$('input[name="calculator"]').val('');
        $('input[name="calculator"]').val( $('form#calculator-form').serialize() );*/
    });

    var link = $('input[name="surcharge"]');
    link.on('change', function() {
        total();
        $('input[name="calculator"]').val('');
        $('input[name="calculator"]').val( $('form#calculator-form').serialize() );
    });

    //save calculator data
    var link = $('[data-action="save_calculator_data"]');
    link.on('click', function() {
        //console.log('qq');
        save_calculator_data($('input[name="calculator"]').val(), link.data("action_mode"));
    });


    ///////////////listeners /*
    /*$('span.op-input > input[name="amount"]').bind( 'DOMSubtreeModified',function(){
        if($('span.op-input > input[name="amount"]').val() > 0 )
            $('form#order-form-main > input[name="amount"]').val($('span.op-input > input[name="amount"]').val());

    });*/

    $('[data_name="total_garniture"]').bind( 'DOMSubtreeModified',function(){
        if($('[data_name="total_garniture"]').text() > 0 )
            $('input[value="dry-cleaning"]').prop('checked', true);
            
    });

    $('[name="total_cover"]').bind( 'DOMSubtreeModified',function(){
        if($('[name="total_cover"]').text() > 0 )
            $('input[value="dry-cleaning"]').prop('checked', true);

    });

    $('[name="total-windows_and_jaluzi"]').bind( 'DOMSubtreeModified',function(){
        if($('[name="total-windows_and_jaluzi"]').text() > 0 )
            $('input[value="front"]').prop('checked', true);

    });
    //  */

})

function save_calculator_data(old_data, mode) {
    //console.log($('[name="dry_cleaning"]').prop("checked"));
    //проверить базовую инфу
    var base_info = false;
    if( $('[name="amount"]').val() > 0) {
        $('[name="base_info"]').each(function (i) {
            //console.log($(this).prop("checked"));
            if($(this).prop("checked") == true) {
                base_info = true;
            }

        });

        if(!base_info)
            return false;
    }

    //$('input[name="calculator"]').val( $('form#calculator-form').serialize() );
    var total_vals = '&amount='+$('input[name="calculator_amount"]').val()+'&total1='+$('[data_name="total1"]').text() + '&total-windows_and_jaluzi='+$('[data_name="total_windows_and_jaluzi"]').text() + '&total_garniture='+$('[data_name="total_garniture"]').text() + '&total_cover='+$('[data_name="total_cover"]').text() + '&total_other_services='+$('[data_name="total_other_services"]').text() + '&total_additionally_works=' + $('[data_name="total_additionally_works"]').text() + '&total='+$('[data_name="total"]').text() + '&total-discount='+$('[data_name="total-discount"]').text();
    $('input[name="calculator"]').val( $('form#calculator-form').serialize() + total_vals );

    /*if(mode == 'update') {
        var form = document.getElementById('order-form-main');
        var action = $(form).attr('action');
        $(form).attr('action', action + '?route=back')
    }*/

    //console.log($('[name="order_id"]').val());

    if(mode != 'ajax') {
        document.getElementById('order-form-main').submit();
    } else {
        $.post(
            '/admin/orders/update_calculator',
            {
                '_token': $('[name="_token"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'calculator': $('input[name="calculator"]').val(),
                'old_data': old_data
            },
            function (data) {
                //$('[name="calculator"]').val(data);

                $.preloader.show({'attach' : $('.dash-wrap')});
                setTimeout(function() {
                    $.preloader.hide({'attach' : $('.dash-wrap')});
                }, 1000);
            }
        );
    }
}

function windows_and_jaluzi_total() {
    //windows
    var price = 0;

    if( $('[name="two_sides_stvorok"]').prop('checked')) {
        price = price + ( parseInt($('[ name="windows-stvorok"]').attr('price'))*2* parseInt($('[ name="windows-stvorok"]').val()) || 0);
    }
    else
        price = price + ( parseInt($('[ name="windows-stvorok"]').attr('price')) * parseInt($('[ name="windows-stvorok"]').val()) || 0);

    if( $('[name="two_sides_metrov"]').prop('checked')) {
        price = price + ( parseInt($('[ name="windows-metrov"]').attr('price'))*2* parseInt($('[ name="windows-metrov"]').val()) || 0);
    }
    else
        price = price + ( parseInt($('[ name="windows-metrov"]').attr('price')) * parseInt($('[ name="windows-metrov"]').val()) || 0);

    price = price + ( parseInt($('[name="windows-panaram"]').attr('price')) * parseInt($('[name="windows-panaram"]').val()) || 0 );

    //windows_total = price.toFixed(2);
    windows_total = price;

    //jaluzi
    price =  parseInt($('[name="jaluzi_shtuk"]').val() * $('[name="jaluzi_shtuk"]').attr('price')) || 0;
    price = price + parseInt($('[name="jaluzi_metrov"]').val() * $('[name="jaluzi_metrov"]').attr('price')) || 0;

    //jaluzi_total = price.toFixed(2);
    jaluzi_total = price;

    //console.log((windows_total + jaluzi_total).toFixed(2).toString() );
    //$('[name="total-windows_and_jaluzi"]').text((windows_total + jaluzi_total).toFixed(2));
    $('[data_name="total_windows_and_jaluzi"]').text((windows_total + jaluzi_total).toFixed(2));

    if( (windows_total + jaluzi_total) > 0 )
        $('[calculator="front"]').prop('checked', true);
    else
        $('[calculator="front"]').prop('checked', false);

    total();
    /*$('input[name="calculator"]').val('');
    $('input[name="calculator"]').val( $('form#calculator-form').serialize() );*/
}


function cover() {
    //init price
    var price_kover = parseInt($('[name="price_kover"]').attr('price')) || 0;

    var price_cover = 0;
    var cover_metrov = parseInt($('[name="cover_metrov"]').val()) || 0;
    if( 0 < cover_metrov && cover_metrov <= 50 )
        price_cover = parseInt($('[name="price_cover"]').attr('price50')) || 0;
    else if( 50 < cover_metrov && cover_metrov <= 75 )
        price_cover = parseInt($('[name="price_cover"]').attr('price75')) || 0;
    else if( 75 < cover_metrov && cover_metrov <= 90 )
        price_cover = parseInt($('[name="price_cover"]').attr('price90')) || 0;
    else if( 90 < cover_metrov && cover_metrov <= 120 )
        var price_cover = parseInt($('[name="price_cover"]').attr('price120')) || 0;
    else if( 120 < cover_metrov && cover_metrov <= 200 )
        var price_cover = parseInt($('[name="price_cover"]').attr('price200')) || 0;
    else if( 200 < cover_metrov && cover_metrov <= 300 )
        var price_cover = parseInt($('[name="price_cover"]').attr('price300')) || 0;
    else if( cover_metrov > 300 )
        var price_cover = parseInt($('[name="price_cover"]').attr('price400')) || 0;

    
    //vorce
    radios = $('[name="vorce"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    price_kover = price_kover + price_kover /100 * procent;
    var kover_sum = ( price_kover * $('[name="kover_shtuk"]').val() ) || 0;
    $('[name="kover_shtuk"]').next().val( kover_sum );

    var data = {};
    data.price_kover = price_kover.toFixed(2);
    data.kover_sum = kover_sum.toFixed(2);

    data.price_cover = price_cover.toFixed(2);
    if( cover_metrov > 300 )
        data.cover_sum = ( price_cover * $('[name="cover_metrov"]').val() ) || 0;
    else
        data.cover_sum = price_cover || 0;

    data.block_total = parseInt(data.kover_sum) + parseInt(data.cover_sum);
    //console.log(data);
    //return data;

    //degree_kover
    radios = $('[name="degree_kover"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    data.block_total = data.block_total + data.block_total /100 * procent;

    $('[name="price_kover"]').val(data.price_kover);
    $('[name="price_cover"]').val(data.price_cover);
    $('[name="sum_kover"]').val(data.kover_sum);
    $('[name="sum_cover"]').val(data.cover_sum);
    $('[data_name="total_cover"]').text(data.block_total);
    $('[data_name="total_cover"]').val(data.block_total);

    //console.log(price_cover);
    if( (data.block_total) > 0 )
        $('[calculator="dry_cleaning"]').prop('checked', true);
    else
        $('[calculator="dry_cleaning"]').prop('checked', false);

    total();
    /*$('input[name="calculator"]').val('');
    $('input[name="calculator"]').val( $('form#calculator-form').serialize() );*/
}


function total() {
    var total_within = 0;
    $('[total="within"]').each(function () {
        total_within = total_within + ( parseInt(this.value) || 0);
    });

    var total_additionally = 0;
    $('[data-name="additionally_sum_row"]').each(function () {
        total_additionally = total_additionally + ( parseInt($(this).text()) || 0);
    });
    $('[data_name="total_additionally_works"]').text(total_additionally);

    //console.log(parseInt($('[name="total_garniture"]').text()));
    //var total = ( parseInt($('[name="total1"]').text()) || 0 ) + ( parseInt($('[name="total-windows_and_jaluzi"]').text()) || 0 ) + ( parseInt($('[name="total_garniture"]').text()) || 0 )  + ( parseInt($('[name="total_cover"]').text()) || 0 ) + ( parseInt($('[name="total_other_services"]').text()) || 0 );
    var total = ( parseInt($('[data_name="total1"]').text()) || 0 ) + ( parseInt($('[data_name="total_windows_and_jaluzi"]').text()) || 0 ) + ( parseInt($('[data_name="total_garniture"]').text()) || 0 )  + ( parseInt($('[data_name="total_cover"]').text()) || 0 ) + ( parseInt($('[data_name="total_other_services"]').text()) || 0 ) + total_additionally;

    //nal or beznal
    radios = $('[calculator="payment"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    total = total + total/100*procent;

    total = total.toFixed(2);
    //console.log(total);
    $('[data_name="total"]').text(Math.ceil(total));

    //surcharge
    var surcharge = $('[name="surcharge"]').val() || 0;
    total = Math.ceil(parseInt(total) + parseInt(total) / 100 * parseInt(surcharge));
    $('[data_name="total"]').text( total );
    var _total = $('[data_name="total"]').text();
    var sub = parseInt(_total.substr(_total.length - 2));
    //console.log(sub);
    if( sub > 0 )
        $('[data_name="total"]').text( Math.ceil(parseInt(total) + (100 - sub)) );

    //discount
    var discount = $('[name="discount"]').val() || 0;
    if(discount > 0) {
        $('[data_name="total-discount"]').text( Math.ceil(total - total / 100 * discount) );
        var total_discount = $('[data_name="total-discount"]').text();
        var sub = parseInt(total_discount.substr(total_discount.length - 2));
        //console.log(discount);
        if( sub > 0 )
            $('[data_name="total-discount"]').text( Math.ceil(parseInt(total_discount) + (100 - sub)) );

        //$('[name="amount"]').val($('[data_name="total-discount"]').text());
    } /*else
        $('[name="amount"]').val($('[data_name="total"]').text());*/

    if( $('[data_name="total-discount"]').text().length == 0 ) {
        $('input[name="calculator_amount"]').val( $('[data_name="total"]').text() );
        //$('input[name="amount"]').val( $('[data_name="total"]').text() );
    }
    else {
        $('input[name="calculator_amount"]').val($('[data_name="total-discount"]').text());
        //$('input[name="amount"]').val($('[data_name="total-discount"]').text());
    }
}

/*function garniture(percent) {
    //garniture block
    if( percent > 0 ) {
        $('[calculator="garniture"]').each(function () {
            var old_price = (parseInt($(this).parent().prev().text()) || 0);
            var new_price = old_price + old_price/100 * percent;
            //console.log(parseInt($(this).val()) || 0);
            $(this).parent().prev().text( (new_price || ''));
            $(this).parent().next().text( (new_price * (this.value || 0)) || '');
        });
    } else {
        $('[calculator="garniture"]').each(function () {
            var td_price = $(this).parent().prev();
            var new_price = (parseInt(td_price.attr('price')) || 0);
            //console.log(new_price);
            td_price.text( (new_price || ''));
            $(this).parent().next().text( (new_price * this.value || '') );
        });
    }

    var sum = 0;
    $('[calculator="garniture"]').each(function () {
        //console.log(parseInt($(this).parent().next().text()));
        sum = sum + ( parseInt($(this).parent().next().text()) || 0 );
    });
    $('[name="total_garniture"]').text(toString(sum));
}*/

/*function tura() {
    var tura_quantity = parseInt($('[name="tura_quantity"]').val()) || 0;
    if( tura_quantity > 0 ) {
        price = 0;
        radios = $('[name="tura"]');
        for (var i = 0, length = radios.length; i < length; i++)
        {
            if (radios[i].checked)
            {
                //console.log(radios[i].value);
                price = parseInt(radios[i].value) || 0;
                break;
            }
        }

        //nal or beznal
        radios = $('[calculator="payment"]');
        procent = 0;
        for (var i = 0, length = radios.length; i < length; i++)
        {
            if (radios[i].checked)
            {
                //console.log(radios[i].value);
                procent = radios[i].value;
                break;
            }
        }
        price = price + price /100 * procent;

        $('[name="tura_sum"]').val(price * tura_quantity);
    }
}*/

function price_kv_metr() {
    //init price ген./послестрой
    var price = parseInt($('[name="price_kv_metr"]').attr('price')) || 0;
    //var price = parseInt($('[name="price_kv_metr"]').val()) || 0;
    var radios = $('[calculator="typ"]');
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            price = parseInt(radios[i].value);
            break;
        }
    }
    //console.log(price);

    //night_rate
    /*var night_rate = parseInt($('[calculator="night_rate"]').val()) || 0;
    price = price + price /100 * night_rate;*/
    
    //потолки
    //var radios = document.getElementsByName('genderS');
    radios = $('[calculator="ceiling_height"]');
    var procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    price = price + price /100 * procent;
    //console.log(price);

    //furniture
    radios = $('[calculator="furniture"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    price = price + price /100 * procent;
    //console.log(price);

    //degree
    radios = $('[calculator="degree"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    price = price + price /100 * procent;

    //nal or beznal
    /*radios = $('[calculator="payment"]');
    procent = 0;
    for (var i = 0, length = radios.length; i < length; i++)
    {
        if (radios[i].checked)
        {
            //console.log(radios[i].value);
            procent = radios[i].value;
            break;
        }
    }
    price = price + price /100 * procent;*/

    $('[name="price_kv_metr"]').val(price.toFixed(2));
    if( parseInt($('[calculator="area"]').val()) > 0 ) {
        //var total1 = (price * parseInt($('[calculator="area"]').val())).toFixed(2) || 0;
        var total1 = (price * parseInt($('[calculator="area"]').val())) || 0;
        //console.log(parseInt(total1));
        $('[data_name="total1"]').text(''+total1.toFixed(2));
    }
    else
        $('[data_name="total1"]').text('0');

    //$('[name="total"]').val(data.total);
}

function additionally_works(elm) {
    //console.log(elm.name);
    if(elm.name == 'additionally_quantity[]') {
        var quantity = parseInt(elm.value) || 0;
        var price = parseInt($(elm).parents('tr').find('[name="additionally_price[]"]').val()) || 0;
    }
    else if(elm.name == 'additionally_price[]') {
        var quantity = parseInt($(elm).parents('tr').find('[name="additionally_quantity[]"]').val()) || 0;
        var price = parseInt(elm.value) || 0;
    }
    //console.log(quantity, price);
    $(elm).parents('tr').find('[data-name="additionally_sum_row"]').html((quantity * price)+' руб.');
    total();
}

function calculate(elm) {
    var param = $(elm).attr('calculator');
    //console.log(param);
    switch (param) {
        case 'typ':
            price_kv_metr();
            break;
        case 'ceiling_height':
            price_kv_metr();
            break;
        case 'furniture':
            price_kv_metr();
            break;
        case 'degree':
            price_kv_metr();
            break;
        case 'night_rate':
            price_kv_metr();
            break;
        case 'area':
            price_kv_metr();
            break;
        case 'windows':
            windows_and_jaluzi_total();
            break;
        case 'jaluzi':
            windows_and_jaluzi_total();
            break;
        case 'cover':
            cover();
            break;
        case 'garniture':
            var price = parseInt($(elm).parent().prev().text());
            $(elm).parent().next().text(price * elm.value);

            var sum = 0;
            $('[calculator="garniture"]').each(function () {
                //console.log(parseInt($(this).parents('td.col-pcs').next().find('span').text()));
                sum = sum + ( parseInt($(this).parents('td.col-pcs').next().find('span').text()) || 0 );
            });

            if( sum > 0 )
                $('[calculator="dry_cleaning"]').prop('checked', true);
            else if( parseInt($('[data_name="total_cover"]').text()) == 0 )
                $('[calculator="dry_cleaning"]').prop('checked', false);

            $('[data_name="total_garniture"]').text(sum);
            break;
        case 'tura':
            tura();
            break;
        case 'within':
            $(elm).next().val( parseInt($(elm).attr('price') * $(elm).val()) || 0 );

            //$('[name="total"]').val(data.total);
            break;
        case 'additionally':    //доп. работы
            if(elm.name == 'additionally_quantity[]') {
                var quantity = parseInt(elm.value) || 0;
                var price = parseInt($(elm).parents('tr').find('[name="additionally_price[]"]').val()) || 0;
            }
            else if(elm.name == 'additionally_price[]') {
                var quantity = parseInt($(elm).parents('tr').find('[name="additionally_quantity[]"]').val()) || 0;
                var price = parseInt(elm.value) || 0;
            }
            //console.log(quantity, price);
            $(elm).parents('tr').find('[data-name="additionally_sum_row"]').html((quantity * price)+' руб.');
            total();
            break;
        case 'payment':
            //total_blocks( parseInt($(elm).val()) || 0 );
            /*price_kv_metr();
            windows_and_jaluzi_total();
            cover();*/
            total();
            break;
        default:
            //alert( 'Я таких значений не знаю' );
    }
                
}


