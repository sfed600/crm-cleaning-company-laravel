function errorsCheck3(formId){

    $("#"+formId+" .imp").each(function(indx,element){
        var valueInput = $(element).val(),
            typeInput = $(element).prop('type'),
            isKyr = function (str) {
                return /^[а-яА-ЯёЁa-zA-Z ]+$/.test(str);
            };

        var $form = $("#"+formId);
        var $getType = $(element).attr('data-type');
        if($getType)
        {
            typeInput = $getType;
        }

        if(typeInput == 'tel'){
            valueInput = valueInput.replace(/_/g,'');
            if(valueInput.length<14){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }
        }else if(typeInput == 'tel_code'){
            if(valueInput.length<4){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }
        }else if($(element).hasClass("datepicker")){
            $( ".datepicker" ).mask('99.99.9999');
            if(valueInput.length<10){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }
        }else if(typeInput == 'email'){
            //console.log(isemail(valueInput));
            isemail = function (str) {
                return /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/.test(str);
            };
            if (!isemail(valueInput) || valueInput=="" || valueInput.length<3){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }
        }else if(typeInput == "password"){
            if($(element).val().length >0 && $('#inputPasswordNew').val() == $('#inputPasswordNewCh').val()){
                $(element).removeClass('error');
            }else{
                $(element).addClass('error');
            }
        }else if(typeInput == "file"){


        }else if(typeInput == "ignore"){

        }else if(typeInput == "pass"){
            if($(element).val().length)
            {
                var $getPass1 = $form.find('.inputPasswordNew').val();
                var $getPass2 = $form.find('.inputPasswordNewCh').val();
                if($(element).val().length >0 && $getPass1 == $getPass2 && $getPass1.length >= 6){
                    $(element).removeClass('error');
                }else{
                    $(element).addClass('error');
                }
            }

        }else if(typeInput == "inn"){
            valueInput = valueInput.replace(/_/g,'');
            //if (valueInput=="" || valueInput.length != 12){
            if (valueInput=="" || (valueInput.length != 12 && valueInput.length != 10) ){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }

        }else if(typeInput == "fio"){
            var $getSpace = valueInput.split(' ');
            valueInput = valueInput.replace(/_/g,'');
            if (!isKyr(valueInput) || valueInput=="" || valueInput.length<2 || $getSpace.length < 2 || $getSpace[0].length < 3 || $getSpace[1].length < 3){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }

        }else{
            //console.log(isKyr(valueInput));
            if (!isKyr(valueInput) || valueInput=="" || valueInput.length<2){
                $(element).addClass('error');
            }else{
                $(element).removeClass('error');
            }
        }
    });

    var totalError = 0;
    $("#"+formId+" .error").each(function(){
        totalError += 1;
    });


    if (totalError == 0) {
        $("#"+formId+" .add_message").addClass('active');
        $("#"+formId+" .add_message").removeProp('disabled');
        $("#"+formId+" .add_message").removeAttr('disabled');

        $("#"+formId+" .button_send").addClass('active');
        $("#"+formId+" .button_send").removeProp('disabled');
        $("#"+formId+" .button_send").removeAttr('disabled');

        $("#"+formId+" .sub_btn").addClass('active');
        $("#"+formId+" .sub_btn").removeProp('disabled');
        $("#"+formId+" .sub_btn").removeAttr('disabled');

    }else{

        $("#"+formId+" .add_message").removeClass('active');
        $("#"+formId+" .add_message").prop("disabled","disabled");
        $("#"+formId+" .add_message").attr("disabled","disabled");

        $("#"+formId+" .button_send").removeClass('active');
        $("#"+formId+" .button_send").prop("disabled","disabled");
        $("#"+formId+" .button_send").attr("disabled","disabled");

        $("#"+formId+" .sub_btn").removeClass('active');
        $("#"+formId+" .sub_btn").prop("disabled","disabled");
        $("#"+formId+" .sub_btn").attr("disabled","disabled");
    }
}

$(document).ready(function() {
    //$('[data-jsmask="inn"]').inputmask({"mask": "999999999999", placeholder: "__________"});
    $('[data-jsmask="inn"]').inputmask({regex: "[0-9]+"});
});