jQuery(function($) {
    /*var link = $('#staff-order-form');
    link.submit(function(event) {
        //console.log('qwerty');
        $('.type-driver input[user_id]').each(function () {
            link.prepend('<input type="hidden" name="'+$(this.data('name'))+'" value="'+ $(this.data('user_id')) +'">');
        });

        event.preventDefault();
        return false;

    });*/
    
    total_turns_values();
});


function submit_staff_order_form() {
    var form = $('#staff-order-form');

    //console.log(form.find('.type-driver input[user_id]').length);
    form.find('.type-driver input[user_id]').each(function () {
        //form.prepend('<input type="hidden" name="'+$(this).data('name')+'" value="'+ $(this).attr('user_id') +'">');
        var j = $('.kp-tab-personal-right').index($(this).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)
        form.append('<input type="hidden" name="user_id_v['+j+'][]" value="'+ $(this).attr('user_id') +'">');
    });

    form.find('.cleaner-users input[user_id]').each(function () {
        //form.prepend('<input type="hidden" name="'+$(this).data('name')+'" value="'+ $(this).attr('user_id') +'">');
        var j = $('.kp-tab-personal-right').index($(this).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)
        form.append('<input type="hidden" name="user_id['+j+'][]" value="'+ $(this).attr('user_id') +'">');
    });

    //упорядочить дни(данные)
    $('.kp-tab-personal-right').each(function (i) {
        //console.log(i);
        $(this).find('[name="dates[]"]').prop('name', 'dates['+(i+1)+']');
        $(this).find('[name="times[]"]').prop('name', 'times['+(i+1)+']');
        //$(this).find('[name^="nigth"]').prop('name', 'nigth['+(i+1)+']');
    });
    
    //привязать данные строк к user_id
    form.find('input[user_id]').each(function () {
        var j = $('.kp-tab-personal-right').index($(this).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)
        var row = $(this).parents('.kp-personal-person-row');

        /*var user_id = row.find('[name^="user_id"]');
        if( user_id.prop('name').indexOf('_v') != -1 )
            user_id.prop('name', 'user_id_v['+j+'][]');
        else
            user_id.prop('name', 'user_id['+j+'][]');*/

        /*var fouls = row.find('[name^="fouls"]');
        if( fouls.prop('name').indexOf('_v') != -1 )
            fouls.prop('name', 'fouls_v['+j+']['+$(this).attr('user_id')+']');
        else
            fouls.prop('name', 'fouls['+j+']['+$(this).attr('user_id')+']');*/

        /*var brigadir_rating = row.find('[name^="brigadir_rating"]');
        brigadir_rating.prop('name', 'brigadir_rating['+j+']['+$(this).attr('user_id')+']');*/

        var main_v = row.find('[name^="main_v"]');
        //main_v.prop('name', 'main_v['+j+']['+$(this).attr('user_id')+']');
        main_v.prop('value', $(this).attr('user_id'));

        /*var amount = row.find('[name^="amount"]');
        if( amount.prop('name').indexOf('_v') != -1 )
            amount.prop('name', 'amount_v['+j+']['+$(this).attr('user_id')+']');
        else
            amount.prop('name', 'amount['+j+']['+$(this).attr('user_id')+']');*/

        /*var comment = row.find('[name^="comment"]');
        if( comment.prop('name').indexOf('_v') != -1 )
            comment.prop('name', 'comment_v['+j+']['+$(this).attr('user_id')+']');
        else
            comment.prop('name', 'comment['+j+']['+$(this).attr('user_id')+']');*/
    });

    form.submit();
}

function total_turns_values() {
    //console.log('zzz');
    var total_turns_users = 0;
    var total_turns_sum = 0;

    $('[data-btn-personal-loadr]').each(function (i) {
        //console.log(i);
        $(this).find('td:eq(0)').text(i+1);

        total_turns_users = total_turns_users + (parseInt($(this).find('td:eq(3)').text()) || 0);
        total_turns_sum = total_turns_sum + (parseInt($(this).find('td:eq(4)').text()) || 0);
    });

    $('#total_turns_users').text('Итого: ' + total_turns_users);
    $('#total_turns_sum').text('Итого: ' + total_turns_sum);
}

function ocenka_brigady(elm) {
    //console.log('qqq');
    var parent = $(elm).parents('.kp-tab-personal-right');
    parent.find('[name^="brigadir_rating"][value="'+ $(elm).val()+'"]').prop('checked', true);
    parent.find('[name^="sotrudnik_rating"][value="'+ $(elm).val()+'"]').prop('checked', true);
    parent.find('[name^="brigadir_rating"][value="'+ $(elm).val()+'"]').each(function() {
        $(this).prop("checked", true);
        $(this).attr("checked", true);
    });
}

function turn_delete(elm) {
    //удалить день(id)
    var j = $('.kp-tab-personal-right').index($(elm).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)
    //console.log(j);
    $('[name="day_ids['+j+']"]').remove();

    //удалить день(html)
    var temp_id = $(elm).data('temp_id');
    $('[data-temp_id='+temp_id+']').remove();

    //упорядочить
    $('.kp-tab-personal-right').each(function (i, row) {
        $(row).find('input[name^="ocenka_brigady"]').each(function (k, item) {
            $(item).prop('name', 'ocenka_brigady['+i+']');
        });

        $(row).find('input[name^="nigth"]').each(function (n, item1) {
            $(item1).prop('name', 'nigth['+i+']');
        });
    });

    $('[data-btn-personal-loadr]:last').addClass('current');
    $('.kp-tab-personal-right:last').show();

    total_turns_values();

    //delete orderDay
    /*$.post(
        '/admin/orderDay/destroy',
        {
            'id': 1,
            '_token': $('[name="_token"]').val()
        },
        function (data) {
            $.preloader.show({'attach' : $('.dash-wrap')});
            setTimeout(function() {
                $.preloader.hide({'attach' : $('.dash-wrap')});
            }, 1000);
        }
    );*/
}

function plus_turn_user_row(plus, office) {
    var row = $(plus).parents('.kp-personal-person-row');
    var j = $('.kp-tab-personal-right').index($(row).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)
    //var i = $('.type-driver > .kp-personal-person-row').index(row) + 1;    // № сотрудника

    var klon = row.clone();

    if(office == 'driver') {
        var i = $(row).parents('.type-driver').find('.kp-personal-person-row').length - 1;    // № сотрудника

        klon.find('input[name^="brigadir_rating"]').prop('name', 'brigadir_rating['+j+']['+i+']');
        klon.find('input[name^="fouls_v"]').prop('name', 'fouls_v['+j+']['+i+']');
        klon.find('input[name^="fouls_v"]').prop('checked', false);

        klon.find('input[name^="amount_v"]').prop('name', 'amount_v['+j+']['+i+']');
        klon.find('textarea[name^="comment_v"]').prop('name', 'comment_v['+j+']['+i+']');
    }
    else if(office == 'cleaner') {
        var i = $(row).parents('.cleaner-users').find('.kp-personal-person-row').length - 1;    // № сотрудника

        klon.find('input[name^="sotrudnik_rating"]').prop('name', 'sotrudnik_rating['+j+']['+i+']');
        klon.find('input[name^="fouls"]').prop('name', 'fouls['+j+']['+i+']');
        klon.find('input[name^="fouls"]').prop('checked', false);

        klon.find('input[name^="amount"]').prop('name', 'amount['+j+']['+i+']');
        klon.find('textarea[name^="comment"]').prop('name', 'comment['+j+']['+i+']');
    }
        
    //clear user field
    klon.find('input[data-user_block]').val('');
    klon.find('input[data-user_block]').removeAttr('user_id');
    klon.find('input[name^="amount"]').val('');
    klon.find('input[name^="main_v"]').prop('checked', false);

    row.after(klon);
    //$(row).parents('.type-driver').find('.row-heads').after(klon);

    init_select_schosen();
    total_turns_values();
}

function minus_turn_user_row(minus, office) {
    var $this = $(minus);
    var items_box = $(minus).parents('.kp-personal-person-items-box');
    if( $(items_box).find('.kp-personal-person-row').length > 2 )
    {
        var row = $(minus).parents('.kp-personal-person-row');
        row.remove();

        var j = $('.kp-tab-personal-right').index($(items_box).parents('.kp-tab-personal-right')) + 1;    //№ дня(смены)

        //упорядочить
        $(items_box).find('.kp-personal-person-row').each(function (i, row) {
            //console.log(i);
            if( !$(row).hasClass('row-heads')) {
                if(office == 'driver') {
                    $(row).find('input[name^="brigadir_rating"]').each(function (k, elm) {
                        $(elm).prop('name', 'brigadir_rating[' + j + '][' + (i-1) + ']');
                    });

                    $(row).find('input[name^="fouls_v"]').each(function (k, elm) {
                        $(elm).prop('name', 'fouls_v['+j+']['+ (i-1) +']');
                    });
                } else if(office == 'cleaner') {
                    $(row).find('input[name^="sotrudnik_rating"]').each(function (k, elm) {
                        $(elm).prop('name', 'sotrudnik_rating[' + j + '][' + (i-1) + ']');
                    });

                    $(row).find('input[name^="fouls"]').each(function (k, elm) {
                        $(elm).prop('name', 'fouls['+j+']['+ (i-1) +']');
                    });
                }

            }
        });

        if(office == 'driver') {
            $('input[user_ids_v="'+$this.attr('user_ids_v')+'"]').remove();
        }
        else if(office == 'cleaner') {
            $('input[user_ids="'+$this.attr('user_ids')+'"]').remove();
        }
        
    }
}

function turn_day_time(day_time) {
    var tr = $('.current[data-btn-personal-loadr]');
    tr.find('td:eq(1)').html(day_time);
}

function add_turn() {
    $('[data-btn-personal-loadr]').removeClass('current');

    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        timezone: 'UTC'
    };

    var strDate = new Date().toLocaleString("ru", options);
    //$('[data-jsmask="date"]').val(strDate);

    /*$('.kp-tab-personal-left table tbody').prepend('<tr onclick="turn_view();" data-btn-personal-loadr="" class="current"><td>1</td><td>День</td><td>'+strDate+'</td><td></td><td></td></tr>');
     $('.kp-tab-personal-right').show();*/
    $.get(
        '/admin/orders/turn/' + $('tr[data-btn-personal-loadr]').length +'/',
        {
            //'count': $('tr[data-btn-personal-loadr]').length
        },
        function (data) {
            //console.log('zzz');
            $('.kp-tab-personal-right').hide();

            if( $('.kp-tab-personal-left table tbody tr[data-btn-personal-loadr]').length > 0 )
                $('.kp-tab-personal-left table tbody tr[data-btn-personal-loadr]:last').after(data.left);
            else
                $('.kp-tab-personal-left table tbody').prepend(data.left);

            if( $('.kp-tabbox-staff:last > form .kp-tab-personal-right').length > 0 )
                $('.kp-tabbox-staff:last > form .kp-tab-personal-right:last').after(data.right);
            else
                $('.kp-tabbox-staff:last > form').prepend(data.right);

            //$('.chosen-select').chosen({allow_single_deselect:true});
            //$('.chosen-select').chosen();
            init_select_schosen();

            init_mask_time();
        },
        'json'
    );
}


function init_mask_time() {
    $('[data-jsmask]').each(function () {
        var $this = $(this);
        var $mask = $this.attr('data-jsmask');
        var $find = $this.not('.init-mask').addClass('init-mask');
        if($mask == 'date')
        {
            $find.inputmask({"mask": "99.99.9999", placeholder: ""});

            //600 order_company date_register /*
            //console.log($find.val());
            $this.inputmask({"mask": "99.99.9999", placeholder: ""});
            //600   */
        }
        else if($mask == 'time')
        {
            $find.inputmask({"mask": "99:99", placeholder: ""});
        }
        else if($mask == 'tel')
        {
            //$find.inputmask({"mask": "(999) 999-9999", placeholder: "(___) ___-____"});
            $find.inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});
        }
    });
}

function select_all_turn_users(elm) {
    if( $(elm).prop('checked') == true) {
        $('[name="sotrudnik_item[]"]').prop('checked', true);
        $('[name="person_item[]"]').prop('checked', true);
    } else {
        $('[name="sotrudnik_item[]"]').prop('checked', false);
        $('[name="person_item[]"]').prop('checked', false);
    }
}


/*function turn_view(temp_id) {
    $('.kp-tab-personal-right').hide();
    $('[data-temp_id='+temp_id+']').show();
}*/

function turn_view(row) {
    $('.kp-tab-personal-right').hide();
    var j = $('tr[data-btn-personal-loadr]').index($(row));    //№ дня(смены)
    //console.log(j);
    $('.kp-tab-personal-right:eq('+j+')').show();
}