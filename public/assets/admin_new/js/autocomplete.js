jQuery(function($) {
    init_select_schosen();
    
    /////////////////////////////////////////////////////////////////////
    $('.chosen-select').chosen({allow_single_deselect:true});

    if($('.chosen-autocomplite-base').length) {
        $('.chosen-autocomplite-base').each(function(){
            var $id = $(this).attr('id');
            $id = $id.replace(/\-/gi, "_");
            var selector = '#'+$id+'_chosen .chosen-search input';
            var $url = $(this).data('url');
            var $group = $(this).data('group');
            var MySelect = $(this);
            //console.log(selector);
            $(selector).autocomplete({
                source: function( request, response ) {
                    $search_param = $(selector).val();
                    var data = {
                        search_param: $search_param
                    };
                    if(typeof $group != 'undefined')
                        data.group = $group;
                    
                    if($search_param.length > 2) { //отправлять поисковой запрос к базе, если введено более 2 символов
                        $.post($url, data, function onAjaxSuccess(data) {
                            if((data.length != '0')) {
                                $('ul.chosen-results').find('li').each(function () {
                                    $(this).remove();//отчищаем выпадающий список перед новым поиском
                                });
                                MySelect.find('option').each(function () {
                                    $(this).remove(); //отчищаем поля перед новым поисков
                                });
                            }
                            //console.log(data);
                            jQuery.each(data, function(){
                                MySelect.append('<option value="' + this.id + '" >' + this.name + ' </option>');
                            });
                            //MySelect.append('<option value="66">Абдиев Ибрахим Абдиманнапович </option><option value="229">Абдиев Уктам </option><option value="79">Абдираим Бактыбек уллу </option><option value="219">Абдыкахарова Эркинай Абдыкахаровна </option>');
                            MySelect.trigger("chosen:updated");
                            $(selector).val($search_param);
                            anSelected = MySelect.val();
                        });
                    }
                }
            });

        });
    }

    $(document).on("click",".dynamic-input-contact .minus",function(e) {
        e.preventDefault();
        $(this).closest('.dynamic-input-contact').remove();
    });

});

function init_select_schosen() {
    var $dataPerson = {};

    //$('input[data-op-input="get"]').autocomplete({
    //$('#js-kp-order-person-search').autocomplete({
    $('input[data-op-input="get"], #js-kp-order-person-search').autocomplete({
        //source: availablePerson,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/users/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    search_param: request.term,
                    block: $(this).data('user_block')
                },
                method: "POST",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            if( $(this).data('user_block') == 'Workers' ) {
                                $dataPerson[obj['name']] = {
                                    'id': obj['id'],
                                    'name' : obj['name']
                                };
                            } else {
                                $dataPerson[obj['name']] = {
                                    'id': obj['id'],
                                    'name' : obj['name'],
                                    'avatar' : '/assets/imgs/users/preview/' + obj['avatar'],
                                };
                            }
                        }
                    }
                    //console.log($dataPerson);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            if(ui['item']['value'] && $dataPerson[ui['item']['value']])
            {
                var $data = $dataPerson[ui['item']['value']];
                /*var field_name = $(this).data('name');
                 var form = $(this).parents('form');

                 form.find('input[data-user_id="'+$data['id']+'"]').remove();
                 form.prepend('<input type="hidden" name="'+field_name+'" value="'+ $data['id'] +'" data-name="'+ field_name +'" data-user_id="'+$data['id']+'">');*/

                $(this).attr("user_id", $data['id']);
                //console.log($data);
            }

            //$(this).data("user_id", $data['id']);
            //$(this).prop("user_id", $data['id']);
            //$( this ).val('').trigger('op-input-change');
            //return false;
        },
        minLength: 3
    });

    ///////////////////
    var $dataMetro = {};
    $('input[data-op-input="metro"]').autocomplete({
        //source: availablePerson,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/metro',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    search_param: request.term
                },
                method: "POST",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataMetro[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name']
                            };
                        }
                    }
                    //console.log($dataMetro);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            //console.log($(this).attr('name'));
            if(ui['item']['value'] && $dataMetro[ui['item']['value']])
            {
                var $data = $dataMetro[ui['item']['value']];
                var form = $(this).parents('form');
                var day = $(this).data("day");

                form.find('[name="metro_id['+day+']"]').remove();
                //form.prepend('<input type="hidden" name="metro_id['+day+']" value="'+ $data['id'] +'">');
                form.prepend('<input type="hidden" name="metro_id['+day+']" data-metro_id="'+day+'" value="'+ $data['id'] +'">');
                //console.log($(this).data('day'));
                //$(this).val($data['id']);
            }

            //$( this ).val('').trigger('op-input-change');
            //$(this).val($data['id']).trigger('op-input-change');
            //console.log();
            //return false;
        },
        minLength: 3
    });


    //////////////////////////
    /* ORDER COMPANY :: Order autocomplete add */
    var $dataCompany = {};

    $( "#js-kp-order-boxdata-company-search" ).autocomplete({
        //$('input[data-op-input="company"]').autocomplete({
        //source: $tagsCompany,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/order_companies/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    term: request.term
                },
                method: "GET",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataCompany[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name'],
                                'site' : obj['site'],
                                'phone' : obj['phone'],
                                'email' : obj['email']
                            };
                        }
                    }
                    //console.log($dataMetro);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            id: item.id,
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            var $template = $($('#js-kp-order-boxdata-company-template').html());

            //console.log($(this).attr('context'));
            if(ui['item']['value'] && $dataCompany[ui['item']['value']])
            {
                //order_company ajax
                /*if( $(this).attr('context') != 'contact') {
                    $.post(
                        '/admin/orders/update_ajax',
                        {
                            '_token': $('[name="_token"]').val(),
                            'id': $('[name="order_id"]').val(),
                            'field': 'order_company_id',
                            'val': ui['item']['id']
                        }
                    );
                } else {
                    //activate save-btn
                    if( $('.page-kp-form-bottom-button').is(":hidden") )
                        $('.page-kp-form-bottom-button').show();
                }*/

                $.each($dataCompany[ui['item']['value']], function ($indx, $data) {
                    $template.find('[data-name="' + $indx + '"]').val($data);

                    if($indx == 'id')
                        $template.find('[data-order_company_href] > a').attr('href', '/admin/order_companies/' + $data + '/edit');
                    //console.log($indx, $data);
                });

                $('[name="order_company_id"]').val(ui['item']['id']);
            }

            $('#js-kp-order-boxdata-company-insert').html('');
            $template.prependTo($('#js-kp-order-boxdata-company-insert'));

            $template.find('[data-name="name"]').trigger('op-input-change');

            $( this ).val('').trigger('op-input-change');

            $('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

            if( $(this).attr('context') == 'contact') {
                //change order_company ajax
                $template.find('input').on('change', function() {
                    //console.log($('[name="order_company_id"]').val());

                    $.post(
                        '/admin/order_companies/update_ajax',
                        {
                            '_token': $('[name="_token"]').val(),
                            'order_company_id': $('[name="order_company_id"]').val(),
                            //'order_id': $('[name="order_id"]').val(),
                            'field': $(this).prop('name'),
                            'val': $(this).val()
                        }
                    );
                });
            }

            return false;
        }
    });

    //////////////////////////
    /* COMPANY :: Order autocomplete add */
    var $dataCompany = {};

    $( "#js-boxdata-user-company-search" ).autocomplete({
        //$('input[data-op-input="company"]').autocomplete({
        //source: $tagsCompany,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/companies/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    term: request.term
                },
                method: "GET",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataCompany[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name'],
                                'site' : obj['site'],
                                'phone' : obj['phone'],
                                'email' : obj['email']
                            };
                        }
                    }
                    //console.log($dataMetro);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            id: item.id,
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            var $template = $($('#js-kp-order-boxdata-company-template').html());

            //console.log($(this).attr('context'));
            if(ui['item']['value'] && $dataCompany[ui['item']['value']])
            {
                //order_company ajax
                if( $(this).attr('context') != 'contact') {
                    $.post(
                        '/admin/users/add_company',
                        {
                            '_token': $('[name="_token"]').val(),
                            'id': $('[name="user_id"]').val(),
                            'field': 'company_id',
                            'val': ui['item']['id']
                        }
                    );
                } else {
                    /*$.post(
                     '/admin/order_contacts/update_ajax',
                     {
                     '_token': $('[name="_token"]').val(),
                     'id': $('[name="contact_id"]').val(),
                     'field': 'order_company_id',
                     'val': ui['item']['id']
                     }
                     );*/

                    //activate save-btn
                    if( $('.page-kp-form-bottom-button').is(":hidden") )
                        $('.page-kp-form-bottom-button').show();
                }

                $.each($dataCompany[ui['item']['value']], function ($indx, $data) {
                    $template.find('[data-name="' + $indx + '"]').val($data);
                });

                $('[name="company_id"]').val(ui['item']['id']);
            }

            $('#js-kp-order-boxdata-company-insert').html('');
            $template.prependTo($('#js-kp-order-boxdata-company-insert'));

            $template.find('[data-name="name"]').trigger('op-input-change');

            $( this ).val('').trigger('op-input-change');

            $('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

            /*if( $(this).attr('context') == 'contact') {
                //change order_company ajax
                $template.find('input').on('change', function() {
                    //console.log($('[name="order_company_id"]').val());

                    $.post(
                        '/admin/order_companies/update_ajax',
                        {
                            '_token': $('[name="_token"]').val(),
                            'order_company_id': $('[name="order_company_id"]').val(),
                            //'order_id': $('[name="order_id"]').val(),
                            'field': $(this).prop('name'),
                            'val': $(this).val()
                        }
                    );
                });
            }*/

            return false;
        }
    });


    //////////////////////////
    /* CONTACT :: Order autocomplete add */
    var $dataContacts = {};

    $( "#js-kp-order-boxdata-contacts-search" ).autocomplete({
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/order_contacts/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    term: request.term
                },
                method: "GET",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataContacts[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name'],
                                //'avatar' : '/assets/imgs/users/preview/' + obj['avatar'],
                                'post' : obj['post'],
                                'tel' : obj['tel'],
                                'mobile': obj['mobile'],
                                'email': obj['email']
                            };
                        }
                    }
                    //console.log($dataContacts);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            id: item.id,
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            var $template = $($('#js-kp-order-boxdata-contacts-template').html());

            if(ui['item']['value'] && $dataContacts[ui['item']['value']])
            {
                //attach contact to order
                $.post(
                    '/admin/order_contacts/attach_to_order',
                    {
                        '_token': $('[name="_token"]').val(),
                        'order_id': $('[name="order_id"]').val(),
                        'contact_id': $dataContacts[ui['item']['value']]['id']
                    }
                );

                $.each($dataContacts[ui['item']['value']], function ($indx, $data) {
                    $template.find('[data-name="' + $indx + '"]').val($data);

                    //для свернутых
                    $template.find('[div-data-name="' + $indx + '"]').text($data);

                    if($indx == 'id')
                        $template.find('[data-order_contact_href] > a').attr('href', '/admin/order_contacts/' + $data + '/edit');
                });
            }

            $('#js-kp-order-boxdata-contacts-insert').find('[data-boxtoggle-fields="wrap"]').removeClass('toggle-open').find('[data-boxtoggle-fields="drop"]').hide();
            $template.prependTo($('#js-kp-order-boxdata-contacts-insert'));
            $template.find('[data-name="fio"]').trigger('op-input-change');

            $( this ).val('').trigger('op-input-change');

            $('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

            return false;
        }
    });


    ////////////////////////////////
    /* Ответственный для карточка контакта и компания заказа */

    var __addPerson = function ($this, $data) {
        var $wrap = $this.parents('[data-persons="wrap"]');
        var $input = $wrap.find('[data-op-input="get"]');

        var $tpls = $('#kp-order-person-additem').html();

        $tpls = $tpls.replace(/\[avatar\]/g, $data['avatar']);
        $tpls = $tpls.replace(/\[name\]/g, $data['name']);
        $tpls = $tpls.replace(/\[user_id\]/g, $data['id']);

        var $appendElem = $($tpls);
        $wrap.find('.kp-order-form-person-item').remove();
        $appendElem.appendTo($wrap);
        $input.val("").trigger('op-input-change').focus();
    };

    var $dataPerson = {};

    $('input[data-op-input="person-search"]').autocomplete({
        //source: availablePerson,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/users/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    search_param: request.term,
                    block: 'Management'
                },
                method: "POST",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataPerson[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name'],
                                'avatar' : '/assets/imgs/users/preview/' + obj['avatar'],
                            };
                        }
                    }
                    //console.log($dataPerson);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            //console.log($dataPerson[ui['item']['value']]);
            if(ui['item']['value'] && $dataPerson[ui['item']['value']])
            {
                var $data = $dataPerson[ui['item']['value']];
                __addPerson($( this ), $data);

                $('[name="user_id"]').val($data['id']);

                //activate save-btn
                if( $('.page-kp-form-bottom-button').is(":hidden") )
                    $('.page-kp-form-bottom-button').show();

                /*if( window.location.pathname.indexOf('order_companies') != -1 )
                    update_ajax('user_id', 'order_companies');
                else if( window.location.pathname.indexOf('order_contacts') != -1 )
                    update_ajax('user_id', 'order_contacts');*/
            }

            $( this ).val('').trigger('op-input-change');
            return false;
        },
        minLength: 3
    });

}
