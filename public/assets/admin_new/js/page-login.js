$(document).ready(function() {
    var $document = $(document);


    var defaults = {
        btnTpl: {
            smallBtn: '<span data-fancybox-close class="fancybox-close-large" title="{{CLOSE}}"></span>',
        },
        touch:false,
        afterShow: function( instance, slide ) {
            $document.trigger('init-mask');
        }
    };
    $("[data-fancybox-inline]").fancybox(defaults);



    function __unlockSend() {
        var $timeValue = 59;
        var $setTimeValue = $timeValue;
        var $timerOuts = setInterval(function() {
            $setTimeValue = $setTimeValue - 1;

            var s = $setTimeValue+"";
            while (s.length < 2) s = "0" + s;
            $setTimeValue = s;

            $('.js-unlock-timeout').text('00:' + $setTimeValue);
            if($setTimeValue == '00')
            {
                clearInterval($timerOuts);
                $setTimeValue = $timeValue;
                $('.js-unlock-send').removeClass('disabled');
            }
        }, 1000);
    }

    $document.on('click', '.js-unlock-send', function () {
        var $this = $(this);
        if($this.hasClass('disabled'))
        {
            return;
        }
        $this.addClass('disabled');
        __unlockSend();
    });


    // Шаги формы - кнопка
    $document.on('click', '[data-btn-form-register-step]', function () {
    //$document.on('click', '[data-btn-form-register-step="2"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-form-register-step]');
        var $do = $this.attr('data-btn-form-register-step');
        $wrap.attr({'data-form-register-step' : $do});

        //600 get data company by inn..
        if($do == 2) {
            $.post(
                '/inn',
                {
                    '_token': $('[name="_token"]').val(),
                    'inn': $('[data-jsmask="inn"]').val()
                },
                function (data) {
                    //console.log(data.inn);
                    if( typeof(data.inn) != "undefined") {
                        $('[name="company_name"]').val(data.company_name);
                        $('[data-type="fio"]').val(data.director);

                        $wrap.find('[data-step="' + $do + '"]').fadeIn(500);
                        $this.hide();
                    } else {
                        $('[data-jsmask="inn"]').addClass('error');
                    }
                },
                'json'
            );
        }
        else if($do == 3){
            $wrap.find('[data-step="' + $do + '"]').fadeIn(500);
            $this.hide();
        }
    });


    // Шаги формы - проверка
    var formStep_1 = 'form-register-step-01';
    $("#" + formStep_1 + " .imp").bind('keypress keyup focusin focusout focusin change input', function(){
        errorsCheck3(formStep_1);
    });

    var formStep_2 = 'form-register-step-02';
    $("#" + formStep_2 + " .imp").bind('keypress keyup focusin focusout focusin change', function(){
        errorsCheck3(formStep_2);
    });

    var formStep_3 = 'form-register-step-03';
    $("#" + formStep_3 + " .imp").bind('keypress keyup focusin focusout focusin change', function(){
        errorsCheck3(formStep_3);
    });


    /*$document.on('submit', '#form-register', function () {
        var $form = $(this);
        $form.addClass('form-lock');
        $.fancybox.getInstance().showLoading(  );

        alert('Делаем отправку ajax на сервер, делаем вид, что идет отправка и просто ждем 3 секунды');

        setTimeout(function () {
            $form.removeClass('form-lock');
            $.fancybox.getInstance().hideLoading(  );

            $('#js-form-register-start').hide();

            // Проверяем пароли (вообще хорошо бы с сервера это сравнивать) на заполнение и выдаем нужный результат
            var $getPass1 = $form.find('.inputPasswordNew');
            var $getPass2 = $form.find('.inputPasswordNewCh');
            if($getPass1.val() && $getPass2.val())
            {
                $('#js-form-register-end-pass').show();
            }
            else
            {
                $('#js-form-register-end-sms').show();
                __unlockSend();
            }

        },3000);

        return false;
    });*/

    

    /* END */
});

function submit_form_register() {
    var $form = $('#form-register');
    $form.addClass('form-lock');
    $.fancybox.getInstance().showLoading(  );

    //alert('Делаем отправку ajax на сервер, делаем вид, что идет отправка и просто ждем 3 секунды');


    setTimeout(function () {
        $form.removeClass('form-lock');
        $.fancybox.getInstance().hideLoading(  );

        //request
        $.post(
            '/register_company',
            {
                '_token': $('[name="_token"]').val(),
                'inn': $('[data-jsmask="inn"]').val(),
                'company_name': $('[name="company_name"]').val(),
                'name': $('[name="fio"]').val(),
                'email': $('[name="email"]').val(),
                'phone': $('[name="phone"]').val(),
                'password': $form.find('.inputPasswordNew').val()
            },
            function (data) {
                //console.log(data);
                if(data == 'success') {
                    $('#js-form-register-start').hide();

                    // Проверяем пароли (вообще хорошо бы с сервера это сравнивать) на заполнение и выдаем нужный результат
                    /*var $getPass1 = $form.find('.inputPasswordNew');
                    var $getPass2 = $form.find('.inputPasswordNewCh');
                    if($getPass1.val() && $getPass2.val())
                    {
                        $('#js-form-register-end-pass').show();
                    }*/

                    $('#js-form-register-end-pass').show();
                }
            }
        );

    },3000);
}

