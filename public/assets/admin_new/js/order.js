$(function(){
    //открыть вкладку при загрузке страницы
    if(window.location.search.indexOf("tab=staff") != -1)
        $('[data-findtab="kp-tabbox-staff"]').trigger('click');

    //сохранить калькулятор при закрытии страницы/вкладки
    window.onbeforeunload = function() {
        if( $('[data-findtab = "kp-tabbox-calc"]').hasClass('current')) {
            save_calculator_data(null, 'ajax');

            /*$.preloader.show({'attach' : $('.dash-wrap')});
            setTimeout(function() {
                $.preloader.hide({'attach' : $('.dash-wrap')});
            }, 1000);*/

            //return "Данные не сохранены. Точно перейти?";
        }
    };

    //click по вкладкам заказа
    $('[data-tab="dash-kp"]').on('click', function(){
        if( $('[data-findtab = "kp-tabbox-calc"]').hasClass('current')) {
            //console.log($('[name="amount"]').val()>0);
            var base_info = false;
            //if( $('[name="amount"]').val() > 0) {
            if( $('[name="calculator_amount"]').val() > 0) {
                $('[name="base_info"]').each(function (i) {
                    //console.log($(this).prop("checked"));
                    if($(this).prop("checked") == true) {
                        base_info = true;
                    }
                });

                if(!base_info)
                    return false;
                else {
                    save_calculator_data( $('input[name="calculator"]').val(), 'ajax');
                }
            }
        }
    });

    //order save btn click
    $('.page-kp-form-button').on('click', function(){
        save_calculator_data('save');
    });
    
    //order's period log/////////////////
    var link = $('[name="time_start[]"]');
    link.on('change', function(){
        $.post(
            '/admin/orders/save_periods',
            {
                '_token': $('[name="_token"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'periods': periods
            },
            function (err) {
                if(err)
                    $.notifye('error', err, 10000);
            }
        );
    });

    /*var link = $('#order-form-main').find('[data-update-ajax]');
    link.on('change', function(){
        if( $(this).prop('id') == 'order_heads_label')
            update_ajax('name', 'orders');
        else
            update_ajax($(this).prop('name'), 'orders');
    });

    //address
    var link = $('#order-form-main').find('input#suggest');
    link.focusout(function() {
        //console.log(link.val());
        update_ajax('address', 'orders');
    });

    //other fields
    var link = $('input#order-header-name');
    link.on('change', function(){
        //console.log($(this).val());
        update_ajax('name', 'orders');
    });

    var link = $('#order-form-main').find('textarea[name="note"]');
    link.on('change', function(){
        //console.log($(this).val());
        update_ajax('note', 'orders');
    });
    
    //order_company
    var link = $('[data-update-ajax-order_company]').find('input');
    link.on('change', function() {
        //show btn save
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
        
        var field = $(this).prop('name');
        if(field.length == 0)
            field = $(this).data('name');

        $.post(
            '/admin/order_companies/update_ajax',
            {
                '_token': $('[name="_token"]').val(),
                'id': $('[name="order_company_id"]').val(),
                'field': field,
                'val': $(this).val()
            }
        );
    });

    //order_contacts
    var link = $('.type-contact').find('input');
    link.on('change', function() {
        //console.log($(this).parents('.type-contact').find('[name="contact_ids[]"]').val());
        var contact_id = $(this).parents('.type-contact').find('[name="contact_ids[]"]').val();
        var prop = $(this).prop('name').replace('[]', '');

        var data = {
            '_token': $('[name="_token"]').val(),
            'id': contact_id,
            'field': $(this).prop('name').replace('[]', ''),
            'val': $(this).val()
        };

        if(prop == 'mobile') {
            data.phone_type = 'mobile';
        } else if( prop == 'tel') {
            data.phone_type = 'work';
        }

        $.post(
            '/admin/order_contacts/update_ajax',
            data
        );
    });*/

    //orderContact ajax//////////////////////////////////////////////////////
    /*var link = $('textarea[name="comments"]');
    link.on('change', function(){
        //console.log($(this).val());
        update_orderContact_ajax('comments');
    });

    var link = $('#order-contact-form').find('[update-contact-ajax]');
    //console.log(link.length);
    link.on('change', function(){
        //console.log($(this).val(), $(this).prop('name'));

        update_orderContact_ajax($(this).prop('name'))
    });

    //address
    var link = $('input#suggest');
    link.on('change', function(){
        //console.log($(this).val());
        setTimeout(update_orderContact_ajax('address'), 3000);
    });*/

})

//ajax order edit//////////////////////////////////
function create_order_company() {
    $.post(
        '/admin/orders/create_order_company',
        {
            '_token': $('[name="_token"]').val(),
            'order_id': $('[name="order_id"]').val(),
            //'field': 'order_company_id',
            'name': $('#js-kp-order-boxdata-company-search').val()
        }
    );
}

/*function update_order_ajax(field) {
    $.post(
        '/admin/orders/update_ajax',
        {
            '_token': $('[name="_token"]').val(),
            'order_id': $('[name="order_id"]').val(),
            'field': field,
            'val': $('[name="'+ field +'"]').val()
        }
    );
}*/

function plus_order_period_row(plus) {
    //console.log('plus_order_period_row');
    var row = $(plus).parents('[data-kporder-date="clone"]');
    var klon = row.clone(true);

    var i = $(row).parents('.kp-order-form-wrap').find('[data-kporder-date="clone"]').length - 1;    // № сотрудника

    //clear user fields
    klon.find('input').val('');
    klon.find('[name="time_start[]"]').val('09:00');
    klon.find('[name="time_finish[]"]').val('18:00');
    //klon.find('.kp-personal-person-plusminus-button:first').remove();

    row.after(klon);
    //$(row).parents('.type-driver').find('.row-heads').after(klon);

}

function minus_order_period_row(minus) {
    if( $('[data-kporder-date="clone"]').length > 1 )
    {
        var row = $(minus).parents('[data-kporder-date="clone"]');
        row.remove();

        if( $('[name="order_id"]').val() > 0 ) {
            //console.log('zzz');
            save_order_periods(minus, 'delete');
        }
    }
}

function save_order_periods(elm, action) 
{
    //console.log('zzz');
    var $this = $(elm);
    var row = $this.parents('[data-kporder-date="clone"]');
    var periods = [];

    //if dates not correct..
    if( !(row.find('[name="date[]"]').val().length == 10 && row.find('[name="date_finish[]"]').val().length == 10) )
        return;

    if( action == 'change') {
        //if dates not correct.. 
        if( !(($this.prop('name').indexOf('date') != -1 && $this.val().length == 10) || $this.prop('name').indexOf('time') != -1 && $this.val().length == 5) ) {
            return;
        }

    }
    else if( action == 'delete') {
        //if dates not correct.. 
        if( !(row.find('[name="date[]"]').val().length == 10 && row.find('[name="date_finish[]"]').val().length == 10) )
            return;
    }

    $('[data-kporder-date="clone"]').each(function (i) {
        $this = $(this);

        var period = {
            "date_start": $this.find('[name="date[]"]').val(),
            "time_start": $this.find('[name="time_start[]"]').val(),
            "date_finish": $this.find('[name="date_finish[]"]').val(),
            "time_finish": $this.find('[name="time_finish[]"]').val()
        };

        periods[i] = period;
    });
    //console.log(periods);

    $.post(
        '/admin/orders/save_periods',
        {
            '_token': $('[name="_token"]').val(),
            'order_id': $('[name="order_id"]').val(),
            'periods': periods
        },
        function (err) {
            if(err)
                $.notifye('error', err, 10000);
        }
    );
    
}
