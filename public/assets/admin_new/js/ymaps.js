$(function(){
    var link = $('input[name="address"]');
    //console.log(window.address);
    if( link.val().length > 0)
        window.address = link.val();
    else
        window.address = 'Москва';

    link.blur(function() {
        if( link.val().length > 0) {
            window.address = link.val();
            window.myMap.destroy();
            init();
            //document.getElementById("side-ya-map").style.display = "block";
        }
    });

    link.click(function() {
        if( link.val().length > 0) {
            window.address = link.val();
            window.myMap.destroy();
            init();
            //document.getElementById("side-ya-map").style.display = "block";
        }
    });
    
})


ymaps.load(function () {
    //console.log('load');
    //var input = document.getElementById('suggest');
    /*var suggestView = new ymaps.SuggestView('suggest', {
     offset: [10, 10]
     });*/
    //var suggestView = new ymaps.SuggestView('suggest');

    /*suggestView.state.events.add('change', function () {
     var activeIndex = suggestView.state.get('activeIndex');
     if (typeof activeIndex == 'number') {
     activeItem = suggestView.state.get('items')[activeIndex];

     if (activeItem && activeItem.value != input.value) {
     input.value = activeItem.value;
     }
     }
     //suggestView.destroy();
     });

     suggestView.state.events.add('select', function () {
     suggestView.destroy();
     });*/

});

ymaps.ready(init);

function init() {
    //console.log('init');
    /*var suggestView = new ymaps.SuggestView('suggest'),
        map,
        placemark;*/
    
    //console.log('ymaps');
    //window.myMap = new ymaps.Map('order-col-right', {
    window.myMap = new ymaps.Map('side-ya-map', {
        center: [55.753994, 37.622093],
        zoom: 9
    });

    // Поиск координат центра Нижнего Новгорода.
    //ymaps.geocode('Москва, Тверская улица, дом 18', {
    ymaps.geocode(window.address, {
        /**
         * Опции запроса
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
         */
        // Сортировка результатов от центра окна карты.
        // boundedBy: myMap.getBounds(),
        // strictBounds: true,
        // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
        // Если нужен только один результат, экономим трафик пользователей.
        results: 1
    }).then(function (res) {
        // Выбираем первый результат геокодирования.
        var firstGeoObject = res.geoObjects.get(0),
        // Координаты геообъекта.
            coords = firstGeoObject.geometry.getCoordinates(),
        // Область видимости геообъекта.
            bounds = firstGeoObject.properties.get('boundedBy');

        firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
        // Получаем строку с адресом и выводим в иконке геообъекта.
        firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

        // Добавляем первый найденный геообъект на карту.
        myMap.geoObjects.add(firstGeoObject);
        // Масштабируем карту на область видимости геообъекта.
        myMap.setBounds(bounds, {
            // Проверяем наличие тайлов на данном масштабе.
            checkZoomRange: true
        });

        /**
         * Все данные в виде javascript-объекта.
         */
        //console.log('Все данные геообъекта: ', firstGeoObject.properties.getAll());
        /**
         * Метаданные запроса и ответа геокодера.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderResponseMetaData.xml
         */
        //console.log('Метаданные ответа геокодера: ', res.metaData);
        /**
         * Метаданные геокодера, возвращаемые для найденного объекта.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/GeocoderMetaData.xml
         */
        //console.log('Метаданные геокодера: ', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData'));
        /**
         * Точность ответа (precision) возвращается только для домов.
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/precision.xml
         */
        //console.log('precision', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.precision'));
        /**
         * Тип найденного объекта (kind).
         * @see https://api.yandex.ru/maps/doc/geocoder/desc/reference/kind.xml
         */
        /*console.log('Тип геообъекта: %s', firstGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind'));
        console.log('Название объекта: %s', firstGeoObject.properties.get('name'));
        console.log('Описание объекта: %s', firstGeoObject.properties.get('description'));
        console.log('Полное описание объекта: %s', firstGeoObject.properties.get('text'));*/
        /**
         * Прямые методы для работы с результатами геокодирования.
         * @see https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeocodeResult-docpage/#getAddressLine
         */
        /*console.log('\nГосударство: %s', firstGeoObject.getCountry());
        console.log('Населенный пункт: %s', firstGeoObject.getLocalities().join(', '));
        console.log('Адрес объекта: %s', firstGeoObject.getAddressLine());
        console.log('Наименование здания: %s', firstGeoObject.getPremise() || '-');
        console.log('Номер здания: %s', firstGeoObject.getPremiseNumber() || '-');*/

        /**
         * Если нужно добавить по найденным геокодером координатам метку со своими стилями и контентом балуна, создаем новую метку по координатам найденной и добавляем ее на карту вместо найденной.
         */
        /**
         var myPlacemark = new ymaps.Placemark(coords, {
             iconContent: 'моя метка',
             balloonContent: 'Содержимое балуна <strong>моей метки</strong>'
             }, {
             preset: 'islands#violetStretchyIcon'
             });

         myMap.geoObjects.add(myPlacemark);
         */
    });
}