__workerCalcs = function ($container) {
    var $return = {'total' : 0, 'counter' : 0};
    $container.find('[data-worker-calcs="item"]').each(function () {
        var $item = $(this);
        var $isAllowCalc = $item.find('[data-worker-calcs="allow"]').is(':checked');
        if(!$isAllowCalc) return;

        var $typeSum = $item.find('[data-worker-calcs="typepay"]').val();

        var $getValue = $item.find('[data-worker-calcs="value"]').html();
        $getValue = parseInt($getValue.replace(/\D+/g,""));

        var $paidValue = $item.find('[data-input-name="paid"]').html();
        $paidValue = parseInt($paidValue.replace(/\D+/g,""));
        //console.log($paidValue);

        //делаем выплачено = начислено
        /*var paid = $item.find('.tbl-paid > span');
        paid.html($getValue);*/

        if($typeSum === 'penalty')
        {
            //$return.total = $return.total - $getValue;
            $return.total = $return.total - ($getValue - $paidValue);
            $return.counter = $return.counter + 1;
            return;
        }

        //$return.total = $return.total + $getValue;
        $return.total = $return.total + ($getValue - $paidValue);
        $return.counter = $return.counter + 1;
    });


    var $calcsTotal = $container.find('[data-worker-calcs="total"]');


    if($return.counter)
    {
        var $calcsTotalCounter = $calcsTotal.find('[data-worker-calcs="total_counter"]');
        var $calcsTotalSum = $calcsTotal.find('[data-worker-calcs="total_sum"]');
        $calcsTotalCounter.html($return.counter);
        $calcsTotalSum.html($return.total);
        $calcsTotal.fadeIn(300);
    }
    else
    {
        $calcsTotal.hide();
    }
};

$(document).ready(function() {
    var $document = $(document);

    //600   /*
    
    //добавить отзыв
    /*var link = $('[data-addreviews-form="submit"]');
    link.on('click', function () {
        $.post(
            '/admin/users/review_add',
            {
                '_token': $('[name="_token"]').val(),
                'review': $('[name="addreviews_message"]').val(),
                'user_id': $('[name="user_self_id"]').val(),
                'order_id': $('[name="addreviews_order_id"]').val()
            },
             function(data) {
                 if(data)
                    $.notifye('success', 'Начисления произведены.', 4000);
             }
        );
    });*/


    //сохр имя(name)
    //console.log(window.location.pathname);
    if( (window.location.pathname.indexOf('/admin/users/') + 1) > 0 ) {
        var link = $('input#user_head_fio');
        link.on('change', function () {
            //$('form.user').find('input[name="fio"]').val(this.value);
            $('form.user').find('input[name="fio"]').val(this.value);
        });
    }
    //600   */

    /* Region checked */
    $document.on('change', '[data-regioncheck]', function () {
        var $this = $(this);
        var $wrapUL = $this.closest('ul');
        var $wrapUL_ch = $wrapUL.find(':checkbox');
        var $wrapUL_chLength = $wrapUL_ch.length;
        var $wrapUL_chLengthChecked = $wrapUL_ch.filter(':checked').length;

        var $wrapLI = $this.closest('li');
        var $subList = $wrapLI.find('> ul');
        var $isCheck = $this.is(':checked');


        // Ищем потомков и выставляем им состоянии в соответствии текущего чекбокса
        var $checkboxes = $subList.find(':checkbox');
        $checkboxes.prop('checked', $isCheck);

        // Добавляем кусочек отчекнутых
        if($wrapUL_chLength === $wrapUL_chLengthChecked)
        {
            $wrapUL.closest('li').find('> label [data-regioncheck]').prop('checked', true);
        }
        else
        {
            $wrapUL.closest('li').find('> label [data-regioncheck]').prop('checked', false);
            if($wrapUL_chLengthChecked)
            {
                $wrapUL.closest('li').addClass('scrap');
            }
            else
            {
                $wrapUL.closest('li').removeClass('scrap');
            }
        }

        if($isCheck)
        {
            $wrapLI.addClass('open-tree-region');
            return;
        }

        // Удаляем кусочек отчекнутых
        $wrapLI.removeClass('scrap');
    });


    $document.on('click', '[data-regioncheck-wrap] .btn-regionwork-tree', function () {
        var $this = $(this);
        var $wrapLI = $this.closest('li');
        $wrapLI.toggleClass('open-tree-region');
    });


    $document.on('click', '[data-regioncheck-quickly]', function () {
        var $this = $(this);
        var $toChecked = $this.attr('data-regioncheck-quickly');
        //console.log($toChecked)
        if($toChecked == '#input-region-moscow') {
            var $elChecked = $('[name = "region_work[]"][value = "42"]');
            if(!$elChecked.length) return;
            $elChecked.click();

            var $elChecked = $('[name = "region_work[]"][value = "43"]');
            if(!$elChecked.length) return;
            $elChecked.click();
        } else if($toChecked == '#input-region-sanktpiter') {
            var $elChecked = $('[name = "region_work[]"][value = "37"]');
            if(!$elChecked.length) return;
            $elChecked.click();

            var $elChecked = $('[name = "region_work[]"][value = "59"]');
            if(!$elChecked.length) return;
            $elChecked.click();
        }

        /*var $elChecked = $($toChecked);
        if(!$elChecked.length) return;
        $elChecked.click();*/
    });


    $document.on('click', '[data-regioncheck-reset]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-regioncheck-control]');
        var $toAttach = $this.attr('data-regioncheck-reset');
        var $elAttach = $($toAttach);
        if($elAttach.length)
        {
            $wrap.removeClass('regioncheck-control-show');
            $wrap.find('[data-regioncheck-counter]').html('');
            $elAttach.find('[data-regioncheck]').prop('checked', false);
            $elAttach.find('.scrap').removeClass('scrap');
            $.notifye('success', 'Регионы работы сотрудника сброшены', 4000);
        }
    });


    //600 перед сохранением собрать отмеченные work_city
    $document.on('submit', 'form.user', function (e) {
        //console.log('submit');
        var $this = $('[data-regioncheck-choose]');
        var $wrap = $this.parents('[data-regioncheck-wrap]');
        var $inputs = $wrap.find('input:checkbox');
        var $inputsCheckedLength = $inputs.filter('[data-regioncheck="city"]:checked').length;

        if($inputsCheckedLength)
        {
            /* Собираем отмеченные чеки и отправляем на сервер */
            var $dataInput = $inputs.serialize();
            //console.log($dataInput);
            $('input[name="str_city_work"]').val($dataInput);
        }
        //console.log($dataInput);
        //e.preventDefault();
        //return false;
    });


    $document.on('click', '[data-regioncheck-choose]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-regioncheck-wrap]');
        var $inputs = $wrap.find('input:checkbox');
        var $inputsCheckedLength = $inputs.filter('[data-regioncheck="city"]:checked').length;

        var $modal = $this.parents('.modal[id]');
        var $modalId = $modal.attr('id');

        var $btnModalOpen = $('[data-regioncheck-open="#' + $modalId + '"]');

        if($inputsCheckedLength)
        {
            $btnModalOpen.find('[data-regioncheck-counter]').html('(' + $inputsCheckedLength + ')');
            $btnModalOpen.parents('[data-regioncheck-control]').addClass('regioncheck-control-show');
            $.notifye('success', 'Регионы работы сотрудника (Выбрано ' + $inputsCheckedLength + ')', 4000);
        }
        else
        {
            $btnModalOpen.find('[data-regioncheck-counter]').html('');
            $btnModalOpen.parents('[data-regioncheck-control]').removeClass('regioncheck-control-show');
            $.notifye('success', 'Регионы работы сотрудника очищены', 4000);
        }

        /* Собираем отмеченные чеки и отправляем на сервер */
        var $dataInput = $inputs.serialize();
        //console.log($dataInput);
        $('input[name="city_work"]').val($dataInput);


        $.modal.close();
    });

    $document.on('click', '[data-regioncheck-open]', function () {
        var $toAttach = $(this).attr('data-regioncheck-open');
        var $elAttach = $($toAttach);
        if(!$elAttach.length) return;
        $modal = $elAttach.modal({});
    });

    $document.on('click', '[data-regioncheck-cancel]', function () {
        $.modal.close();
    });
    /* END :: Region checked */




    $document.on('focus', '[data-kpcalendar="date"]', function () {
        var $this = $(this);
        if($this.hasClass('init-script')) return;
        $this.addClass('init-script');

        var $options = {
            navTitles: {
                days: 'MM yyyy'
            },
            prevHtml: '<span class="datepicker--nav-btn-prev"><span></span></span>',
            nextHtml: '<span class="datepicker--nav-btn-next"><span></span></span>',
            moveToOtherYearsOnSelect: false,
            dateFormat: '',
            toggleSelected: false,
            showOtherYears: false,
            selectOtherYears: false,
            autoClose: true,
            onRenderCell: function(date, cellType) {
                if(cellType == 'day')
                {
                    return {html : '<span class="day-value"><span class="day-date">' + date.getDate() + '</span></span>'};
                }
            },
            onSelect : function () {
                $this.trigger('change');
            }
        };
        var $datepicker = $this.datepicker($options).data('datepicker');
    });


    $('[data-jsmask="num_charged"]').inputmask({"mask": "999999", placeholder: ""});



    /* WORKER :: Block photo */
    $document.on('click', '[data-worker-block-photo="upload"]', function () {
        var $this = $(this);
        if($this.hasClass('current')) return;
        $this.addClass('current');
        var $thisId = $this.attr('id');

        modalConfirmit({
            'title'     : 'Вы хотите загрузить новое изображение профиля в замен существующего?',
            'btnN'      : 'Нет, оставить это',
            'callBtnY_html'      : '<label for="' + $thisId + '" class="jmodal-button o-hvr bgs-orange" data-btnmethod="y">[btnY]</label>',
            'callBtnN'  : function () {
                // Функция для кнопки НЕТ
            },
            'callBtnY'  : function () {
                // Функция для кнопки ДА
            },
            'AFTER_CLOSE'  : function () {
                setTimeout(function () {
                    $this.removeClass('current');
                },50);
            }
        });
        return false;
    });

    $document.on('change', '[data-worker-block-photo="upload"]', function () {
        var $this = $(this);
        var $wrap = $('[data-worker-block-photo="wrap"]');

        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var $elImage = $wrap.find('[data-worker-block-photo="image"]');
                $elImage.attr({'href' : e.target.result}).css({'background-image' : 'url(' + e.target.result + ')'});
                $wrap.addClass('type-uploads');
            };
            reader.readAsDataURL(this.files[0]);
        };
    });
    /* END :: Block photo */



    // Checkeds in items for table
    $document.on('click refresh-checked-worker', '[data-item-check-worker="current"]:visible', function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parents('tr').addClass('current');
            return;
        }
        $this.parents('tr').removeClass('current');
    });

    $('[data-item-check-worker="all-current"]').change(function () {
        var checkboxes = $(this).parents('[data-item-check-worker="wrap"]').find('[data-item-check-worker="current"]:visible');
        checkboxes.prop('checked', $(this).is(':checked')).trigger('refresh-checked-worker');
        __workerCalcs($('.worker-calcs-wrap'));
    });

    // Подсчет суммы начислений
    $document.on('change', '[data-item-check-worker="current"]', function () {
        __workerCalcs($('.worker-calcs-wrap'));
    });

    $document.on('change', '[data-worker-calcs="typepay"]', function () {
        __workerCalcs($('.worker-calcs-wrap'));
    });



    $document.on('click', '[data-btn-worker-loadr]', function (even) {
        var $item = $(this);
        if ($(even.target).parents('.o-input-box-replaced').length) {
            return;
        }
        var $getId = $item.attr('data-btn-worker-loadr');
        //console.log('Грузим ID в правую часть страницы ' + $getId);
        $item.addClass('current-click').siblings('[data-btn-worker-loadr]').removeClass('current-click');

        $('#side-worker-calculations').html('Грузим ID в правую часть страницы ' + $getId).show();
        $document.on('side-worker-calcs-close closemap-force', function () {
            $('#side-worker-calculations').hide();
        });
    });


    // Копируем значение текущего поля в другое поле
    $document.on('blur', '[data-worker-copyvalueto]', function () {
        var $this = $(this);
        var $getTo = $this.data('worker-copyvalueto');
        var $toInsertValue = $($getTo);
        $toInsertValue.val($this.val());
    });


    // Редактирование ячейки
    $document.on('click', '.js-worker-edit', function () {
        var $this = $(this);
        if($this.hasClass('selected-init')) return;
        $this.addClass('selected-init');

        var $getTitle = $this.data('input-title');
        var $value = $this.html();
        var $defValue = $value;

        if($this.attr('data-jsmask') === 'num_charged')
        {
            $value = parseInt($value.replace(/\D+/g,""));
        }

        if($this.find('.input-value-empty-dotts').length)
        {
            $value = '';
        }

        var $input = $('<input type="text" value="' + $value + '">');
        $this.html($input).addClass('worker-calcs-addrow-input');

        if($this.attr('data-jsmask') === 'num_charged')
        {
            $input.attr({'data-jsmask' : 'num_charged'}).inputmask({"mask": "999999", placeholder: ""});
        }

        $input.focus();

        $input.one('blur', function () {
            $input.remove();
            $this.removeClass('worker-calcs-addrow-input');
            $this.removeClass('selected-init');

            var $getNewValue = $input.val();
            if(String($getNewValue) === String($value))
            {
                $this.html($defValue);
                return;
            }


            //if($getNewValue === '') $getNewValue = $value;
            if($getNewValue === '') $getNewValue = '...';

            $this.html($getNewValue);

            if($getNewValue === $value)
            {
                $.notifye('error', 'Ошибка внесения изменения: заполните поле "' + $getTitle + '" корректно', 4000); return false;
            }
            else
            {
                if($this.data('input-name') === 'paid')
                {
                    $this.parents('[data-status]').attr('data-status', 'paid');
                }
                //$.notifye('success', 'Значение поля "' + $getTitle + '" успешно изменено с "' + $value + '" на "' + $getNewValue + '"', 4000); return false;

                //при изменении поля "Выплачено" делаем доступным checkbox
                if($getNewValue != $value) {
                    var tr = $this.parents('tr');
                    tr.attr('data-status', 'nopaid');
                    tr.find('td.tbl-check').html('<label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check-worker="current" data-worker-calcs="allow"><span class="o-input-box-replaced-check"></span></label>');
                }
            }
        });
    });



    /* Add worker calcs template */
    $document.on('click', '[data-addtbl-worker]', function () {

        var $this = $(this);
        var $isError = 0;
        var $addTemplate = $this.data('addtbl-worker');
        var $getTemplate = $($addTemplate).html();
        var $insertTemplate = $($this.data('addtbl-worker-insert'));

        $insertTemplate.find('[data-input]').each(function () {
            var $input = $(this);
            var $isErr = 0;
            var $inputType = $input.data('input');
            var $inputValue = $input.val();
            if($inputType === 'date' && $inputValue.length !== 10)
            {
                $isErr = 1;
            }
            else if($inputType === 'charged' && $inputValue === '')
            {
                $isErr = 1;
            }
            else if($inputType === 'typepay' && $inputValue === '')
            {
                $isErr = 1;
            }
            else if($inputType === 'comment' && $inputValue !== '')
            {
                var $questionText = $inputValue.replace(/[0-9]/g, '');
                if($questionText.length < 3)
                {
                    $isErr = 1;
                }
            }

            if($isErr)
            {
                $isError++;
                $input.addClass('input-request');
                $input.one('focus change', function () {
                    $input.removeClass('input-request');
                });
            }
            else
            {

            }
        });

        if($isError)
        {
            $.notifye('error', 'Ошибка добавление записи: заполните корректно поля, подсвеченные красным', 10000);
            return false;
        }

        // Возможно здесь должен быть запрос по AJAX, доп проверка на валидность полей, а так же установка ID заказа

        $getTemplate = $getTemplate.replace(/{id}/g, '????');

        var $getValue_date = $insertTemplate.find('[data-input="date"]').val();
        $getTemplate = $getTemplate.replace(/{date}/g, $getValue_date);

        var $getValue_charged = $insertTemplate.find('[data-input="charged"]').val();
        $getTemplate = $getTemplate.replace(/{charged}/g, $getValue_charged);

        var $getValue_paid = $insertTemplate.find('[data-input="paid"]').val();
        var $setValue_paid = $getValue_paid;
        if($getValue_paid === '')
        {
            $setValue_paid = '<span class="input-value-empty-dotts"></span>';
        }
        $getTemplate = $getTemplate.replace(/{paid}/g, $setValue_paid);

        var $getValue_comment = $insertTemplate.find('[data-input="comment"]').val();
        $getTemplate = $getTemplate.replace(/{comment}/g, $getValue_comment);

        var $getOptions_typepay = $insertTemplate.find('[data-input="typepay"]');
        var $getValue_typepay = $getOptions_typepay.val();
        $getOptions_typepay = $getOptions_typepay.html().replace('"' + $getValue_typepay + '"', '"' + $getValue_typepay + '" selected ');
        $getTemplate = $getTemplate.replace(/{typepay}/g, $getOptions_typepay);


        var $itemStatus = 'paid';
        if($getValue_paid === '')
        {
            $itemStatus = 'nopaid';
        }
        $getTemplate = $getTemplate.replace(/{item_status}/g, $itemStatus);


        var $elTemplace = $($getTemplate);
        $elTemplace.hide();

        if ($this.data('addtbl-worker-insert')) {
            $elTemplace.insertAfter($insertTemplate).fadeIn(500);
            $document.trigger('init-chosen');
            //$.notifye('success', 'Новая запись за ' + $getValue_date + ' успешно добавлена', 4000); return false;
        }
    });




    $document.on('change', '[data-calendar-birthday]', function () {
        var $this = $(this);
        var $toAttach = $($this.attr('data-calendar-birthday'));
        var $value = $this.val();

        var $dateData = $value.split('.');
        $value = moment().diff($dateData[2] + '-' + $dateData[1] + '-' + $dateData['0'], 'years');
        $toAttach.html($value);
    });



    // Plugin :: Mask for input
    $document.on('init-mask-card', function () {
        $('[data-input-card="wrap"]:not(.init-mask)').each(function () {
            var $this = $(this);
            $this.addClass('init-mask');
            var $check = $this.find('input[type="checkbox"]');
            var $find = $this.find('input[type="text"]');
            var $numberLength = $find.attr('data-length');

            $check.on('change', function () {
                if($(this).is(':checked'))
                {
                    $find.removeAttr('readonly');
                }
                else
                {
                    $find.attr({'readonly' : 'readonly'}).removeClass('input-request');
                }
            });

            if($numberLength === 'sberbank')
            {
                $find.inputmask({"mask": "9999 9999 9999 9999 99", placeholder: ""});
            }
            else if($numberLength === 'tinkoff')
            {
                $find.inputmask({"mask": "9999 9999 9999 9999", placeholder: ""});
            }

            var $defaultValue = $find.val();

            var $timeBlur = false;

            $find.on('focus', function () {
                if(!$check.is(':checked')) return;

                if($timeBlur) clearInterval($timeBlur);

                var $thisInput = $(this);
                var $thisVal = $thisInput.val();
                $defaultValue = $thisVal;
            });

            $find.bind('op-input-change', function () {
                if($timeBlur) clearInterval($timeBlur);
            });

            $find.on('blur', function () {
                if(!$check.is(':checked')) return;

                var $thisInput = $(this);

                $timeBlur = setTimeout(function () {
                    var $thisInputTitle = $thisInput.attr('data-ntitle');
                    var $thisVal = $thisInput.val();
                    var $thisValOrig = $thisVal;
                    $thisVal = $thisVal.replace(/ /g, '');
                    var $isError = false;
                    if($numberLength === 'sberbank' && !($thisVal.length === 16 || $thisVal.length === 18))
                    {
                        $isError = true;
                    }
                    else if($numberLength === 'tinkoff' && !($thisVal.length === 16))
                    {
                        $isError = true;
                    }

                    if($thisVal === '' || $isError)
                    {
                        $.notifye('error', 'Ошибка заполнения', 4000);
                        $thisInput.val('').addClass('input-request');
                        return;
                    }

                    $thisInput.removeClass('input-request');

                    if($defaultValue === $thisValOrig)
                    {
                        return;
                    }

                    // AJAX response

                    if($defaultValue === '')
                    {
                        $.notifye('success', 'Для поля "' + $thisInputTitle + '" было добавлено и сохранено значение "' + $thisValOrig + '"', 4000);
                        return;
                    }

                    $.notifye('success', 'В поле "' + $thisInputTitle + '" значение "' + $defaultValue + '" было изменено и сохранено на "' + $thisValOrig + '"', 4000);
                }, 150);
            });


        });
    });
    $document.trigger('init-mask-card');



    /* REVIEWS */
    /* Order autocomplete add */
    $( "#js-addreviews-order-search" ).autocomplete({
        source: "example__addreviews_orders.json",
        minLength: 2,
        delay: 100,
        select: function( event, ui )
        {
            if(ui['item']['value'])
            {
                $( this ).val(ui['item']['value']).trigger('op-input-change');
            }
            return false;
        }
    });
    $document.on('click', '[data-wreviews="btn_edit"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-wreviews="item"]');
        var $replace = $wrap.find('[data-wreviews="edited"]');
        var $getText = $replace.text();

        var $tpls = '<div class="op-input type-btns" data-op-input="item"><textarea placeholder="Текст Вашего отзыва..." data-autoheight="true" data-op-input="get">' + $getText + '</textarea></div>';
        //$replace.addClass('edited-allow').attr({'contenteditable' : true});

        $replace.addClass('edited-allow').html($tpls);

        var $inputItem = $replace.find('[data-op-input="item"]');
        var $inputGet = $replace.find('[data-op-input="get"]');
        $inputGet.trigger('input');

        $inputGet.focus();
        setTimeout(function () {
            $inputGet[0].setSelectionRange($inputGet[0].value.length,$inputGet[0].value.length);
        },5);


        var $outFocus = true;
        $replace.on('mouseenter.revedited', '[data-op-input="clear"]', function (even) {
            $outFocus = false;
            //console.log('Hover');
        });
        $replace.on('mouseleave.revedited click.revedited', '[data-op-input="clear"]', function (even) {
            $outFocus = true;
            //console.log('OutHover');
        });

        $replace.on('blur.revedited', '[data-op-input="get"]', function (even) {
            var $elThis = $(this);
            var $elText = $elThis.val();
            //console.log($this.data('review_id'));
            if($outFocus)
            {
                $replace.removeClass('edited-allow').html($elText);
                $replace.off('mouseenter.revedited', '[data-op-input="clear"]');
                $replace.off('mouseleave.revedited', '[data-op-input="clear"]');
                $replace.off('click.revedited', '[data-op-input="clear"]');
                $replace.off('blur.revedited', '[data-op-input="get"]');

                if($getText != $elText)
                {
                    $.post(
                        '/admin/users/review_update',
                        {
                            '_token': $('[name="_token"]').val(),
                            'review': $elText,
                            'id': $this.data('review_id')
                        },
                        function(data) {
                            if(data == 'success')
                                $.notifye('success', 'Комментарий изменен.', 4000);
                            else
                                $.notifye('error', data, 4000);
                        }
                    );    
                }
            }
        });
        /*
        */
    });


    $document.on('click', '[data-wreviews="btn_delete"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-wreviews="item"]');

        $.post(
            '/admin/users/review_delete',
            {
                '_token': $('[name="_token"]').val(),
                'id': $this.data('review_id')
            },
            function(data) {
                if(data == 'success') {
                    $wrap.fadeOut(500, function () {
                        $wrap.remove();
                        $.notifye('success', 'Комментарий удален', 4000);
                    })
                }
                else
                    $.notifye('error', data, 4000);
            }
        );
    });


    $document.on('click', '[data-addreviews-toggles="btn"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-addreviews-toggles="wrap"]');
        $wrap.find('[data-addreviews-toggles="hide"]').fadeOut(200, function () {
            $wrap.find('[data-addreviews-toggles="show"]').fadeIn(200);
        });
    });



    $document.on('click', '[data-addreviews-form="submit"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-addreviews-form="wrap"]');
        var $inputs = $wrap.find('input, select, textarea');
        var $data = $inputs.serialize();
        var $inputOrderId = $wrap.find('[name="addreviews_order_id"]');
        var $inputOrderId_val = $inputOrderId.val();
        var $inputMessage = $wrap.find('[name="addreviews_message"]');
        var $inputMessage_val = $inputMessage.val();

        var $inputRating = $wrap.find('[name="addreviews_rating"]:checked');
        var $inputRating_val = $inputMessage.val();

        var $isError = [];
        // Проверяем номер заказа
        if($inputOrderId_val === '')
        {
            $inputOrderId.addClass('input-request');
            $inputOrderId.one('input', function () {
                $inputOrderId.removeClass('input-request');
            });

            $isError.push($inputOrderId.attr('data-ntitle'));
        }
        // Проверяем сообщение
        if($inputMessage_val === '')
        {
            $inputMessage.addClass('input-request');
            $inputMessage.one('input', function () {
                $inputMessage.removeClass('input-request');
            });

            $isError.push($inputMessage.attr('data-ntitle'));
        }

        // AJAX
        //console.log('Ajax запрос и передача параметров: ' + $data);
        // Success

        if($isError.length)
        {
            /*$isError.push('<br><i>Для программиста:</i>');
            $isError.push('<i>Можно через AJAX заранее передать ошибку</i>');
            $isError.push('<i>Например № заказа не найден, и применить код выше для перекраски поля</i>');*/
            $.notifye('error', $isError.join('<br>'), 4000);
            return false;
        }

        //добавить отзыв
        $.post(
            '/admin/users/review_add',
            {
                '_token': $('[name="_token"]').val(),
                'review': $('[name="addreviews_message"]').val(),
                'user_id': $('[name="user_self_id"]').val(),
                'order_id': $('[name="addreviews_order_id"]').val()
            },
            function(data) {
                //console.log(data.error);
                if (typeof data.error != 'undefined')
                    $.notifye('error', data.error, 4000);
                else if(data) {
                    var $tpls = $('#tpl-addreviews-item').html();

                    $wrap.find('input[type="text"], select, textarea').val("");
                    $wrap.find('input[type="radio"]').prop("checked", false);

                    /*var $replaceData = {
                        'photo' : 'img/blank/avatar.png',
                        'name' : 'Осипов Павел',
                        'group' : '(админ)',
                        'company' : 'ООО “Рога и копыта”',
                        'date' : '12.09.2018 - 16:44',
                        'rating' : $wrap.find('input[name="addreviews_rating"]:checked'),
                        'message' : $inputMessage_val,
                        'order_id' : $inputOrderId_val,
                        'order_url' : '/order/view/' + $inputOrderId_val
                    };*/
                    var $replaceData = {
                        'photo' : data.photo,
                        'name' : data.name,
                        'group' : '('+ data.group +')',
                        'company' : '“'+ data.company +'”',
                        'date' : data.date,
                        'rating' : data.rating,
                        'message' : data.message,
                        'order_id' : data.order_id,
                        'order_url' : data.order_url
                    };
                    $.each($replaceData, function ($key, $value) {
                        var $find = "\\[" + $key + "\\]";
                        $tpls = $tpls.replace(new RegExp($find,"g"), $value);
                    });

                    var $reviewItem = $($tpls);
                    $reviewItem.hide();
                    $reviewItem.appendTo($('[data-addreviews-form="added"]'));
                    $reviewItem.fadeIn(500);

                    var $infoPreview = $inputMessage_val.split('.')[0];
                    $.notifye('success', 'Ваш отзыв [' + $infoPreview + '...] успешно добавлен', 4000);
                }
            }, 'json'
        );

        /*var $tpls = $('#tpl-addreviews-item').html();

        $wrap.find('input[type="text"], select, textarea').val("");
        $wrap.find('input[type="radio"]').prop("checked", false);

        var $replaceData = {
            'photo' : 'img/blank/avatar.png',
            'name' : 'Осипов Павел',
            'group' : '(админ)',
            'company' : 'ООО “Рога и копыта”',
            'date' : '12.09.2018 - 16:44',
            'rating' : $wrap.find('input[name="addreviews_rating"]:checked'),
            'message' : $inputMessage_val,
            'order_id' : $inputOrderId_val,
            'order_url' : '/order/view/' + $inputOrderId_val
        };
        $.each($replaceData, function ($key, $value) {
            var $find = "\\[" + $key + "\\]";
            $tpls = $tpls.replace(new RegExp($find,"g"), $value);
        });

        var $reviewItem = $($tpls);
        $reviewItem.hide();
        $reviewItem.appendTo($('[data-addreviews-form="added"]'));
        $reviewItem.fadeIn(500);

        var $infoPreview = $inputMessage_val.split('.')[0];
        $.notifye('success', 'Ваш отзыв [' + $infoPreview + '...] успешно добавлен', 4000);*/
    });

    //ajax загрузка photo////////////////////////////////
    /*var link = $('form.user').find('input[name="img"]');
    link.on('change', function () {
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('photo', file_data);
        form_data.append('_token', $('[name="_token"]').val());
        form_data.append('id', $('[name="user_self_id"]').val());
        form_data.append('data', $('form.user').serialize() + '&fio=' + $('[name="fio"]').val();
        //alert(form_data);
        $.ajax({
            url: '/admin/users/apply_changes',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(response){
                //alert(response);
                if(response.length > 0 ) {
                    //console.log(response);
                    $.notifye('error', response, 10000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notifye('error', 'Файл слишком большой.', 10000);
                //alert(thrownError);
            }
        });
    });*/


    /* END */
});

//кнопка "Применить"
//function user_update(id) {
function aply_user_changes() {
    //works_cities
    var $this = $('[data-regioncheck-choose]');
    var $wrap = $this.parents('[data-regioncheck-wrap]');
    var $inputs = $wrap.find('input:checkbox');
    var $inputsCheckedLength = $inputs.filter('[data-regioncheck="city"]:checked').length;
    if($inputsCheckedLength)
    {
        /* Собираем отмеченные чеки и отправляем на сервер */
        var $dataInput = $inputs.serialize();
        //console.log($dataInput);
        $('input[name="str_city_work"]').val($dataInput);
    }

    //staff
    if( $('[name="staff"]').prop('checked') ) var staff = 1;
    else var staff = 0;
    
    //photo
    var link = $('form.user').find('input[name="img"]');
    var file_data = $(link).prop('files')[0];
    //console.log();
    var form_data = new FormData();
    form_data.append('img', file_data);
    form_data.append('_token', $('[name="_token"]').val());
    form_data.append('id', $('[name="user_self_id"]').val());
    form_data.append('data', $('form.user').serialize() + '&fio=' + $('[name="fio"]').val() + '&staff=' + staff);
        
    //alert(form_data);
    $.ajax({
        url: '/admin/users/apply_changes',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response){
            //alert(response);
            if(response.length > 0 ) {
                //console.log(response);
                $.notifye('error', response, 10000);
            } else {
                $.notifye('success', 'Изменения сохранены.', 10000);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $.notifye('error', 'Файл слишком большой.', 10000);
            //alert(thrownError);
        }
    });
}

function payment_transaction() {
    var data = {};
    $('[data-worker-calcs="item"]').each(function (i) {
        var $row = $(this);
        //console.log($row.data('incomes_id'));
        var is_checked = $row.find('td:eq(0)').find('input').prop('checked');
        if(is_checked === true) {
            var $date = $row.find('td:eq(1)').text();
            var calculated = $row.find('td:eq(3)').find('span').text();

            var paid = $row.find('td:eq(4)').find('span').text();
            if( paid == '...') paid = 0;

            var is_order = ($row.find('td:eq(5)').text().indexOf('#') != -1);
            if(is_order)
                var typ = 'wages';
            else
                typ = $row.find('td:eq(6)').find('select').val();

            if(typ != 'penalty')
                var action = 'outcome';
            else
                var action = 'income';

            data[i] = {
                'id': $row.data('incomes_id'),
                'date': $date,
                'amount': calculated,
                'outcome': paid,
                'type': typ,
                'action': action,
                'company_id': $('[name="company_id"]').val(),
                'user_id': $('[name="user_self_id"]').val()
            };

            /*if( is_order && parseInt(calculated) != parseInt(paid) && parseInt(paid) != 0 )
                $.notifye('error', 'Начисление должно быть равно выплате.', 10000);*/
            
        }
    });
    //console.log(data == {});
    if( data != {} ) {
        $.post(
            '/admin/users/accruals',
            {
                '_token': $('[name="_token"]').val(),
                data: data
            },
            function(data) {
                if(data)
                    $.notifye('success', 'Начисления произведены.', 4000);
            }
        );
    }
}

function user_calculation_period(period, elm) {
    //$('[name="f[period]"]').val(period);
    $('[onclick^="user_calculation_period"]').removeClass('current');
    $(elm).addClass('current');

    $.get(
        '/admin/tabel/get_period_dates/' + period,
        function(data) {
            //console.log(JSON.parse(data).period_start);
            $('[name="f[daterange]"]').val(JSON.parse(data).period_start + ' - ' + JSON.parse(data).period_end);
            window.location.href = window.location.pathname + '?tab=calculations&period=' + period + '&daterange=' + JSON.parse(data).period_start + ' - ' + JSON.parse(data).period_end;
        }
    );
}