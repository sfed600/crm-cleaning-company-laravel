$(document).ready(function() {
    /*window.onbeforeunload = function() {alert();
        return "Данные не сохранены. Точно перейти?";
    };*/

    //при закрытии страницы/браузера если данные не сохранены
    $(window).on("beforeunload", function() {
        //если нажали "Сохранить" то без предупреждения
        if (userClickedSave)
            return;

        //console.log($('.page-kp-form-bottom-button').css('display'));
        if( $('.page-kp-form-bottom-button').is(":visible") )
            return "Данные не сохранены. Точно перейти?";
    })


    var $document = $(document);

    $document.on('click', '[data-contact-block-logo="remove"]', function () {
        var $this = $(this);
        var $wrap = $('[data-contact-block-logo="wrap"]');
        $wrap.removeClass('type-uploads');
    });

    $document.on('change', '[data-contact-block-logo="upload"]', function () {
        var $this = $(this);
        var $wrap = $('[data-contact-block-logo="wrap"]');


        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var $elImage = $wrap.find('[data-contact-block-logo="image"]');
                $elImage.html("").prepend('<img src="' + e.target.result + '" alt="">');
                $wrap.addClass('type-uploads');
            };
            reader.readAsDataURL(this.files[0]);
        };
    });

    //////////////////////////////////
    if( (window.location.pathname.indexOf('/admin/order_companies/') + 1) > 0 ) {
        var link = $('input#order_company_heads_label');
        link.on('change', function () {
            $('input[name="name"]').val(this.value);
        });

        //при изменениии сделать видимой кнопку "Сохранить"
        link.on('keyup', function() {
            if( $('.page-kp-form-bottom-button').is(":hidden") )
                $('.page-kp-form-bottom-button').show();
        });
    }

    //изменение данных ч/з ajax//////////////////////////////////
    //logo
    /*var link = $('[upload-ajax]');
    link.on('change', function () {
        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('logo', file_data);
        form_data.append('_token', $('[name="_token"]').val());
        form_data.append('id', $('[name="company_id"]').val());
        //alert(form_data);
        $.ajax({
            url: '/admin/order_companies/update_ajax',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(response){
                //alert(response);
                if(response.length > 0 ) {
                    //console.log(response);
                    $.notifye('error', response, 10000);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $.notifye('error', 'Файл слишком большой.', 10000);
                //alert(thrownError);
            }
        });
    });

    //comments
    var link = $('#order-company-form').find('textarea[name="comments"]');
    link.on('change', function(){
        update_ajax('comments', 'order_companies');
    });

    //other
    var link = $('#order-company-form').find('[update-ajax]');
    link.on('change', function(){
        if( $(this).prop('id') == 'order_company_heads_label')
            update_ajax('name', 'order_companies');
        else
            update_ajax($(this).prop('name'), 'order_companies');
    });

    //address
    var link = $('#order-company-form').find('input#suggest');
    link.focusout(function() {
        //setTimeout(update_ajax('address', 'order_companies'), 3000);
        update_ajax('address', 'order_companies');
    });*/

    //при изменениии сделать видимой кнопку "Сохранить" /*
    var link = $('[upload-ajax]');
    link.on('change', function () {
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });
    
    var link = $('#order-company-form').find('[update-ajax]');
    /*link.on('change', function() {
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });*/
    link.on('keyup', function() {
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });

    //comments
    var link = $('#order-company-form').find('textarea[name="comments"]');
    link.on('keyup', function(){
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();
    });
    //  */
    
});

function fill_by_inn() {
    var inn = $('[name="inn"]').val();
    //console.log(inn);
    if( inn == '' || (inn.length != 10 && inn.length != 12) ) {
        alert('Введите ИНН!');
        return false;
    }

    $.get(
        '/admin/company/fill_by_inn',
        { 'inn': inn },
        function (data) {
            //console.log(data);
            for (key in data) {
                //console.log(key, data[key]);
                $('input[name="'+ key +'"]').val(data[key]);
            }
        }, 'json'
    );
}