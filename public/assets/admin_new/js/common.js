$(function() {
    triggerOnline();

    //calendar/////////////////////////////////////////////////
    //console.log(window.location.pathname);
    if ((window.location.pathname.indexOf('/admin/calendar') + 1) > 0) {
        var $jsCalendarDayDropinsert = $('#js-calendar-day-dropinsert');
        $(document).on('click', '[data-calendar-day]', function () {
            var $day = $(this);
            if ($day.hasClass('current')) return false;
            
            //$day.addClass('current').siblings('[data-calendar-day]').removeClass('current');
            $('td').find('[data-calendar-day]').removeClass('current');
            $day.addClass('current');

            var $dropText = $day.find('[data-calendar-drop]').html();
            if (!$dropText) {
                $dropText = $('[data-calendar-day-nosearch]').html();
            }

            var $elInsert = $('<div/>').html($dropText);
            $elInsert.hide();

            $jsCalendarDayDropinsert.html("");
            $elInsert.appendTo($jsCalendarDayDropinsert);
            $elInsert.fadeIn(500);
        });
    }
})

//trigger online///////////////////////////////////////////////////////
function triggerOnline() {
    //console.log('triggerOnline init');
    $.get(
        '/admin/users/trigger_online'
    );

    setInterval(function() {
        $.get(
            '/admin/users/trigger_online'
        );
        //console.log('triggerOnline');
    }, 300000);
}

//order contact ajax/////////////////////////////////////////////
function update_ajax(field, context) {
    //console.log(field);
    var data = {
        '_token': $('[name="_token"]').val(),
        'id': $('[name="contact_id"]').val() || $('[name="company_id"]').val() || $('[name="order_id"]').val(),
        'field': field,
        'val': $('[name="'+ field +'"]').val()
    };

    if(field == 'mobile') {
        data.phone_type = 'mobile';
        data.old_mobile = $('[name="old_mobile"]').val();
    } else if(field == 'tel') {
        data.phone_type = 'work';
        data.old_tel = $('[name="old_tel"]').val();
    }

    $.post(
        '/admin/'+ context +'/update_ajax',
        data
    );
}