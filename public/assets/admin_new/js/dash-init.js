$(document).ready(function() {
    var $document = $(document);
    var $body = $('body');
    var $dashboardContent = $('#dashboard-content');

    $(window).load(function () {
        $body.removeClass("preload");
    });


    //Dash kp btn tabs
    var statuses = {};
    statuses['Первичный контакт'] = 'first';
    statuses['Согласовать'] = 'agree';
    statuses['Согласовано'] = 'agreed';
    statuses['Подтвержено'] = 'confirmed';
    statuses['В работе'] = 'work';
    statuses['Работы выполнены'] = 'performed';
    statuses['Успешно реализовано'] = 'final_positive';
    statuses['Не реализовано'] = 'final_negative';

    $document.on('click', '[data-kp-step="btn"]', function(e) {
        //console.log('qw');
        e.preventDefault();
        var $this = $(this);
        var $wrap = $this.parents('[data-kp-step="wrap"]');
        $wrap.find('[data-kp-step="btn"]').removeClass('current');
        $this.addClass('current');

        //console.log($this.find('span').text());
        $('#order-form-main').find('[name="status"]').val(statuses[$this.find('span').text()]);

        //600 ajax order status
        //console.log(statuses[$this.find('span').text()]);
        var data = {
            '_token': $('[name="_token"]').val(),
            'id': $('[name="order_id"]').val(),
            'status': statuses[$this.find('span').text()]
        };

        //alert('Выбрано: ' + $option.data('value'));

        //если не для календаря..
        var data_findtab = $(this).parent().attr('data-findtab');
        if( data_findtab  != 'kp-tabbox-calendar_fact' && data_findtab  != 'kp-tabbox-calendar_plan') {
            //console.log(window.location);
            if( window.location.pathname.indexOf('order_contacts') != -1 ) { //страница контакта изменить тип контакта
                $.post(
                    '/admin/order_contacts/update_ajax',
                    {
                        '_token': $('[name="_token"]').val(),
                        'id': $('[name="contact_id"]').val(),
                        'field': 'typ',
                        'val': $('[name="typ"]').val()
                    },
                    function (data) {
                        $.preloader.show({'attach' : $('.dash-wrap')});
                        setTimeout(function() {
                            $.preloader.hide({'attach' : $('.dash-wrap')});
                        }, 1000);
                    }
                );
            }
            else if( window.location.pathname.indexOf('users') != -1 ) { //страница сотрудника
                /*$.post(
                    '/admin/order_contacts/update_ajax',
                    {
                        '_token': $('[name="_token"]').val(),
                        'id': $('[name="contact_id"]').val(),
                        'field': 'typ',
                        'val': $('[name="typ"]').val()
                    },
                    function (data) {
                        $.preloader.show({'attach' : $('.dash-wrap')});
                        setTimeout(function() {
                            $.preloader.hide({'attach' : $('.dash-wrap')});
                        }, 1000);
                    }
                );*/
            }
            else { //страница заказа изменить статус заказа
                $.post(
                    '/admin/orders/set_status',
                    data,
                    function (data) {
                        $.preloader.show({'attach' : $('.dash-wrap')});
                        setTimeout(function() {
                            $.preloader.hide({'attach' : $('.dash-wrap')});
                        }, 1000);
                    }
                );
            }
        }
        else {
            $('[name="f[tab]"]').val(data_findtab);
            //console.log($(this).parents('form'));
            $('form.dash-search').submit();
        }

    });

    // Background layer for content
    $document.on('loaderOpen', function () {
        $body.addClass('open-container-layer');
    });
    $document.on('loaderClose', function (even, $option) {

        if ($option) {
            var $target = $($option.target);
            if ($target.attr('data-layer') === 'yes') {
                return;
            }
            else
            if ($target.parents('[data-layer]').length) {
                return;
            }
        }

        $body.removeClass('open-container-layer');
        $document.trigger('click.dash-search');
    });


    // Plugin :: Styler for select
    $(".selectpicker").chosen({no_results_text: "Нечего не найдено.", disable_search_threshold: 15});
    $(".select-chosen").chosen({no_results_text: "Нечего не найдено."});


    // Plugin :: Mask for input
    $document.on('init-mask', function () {
        $('[data-jsmask]').each(function () {
            var $this = $(this);
            var $mask = $this.attr('data-jsmask');
            var $find = $this.not('.init-mask').addClass('init-mask');
            if($mask == 'date')
            {
                $find.inputmask({"mask": "99.99.9999", placeholder: ""});

                //600 order_company date_register /*
                //console.log($find.val());
                $this.inputmask({"mask": "99.99.9999", placeholder: ""});
                //600   */
            }
            else if($mask == 'time')
            {
                $find.inputmask({"mask": "99:99", placeholder: ""});
            }
            else if($mask == 'tel')
            {
                //$find.inputmask({"mask": "(999) 999-9999", placeholder: "(___) ___-____"});
                $find.inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});
            }
        });
    });
    $document.trigger('init-mask');


    // Checkeds in items for table
    $document.on('click refresh-checked', '[data-item-check="current"]', function () {
        var $this = $(this);
        if ($this.is(':checked')) {
            $this.parents('tr').addClass('current');
            return;
        }
        $this.parents('tr').removeClass('current');
    });

    $('[data-item-check="all-current"]').change(function () {
        var checkboxes = $(this).parents('[data-item-check="wrap"]').find(':checkbox');
        checkboxes.prop('checked', $(this).is(':checked')).trigger('refresh-checked');
    });


    // Search - option remove
    $document.on('click', '[data-dash-search="remove_option"]', function () {
        var $this = $(this);
        var $option = $this.parents('[data-dash-search="option"]');
        $option.fadeOut(500, function () {
            $option.remove();
        });
        //return false;
    });

    // Search - drop list
    var __dashSearchDrop = function (even) {

        if ($('[data-dash-search="drop"]').css('visibility') === 'visible') {
            return;
        }

        if ($(even.target).attr('data-dash-search') == 'remove_option') {
            return;
        }

        var $input = $(this);
        if ($input.attr('data-layer') === 'yes') {
            $document.trigger('loaderOpen');
        }
        var $form = $input.parents('[data-dash-search="wrap"]');
        $form.addClass('open-drop');

        $document.on('click.dash-search', function (even) {
            if (

                $(even.target).parents('.daterangepicker').length ||
                $(even.target).parents('.prev').length ||
                $(even.target).parents('.next').length ||
                (
                    $(even.target).attr('class') &&
                    (
                        $(even.target).attr('class').indexOf('prev') !== -1 ||
                        $(even.target).attr('class').indexOf('next') !== -1
                    )
                )

            ) {
                return;
            }

            if (!$(even.target).parents('[data-dash-search="wrap"]').length) {
                $form.removeClass('open-drop');
                $document.off('click.dash-search');
                $document.trigger('loaderClose', even);
            }
        });
    };
    $document.on('click', '[data-dash-search="option"]', __dashSearchDrop);
    $document.on('focus', '[data-dash-search="input"]', __dashSearchDrop);
    // END - Search - drop list


    // Dash nav list drop layer
    var $dashNavList = $('.dash-nav-list');
    $document.on({
        mouseenter: function () {
            if ($(this).find('> .dash-nav-drop').length) {
                $dashNavList.addClass('current');
                $document.trigger('loaderOpen');
                $document.trigger('click');
            }
        },
        click: function () {
            if ($(this).find('> .dash-nav-drop').length) {
                $dashNavList.addClass('current');
                $document.trigger('loaderOpen');
                $document.trigger('click');
            }
        },
        mouseleave: function (even) {
            $dashNavList.removeClass('current');
        }
    }, '.dash-nav-list > li');

    $document.on({
        mouseleave: function (even) {
            $document.trigger('loaderClose', false);
        }
    }, '.dash-nav-list');


    // Div dropped
    var __divDrop = function (even) {
        var $title = $(this);
        if ($title.attr('data-layer') === 'yes') {
            $document.trigger('loaderOpen');
        }
        var $selectWrap = $title.parents('[data-dropped="wrap"]');

        if ($selectWrap.hasClass('open-drop')) {
            $document.trigger('click.pseudoDiv');
            return;
        }

        $selectWrap.addClass('open-drop');

        $document.one('click.pseudoDiv', function (even) {
            $selectWrap.removeClass('open-drop');
            $document.trigger('loaderClose', even);
        });
    };
    $document.on('click', '[data-dropped="btn"]', __divDrop);


    // Pseudo span - drop select(click статусы заказа on order index page)
    /*var __pseudoSelect = function (even) {
     var $title = $(this);
     var $selectWrap = $title.parents('[data-pseudo="wrap"]');

     if ($selectWrap.hasClass('open-drop')) {
     $document.trigger('click.pseudo');
     return false;
     }

     $selectWrap.addClass('open-drop');


     $selectWrap.one('click', '[data-value]', function () {
     var $option = $(this);
     var $optionClass = $option.data('class');


     //alert('Выбрано: ' + $option.data('value'));
     $.post(
     '/admin/orders/set_status',
     {
     '_token': $('[name="_token"]').val(),
     'id': $option.data('order_id'),
     'status': $option.data('value')
     }
     );

     // Добавляем класс фона от опции
     if ($optionClass) {
     $title.removeClass(function (index, className) {
     return (className.match(/(^|\s)bgs-\S+/g) || []).join(' ');
     });
     $title.addClass($optionClass);
     }

     $title.text($option.text());

     $option.addClass('current').siblings('[data-value]').removeClass('current');
     $selectWrap.removeClass('open-drop');
     });

     $document.one('click.pseudo', function (even) {
     $selectWrap.off('click');
     $selectWrap.removeClass('open-drop');
     });
     };
     $document.on('click', '[data-pseudo="title"]', __pseudoSelect);*/
    var __pseudoSelect = function (even) {
        var $title = $(this);
        var $selectWrap = $title.parents('[data-pseudo="wrap"]');

        if ($selectWrap.hasClass('open-drop')) {
            $document.trigger('click.pseudo');
            return false;
        }

        $selectWrap.addClass('open-drop');


        $selectWrap.one('click', '[data-value]', function () {
            var $option = $(this);
            var $optionClass = $option.data('class');

            var data = {
                '_token': $('[name="_token"]').val(),
                'id': $option.data('order_id'),
                'status': $option.data('value')
            };

            //console.log($(this).data('value'));
            if ( $(this).data('value').indexOf('pay') != -1 )
                data.typ = 'pay';
            
            //alert('Выбрано: ' + $option.data('value'));
            $.post(
                '/admin/orders/set_status',
                data
            );

            // Добавляем класс фона от опции
            if ($optionClass) {
                $title.removeClass(function (index, className) {
                    return (className.match(/(^|\s)bgs-\S+/g) || []).join(' ');
                });
                $title.addClass($optionClass);
            }

            $title.text($option.text());

            $option.addClass('current').siblings('[data-value]').removeClass('current');
            $selectWrap.removeClass('open-drop');
        });

        $document.one('click.pseudo', function (even) {
            $selectWrap.off('click');
            $selectWrap.removeClass('open-drop');
        });
    };
    $document.on('click', '[data-pseudo="title"]', __pseudoSelect);


    // Report hover hint position
    $('.tbl-box-hint').parents('td').hover(function () {
        var $td = $(this);
        $td.addClass('open-drop');
        var $hint = $td.find('.tbl-box-hint');
        var $w = $hint.offset().left + $hint.width();
        if ($body.width() < $w) {
            $hint.addClass('pos-right');
        }

    }, function () {
        var $td = $(this);
        $td.removeClass('open-drop');
    });


    // Plugin :: Paginator
    $('.js-paginator-report').pagination({
        items: 300,
        itemsOnPage: 10,
        displayedPages: 5,
        prevText : '&laquo;',
        nextText : '&raquo;',
        cssStyle: 'pag-theme'
    });


    var $daterangepickerLocale = {
        "format": "DD.MM.YYYY",
        "separator": " - ",
        "applyLabel": "Выбрать",
        "cancelLabel": "Отмена",
        "fromLabel": "От",
        "toLabel": "До",
        "customRangeLabel": "Custom",
        "daysOfWeek": ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        "monthNames": [
            'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
        ],
        "firstDay": 1
    };
    $document.on('init-datepicker', function () {
        var $picker = $('.datepicker').each(function () {
            var $this = $(this);
            if($this.hasClass('init-date')) return;
            var $value = $this.val();
            $this.addClass('init-date');
            $this.daterangepicker({
                "opens": "center",
                //"drops": "up",
                "locale": $daterangepickerLocale,
                //autoUpdateInput: false
            });
            if(!$value) $this.val('');
        });
        $picker.on('showCalendar.daterangepicker', function(ev, picker) {
            if (picker.element.offset().top - $(window).scrollTop() + picker.container.outerHeight() > $(window).height() - 50) {
                return picker.drops = 'up';
            } else {
                return picker.drops = 'down';
            }
        });
    });
    $document.trigger('init-datepicker');

    var __inlineFunc_inputBtnRemove = function ($input) {
        var $wrap = $input.parents('[data-op-input="item"]');
        var $getValue = $input.val();

        //var $tpls = '<div class="op-input-control-btn" data-op-input="clear"><span class="icon-input-ctrl-clear"></span><span class="icon-input-ctrl-clear-h btn-hover"></span></div>';
        var $tpls = '';

        if ($getValue) {
            if (!$wrap.find('[data-op-input="clear"]').length) {
                $wrap.prepend($tpls);
            }
            $wrap.addClass('allow-remove');
            return
        }
        setTimeout(function () {
            $wrap.find('[data-op-input="clear"]').remove();
        }, 10);
        $wrap.removeClass('allow-remove');
    };
    $document.on('input op-input-change', '[data-op-input="get"]', function () {
        var $input = $(this);
        __inlineFunc_inputBtnRemove($input);
    });
    $('[data-op-input="get"]').each(function () {
        var $input = $(this);
        __inlineFunc_inputBtnRemove($input);
    });


    $document.on('click', '[data-op-input="clear"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-op-input="item"]');
        $wrap.find('[data-op-input="get"]').val("").trigger('op-input-change').focus();

        //600
        /*var $this = $(this);
         var url = '/admin/orders/delete_contact';

         var data = {
         '_token': $('[name="_token"]').val(),
         //'_method': "DELETE",
         'contact_id': $this.attr('contact_id'),
         'order_id': $('[name="order_id"]').val()
         };

         $.post(
         url,
         data,
         function() {
         var $item = $this.parents('.type-contact');
         $item.fadeOut(500, function () {
         $item.remove();
         });
         }
         );*/
    });

    $document.on('click', '[data-op-input="search"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-op-input="item"]');
        var $text = $wrap.find('input').val();
        alert('Ищем: ' + $text);
    });

    //delete order contact  //600
    $document.on('click', '[data-contact="remove"]', function () {
        if( $('[name="contact_ids[]"]').length == 1 )
            return;

        var $this = $(this);
        /*var url = '/admin/orders/delete_contact';

        var data = {
            '_token': $('[name="_token"]').val(),
            //'_method': "DELETE",
            'contact_id': $this.attr('contact_id'),
            'order_id': $('[name="order_id"]').val()
        };

        $.post(
            url,
            data,
            function() {
                var $item = $this.parents('.type-contact');
                $item.fadeOut(500, function () {
                    $item.remove();
                });
            }
        );*/
        var $item = $this.parents('.type-contact');
        $item.fadeOut(500, function () {
            $item.remove();
        });

        $('[name="contact_ids[]"]').find('[value="' + $this.attr('contact_id')+'"]').remove();
    });

    //delete order_company  //600
    $document.on('click', '[data-order-company="remove"]', function () {
        //console.log(window.location);

        var $this = $(this);

        if( window.location.href.indexOf('/admin/orders/') != -1  ) {
            /*var url = '/admin/orders/delete_order_company';

            var data = {
                '_token': $('[name="_token"]').val(),
                //'order_company_id': $this.attr('order_company_id'),
                'order_company_id': $('[name="order_company_id"]').val(),
                'id': $('[name="order_id"]').val()
            };*/
            $('[name="order_company_id"]').val('');

            var $item = $('#js-kp-order-boxdata-company-insert');
            $item.fadeOut(500, function () {
                $item.remove();
            });
        }
        else if( window.location.href.indexOf('/admin/order_contacts/') != -1   ) {
            var url = '/admin/order_contacts/delete_order_company';

            var data = {
                '_token': $('[name="_token"]').val(),
                //'_method': "DELETE",
                'order_company_id': $this.attr('order_company_id'),
                'id': $('[name="contact_id"]').val()
            };

            $.post(
                url,
                data,
                function() {
                    var $item = $('#js-kp-order-boxdata-company-insert');
                    $item.fadeOut(500, function () {
                        $item.remove();
                    });
                }
            );
        }

        /*$.post(
            url,
            data,
            function() {
                var $item = $('#js-kp-order-boxdata-company-insert');
                $item.fadeOut(500, function () {
                    $item.remove();
                });
            }
        );*/

    });

    $document.on('click', '[data-persons="remove"]', function () {
        var $this = $(this);
        var user_id = $this.data('user_id');

        //detach user to order
        //console.log($input.attr("user_id"));
        $.post(
            '/admin/orders/detach_user',
            {

                '_token': $('[name="_token"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'user_id': user_id
            }
        );

        $('input[data_user_id='+user_id+']').remove();

        var $item = $this.parents('[data-persons="item"]');
        $item.fadeOut(500, function () {
            $item.remove();
        });
    });


    var __addPerson = function ($this, $data) {
        var $wrap = $this.parents('[data-persons="wrap"]');
        var $input = $wrap.find('[data-op-input="get"]');

        var $tpls = $('#kp-order-person-additem').html();

        $tpls = $tpls.replace(/\[avatar\]/g, $data['avatar']);
        $tpls = $tpls.replace(/\[name\]/g, $data['name']);
        $tpls = $tpls.replace(/\[user_id\]/g, $data['id']);

        var $appendElem = $($tpls);
        $appendElem.appendTo($wrap);
        $input.val("").trigger('op-input-change').focus();
    };



    $document.on('click', '[data-op-input="add_person"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-persons="wrap"]');
        var $input = $wrap.find('[data-op-input="get"]');
        var $inputText = $input.val();

        //attach user to order
        //console.log($input.attr("user_id"));
        $.post(
            '/admin/orders/attach_user',
            {

                '_token': $('[name="_token"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'user_id': $input.attr("user_id")
            }
        );

        if ($inputText == '') {
            $input.addClass('input-request');
            setTimeout(function () {
                $input.removeClass('input-request');
            }, 500);
            return;
        }

        __addPerson($this, {'avatar' : 'img/blank/avatar.png', 'name' : $inputText});
        $input.val("").trigger('op-input-change').focus();
    });


    /* ответственный(карточка заказа) */
    var $dataPerson = {};

    $( "#js-kp-order-person-search" ).autocomplete({
        //source: availablePerson,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/users/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    search_param: request.term,
                    block: 'Management'
                },
                method: "POST",
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataPerson[obj['name']] = {
                                'id': obj['id'],
                                'name' : obj['name'],
                                'avatar' : '/assets/imgs/users/preview/' + obj['avatar'],
                            };
                        }
                    }
                    //console.log($dataPerson);
                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            if(ui['item']['value'] && $dataPerson[ui['item']['value']])
            {
                var $data = $dataPerson[ui['item']['value']];
                __addPerson($( this ), $data);
                $('form#order-form-main').prepend('<input type="hidden" name="user_id[]" value="'+ $data['id'] +'" data_user_id="'+ $data['id'] +'">');
            }

            $( this ).val('').trigger('op-input-change');
            return false;
        },
        minLength: 3
    });





    /* Auto height for textarea */
    $document.on('input copy', '[data-autoheight="true"]', function () {
        var $this = this;
        $this.style.height = "1px";
        $this.style.height = (25 + $this.scrollHeight) + "px";
    });

    $document.on('click', '[data-boxtoggle-fields="wrap"]', function () {
        var $this = $(this);
        if ($this.hasClass('toggle-open')) return;
        $this.addClass('toggle-open');
        $this.find('[data-boxtoggle-fields="drop"]').slideDown(500);

        var $prev = $this.siblings('[data-boxtoggle-fields="wrap"]');
        $prev.removeClass('toggle-open');

        $prev.find('[data-boxtoggle-fields="drop"]').slideUp(500);
    });

    /* Items remove */
    $document.on('click', '[data-btn-addworks="remove_item"]', function () {
        var $this = $(this);
        var $item = $this.parents('[data-addworks-item]');
        if ($item.length) {
            $item.fadeOut(500, function () {
                $item.remove();
                total(); save_calculator_data(null, 'ajax');
            });
        }
    });

    /* Add template */
    $document.on('click', '[data-addtemplate]', function () {
        var $this = $(this);
        var $addTemplate = $this.data('addtemplate');
        var $getTemplate = $($addTemplate).html();

        if ($this.data('addtemplate-append')) {
            $($this.data('addtemplate-append')).append($getTemplate);
        }
        else
        if ($this.data('addtemplate-prepend')) {
            $($this.data('addtemplate-prepend')).prepend($getTemplate);
        }
    });

    /* Address map */
    $document.on('focus', '[data-openside-map]', function () {
        var $this = $(this);
        var $for = $($this.attr('data-openside-map'));
        if ($for.length) {
            $for.fadeIn(500);
            $for.on('click.closemap', '.kp-yamap-position-btn-close', function (even) {
                $for.hide();
                $for.off('click.closemap');
                $document.off('closemap-force');
            });
            $document.on('closemap-force', function (even) {
                $for.hide();
                $for.off('click.closemap');
                $document.off('closemap-force');
            });
        }
    });


    /* Calc cleaning */
    var __calcCleaning = function ($wrap) {
        var $items = $wrap.find('[data-calc-cleaning="item"]');
        var $alltotal = $wrap.find('[data-calc-cleaning="alltotal"]');
        var $sum = 0;

        $items.each(function () {
            var $item = $(this);
            var $input = $item.find('[data-calc-cleaning="input"]');
            var $cost = $item.find('[data-calc-cleaning="cost"]');
            var $total = $item.find('[data-calc-cleaning="total"]');
            var $calculate = parseFloat($cost.html()) * parseFloat($input.val());
            if (!$calculate) {
                $calculate = 0;
            }
            $sum = $sum + $calculate;
            $total.html($calculate);
        });
        $alltotal.html($sum);
    };

    $document.on('change input', '[data-calc-cleaning="wrap"]', function () {
        __calcCleaning($(this));
    });
    __calcCleaning($('[data-calc-cleaning="wrap"]'));


    /* Calc other */
    var __calcCleaningOther = function ($wrap) {
        var $items = $wrap.find('[data-calc-clean-other="item"]');
        var $alltotal = $wrap.find('[data-calc-clean-other="alltotal"]');
        var $sum = 0;

        $items.each(function () {
            var $item = $(this);
            var $input = $item.find('[data-calc-clean-other="input"]');
            var $cost = $item.find('[data-calc-clean-other="cost"]');
            var $calculate = parseFloat($cost.html()) * parseFloat($input.val());
            if (!$calculate) {
                $calculate = 0;
            }
            $sum = $sum + $calculate;
        });
        $alltotal.html($sum);
    };

    $document.on('change input', '[data-calc-clean-other="wrap"]', function () {
        __calcCleaningOther($(this));
    });
    __calcCleaningOther($('[data-calc-clean-other="wrap"]'));




    /* Calc other */
    /*var __calcCleaningCarpets = function ($wrap) {
     var $items = $wrap.find('input[data-calc-clean-carpets]');
     var $alltotal = $wrap.find('[data-calc-clean-carpets="alltotal"]');
     var $sum = 0;

     $items.each(function () {
     var $item = $(this);
     var $input = $item.attr('data-calc-clean-carpets');

     switch ($item.attr('type')) {
     case 'text':
     var $calculate = parseFloat($input) * parseFloat($item.val());
     if (!$calculate) {
     $calculate = 0;
     }
     $sum = $sum + $calculate;
     break;
     case 'radio':
     if(!$item.is(':checked')) return;
     $sum = $sum + parseFloat($input);
     break;
     }
     });
     $alltotal.html($sum);
     };

     $document.on('change input', '[data-calc-clean-carpets="wrap"]', function () {
     __calcCleaningCarpets($(this));
     });
     __calcCleaningCarpets($('[data-calc-clean-carpets="wrap"]'));*/


    /* Tooltipster */
    $('.form-infos-btn, .tooltipster').tooltipster({
        theme: 'tooltipster-light',
        side: ['top']
    });


    /* Order box add other fields */
    $document.on('click', '[data-addfieldbox-tgl-show="btn"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-addfieldbox-tgl-show="wrap"]');
        var $drop = $wrap.find('[data-addfieldbox-tgl-show="field"]');

        if (!$this.hasClass('current')) {
            $this.addClass('current');
            $drop.slideDown(500);
            return;
        }

        $this.removeClass('current');
        //$drop.slideUp(500);
        $drop.each(function () {
            var $item = $(this);
            var $input = $item.find('input[type="text"]');
            if ($input.val() === '') {
                $item.slideUp(500);
            }
        });
    });


    /* Order date add */
    $document.on('click', '[data-kporder-date="add"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-kporder-date="clone"]');
        var $clone = $wrap.clone();
        $clone.find('[data-kporder-date="add"]').attr('data-kporder-date', 'remove').html('-');
        $clone.find('.init-mask').removeClass('init-mask').removeAttr('im-insert');
        $clone.insertAfter($wrap);
        $document.trigger('init-mask');
        $wrap.find('input[type="text"], input[type="date"]').val('');
    });
    $document.on('click', '[data-kporder-date="remove"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-kporder-date="clone"]');
        $wrap.fadeOut(500, function () {
            $wrap.remove();
        });
    });


    /* CONTACTS :: Order autocomplete add */
    /*var $dataContacts = {
     "Осипов Павел" : {
     'fio' : 'Осипов Павел',
     'work-tel' : '1111',
     'mob-tel' : '2222',
     'email' : '3333',
     'work-position' : '4444',
     },
     "Осипов Павел 2" : {
     'fio' : 'Осипов Павел 2',
     'work-tel' : '1111 2',
     'mob-tel' : '2222 2',
     'email' : '3333 2',
     'work-position' : '4444 2',
     },
     "Осипов Павел 3" : {
     'fio' : 'Осипов Павел 3',
     'work-tel' : '1111 3',
     'mob-tel' : '2222 3',
     'email' : '3333 3',
     'work-position' : '4444 3',
     },
     };

     var availableTags = [
     "Осипов Павел",
     "Осипов Павел 2",
     "Осипов Павел 3",
     "Осипов Павел 4"
     ];

     $( "#js-kp-order-boxdata-contacts-search" ).autocomplete({
     source: availableTags,
     select: function( event, ui ) {
     var $template = $($('#js-kp-order-boxdata-contacts-template').html());

     if(ui['item']['value'] && $dataContacts[ui['item']['value']])
     {
     $.each($dataContacts[ui['item']['value']], function ($indx, $data) {
     $template.find('[data-name="' + $indx + '"]').val($data);
     });
     }

     $('#js-kp-order-boxdata-contacts-insert').find('[data-boxtoggle-fields="wrap"]').removeClass('toggle-open').find('[data-boxtoggle-fields="drop"]').hide();
     $template.prependTo($('#js-kp-order-boxdata-contacts-insert'));

     $template.find('[data-name="fio"]').trigger('op-input-change');

     $( this ).val('').trigger('op-input-change');
     return false;
     }
     });*/

    //add new order contact
    $('#js-kp-order-boxdata-contacts-addnew').click(function () {
        var $this = $(this);
        var $find = $this.parents('[data-op-input="item"]');
        var $input = $find.find('[data-op-input="get"]');

        if ($input.val() == '') {
            $input.addClass('input-request');
            setTimeout(function () {
                $input.removeClass('input-request');
            }, 500);
            return;
        }

        var $template = $($('#js-kp-order-boxdata-contacts-template').html());

        var contact_ids = [];
        $('[name="contact_ids[]"]').each(function () {
            contact_ids.push($(this).val());
        });
        //console.log(contact_ids);

        $.post(
            '/admin/order_contacts',
            {
                '_token': $('[name="_token"]').val(),
                'name': $input.val(),
                'user_id': $('[name="contact_user_id"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'contact_ids': contact_ids,
                'order_company_id': $('[name="order_company_id"]').val()
            },
            function(contact_id)
            {
                //console.log(contact_id);
                if(contact_id > 0) {
                    $template.find('[name="contact_ids[]"]').val(contact_id);
                    $template.find('[data-contact="remove"]').attr('contact_id', contact_id);

                    $template.find('[data-name="fio"]').val($input.val());

                    //600
                    $template.find('[data-name="name"]').val($input.val());
                    $template.find('[data-name="id"]').val(contact_id);

                    //для свернутых
                    $template.find('[div-data-name="name"]').text($input.val());

                    $input.val('');
                    $find.find('[data-op-input="clear"]').remove();

                    $('#js-kp-order-boxdata-contacts-insert').find('[data-boxtoggle-fields="wrap"]').removeClass('toggle-open').find('[data-boxtoggle-fields="drop"]').hide();

                    $template.prependTo($('#js-kp-order-boxdata-contacts-insert'));

                    $template.find('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

                    //ajax order_contacts
                    $template.find('input').on('change', function() {
                        //console.log($('[name="order_company_id"]').val());

                        var field = $(this).prop('name').replace('[]', '');
                        /*if(field == 'contact_names')
                         field = 'name';*/

                        var data = {
                            '_token': $('[name="_token"]').val(),
                            //'id': $(this).parents('[type-contact]').find('[name="contact_ids[]"]').val(),
                            'id': $template.find('[name="contact_ids[]"]').val(),
                            'field': field,
                            'val': $(this).val()
                        };

                        if( $(this).data('jsmask') == 'tel' ) {
                            //console.log($(this).prop('name').replace('[]', ''));
                            var input_phone_name = $(this).prop('name').replace('[]', '');
                            if( input_phone_name == 'tel' )
                                data.phone_type = 'work';
                            else if( input_phone_name == 'mobile' )
                                data.phone_type = 'mobile';
                        }

                        $.post(
                            '/admin/order_contacts/update_ajax',
                            data
                        );
                    });

                    $template.find('[data-name="fio"]').trigger('op-input-change');
                }
            }
        );

        /*$template.find('[data-name="fio"]').val($input.val());

         //600
         $template.find('[data-name="name"]').val($input.val());
         //для свернутых
         $template.find('[div-data-name="name"]').text($input.val());

         $input.val('');
         $find.find('[data-op-input="clear"]').remove();

         $('#js-kp-order-boxdata-contacts-insert').find('[data-boxtoggle-fields="wrap"]').removeClass('toggle-open').find('[data-boxtoggle-fields="drop"]').hide();

         $template.prependTo($('#js-kp-order-boxdata-contacts-insert'));
         $template.find('[data-name="fio"]').trigger('op-input-change');*/
    });


    /* COMPANY :: Order autocomplete add */
    var $dataCompany = {};

    $( "#js-kp-order-boxdata-company-search" ).autocomplete({
        //source: $tagsCompany,
        source: function(request, response){
            // организуем кроссдоменный запрос
            $.ajax({
                url: '/admin/order_companies/search',
                dataType: "json",
                // параметры запроса, передаваемые на сервер (последний - подстрока для поиска):
                data:{
                    //maxRows: 12,
                    term: request.term
                },
                // обработка успешного выполнения запроса
                success: function(data){
                    //console.log(data);
                    for (var i in data) {
                        var obj = data[i];
                        for (var key in obj) {
                            $dataCompany[obj['name']] = {
                                'id' : obj['id'],
                                'name' : obj['name'],
                                'tel' : obj['phone'],
                                'www' : obj['site'],
                                'email' : obj['email'],
                            };
                        }
                    }

                    // приведем полученные данные к необходимому формату и передадим в предоставленную функцию response
                    response($.map(data, function(item){
                        return{
                            label: item.name,
                            value: item.name
                        }
                    }));
                }
            });
        },
        select: function( event, ui ) {
            var $template = $($('#js-kp-order-boxdata-company-template').html());

            if(ui['item']['value'] && $dataCompany[ui['item']['value']])
            {
                $('[name="order_company_id"]').val($dataCompany[ui['item']['value']]['id']);

                $.each($dataCompany[ui['item']['value']], function ($indx, $data) {
                    //console.log($data);
                    $template.find('[data-name="' + $indx + '"]').val($data);
                });
            }

            $('#js-kp-order-boxdata-company-insert').html('');
            $template.prependTo($('#js-kp-order-boxdata-company-insert'));
            $template.find('[data-name="name"]').trigger('op-input-change');

            $( this ).val('').trigger('op-input-change');
            return false;
        },
        minLength: 3
    });


    /* Other fields add */
    $('#js-kp-order-boxdata-company-addnew').click(function () {
        //console.log('qazxsw');
        var $this = $(this);
        var $find = $this.parents('[data-op-input="item"]');
        var $input = $find.find('[data-op-input="get"]');

        if ($input.val() == '') {
            $input.addClass('input-request');
            setTimeout(function () {
                $input.removeClass('input-request');
            }, 500);
            return;
        }

        var $template = $($('#js-kp-order-boxdata-company-template').html());

        //600
        //show btn save
        if( $('.page-kp-form-bottom-button').is(":hidden") )
            $('.page-kp-form-bottom-button').show();

        //create order company ajax
        var contact_ids = [];
        $('[name="contact_ids[]"]').each(function () {
            contact_ids.push($(this).val());
        });
        //console.log(contact_ids);

        $.post(
            '/admin/orders/create_order_company',
            {
                '_token': $('[name="_token"]').val(),
                'order_id': $('[name="order_id"]').val(),
                'contact_ids': contact_ids,
                'name': $('#js-kp-order-boxdata-company-search').val()
            },
            function(order_company_id)
            {
                //console.log(order_company_id);
                if(order_company_id > 0) {
                    $('[name="order_company_id"]').val(order_company_id);

                    var $template = $($('#js-kp-order-boxdata-company-template').html());

                    $template.find('[data-name="name"]').val($input.val());
                    $input.val('');
                    $find.find('[data-op-input="clear"]').remove();

                    $('#js-kp-order-boxdata-company-insert').html('');

                    $template.prependTo($('#js-kp-order-boxdata-company-insert'));

                    $template.find('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

                    $template.find('[data-name="name"]').trigger('op-input-change');

                    //change order_company ajax
                    $template.find('input').on('change', function() {
                        //show btn save
                        if( $('.page-kp-form-bottom-button').is(":hidden") )
                            $('.page-kp-form-bottom-button').show();

                        $.post(
                            '/admin/order_companies/update_ajax',
                            {
                                '_token': $('[name="_token"]').val(),
                                //'order_company_id': $('[name="order_company_id"]').val(),
                                'id': $('[name="order_company_id"]').val(),
                                'order_id': $('[name="order_id"]').val(),
                                'field': $(this).prop('name'),
                                'val': $(this).val()
                            }
                        );
                    });

                }
            }
        );

        /*$template.find('[data-name="name"]').val($input.val());
         $input.val('');
         $find.find('[data-op-input="clear"]').remove();

         $('#js-kp-order-boxdata-company-insert').html('');

         $template.prependTo($('#js-kp-order-boxdata-company-insert'));

         $template.find('[data-jsmask="tel"]').inputmask({"mask": "+7(999) 999-9999", placeholder: "+7(___) ___-____"});

         $template.find('[data-name="name"]').trigger('op-input-change');*/

        $template.find('[data-name="name"]').trigger('op-input-change');
    });


    /* Remove and clear input */
    $document.on('click', '#js-kp-order-boxdata-contacts-insert [data-op-input="clear"]', function () {
        var $this = $(this);
        var $wrap = $this.parents('[data-boxtoggle-fields="wrap"]');
        $wrap.find('input[type]').val('').trigger('op-input-change');
    });


    //Dash tabs
    $document.on('click', '[data-tab]', function(e) {
        var $this = $(this);
        if($this.hasClass('current')) return false;
        $document.trigger('closemap-force');
        $forBox = $this.attr('data-findtab');
        if(!$forBox)
            return false;
        else {
            if($forBox == 'kp-tabbox-main' || $forBox == 'kp-tabbox-calc') {
                $('.order-navbar-btn').text('+ Добавить заказ');
                $('.dash-panel-dropdown-drop').hide();
            }
            else if($forBox == 'kp-tabbox-offers') {
                $('.order-navbar-btn').text('+ Добавить КП');
                $('.dash-panel-dropdown-drop').show();
            }
            else if($forBox == 'kp-tabbox-invoices') {
                $('.order-navbar-btn').text('+ Добавить счет');
                $('.dash-panel-dropdown-drop').hide();
            }

        }

        //табы
        if(window.location.pathname.indexOf('admin/orders') != -1) {
            var $forWrap = $this.attr('data-tab');
             var $wrap = $('#' + $forWrap);
             $wrap.find('[data-tab-box]').hide();
             $wrap.find('[data-tab-box].' + $forBox).show();

             $('[data-tab="' + $forWrap + '"]').removeClass('current');
             $this.addClass('current');

             e.preventDefault();
        } else if(window.location.pathname.indexOf('admin/users') != -1) {
            var $forWrap = $this.attr('data-tab');
            var $wrap = $($forWrap);
            $wrap.find('[data-tab-box]').hide();
            $wrap.find('[data-tab-box].' + $forBox).show();

            $wrap.removeClass(function (index, className) {
                return (className.match(/(^|\s)tbs-\S+/g) || []).join(' ');
            });
            $wrap.addClass('tbs-' + $forBox.replace(/\./g, ''));

            $('[data-tab="' + $forWrap + '"]').removeClass('current');
            $this.addClass('current');
            e.preventDefault();
        }
    });


    //Dash kp btn tabs
    $document.on('click', '[data-kp-step="btn"]', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $wrap = $this.parents('[data-kp-step="wrap"]');
        $wrap.find('[data-kp-step="btn"]').removeClass('current');
        $this.addClass('current');
    });


    /* END */
});