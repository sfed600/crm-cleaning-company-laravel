@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Код подтверждения</div>
                    <div class="panel-body">

                        @include('admin.message')

                        <form class="form-horizontal" role="form" method="POST" action="/smsVerify">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_name" value="{{$request->name}}">
                            <input type="hidden" name="inn" value="{{$request->inn}}">
                            <input type="hidden" name="company_name" value="{{$request->company_name}}">
                            <input type="hidden" name="phone" value="{{$request->phone}}">
                            <input type="hidden" name="email" value="{{$request->email}}">
                            <input type="hidden" name="password" value="{{$request->password}}">

                            <div class="form-group{{ $errors->has('inn') ? ' has-error' : '' }}">
                                <label for="sms_code" class="col-md-4 control-label">Смс код </label>

                                <div class="col-md-6">
                                    <input id="sms_code" type="text" class="form-control" name="sms_code" value="{{ old('sms_code') }}" required>

                                    @if ($errors->has('sms_code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('sms_code') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn"></i> Подтвердить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
