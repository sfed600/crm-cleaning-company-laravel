@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Форма регистрации</div>
                    <div class="panel-body">

                        @include('admin.message')

                        <form class="form-horizontal" role="form" method="POST" action="/inn">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('inn') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">ИНН</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="inn" value="{{ old('inn', $request->inn) }}" required>

                                    @if ($errors->has('inn'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('inn') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn"></i> Дальше
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
