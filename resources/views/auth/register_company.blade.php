@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Форма регистрации</div>
                <div class="panel-body">

                    @include('admin.message')

                    <form class="form-horizontal" role="form" method="POST" action="/register_company">
                        {{ csrf_field() }}
                        <input type="hidden" name="company_name" value="{{@$request->company_name}}">

                        <div class="form-group{{ $errors->has('inn') ? ' has-error' : '' }}">
                            <label for="inn" class="col-md-4 control-label">ИНН</label>

                            <div class="col-md-6">
                                <input readonly id="inn" type="text" class="form-control" name="inn" value="{{ old('inn', $request->inn) }}" required>

                                @if ($errors->has('inn'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('inn') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(isset($request->company_name))
                            <div class="form-group{{ $errors->has('inn') ? ' has-error' : '' }}">
                                <label for="company_name" class="col-md-4 control-label">Компания</label>

                                <div class="col-md-6">
                                    <input readonly id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name', $request->company_name) }}" required>
                                </div>
                            </div>
                        @endif


                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">ФИО</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Тел.</label>

                            <div class="col-md-6">
                                <input class="phone-mask form-control" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phone" value="{{ old('phone', $request->phone) }}">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Пароль</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Повторите пароль</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-next"></i> Дальше
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{--<script>
    $(document).ready(function($) {
        $('.input-mask').each(function () {
            $(this).mask($(this).data('mask'));
        });
    });
</script>--}}
@endsection
