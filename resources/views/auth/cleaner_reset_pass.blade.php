@extends('cleaner.layout')

@section('main')
    <form method="POST" action="{{ url('/cleaner/login') }}">
        {{ csrf_field() }}
        <section class="infobar inner reg">
            <div class="w-container">
                <div class="info_block">
                    <div class="w-row">
                        <div class="w-col w-col-1"></div>
                        <div class="w-col w-col-10">
                            <h1 class="current_head">Восстановить пароль</h1>
                        </div>
                        <div class="w-col w-col-1"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main reg">
            <div class="w-container">
                <div class="formblock reg">
                    <div class="reg_info">Введите номер своего телефона, который Вы сообщили менеджеру</div>
                    <div class="reg_row w-row">
                        <div class="w-clearfix w-col w-col-6 w-col-small-12">
                            <input class="input right field w-input mask" data-name="Field 13" id="field-13"  name="login" type="tel" value="{{@Session::get('phone')}}">
                        </div>
                        <div class="w-clearfix w-col w-col-6 w-col-small-12">
                            <a class="btn_gray download getout_btn left more w-button" href="#" onclick="getPassFromSms();">Получить пароль по смс</a>
                            {{--<input type="submit" class="btn_gray download getout_btn left more w-button" value="Получить пароль по смс" />--}}
                        </div>
                    </div>

                    <div class="reg_row w-row step-2" @if(!empty(Session::get('err'))) style="display: block;" @endif>
                        <div class="reg_info">Пароль придет в течение 10 минут, после чего введите его в это поле:</div>
                        <div class="w-clearfix w-col w-col-6 w-col-small-12">
                            <div class="input right">
                                <div class="placeholder">Пароль <span class="asterix">*</span>
                                </div>
                                <input class="field w-input" data-name="Field 14" id="field-14" maxlength="256" name="password" type="tel">
                            </div>
                        </div>
                        <div class="w-clearfix w-col w-col-6 w-col-small-12">
                            <a class="btn_gray left submit w-button" href="javascript:void(0);" onclick="document.forms[0].submit();">Войти</a>
                        </div>
                    </div>

                    <div class="reg_row w-row step-3" @if(!empty(Session::get('err'))) style="display: block;" @endif>
                        <div class="w-col w-col-6 w-col-small-12">
                            <div class="inf">
                                {{--<span class="pink">Введен неверный пароль.</span>--}}
                                {{--@if($errors->any())
                                    <span class="pink">{{$errors->first()}}</span>
                                @endif--}}
                                @if(!empty(Session::get('err'))) <span class="pink">{{Session::get('err')}}</span> @endif
                                <br>Нажмите «выслать пароль повторно»,
                                <br>чтобы получить Ваш пароль</div>
                        </div>
                        <div class="w-clearfix w-col w-col-6 w-col-small-12">
                            <a id="sendRepeat" class="download getout_btn left more w-button" href="#" onclick="getPassFromSms();">Выслать пароль повторно</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>

@stop