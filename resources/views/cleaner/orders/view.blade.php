@extends('cleaner.layout')

@section('main')
    <div class="orderitem">
        <section class="infobar inner">
            <div class="w-container">
                <div class="info_block">
                    <div class="w-row">
                        <div class="w-col w-col-1"></div>
                        <div class="w-col w-col-10">
                            <h1 class="current_head">Заказ</h1>
                            <div class="order_number">Заказ №1756</div>
                        </div>
                        <div class="w-col w-col-1"></div>
                    </div>
                </div>
            </div>
            <a class="back w-inline-block" href="orders.php"></a>
        </section>
        <div class="getout_item first">
            <div class="w-container">
                <div class="first getout_row">
                    <div class="getout_name" style="display:none;">
                        <div class="info_bind">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                    </div>
                    <div class="order_location">
                        <div class="geetaout_cell time">
                            <div class="order_time">02.08.2016, 09:00</div>
                        </div>
                        <div class="address geetaout_cell">
                            <div class="getout_address">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="getout_item meet">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Встреча</div>
                    </div>
                    <div class="geetaout_cell time">
                        <div class="order_times">
                            <div class="bg getout_date">02.08.2016</div>
                            <div class="bg getout_time">08:00</div>

                        </div>
                        <a id="imhier" class="getout_btn w-button" href="#">Я на месте</a>
                    </div>
                    <div class="address geetaout_cell">
                        <div class="getout_address order">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                        <div class="order_cta">Нажмите на эту кнопку, когда встретитесь с
                            водителем в обозначенном месте</div>
                    </div>
                    <div class="geetaout_cell"><a class="getout_link order" href="https://yandex.ru/maps/" target="_blank">На карте</a>
                    </div>
                </div>
            </div>
            <div class="order_badge">
                <div>Сегодня</div>
            </div>
        </div>
        <div class="getout_item transport">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Машина</div>
                    </div>
                    <div class="geetaout_cell">
                        <div class="auto">Volkswagen Transporter &nbsp;<span><strong>Р 156 СУ 199</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item transport driver">
            <div class="w-container">
                <div class="client getout_row">
                    <div class="getout_name">
                        <div>Водитель</div>
                    </div>
                    <div class="address geetaout_cell">
                        <div class="getout_client">Русакова Оксана Геннадьевна</div>
                    </div>
                    <div class="geetaout_cell">
                        <a class="call_client w-inline-block" href="tel:+74956422604"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="order_bot">
            <div class="w-container">
                <p class="bot_text">Ритм современной жизни требует от человека большой активности. Однако, не смотря на то, какой насыщенной является сегодня жизнь людей всего мира, в обычных сутках до сих пор остается всего лишь 24 часа. В этот, на самом деле маленький отрезок времени, необходимо уложить и деловые встречи, и часы активной работы, и передвижение на транспорте и прочие многочисленные рабочие задачи. А еще нужно выделить время лноценный отдых. Таким образом, для решения бытовых вопросов, таких, как уборка домов. современного человека практически не остается времени.</p>
                <div class="badge">Если вдруг вы не сможете приехать на &nbsp;заказ - незамедлительно сообщите об &nbsp;этом &nbsp;по телефону вашему менеджеру!</div>
            </div>
        </div>


    </div>
@stop  