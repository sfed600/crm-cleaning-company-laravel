@extends('cleaner.layout')

@section('main')
    <section class="infobar inner orders">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-1"></div>
                    <div class="w-col w-col-10">
                        <h1 class="current_head">Заказы</h1>
                    </div>
                    <div class="w-col w-col-1"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="main orders">
        <div class="order_section new">
            <div class="order_badge">
                <div>Завтра</div>
            </div>
            <div class="w-container">
                <div class="new order_item">
                    <div class="order_descript">
                        <div class="info_bind" style="display:none">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                        <div class="order_content">
                            <h3 class="curr_head">Приглашение на работу: Заказ № 1756</h3>
                            <p class="order_p">Краткий комментарий &nbsp;к заказ или к приглашению на работу комментарий заказ или к приглашению на работу</p>
                        </div>
                    </div>
                    <div class="order_meet">
                        <h3 class="curr_meet">Встреча</h3>
                        <div class="curr_time">02.08.2016, 09:00</div>
                        <div class="orders_place">
                            <div class="curr_ob">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                            <a class="curr_map" href="https://yandex.ru/maps/" target="_blank">На карте</a>
                        </div>


                        <a class="left order submit w-button getOrder" href="#">Принять заказ</a>
                    </div>
                    <div class="order_text">Нажимая «Принять заказ», вы соглашаетесь с выполнением принимаемой работы и обязаны быть в указанное время на месте встречи.</div>
                </div>
            </div>
        </div>
        <div class="order_section current">
            <div class="order_badge">
                <div>Сегодня</div>
            </div>
            <div class="w-container">
                <div class="order_item">
                    <div class="order_descript">
                        <div class="info_bind" style="display:none">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                        <div class="order_content">
                            <h3 class="curr_head">Текущая работа: Заказ № 1756</h3>
                            <p class="order_p">Краткий комментарий  к заказ или к приглашению на работу </p>
                        </div>
                    </div>
                    <div class="order_meet">
                        <h3 class="curr_meet">Встреча</h3>
                        <div class="curr_time">02.08.2016, 09:00</div>
                        <div class="orders_place">
                            <div class="curr_ob">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                            <a class="curr_map" href="https://yandex.ru/maps/" target="_blank">На карте</a>
                            <a class="download getout_btn more w-button" href="order.php">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>


    </section>
    <div class="info_mess">
        <div>Вы закреплены на заказе <span class="pink">номер 1234</span>,
            <br>вас будут ожидать в назначенное
            <br>время в назначенном месте</div>
    </div>
    <div class="overlay"></div>
@stop