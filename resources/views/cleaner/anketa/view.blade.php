@extends('cleaner.layout')

@section('main')
    <section class="infobar anketa-full">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_bind" style="display:none">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 w-col-tiny-10">
                        <h1>Личный кабинет</h1>
                        <div class="info_text">Вы авторизованы в своем личном кабинее</div>
                        <div class="info_tell">{{$user->phones()->first()->phone}}</div>
                        <div class="info_user"><span class="info_unite">Работник:</span> <span class="info_name">{{$user->surname.' '.$user->name}}</span>
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_rait" style="display:none">
                            <div class="rait_text">Рейтинг</div>
                            <div class="info_digits">95.7</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main anketa-full">
        <div class="w-container">
            <div class="formblock">

                <div class="data_wrap">
                    <div class="data_ava"><img class="ava" src="/{{$user->getImgPreviewPath().$user->img}}">
                    </div>
                    <div class="mob_statuser">
                        <div class="info_rait">
                            <div class="rait_text">Рейтинг</div>
                            <div class="info_digits">95.7</div>
                        </div>
                        <div class="info_bind">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                    </div>
                    <div class="data_main w-clearfix">
                        <div class="data_item">
                            <div class="data_unite">Е-mail:</div>
                            <div class="data_data">{{$user->email}}</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">Пол:</div>
                            <div class="data_data">@if($user->gender == 'male') мужской @elseif($user->gender == 'female') женский @endif</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">День рождения:</div>
                            <div class="data_data">{{date('d.m.Y', strtotime($user->bithdate))}}</div>
                        </div>
                        <div class="data_item norow">
                            <div class="data_unite">Адрес прописки:</div>
                            <div class="data_data">{{$user->address}}</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">Гражданство:</div>
                            <div class="data_data">{{$user->citizenship}}</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">Паспортные данные:</div>
                            <div class="data_data">{{@$user->pasport->series.' '.@$user->pasport->number}}</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">Выдан:</div>
                            <div class="data_data">{{@$user->pasport->issued_by}}</div>
                        </div>
                        <div class="data_item">
                            <div class="data_unite">Дата выдачи:</div>
                            <div class="data_data">{{@$user->pasport->date_issue}}</div>
                        </div>
                        <div class="data_pay">
                            <div class="data_item">
                                <div class="data_unite">Карты для зарплаты:</div>
                                {{--<img class="data_data" src="images/sber.png">--}}
                            </div>
                            @forelse($user->cards as $card)
                                @if($card->bank == 'sberbank')
                                    <img class="data_data" src="/assets/cleaner/images/sber.png"><br>
                                @else
                                    <img class="data_data" src="/assets/cleaner/images/tinkoff.png"><br>
                                @endif
                                <div class="data_item">
                                    <div class="data_unite">Номер карты:</div>
                                    @if($card->bank == 'sberbank')
                                        <div class="data_data">{{$sber_field1.' '.$sber_field2.' '.$sber_field3.' '.$sber_field4}}</div>
                                    @else
                                        <div class="data_data">{{$tinkoff_field1.' '.$tinkoff_field2.' '.$tinkoff_field3.' '.$tinkoff_field4}}</div>
                                    @endif
                                </div>
                            @empty
                            @endforelse
                        </div><a class="left submit w-button" href="{{route('cleaner.anketa.edit')}}">Редактировать</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop