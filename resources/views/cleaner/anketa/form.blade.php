@extends('cleaner.layout')

@section('main')
    <section class="infobar anketa">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_bind" style="display:none;">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 w-col-tiny-10">
                        <h1>Личный кабинет</h1>
                        <div class="info_text">Вы авторизованы в своем личном кабинее</div>
                        <div class="info_tell">{{$user->phones()->first()->phone}}</div>
                        <div class="info_user"><span class="info_unite">Работник:</span> <span class="info_name">{{$user->surname.' '.$user->name}}</span>
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_rait" style="display:none;">
                            <div class="rait_text">Рейтинг</div>
                            <div class="info_digits">95.7</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main">
        <div class="w-container">
            <div class="formblock">
                <h2 class="form_head">Заполните Вашу личную анкету</h2>
                <div class="formblock_formwr form">
                    {{--<form id="anketa-form" action="" name="email-form" enctype="multipart/form-data">--}}
                    <form id="anketa-form" action="{{route('cleaner.anketa.edit')}}" method="POST" enctype="multipart/form-data">
                        {{--<input type="hidden" name="_method" value="PUT">--}}
                        <input name="_token" type="hidden" value="{{csrf_token()}}">

                        <div class="w-row">
                            <div class="col_addphoto w-col w-col-4 w-col-medium-6">
                                <button type="button" class="addphoto w-inline-block" href="#"></button>
                                <input type="file" name="img" value="" id="file">
                                <div class="addphoto_text">Добавить фото</div>
                            </div>
                            <div class="w-col w-col-8 w-col-medium-6">
                                <div class="w-row">
                                    <div class="w-col w-col-6 w-col-medium-12">
                                        <div class="form_info">Обязательные поля отмечены <span class="asterix">*</span>
                                        </div>
                                        <div class="input">
                                            <div class="placeholder" @if(isset($user->patronicname)) style="opacity: 0;" @endif >Отчество</div>
                                            <input class="field w-input" id="field" maxlength="256" name="patronicname" type="text" value="{{ old('patronicname', $user->patronicname) }}">
                                        </div>
                                    </div>
                                    <div class="w-clearfix w-col w-col-6 w-col-medium-12">
                                        <div class="input mt right">
                                            <div class="placeholder" @if(isset($user->patronicname)) style="opacity: 0;" @endif>E-mail</div>
                                            <input class="field w-input" data-name="Field 2" id="email" maxlength="256" name="email" type="email" value="{{ old('email', $user->email) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="w-row">
                                    <div class="w-col w-col-6 w-col-medium-12">
                                        <div class="radios sex">
                                            <div class="radio_prelabel">Пол<span class="asterix">*</span> :</div>
                                            <div class="radio @if($user->gender == 'male') active @endif"></div>
                                            <div class="radio_label">мужской</div>
                                            <div class="radio @if($user->gender == 'female') active @endif"></div>
                                            <div class="radio_label">женский</div>
                                            <input id="sex" type="hidden" name="gender" value="{{$user->gender}}">
                                        </div>
                                    </div>
                                    <div class="w-col w-col-6 w-col-medium-12"></div>
                                </div>
                                <div class="w-row">
                                    <div class="w-col w-col-6 w-col-medium-12">
                                        <div class="input placeholderhover">
                                            <div class="placeholder" @if(isset($user->bithdate)) style="opacity: 0;" @endif>День рожденья <span class="asterix">*</span>
                                            </div>
                                            <input class="field w-input" data-name="Field 3" id="birthday" maxlength="256" name="bithdate" type="tel" data-inputmask-inputformat="dd.mm.yyyy" required value="{{ old('bithdate', date('d.m.Y', strtotime($user->bithdate))) }}">
                                        </div>
                                    </div>
                                    <div class="w-col w-col-6 w-col-medium-12">
                                        <div class="w-dropdown" data-delay="0">
                                            <div class="select w-dropdown-toggle">
                                                <div>{{$user->citizenship or 'Гражданство'}}</div>
                                            </div>
                                            <nav class="w-dropdown-list">
                                                @foreach(\App\User::$citizenships as $citizenship)
                                                    <a class="select_option w-dropdown-link" href="#">{{$citizenship}}</a>
                                                @endforeach
                                                <input type="hidden" name="citizenship" value="{{$user->citizenship}}">
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row_passport">
                            <div class="form_info left">Паспортные данные:<span class="asterix"></span>
                            </div>
                            <div class="passport_data">
                                <div class="input serial">
                                    <div class="placeholder" @if(isset($user->pasport->series)) style="opacity: 0;" @endif>Серия<span class="asterix">*</span></div>
                                    <input class="field w-input" data-name="Field 4" id="field-4" maxlength="32" name="pasport-series" type="tel" value="{{old('pasport-series', @$user->pasport->series)}}">
                                </div>
                                <div class="input number">
                                    <div class="placeholder" @if(isset($user->pasport->number)) style="opacity: 0;" @endif>Номер<span class="asterix">*</span></div>
                                    <input class="field w-input" data-name="Field 5" id="field-5" maxlength="32" name="pasport_number" type="tel" value="{{old('pasport-number', @$user->pasport->number)}}">
                                </div>
                                <div class="input placeholderhover mr dategetpass">
                                    <div class="placeholder" @if(isset($user->pasport->date_issue)) style="opacity: 0;" @endif>Дата выдачи<span class="asterix">*</span></div>
                                    <input class="field w-input" data-name="Field 7" id="field-date-of-issue" maxlength="10" name="pasport_date" type="tel" value="{{old('pasport_date', date('d.m.Y', strtotime($user->pasport->date_issue)))}}">
                                </div>
                                <div class="input mr whopass">
                                    <div class="placeholder" @if(isset($user->pasport->issued_by)) style="opacity: 0;" @endif>Кем выдан<span class="asterix">*</span></div>
                                    <input class="field w-input" data-name="Field 6" id="field-6" maxlength="256" name="pasport_issued" type="text" value="{{old('pasport-issued_by', @$user->pasport->issued_by)}}">
                                </div>

                            </div>
                        </div>
                        <div class="form_address">
                            <div class="full input">
                                <div class="placeholder" @if(isset($user->address)) style="opacity: 0;" @endif>Адрес прописки <span class="asterix">*</span>
                                </div>
                                <input class="field w-input" data-name="Field 8" id="field-8" maxlength="256" name="address" type="text" value="{{ old('address', $user->address) }}">
                            </div>
                        </div>
                        <div class="row_bank">
                            <div class="bank form_info left">Карта для зарплаты:<span class="asterix"></span>
                            </div>
                            <div class="bank_wrap">
                                <div class="bank radios">
                                    <div class="active first radio" onclick="toogle_card_fields('sber');"></div>
                                    <img alt="Сберабанк" class="radio_sticker" src="/assets/cleaner/images/sber.png">
                                    <div class="radio" onclick="toogle_card_fields('tinkoff');"></div>
                                    <img class="radio_sticker" alt="Тинькофф" src="/assets/cleaner/images/tinkoff.png">
                                    <input type="hidden" value="Сбербанк">
                                </div>
                                <div class="bank_card" id="sber_fields">
                                    <div class="card input">
                                        <input class="field w-input" data-name="Sber field 1" id="sber-field-1" maxlength="4" name="sber-field-1" type="tel" value="{{old('sber-field-1', $sber_field1)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Sber field 2" id="sber-field-2" maxlength="4" name="sber-field-2" type="tel" value="{{old('sber-field-2', $sber_field2)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Sber field 3" id="sber-field-3" maxlength="4" name="sber-field-3" type="tel" value="{{old('sber-field-3', $sber_field3)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Sber field 4" id="sber-field-4" maxlength="6" name="sber-field-4" type="tel" value="{{old('sber-field-4', $sber_field4)}}">
                                    </div>
                                </div>
                                {{--<div class="bank_card" id="tinkoff_fields" style="display: none;">
                                    <div class="card input">
                                        <input class="field w-input" data-name="Field 9" id="field-9" maxlength="4" name="field-9" type="tel">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Field 10" id="field-10" maxlength="4" name="field-10" type="tel">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Field 11" id="field-11" maxlength="4" name="field-11" type="tel">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Field 12" id="field-12" maxlength="6" name="field-12" type="tel">
                                    </div>
                                </div>--}}
                                <div class="bank_card" id="tinkoff_fields" style="display: none;">
                                    <div class="card input">
                                        <input class="field w-input" data-name="Tinkoff field 1" id="tinkoff-field-1" maxlength="4" name="tinkoff-field-1" type="tel" value="{{old('tinkoff-field-1', $tinkoff_field1)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Tinkoff field 2" id="tinkoff-field-2" maxlength="4" name="tinkoff-field-2" type="tel" value="{{old('tinkoff-field-2', $tinkoff_field2)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Tinkoff field 3" id="tinkoff-field-3" maxlength="4" name="tinkoff-field-3" type="tel" value="{{old('tinkoff-field-3', $tinkoff_field3)}}">
                                    </div>
                                    <div class="card input">
                                        <input class="field w-input" data-name="Tinkoff field 4" id="tinkoff-field-4" maxlength="6" name="tinkoff-field-4" type="tel" value="{{old('tinkoff-field-4', $tinkoff_field4)}}">
                                    </div>
                                </div>
                                <div class="bank form_info left">Вы можете не заполнять банковскую карту,
                                    но мы не сможем перечислять зарплату.</div>
                            </div>
                        </div>

                        <input class="submit w-button" type="submit" value="Сохранить">
                    </form>

                </div>
            </div>
        </div>
    </section>

    <script>
        function toogle_card_fields(bank) {
            var sberFields = document.getElementById("sber_fields");
            var tinkoffFields = document.getElementById("tinkoff_fields");

            if(bank == 'sber') {
                sberFields.style.display = 'flex';
                tinkoffFields.style.display = 'none';
            } else if(bank == 'tinkoff') {
                tinkoffFields.style.display = 'flex';
                sberFields.style.display = 'none';
            }
        }
    </script>
@stop