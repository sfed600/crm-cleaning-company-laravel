@extends('cleaner.layout')

@section('main')
    <section class="infobar main">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-1"></div>
                    <div class="w-col w-col-10">
                        <h1 class="current_head">Взаиморасчеты</h1>
                        <div class="current_head_sub">Ниже находятся заказы, по которым от вас деньги еще не были получены</div>
                    </div>
                    <div class="w-col w-col-1"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="main rigs">
        <div class="reckoning-item">
            <div class="reckoning-item-title">Заказ № 1756</div>
            <div class="reckoning-item-label">02.08.2018</div>
            <div class="reckoning-item-subtitle">Дата</div>
            <div class="reckoning-item-date">23.10.2018, 10:00</div>

            <div class="reckoning-item-subtitle">Сумма</div>
            <div class="reckoning-item-prop">Наличные: 5100 руб.</div>
        </div>

        <div class="reckoning-item">
            <div class="reckoning-item-title">Заказ № 1788</div>
            <div class="reckoning-item-label">Вчера</div>
            <div class="reckoning-item-subtitle">Дата</div>
            <div class="reckoning-item-date">23.10.2018, 10:00</div>

            <div class="reckoning-item-subtitle">Сумма</div>
            <div class="reckoning-item-prop">Наличные: 5100 руб.</div>
        </div>

    </section>
@stop