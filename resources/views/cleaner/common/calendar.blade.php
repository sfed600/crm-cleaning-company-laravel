@extends('cleaner.layout')

@section('main')
    <section class="infobar main">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-12">
                        <h1 class="current_head">Мой календарь</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main rigs">

        <div class="w-container">
            <div class="filter">
                <div class="w-dropdown" data-delay="100" data-hover="1">
                    <div class="filter_year w-dropdown-toggle">
                        <div>2016</div>
                    </div>
                    <nav class="dropdown_list w-dropdown-list"><a class="item_year odd w-dropdown-link" href="#">2015</a><a class="item_year w-dropdown-link" href="#">2014</a><a class="item_year odd w-dropdown-link" href="#">2013</a>
                        <input type="hidden" value="">
                    </nav>
                </div>
                <div class="w-dropdown" data-delay="100" data-hover="1">
                    <div class="filter_month w-dropdown-toggle">
                        <div>Январь</div>
                    </div>
                    <nav class="dropdown_list w-dropdown-list">
                        <a class="item_month left odd w-dropdown-link" href="#">Январь</a>
                        <a class="item_month odd w-dropdown-link" href="#">Июль</a>
                        <a class="item_month left w-dropdown-link" href="#">Февраль</a>
                        <a class="item_month w-dropdown-link" href="#">Август</a>
                        <a class="item_month left odd w-dropdown-link" href="#">Март</a>
                        <a class="item_month odd w-dropdown-link" href="#">Сентябрь</a>
                        <a class="item_month left w-dropdown-link" href="#">Апрель</a>
                        <a class="item_month w-dropdown-link" href="#">Октябрь</a>
                        <a class="item_month left odd w-dropdown-link" href="#">Май</a>
                        <a class="item_month odd w-dropdown-link" href="#">Ноябрь</a>
                        <a class="item_month left w-dropdown-link" href="#">Июнь</a>
                        <a class="item_month w-dropdown-link" href="#">Декабрь</a>
                        <input type="hidden" value="">
                    </nav>
                </div>

                <div class="w-dropdown type_company" data-delay="100" data-hover="1">
                    <div class="filter_company w-dropdown-toggle">
                        <div>Все компании в которых работаю</div>
                    </div>
                    <nav class="dropdown_list w-dropdown-list">
                        <a class="item_year odd w-dropdown-link" href="#">ООО "Сфера Чистоты"</a>
                        <a class="item_year w-dropdown-link" href="#">ООО "Чистота и порядок"</a>
                        <a class="item_year odd w-dropdown-link" href="#">ООО "Белый ветер"</a>
                        <input type="hidden" value="">
                    </nav>
                </div>

            </div>


            <div class="my-calendar-container">
                <div class="my-calendar-table">

                    <script>
                        var $daysData = {
                            '2018-06-01' : {'status' : 'aviable', 'html' : '<div class="my-calendar-items"><div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты 000»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div></div>'},
                            '2018-06-12' : {'status' : 'aviable', 'html' : '<div class="my-calendar-items"><div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты2»</div><div class="my-calendar-item-order">Заказ №2788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты 111»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div></div>'},
                            '2018-06-15' : {'status' : 'aviable', 'html' : '<div class="my-calendar-items"><div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты3»</div><div class="my-calendar-item-order">Заказ №3788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты 222»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div></div>'},
                            '2018-06-20' : {'status' : 'aviable', 'html' : '<div class="my-calendar-items"><div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты4»</div><div class="my-calendar-item-order">Заказ №4788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты 333»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div></div>'},
                            '2018-07-01' : {'status' : 'aviable', 'html' : '<div class="my-calendar-items"><div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты5»</div><div class="my-calendar-item-order">Заказ №5788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div>  <div class="my-calendar-item"><div class="my-calendar-item-title">ООО «Сфера Чистоты 444»</div><div class="my-calendar-item-order">Заказ №1788</div><div class="my-calendar-item-date">09:00 - 16:00</div><div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div><div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div></div></div>'}
                        };
                    </script>
                    <div id="my-calendar-datepicker"></div>
                </div>

                <div class="my-calendar-content" id="my-calendar-content">
                    <div class="my-calendar-items">
                        <div class="my-calendar-item">
                            <div class="my-calendar-item-title">ООО «Сфера Чистоты»</div>
                            <div class="my-calendar-item-order">Заказ №1788</div>
                            <div class="my-calendar-item-date">09:00 - 16:00</div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div>
                        </div>
                        <div class="my-calendar-item">
                            <div class="my-calendar-item-title">ООО «Сфера Чистоты»</div>
                            <div class="my-calendar-item-order">Заказ №1788</div>
                            <div class="my-calendar-item-date">09:00 - 16:00</div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div>
                            <div class="my-calendar-item-prop type-nosell"><span class="txt-label">Выплачено:</span><span class="txt-value">Не выплачено</span></div>
                            <div class="my-calendar-item-prop type-nosell"><span class="txt-label">Дата выплаты:</span><span class="txt-value">Не выплачено</span></div>
                        </div>
                        <div class="my-calendar-item">
                            <div class="my-calendar-item-title">ООО «Сфера Чистоты»</div>
                            <div class="my-calendar-item-order">Заказ №1788</div>
                            <div class="my-calendar-item-date">09:00 - 16:00</div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Начислено:</span><span class="txt-value">2000 руб.</span></div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Выплачено:</span><span class="txt-value">2000 руб.</span></div>
                            <div class="my-calendar-item-prop"><span class="txt-label">Дата выплаты:</span><span class="txt-value">20.05.2018</span></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>


    </section>
@stop