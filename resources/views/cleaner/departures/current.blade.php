@extends('cleaner.layout')

@section('main')
    <section class="infobar inner rig">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-1"></div>
                    <div class="w-col w-col-10">
                        <h1 class="current_head">Текущий выезд</h1>
                        <div class="order_number">Заказ №1756</div>
                    </div>
                    <div class="w-col w-col-1"></div>
                </div>
            </div>
        </div>
        <a class="back w-inline-block" href="http://trixte.bget.ru/clean2/index.php"></a>
    </section>
    <section class="main rig">
        <div class="getout_item first">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Машина</div>
                    </div>
                    <div class="geetaout_cell model_cell">
                        <div class="model">Volkswagen Transporter &nbsp;<span><strong>Р 156 СУ 199</strong></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="getout_item">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Список работ</div>
                    </div>
                    <div class="geetaout_cell model_cell">

                        <ul class="listing_li type-bold">
                            <li>Тип: "Производственное помещение"</li>
                            <li>Площадь: "900 м2"</li>
                            <li>Тип уборки: "Послестроительная"</li>
                            <li>Степень загрязнения: "4 степень"</li>
                            <li><a href="#box-work-full-list" data-open-box>Полный перечень</a></li>
                        </ul>

                        <div class="box-layer-open" id="box-work-full-list">
                            <ul class="listing_li type-bold">
                                <li>Тип: "Производственное помещение"</li>
                                <li>Площадь: "900 м2"</li>
                                <li>Тип уборки: "Послестроительная"</li>
                                <li>Мебель: "Без мебели"</li>
                                <li>Высота потолков: "> 3 метров"</li>
                                <li>Мойка окон: "створок: 8, 40 м2, панорамное остекление 10м2"</li>
                                <li>Жалюзи: "створок: 4, 20 м2";</li>
                                <li>Химчистка мебели: "Диван 2 места: 2 шт. , диван 3 места: 1 шт."</li>
                                <li>Химчистка ковров: "Ковры: 5 ед. , ковровое покрытие: 50 м2, длинный ворс, степень загрязнения: 3"</li>
                                <li>Другие услуги: "Мойка холодильника: 1 шт. , Мойка микроволновки: 1 шт. , Дополнительный человек: 1 ед. , Доставка тары большой: 2 шт. , Стоянка: 2 ед."</li>
                                <li>Дополнительные работы: "Полировка фаянса и мрамора: 2 ед. , Глажка постельного белья: 10 ед. , Вынос мусора: 2 ед."</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div class="getout_item">
            <div class="w-container">
                <div class="getout_row rewers ul">
                    <div class="getout_name">
                        <div>Встреча</div>
                    </div>
                    <div class="geetaout_cell time">
                        <div class="when">
                            <div class="getout_date">02.08.2016</div>
                            <div class="getout_time">08:00</div>
                        </div>
                        <a class="getout_btn w-button" href="#">На месте</a>
                    </div>
                    <div class="address geetaout_cell">
                        <div class="getout_address">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                    </div>
                    <div class="geetaout_cell map"><a class="getout_link" href="https://yandex.ru/maps/" target="_blank">На карте</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item team">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_namewr">
                        <div class="getout_name">
                            <div>Команда</div>
                        </div>
                    </div>
                    <div class="getout_teamwr">
                        <div class="getout_team">
                            <div class="team_person">
                                <div><span class="ord">1. </span>Абдулов Хафиз Мурадович</div>
                            </div>
                            <div class="team_btns"><a class="team_status w-button" href="#">На месте</a><a class="nobody team_status w-button" href="#">Неявка</a>
                            </div>
                            <a class="team_call w-inline-block" href="tel:+74956422604"></a>
                        </div>
                        <div class="getout_team">
                            <div class="team_person">
                                <div><span class="ord">2. </span>+7 926 414-88-39</div>
                            </div>
                            <div class="team_btns"><a class=" team_status w-button" href="#">На месте</a><a class="nobody team_status w-button" href="#">Неявка</a>
                            </div>
                            <a class="team_call w-inline-block" href="tel:+74956422604"></a>
                        </div>
                        <div class="getout_team">
                            <div class="team_person">
                                <div><span class="ord">3. </span>Гулине Ариана Фахидовна</div>
                            </div>
                            <div class="team_btns"><a class="team_status w-button" href="#">На месте</a><a class=" nobody team_status w-button" href="#">Неявка</a>
                            </div>
                            <a class="team_call w-inline-block" href="tel:+74956422604"></a>
                        </div>
                        <div class="getout_team last">
                            <div class="team_person">
                                <div><span class="ord">4. </span>Абдулов Хафиз Мурадович</div>
                            </div>
                            <div class="team_btns"><a class="team_status w-button" href="#">На месте</a><a class="nobody team_status w-button" href="#">Неявка</a>
                            </div>
                            <a class="team_call w-inline-block" href="tel:+74956422604"></a>
                        </div><a class="left submit w-button" href="#">Готово</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item order--who">
            <div class="w-container">
                <div class="getout_row ul">
                    <div class="getout_name">
                        <div>Заказ №1756</div>
                    </div>
                    <div class="geetaout_cell time">
                        <div class="getout_time">09:00</div>
                    </div>
                    <div class="address geetaout_cell">
                        <div class="getout_address">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                    </div>
                    <div class="geetaout_cell map"><a class="getout_link" href="https://yandex.ru/maps/" target="_blank">На карте</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item client">
            <div class="w-container">
                <div class="client getout_row">
                    <div class="getout_name">
                        <div>Клиент</div>
                    </div>
                    <div class="address geetaout_cell">
                        <div class="getout_client">Русакова Оксана Геннадьевна</div>
                    </div>
                    <div class="geetaout_cell">
                        <a class="call_client w-inline-block" href="tel:+74956422604"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item comment">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Комментарий</div>
                    </div>
                    <div class="geetaout_cell time">
                        <p>Ритм современной жизни требует от человека большой активности. Однако, не смотря на то, какой насыщенной является сегодня жизнь людей всего мира, в обычных сутках до сих пор остается всего лишь 24 часа. В этот, на самом деле маленький отрезок времени, необходимо уложить и деловые встречи, и часы активной работы, и передвижение на транспорте и прочие многочисленные рабочие задачи. А еще нужно выделить время на полноценный отдых. Таким образом, для решения бытовых вопросов, таких, как уборка домов. современного человека практически не остается времени.</p><a download class="download getout_btn w-button" href="file.xsls">Скачать наряд</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="getout_item gold">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_name">
                        <div>Деньги</div>
                    </div>
                    <div class="geetaout_cell time">
                        <div class="total_text">Получить от клиента &nbsp;<span class="total">7800&nbsp;руб.</span> по факту выполнения работ</div><a id="done" class="getout_done submit w-button" href="#">Завершить рабочий день</a>
                    </div>
                </div>
            </div>
        </div>



        <div class="getout_item team_rating display-none">
            <div class="w-container">
                <div class="getout_row">
                    <div class="getout_namewr">
                        <div class="getout_name">
                            <div>Оцените качество работы сотрудников</div>
                        </div>
                    </div>
                    <div class="getout_teamwr">
                        <div class="team_rating-item">
                            <div class="team_rating-item-name">
                                <span class="ord">1. </span>Абдулов Хафиз Мурадович
                            </div>
                            <div class="team_rating-item-right">
                                <div class="team_rating-item-btns-wrap">
                                    <div class="team_rating-item-btns">
                                        <a class="team_rating_button w-button" href="#"><span>1</span> балл</a>
                                        <a class="team_rating_button w-button" href="#"><span>2</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>3</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>4</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>5</span> баллов</a>
                                        <a class="team_rating_button w-button" href="#"><span>Без</span> оценки</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="team_rating-item">
                            <div class="team_rating-item-name">
                                <span class="ord">2. </span>Гулине Ариана Фахидовна
                            </div>
                            <div class="team_rating-item-right">
                                <div class="team_rating-item-btns-wrap">
                                    <div class="team_rating-item-btns">
                                        <a class="team_rating_button w-button" href="#"><span>1</span> балл</a>
                                        <a class="team_rating_button w-button" href="#"><span>2</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>3</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>4</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>5</span> баллов</a>
                                        <a class="team_rating_button w-button" href="#"><span>Без</span> оценки</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="team_rating-item">
                            <div class="team_rating-item-name">
                                <span class="ord">3. </span>Гулине Ариана Фахидовна
                            </div>
                            <div class="team_rating-item-right">
                                <div class="team_rating-item-btns-wrap">
                                    <div class="team_rating-item-btns">
                                        <a class="team_rating_button w-button" href="#"><span>1</span> балл</a>
                                        <a class="team_rating_button w-button" href="#"><span>2</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>3</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>4</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>5</span> баллов</a>
                                        <a class="team_rating_button w-button" href="#"><span>Без</span> оценки</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="team_rating-item">
                            <div class="team_rating-item-name">
                                <span class="ord">4. </span>Гулине Ариана Фахидовна
                            </div>
                            <div class="team_rating-item-right">
                                <div class="team_rating-item-btns-wrap">
                                    <div class="team_rating-item-btns">
                                        <a class="team_rating_button w-button" href="#"><span>1</span> балл</a>
                                        <a class="team_rating_button w-button" href="#"><span>2</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>3</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>4</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>5</span> баллов</a>
                                        <a class="team_rating_button w-button" href="#"><span>Без</span> оценки</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="team_rating-item">
                            <div class="team_rating-item-name">
                                <span class="ord">5. </span>Абдулов Хафиз Мурадович
                            </div>
                            <div class="team_rating-item-right">
                                <div class="team_rating-item-btns-wrap">
                                    <div class="team_rating-item-btns">
                                        <a class="team_rating_button w-button" href="#"><span>1</span> балл</a>
                                        <a class="team_rating_button w-button" href="#"><span>2</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>3</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>4</span> балла</a>
                                        <a class="team_rating_button w-button" href="#"><span>5</span> баллов</a>
                                        <a class="team_rating_button w-button" href="#"><span>Без</span> оценки</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <a class="team_rating_button_submit" href="#">Сохранить оценки и закрыть рабочий день</a>
                    </div>
                </div>
            </div>
        </div>


    </section>
@stop