@extends('cleaner.layout')

@section('main')
    <section class="infobar main">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-1"></div>
                    <div class="w-col w-col-10">
                        <h1 class="current_head">Текущий выезд</h1>
                    </div>
                    <div class="w-col w-col-1"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="main rigs">
        <div class="curr_getout-item">
            <div class="curr_getout">
                <h3 class="curr_head">Текущий выезд: <span>Заказ № 1756</span></h3>
                <div class="curr_row w-row">
                    <div class="w-col w-col-6 w-col-small-12 w-col-medium-12">
                        <h3 class="curr_meet">Встреча</h3>
                        <div class="curr_time">02.08.2016, 09:00</div>
                        <div class="curr_ob">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                    </div>
                    <div class="w-col w-col-6 w-col-small-12 w-col-medium-12 ob_row">
                        <h3 class="curr_meet">Объект</h3>
                        <div class="curr_time">10:00</div>
                        <div class="curr_ob">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                    </div>
                </div><a class="download getout_btn more w-button" href="rig.php">Подробнее</a>
            </div>
            <div class="curr_getout-item-time">Завтра</div>
        </div>
        <div class="curr_getout-item">
            <div class="curr_getout tomrw">
                <div class="curr_row">
                    <h3 class="curr_head">Завтрашний выезд: Заказ №1788</h3>
                    <div class="curr_time">02.08.2016, 09:00</div>
                    <div class="curr_ob">м. Новые Черемушки, ул. Шахтеров , д.23</div>
                </div><a class="download getout_btn more w-button" href="rig.php">Подробнее</a>
            </div>
            <div class="curr_getout-item-time">Сегодня</div>
        </div>

    </section>
@stop