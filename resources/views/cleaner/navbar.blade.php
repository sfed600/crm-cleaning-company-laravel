<body>
<div class="wrapper">
    <div class="content">
        <div class="navbar w-nav" data-animation="over-left" data-collapse="all" data-duration="400">
            <div class="naav_cont w-container">
                <nav class="menu w-nav-menu" role="navigation">
                    <!--<div class="nav_shadow">-->
                    <img class="menu_logo" src="/assets/cleaner/images/logo.png">

                    <a class="menu_link odd w-nav-link @if(\Request::route()->getName() == 'cleaner.password.reset') w--current @endif" href="{{route('cleaner.password.reset')}}">Регистрация</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.departures') w--current @endif" href="{{route('cleaner.departures')}}">Выезды</a>
                    <a class="menu_link odd w-nav-link @if(\Request::route()->getName() == 'cleaner.tabel') w--current @endif" href="{{route('cleaner.tabel')}}">Табель</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.anketa.edit') w--current @endif" href="{{route('cleaner.anketa.edit')}}">Анкета</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.anketa.view') w--current @endif" href="{{route('cleaner.anketa.view')}}">Анкета заполнена</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.departures.current') w--current @endif" href="{{route('cleaner.departures.current')}}">Текущий выезд</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.orders') w--current @endif" href="{{route('cleaner.orders')}}">Заказы</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.orders.view') w--current @endif" href="{{route('cleaner.orders.view')}}">Заказ</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.mutual_settlements') w--current @endif" href="{{route('cleaner.mutual_settlements')}}">Взаиморасчеты</a>
                    <a class="menu_link w-nav-link @if(\Request::route()->getName() == 'cleaner.calendar') w--current @endif" href="{{route('cleaner.calendar')}}">Мой календарь</a>
                    <!--</div> -->
                </nav>

                <div class="menu_btn w-nav-button">
                    <div class="logout_text">меню</div>
                </div>
                @if(Auth::check())
                    <a class="menu_logout w-inline-block" href="{{url('/logout')}}">
                        <div class="logout_text">Выход</div>
                    </a>
                @endif
                <div class="menu_desc"><span>Личный кабинет работника</span></div>
            </div>
        </div>
        <!-- <div class="swipe-area-left"></div>-->