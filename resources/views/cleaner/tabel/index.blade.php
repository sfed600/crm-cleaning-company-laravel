@extends('cleaner.layout')
@section('main')
    {{--{{dump($companies)}}--}}
    <form id="cleaner-tabel-filter" method="GET" action="{{route('cleaner.tabel', $user->id)}}">
        {{--<input type="hidden" name="f[year]">
        <input type="hidden" name="f[month]">--}}
        <input type="hidden" name="f[company]" value="{{$filters['company'] or ''}}">
        <input type="hidden" name="f[date-range]" value="{{$filters['date-range'] or ''}}">
    </form>

    <section class="infobar tabel">
        <div class="w-container">
            <div class="info_block">
                <div class="w-row">
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_bind" style="display:none;">
                            <div class="bind_text">Ставка</div>
                            <div class="info_digits">1200 р.</div>
                        </div>
                    </div>
                    <div class="w-col w-col-6 w-col-small-6 w-col-tiny-10">
                        <h1>Табель</h1>
                        <div class="info_user"><span class="info_unite">Работник:</span> <span class="info_name">{{Auth::user()->surname.' '.Auth::user()->name}}</span>
                        </div>
                    </div>
                    <div class="w-col w-col-3 w-col-small-3 w-col-tiny-1">
                        <div class="info_rait" style="display:none;">
                            <div class="rait_text">Рейтинг</div>
                            <div class="info_digits">95.7</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="info_total">
                <div class="total_item">
                    <div class="total-unite">Выходов за месяц:</div>
                    <div class="total_data">{{$dates->sum('cef')}}</div>
                </div>
                <div class="total_item">
                    <div class="total-unite">Начислено за месяц:</div>
                    {{--<div class="total_data">{{$dates->sum('amount') - $dates->sum('outcome')}} руб.</div>--}}
                    <div class="total_data">{{$dates->sum('amount')}} руб.</div>
                </div>
                <div class="total_item">
                    <div class="total-unite">Выплачено за месяц:</div>
                    <div class="total_data">{{$dates->sum('outcome')}} руб.</div>
                </div>
                <div class="total_item">
                    <div class="total-unite">Остаток:</div>
                    {{--<div class="total_data">{{$balance}} руб.</div>--}}
                    <div class="total_data">{{$dates->sum('amount') - $dates->sum('outcome')}} руб.</div>
                </div>
            </div>
        </div>
    </section>
    <section class="main tabel">
        <div class="w-container">
            <div class="filter">
                <div class="w-dropdown" data-delay="100" data-hover="1">
                    <div class="filter_year w-dropdown-toggle">
                        <div>{{$year}}</div>
                    </div>
                    <nav class="dropdown_list w-dropdown-list">
                        <a class="item_year odd w-dropdown-link" href="#" onclick="set_period_year(this);">2018</a>
                        <a class="item_year odd w-dropdown-link" href="#" onclick="set_period_year(this);">2017</a>
                        <a class="item_year w-dropdown-link" href="#" onclick="set_period_year(this);">2016</a>
                        <a class="item_year odd w-dropdown-link" href="#" onclick="set_period_year(this);">2015</a>
                        <input type="hidden" value="">
                    </nav>
                </div>
                <div class="w-dropdown" data-delay="100" data-hover="1">
                    <div class="filter_month w-dropdown-toggle">
                        <div>{{$month}}</div>
                    </div>
                    <nav class="dropdown_list w-dropdown-list">
                        <a class="item_month left odd w-dropdown-link" href="#" onclick="set_period_month(this);">Январь</a>
                        <a class="item_month odd w-dropdown-link" href="#" onclick="set_period_month(this);">Июль</a>
                        <a class="item_month left w-dropdown-link" href="#" onclick="set_period_month(this);">Февраль</a>
                        <a class="item_month w-dropdown-link" href="#" onclick="set_period_month(this);">Август</a>
                        <a class="item_month left odd w-dropdown-link" href="#" onclick="set_period_month(this);">Март</a>
                        <a class="item_month odd w-dropdown-link" href="#" onclick="set_period_month(this);">Сентябрь</a>
                        <a class="item_month left w-dropdown-link" href="#" onclick="set_period_month(this);">Апрель</a>
                        <a class="item_month w-dropdown-link" href="#" onclick="set_period_month(this);">Октябрь</a>
                        <a class="item_month left odd w-dropdown-link" href="#" onclick="set_period_month(this);">Май</a>
                        <a class="item_month odd w-dropdown-link" href="#" onclick="set_period_month(this);">Ноябрь</a>
                        <a class="item_month left w-dropdown-link" href="#" onclick="set_period_month(this);">Июнь</a>
                        <a class="item_month w-dropdown-link" href="#" onclick="set_period_month(this);">Декабрь</a>
                        <input type="hidden" value="">
                    </nav>
                </div>

                <div class="w-dropdown type_company" data-delay="100" data-hover="1">
                    <div class="filter_company w-dropdown-toggle">{{--{{dump($filters['company'])}}--}}
                        @if( isset($filters['company']) && $filters['company'] > 0 )
                            <div>{{\App\Models\Company::find($filters['company'])->name}}</div>
                        @else
                            <div>Все компании в которых работаю</div>
                        @endif
                    </div>
                    <nav class="dropdown_list w-dropdown-list">
                        {{--<a class="item_year odd w-dropdown-link" href="#">ООО "Сфера Чистоты"</a>
                        <a class="item_year w-dropdown-link" href="#">ООО "Чистота и порядок"</a>
                        <a class="item_year odd w-dropdown-link" href="#">ООО "Белый ветер"</a>
                        <input type="hidden" value="">--}}
                        <a class="item_year w-dropdown-link" href="#" onclick="set_company_filter(0);">Все компании в которых работаю</a>
                        @foreach($companies as $company)
                            <a class="item_year w-dropdown-link" href="#" onclick="set_company_filter({{$company->id}});">{{$company->name}}</a>
                        @endforeach
                    </nav>
                </div>

            </div>
            <div class="table_wrap">
                <table>
                    <thead>
                    <tr>
                        <td>Дата</td>
                        <td>Начислено</td>
                        <td>Смена</td>
                        <td>Выплачено</td>
                        <td>Комментарий к ставке</td>
                        <td>Вид оплаты</td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($dates as $date)
                            <tr>
                                <td>{{date('d.m.Y', strtotime($date->date))}}</td>
                                <td><strong>{{$date->amount or ''}}</strong></td>
                                <td>{{$date->cef or ''}}</td>
                                <td><strong>{{$date->outcome or ''}}</strong></td>
                                <td>{{$date->outcome_comment or $date->comment}}</td>
                                <td>{{$date->type or ''}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop