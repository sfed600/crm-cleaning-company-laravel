<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, minimal-ui">

    <link rel="stylesheet" href="/assets/cleaner/css/style2018_05.css" />
    <link rel="stylesheet" type="text/css" href="/assets/cleaner/css/jquery.modal.min.css">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
</head>

<body>


@include('cleaner.navbar')

@yield('main')

@include('cleaner.footer')