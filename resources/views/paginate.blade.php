@if ($paginator->lastPage() > 1)
<div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix">
        <!-- Pagination -->
        <div id="pagination_bottom" class="pagination clearfix"></div>
        <div class="product-count">
            <font class="text">Товары {{$paginator->perPage() * ($paginator->currentPage() - 1) + 1}} - {{min($paginator->currentPage() * $paginator->perPage(), $paginator->total()) }} из {{$paginator->total()}}<br></font>
            <font class="text">
                @if($paginator->currentPage() > 1)
                    <a href="{{$paginator->url(1)}}">Начало</a>
                @else
                    Начало
                @endif
                &nbsp;|&nbsp;
                @if($paginator->currentPage() > 1)
                    <a href="{{$paginator->previousPageUrl()}}">Пред.</a>
                @else
                        Пред.
                @endif
                &nbsp;|&nbsp;
                @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                    @if($paginator->currentPage() == $i)
                        <b>{{$i}}</b>
                    @else
                        <a href="{{ $paginator->url($i)}}">{{$i}}</a>
                    @endif
                @endfor
                &nbsp;|&nbsp;
                @if($paginator->currentPage() != $paginator->lastPage())
                   <a href="{{$paginator->nextPageUrl()}}">След.</a>
                @else
                   След.
                @endif
                &nbsp;|&nbsp;
                @if($paginator->currentPage() != $paginator->lastPage())
                    <a href="{{$paginator->url($paginator->lastPage())}}">Конец</a>
                @else
                    Конец
                @endif
            </font>
        </div>
        <!-- /Pagination -->
    </div>
</div>
@endif
