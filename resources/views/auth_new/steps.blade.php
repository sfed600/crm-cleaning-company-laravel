<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Авторизация - формы</title>
    <link rel="icon" type="image/png" href="img/favicon.png">
    @include('auth_new.css')
    <link href="/assets/admin_new/css/style.css" rel="stylesheet">
    <link href="/assets/admin_new/css/page_login.css" rel="stylesheet">
</head>

<body class="preload">

<div class="dash-wrap">
    {{--@include('auth_new.header.blade.php')--}}
    <div class="dash-panel">
        <div class="dash-left">
            <div class="dash-logo"><a href="/"><img src="/assets/admin_new/img/logo.png" alt=""></a></div>
            <div class="dash-left-text">
                <div class="dash-left-text-title">Сфера чистоты</div>
                <div class="dash-left-text-sub">Клининговая компания</div>
            </div>
        </div>
        <div class="dash-panel-title">
            <span class="txt-label">Авторизация</span>
        </div>
    </div>
    <div class="dash-container">
        <div class="dash-content">


            <div style="padding:30px;">
                <a data-fancybox-inline data-src="#box-registers" href="javascript:;">Открыть пошаговую форму (регистрация + валидация + форма доступа)</a><br><br>

                {{--<a data-fancybox-inline data-src="#box-register" href="javascript:;">Открыть отдельно верстку формы регистрации</a><br>
                <a data-fancybox-inline data-src="#box-unlock" href="javascript:;">Открыть отдельно верстку формы доступа</a><br>
                <a data-fancybox-inline data-src="#box-hello" href="javascript:;">Открыть отдельно верстку формы приветствия</a>--}}
            </div>


            <div style="display:none;">
                <div class="showbox-wrap" id="box-registers">
                    <div id="js-form-register-start">
                        <div class="showbox-header">Регистрация компании в cleancrm.ru</div>
                        <div class="showbox-header-text">
                            После регистрации вы получите доступ к лучшей на данный момент системе cleancrm.ru по автоматизированному управлению заказчиками, уборками и персоналом
                        </div>

                        {{--<form class="showbox-form" id="form-register" data-form-register-step="1">--}}
                        <form class="showbox-form" method="POST" action="/register_company" data-form-register-step="1" id="form-register">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="showbox-step-item current" id="form-register-step-01" data-step="1">
                                <div class="showbox-section">
                                    <div class="showbox-section-text">
                                        Введите ИНН вашей компании для получения данных:
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">ИНН</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input">
                                                <input type="text" placeholder="2343856834537" class="imp" data-type="inn" data-jsmask="inn">
                                                <div class="showbox-form-input-err">Корректный номер ИНН должен состоять из 10 или 12 цифр. Введите корректный номер</div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">Компания</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input"><input type="text" value="ИП Ефимов Дмитрий Владимирович" readonly></div>
                                        </div>
                                    </div>--}}
                                </div>
                                <div class="showbox-section type-bottom">
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label"></div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-submit">
                                                <input class="button-showbox-form button_send" data-btn-form-register-step="2" type="button" value="Далее" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="showbox-step-item" id="form-register-step-02" data-step="2">
                                <div class="showbox-section">
                                    <div class="showbox-section-text">
                                        Введите ФИО, электронный адрес и мобильный телефон доверенного лица в вашей компании, кто будет иметь главный доступ к cleancrm.ru
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">Компания</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input">
                                                <input name="company_name" type="text" value="ИП Ефимов Дмитрий Владимирович" readonly></div>
                                        </div>
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">ФИО</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input">
                                                <input name="fio" type="text" placeholder="Dmitriy" class="imp" data-type="fio">
                                                <div class="showbox-form-input-err">Введите корректное ФИО. Только буквы, ввод как минимум Имени и Фамилии</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">E-mail</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input"><input name="email" type="email" placeholder="blamp@mail.ru" class="imp"></div>
                                        </div>
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">Телефон</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input"><input name="phone" type="tel" placeholder="+7 (934) 222-2222" class="phonemask imp" data-jsmask="tel"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="showbox-section type-bottom">
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label"></div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-submit"><input class="button-showbox-form button_send" data-btn-form-register-step="3" type="button" value="Далее" disabled></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="showbox-step-item" id="form-register-step-03" data-step="3">
                                <div class="showbox-section">
                                    <div class="showbox-section-text">
                                        Введите свой пароль или не вводите ничего и тогда мы пришлем вам пароль на email
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">Пароль</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input">
                                                <input name="password" type="password" placeholder="********" class="imp inputPasswordNew" data-type="pass" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label">Повторите пароль</div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-input">
                                                <input name="password_confirmation" type="password" placeholder="********" class="imp inputPasswordNewCh" data-type="pass" autocomplete="off">
                                                <div class="showbox-form-input-err">Для установки пароля, введенные пароли должны полностью совпадать. Минимум 6 символов</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="showbox-section type-bottom">
                                    <div class="showbox-form-row">
                                        <div class="showbox-form-row-col col-label"></div>
                                        <div class="showbox-form-row-col col-input">
                                            <div class="showbox-form-submit">
                                                {{--<input type="button" onclick="submit_form_register();" class="button-showbox-form button_send" value="Зарегистрироваться">--}}
                                                <input type="button" onclick="submit_form_register();" class="button-showbox-form active" value="Зарегистрироваться">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="display-none" id="js-form-register-end-pass">
                        <div class="showbox-header">Доступ к cleancrm.ru отправлен Вам</div>
                        <div class="showbox-text">
                            <p>На введеный Вами электронный адрес было отправлено электронное письмо, в котором содержиться ссылка подтверждения регистрации. Пройдите по ней, чтобы начать пользоваться системой.</p>
                        </div>
                        <div class="showbox-login-button">
                            <a href="/login"><span class="button-showbox-form">Авторизоваться</span></a>
                        </div>
                    </div>
                    <div class="display-none" id="js-form-register-end-sms">
                        <div class="showbox-header">Доступ к cleancrm.ru отправлен Вам</div>
                        <div class="showbox-text">
                            <p>На введенный телефонный номер было отправлено смс с паролем для входа. После получения смс, нажмите пожалуйста на кнопку «Авторизоваться», чтобы начать пользоваться cleancrm.ru.</p>
                        </div>
                        <div class="showbox-login-button">
                            <span class="button-showbox-form">Авторизоваться</span>
                        </div>
                        <div class="showbox-text">
                            <p>Ожидайте смс в течение минуты. Если оно не пришло, нажмите кнопку «Отправить повторное смс» через 1 минуту.</p>
                        </div>

                        <div class="showbox-unlock-repeated">
                            <div class="showbox-unlock-repeated-time">
                                <span class="showbox-unlock-repeated-time-icon"></span>
                                <span class="showbox-unlock-repeated-time-value js-unlock-timeout">00:00</span>
                            </div>
                            <span class="button-showbox-form js-unlock-send disabled">Отправить повторное смс</span>
                        </div>
                    </div>
                </div>



                <div class="showbox-wrap" id="box-hello">
                    <div class="showbox-header-logo"><img src="img/logo-middle.png" alt=""></div>
                    <div class="showbox-text txt-left">
                        <p>Уважаемый партнер!</p>
                        <p>Мы очень рады, что Вы присоединились к нам!</p>
                        <p>Вы получили данное письмо, потому что прошли регистрацию в лучшей на данный момент системе по автоматизированному управлению заказчиками, уборками и персоналом cleancrm.ru.<br>
                            Для того, чтобы начать пользоваться системой перейдите по ссылке ниже. </p>
                        <p><a href="#">Ссылка</a></p>
                        <div class="showbox-how-about">
                            <p><b>А как этим пользоваться:</b></p>
                            <p>1. Сразу после того, как вы окажетесь внутри системы, добавьте ваших клинеров, которые будут участвовать в первом автоматизированном заказе в разделе &quot;Сотрудники&quot;. <br>
                                2. После этого добавьте ваш первый автоматизированный заказ в разделе &quot;Заказы&quot;.<br>
                                3.  Прямо в заказе добавьте компанию или контакт, для кого будет осуществляться уборка; <br>
                                4. Добавьте дни уборок по этому заказу и выберите клинеров, кто будет работать в эти дни и автоматизированно отправьте им смс о том, где, как и что они убирают. <br>
                                5. А после завершения заказа поставьте оценки вашим клинерам - они будут собираться в рейтинг, по которому в дальнейшем вы и ваши менеджеры будут руководствоваться при выборе персонала для уборок; <br>
                                6. Оцените удобство использования системы, написав нам ваш отзыв на e-mail: info@cleancrm.ru </p>
                            <p>Это письмо было сформировано автоматически. Не отвечайте на него. Если вы получили это письмо случайно, то просто игнорируйте его</p>
                        </div>

                        <div class="showbox-copyright"><span class="txt-logo">Cleancrm.ru</span>   Все права защищены 2018г.</div>
                    </div>
                </div>

                <div class="showbox-wrap" id="box-unlock">
                    <div class="showbox-header">Доступ к cleancrm.ru отправлен Вам</div>
                    <div class="showbox-text">
                        <p>На введеный Вами электронный адрес было отправлено электронное письмо, в котором содержиться ссылка подтверждения регистрации. Пройдите по ней, чтобы начать пользоваться системой.</p>
                        <p>На введенный телефонный номер было отправлено смс с паролем для входа. После получения смс, нажмите пожалуйста на кнопку «Авторизоваться», чтобы начать пользоваться cleancrm.ru.</p>
                    </div>
                    <div class="showbox-login-button">
                        <span class="button-showbox-form">Авторизоваться</span>
                    </div>
                    <div class="showbox-text">
                        <p>Ожидайте смс в течение минуты. Если оно не пришло, нажмите кнопку «Отправить повторное смс» через 1 минуту.</p>
                    </div>

                    <div class="showbox-unlock-repeated">
                        <div class="showbox-unlock-repeated-time">
                            <span class="showbox-unlock-repeated-time-icon"></span>
                            <span class="showbox-unlock-repeated-time-value js-unlock-timeout">00:00</span>
                        </div>
                        <span class="button-showbox-form js-unlock-send">Отправить повторное смс</span>
                    </div>
                </div>

                {{--<div class="showbox-wrap" id="box-register">
                    <div class="showbox-header">Регистрация компании в cleancrm.ru</div>
                    <div class="showbox-header-text">
                        После регистрации вы получите доступ к лучшей на данный момент системе cleancrm.ru по автоматизированному управлению заказчиками, уборками и персоналом
                    </div>

                    <div class="showbox-form">
                        <div class="showbox-section">
                            <div class="showbox-section-text">
                                Введите ИНН вашей компании для получения данных:
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">ИНН</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input"><input type="text" placeholder="2343856834537"></div>
                                </div>
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">Компания</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input type="text" placeholder="ИП Ефимов Дмитрий Владимирович" name="company_name">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="showbox-section">
                            <div class="showbox-section-text">
                                Введите ФИО, электронный адрес и мобильный телефон доверенного лица в вашей компании, кто будет иметь главный доступ к cleancrm.ru
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">ФИО</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input type="text" placeholder="Dmitriy" name="name">
                                    </div>
                                </div>
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">E-mail</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input name="email" type="text" placeholder="blamp@mail.ru">
                                    </div>
                                </div>
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">Телефон</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input name="phone" type="text" placeholder="+7 (934) 222-22-22" class="phonemask" data-jsmask="tel">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="showbox-section">
                            <div class="showbox-section-text">
                                Введите свой пароль или не вводите ничего и тогда мы пришлем вам пароль в смс
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">Пароль</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input name="password" type="text" placeholder="********">
                                    </div>
                                </div>
                            </div>
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label">Повторите пароль</div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-input">
                                        <input name="password_confirmation" type="text" placeholder="********">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="showbox-section type-bottom">
                            <div class="showbox-form-row">
                                <div class="showbox-form-row-col col-label"></div>
                                <div class="showbox-form-row-col col-input">
                                    <div class="showbox-form-submit"><input class="button-showbox-form" type="submit" value="Зарегистрироваться"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}
            </div>

        </div>
        <div class="dash-container-layer"></div>
    </div>

</div>


@include('auth_new.js')
<script src="/assets/admin_new/js/page-login.js?v=44"></script>
</body>
</html>