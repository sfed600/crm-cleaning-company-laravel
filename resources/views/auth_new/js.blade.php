<script src="/assets/admin_new/js/jquery.min.js"></script>
<script src="/assets/admin_new/js/moment/moment.min.js"></script>

<script src="/assets/admin_new/js/form-validator.js?v=44"></script>
<script src="/assets/admin_new/js/chosen/chosen.jquery.min.js"></script>
<script src="/assets/admin_new/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/admin_new/js/daterangepicker/daterangepicker.js"></script>
<script src="/assets/admin_new/js/tooltipster/tooltipster.bundle.min.js"></script>
<script src="/assets/admin_new/js/jquery.maskedinput.min.js"></script>
<script src="/assets/admin_new/js/jquery.simplePagination.js"></script>
<script src="/assets/admin_new/js/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/admin_new/js/dash-init.js"></script>