@if (count($errors) > 0)
<div class="jq-message" style="display: block;">
    <div class="error">
        @foreach ($errors->all() as $error)
        {{$error}}<br>
        @endforeach
    </div>
</div>
@endif