<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>500 Ошибка</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="/images/589e1ed2acf809b402082fb2_favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="/images/webclip.png" rel="apple-touch-icon">
    <!-- CUSTOM CODE -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>

<body>
<div class="flex-wrapper">
    <div class="footer-section">
        @include('blocks.footer')
    </div>
</div>
</body>

</html>