<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <title>Доступ запрещен</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.jpg" type="image/x-icon" />

    <link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>
<div class="inner i404">
    <div class="logo">
        <a href="{{route('index')}}"><img src="/img/logo.png" alt="" /></a>
    </div>
    <div class="text4">403</div>
    <div class="p404">к сожалению, доступ к этой странице запрещен</div>
    @include('blocks.menu')
</div>
<link rel="stylesheet" type="text/css" href="/css/queries.css" />
<!--[if lt IE 9]!><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<script>
    if ( /*@cc_on!@*/ false) {
        document.documentElement.className += ' ie10';
    }
</script>
<!--<![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" href="/css/ie9.css" type="text/css" />
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" href="/css/ie8.css" type="text/css" />
<![endif]-->
<script src="/js/jquery.2.2.4.js"></script>
<script type="text/javascript">
    $(function() {
        var pull 		= $('#pull');
        menu 		= $('.top-menu ul');
        menuHeight	= menu.height();

        $(pull).on('click', function(e) {
            e.preventDefault();
            menu.slideToggle();
        });
    });
    $(window).resize(function(){
        var w = $(window).width();
        if(w > 320 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
</script>
</body>
</html>