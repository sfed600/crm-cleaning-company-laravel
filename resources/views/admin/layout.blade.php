<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Сфера чистоты</title>
    <link rel="icon" type="image/png" href="/assets/admin_new/img/favicon.png">
    {{--<link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css" />--}}

    <link href="/assets/admin_new/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="/assets/admin_new/js/chosen/chosen.min.css" rel="stylesheet">
    <link href="/assets/admin_new/js/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="/assets/admin_new/js/tooltipster/tooltipster.bundle.min.css" rel="stylesheet">
    <link href="/assets/admin_new/js/fancybox/jquery.fancybox.min.css" rel="stylesheet">
    <link href="/assets/admin_new/js/air-datepicker/css/datepicker.min.css" rel="stylesheet">
    <link href="/assets/admin_new/js/jquery.modal/jquery.modal.min.css" rel="stylesheet">

    <link href="/assets/admin_new/css/style.css?v=48" rel="stylesheet">

    <link href="/assets/admin_new/css/page_kp.css?v=44" rel="stylesheet">

    @if( strpos(Route::current()->getName(), 'settings') != null || strpos(Route::current()->getName(), 'order_companies') != null )
        <link href="/assets/admin_new/css/page_company.css?v=44" rel="stylesheet">
    @endif

    @if( strpos(Route::current()->getName(), 'order_contacts') != null )
        <link href="/assets/admin_new/css/page_contact.css?v=44" rel="stylesheet">
    @endif

    @if( strpos(Route::current()->getName(), 'calendar') != null )
        <link href="/assets/admin_new/css/page_calendar.css?v=44" rel="stylesheet">
    @endif

    @if( strpos(Route::current()->getName(), 'user') != null )
        <link href="/assets/admin_new/css/page_worker.css?v=44" rel="stylesheet">
    @endif

    {{--@if( Route::current()->getName() == "admin.orders.index" )
        <link href="/assets/admin_new/css/pagination.css" rel="stylesheet">
    @endif--}}
    <link href="/assets/admin_new/css/pagination.css" rel="stylesheet">

    <link href="/assets/admin_new/css/style600.css" rel="stylesheet">

    {{--@if( Route::current()->getName() == "admin.orders.create" || Route::current()->getName() == "admin.orders.edit" )
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="/assets/admin/js/ymaps.js?48"></script>
    @endif--}}
</head>

<body class="preload">

    <div class="dash-wrap">
        @include('admin.sidebar')

        <div class="dash-panel">
            <div class="dash-left">
                @if(Auth::user()->company)
                    {{--<a href="{{route('admin.main')}}" class="navbar-brand">
                        <div>
                            <span style="font-family: Lato Reqular;!important;">{{Auth::user()->company->name}}</span>
                            <span style="font-family: Lato Reqular;!important;">Клининговая компания</span>
                        </div>
                    </a>--}}
                    {{--<a href="{{route('admin.main')}}"><img src="/assets/admin_new/img/logo.png" alt=""></a>--}}

                        <div class="dash-logo"><a href="{{route('admin.main')}}"><img src="img/logo.png" alt=""></a></div>
                        <div class="dash-left-text">
                            <div class="dash-left-text-title">{{Auth::user()->company->name}}</div>
                            <div class="dash-left-text-sub">Клининговая компания</div>
                        </div>
                @endif
            </div>
            @if(Route::current()->getName() == 'admin.orders.edit' || Route::current()->getName() == 'admin.orders.create')
                <div class="dash-panel-heads">
                   <span class="dash-panel-heads-label">
                      <span class="op-input">
                          <input id="order-header-name" type="text" placeholder="..." value="{{old('name', @$order->name)}}">
                      </span>
                      <span class="txt-subtitle">Заказ № {{$order->number or $order_number}}</span>
                       <a href="{{route('admin.orders.index')}}" class="dash-panel-heads-button"></a>
                   </span>
                </div>
            @endif

            @if(Route::current()->getName() == 'admin.order_companies.edit' || Route::current()->getName() == 'admin.order_companies.create')
                <div class="dash-panel-heads">
                    <span class="dash-panel-heads-label">
                        <span class="op-input">
                            <input id="order_company_heads_label" type="text" placeholder="..." value="{{old('name', @$company->name)}}" @if(Route::current()->getName() == 'admin.order_companies.edit') update-ajax @endif>
                        </span>
                        <a href="{{route('admin.order_companies.index')}}" class="dash-panel-heads-button"></a>
                    </span>
                </div>
            @endif

            @if(Route::current()->getName() == 'admin.order_contacts.edit' || Route::current()->getName() == 'admin.order_contacts.create')
                <div class="dash-panel-heads">
                   <span class="dash-panel-heads-label">
                      <span class="op-input">
                          <input id="order_contact_heads_label" type="text" placeholder="..." value="{{old('name', @$contact->name)}}" @if(Route::current()->getName() == 'admin.order_contacts.edit') update-ajax @endif>
                      </span>
                      <span class="txt-subtitle">{{@$contact->company->name}}</span>
                      <a href="{{route('admin.order_contacts.index')}}" class="dash-panel-heads-button"></a>
                   </span>
                </div>
            @endif

            {{--<div class="dash-panel-title">
                @if(Route::current()->getName() == 'admin.orders.index')
                    <span class="txt-label">Заказы</span>
                @elseif(Route::current()->getName() == 'admin.tabel')
                    <span class="txt-label">Табель</span>
                @elseif(Route::current()->getName() == 'admin.invoices.index')
                    <span class="txt-label">Счета</span>
                @elseif(Route::current()->getName() == 'admin.offers.index')
                    <span class="txt-label">Предложения</span>
                @elseif(Route::current()->getName() == 'admin.tabel')
                    <span class="txt-label">Персонал</span>
                @elseif(Route::current()->getName() == 'admin.order_contacts.index')
                    <span class="txt-label">Контакты</span>
                @elseif(Route::current()->getName() == 'admin.order_companies.index')
                    <span class="txt-label">Компании</span>
                @elseif(Route::current()->getName() == 'admin.calendar')
                    <span class="txt-label">Календарь</span>
                @endif
            </div>--}}
            @if(Route::current()->getName() == 'admin.orders.index')
                <div class="dash-panel-title">
                    <span class="txt-label">Заказы</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.tabel')
                <div class="dash-panel-title">
                    <span class="txt-label">Табель</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.invoices.index')
                <div class="dash-panel-title">
                    <span class="txt-label">Счета</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.offers.index')
                <div class="dash-panel-title">
                    <span class="txt-label">Предложения</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.tabel')
                <div class="dash-panel-title">
                    <span class="txt-label">Персонал</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.order_contacts.index')
                <div class="dash-panel-title">
                    <span class="txt-label">Контакты</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.order_companies.index')
                <div class="dash-panel-title">
                    <span class="txt-label">Компании</span>
                </div>
            @elseif(Route::current()->getName() == 'admin.calendar')
                <div class="dash-panel-title">
                    <span class="txt-label">Календарь</span>
                </div>
            @endif

            @if(Route::current()->getName() == 'admin.orders.index')
                @include('admin.orders.filter')
            @elseif(Route::current()->getName() == 'admin.invoices.index')
                @include('admin.invoice.filter')
            @elseif(Route::current()->getName() == 'admin.offers.index')
                @include('admin.offers.filter')
            @elseif(Route::current()->getName() == 'admin.tabel')
                @include('admin.tabel.filter')
            @elseif(Route::current()->getName() == 'admin.users.index')
                @include('admin.users.filter')
            @elseif(Route::current()->getName() == 'admin.order_contacts.index')
                @include('admin.order_contacts.filter')
            @elseif(Route::current()->getName() == 'admin.order_companies.index')
                @include('admin.order_companies.filter')
            @elseif(Route::current()->getName() == 'admin.calendar')
                @include('admin.calendar.filter')
            @endif

            <div class="dash-panel-block type-profile">
                <div class="dash-panel-notify">
                    <div class="dash-panel-notify-button">
                        <div class="dash-panel-notify-button-icon">
                            <div class="dash-panel-notify-button-counter">2</div>
                        </div>
                    </div>
                </div>
                <div class="dash-panel-dropdown dropdown-profile" data-dropped="wrap">
                    <div class="dash-panel-dropdown-button" data-dropped="btn" data-layer="yes">
                        {{--<div class="dash-panel-dropdown-ava" style="background-image:url(/assets/admin_new/img/blank/avatar.png)">--}}
                        <div class="dash-panel-dropdown-ava" style="background-image:url(/{{Auth::user()->getImgPreviewPath().Auth::user()->img}})">
                            <div class="dash-panel-dropdown-status type-online"></div>
                        </div>
                        <div class="dash-panel-dropdown-arrw"></div>
                    </div>
                    <div class="dash-panel-dropdown-drop">
                        <a href="{{route('index')}}"><span class="icon-profile ic-main"></span>Главная</a>
                        <a href="{{route('admin.users.show', Auth::user()->id)}}"><span class="icon-profile ic-my"></span>Профиль</a>
                        <a href="{{url('/logout')}}"><span class="icon-profile ic-logout"></span>Выход</a>
                    </div>
                </div>
            </div>

            @if(strpos(Route::current()->getName(), 'admin.orders') !== false )
                @include('admin.orders.navbar')
            @elseif(Route::current()->getName() == 'admin.invoices.index')
                @include('admin.invoice.navbar')
            @elseif( strpos(Route::current()->getName(), 'order_contacts') != null )
                @include('admin.order_contacts.navbar')
            @elseif( strpos(Route::current()->getName(), 'admin.order_companies') !== false )
                @include('admin.order_companies.navbar')
            @elseif(Route::current()->getName() == 'admin.users.create' || Route::current()->getName() == 'admin.users.show')
                @include('admin.users.navbar')
            @elseif(Route::current()->getName() == 'admin.offers.index')
                @include('admin.offers.navbar')
            @elseif(Route::current()->getName() == 'admin.calendar')
                @include('admin.calendar.navbar')
            @elseif(Route::current()->getName() == 'admin.tabel' || Route::current()->getName() == 'admin.users.index')
                <div class="dash-panel-block type-btns">
                    <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
                        <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
                        <div class="dash-panel-dropdown-drop">
                            <a href="#">Ссылка 1</a>
                            <a href="#">Ссылка 2</a>
                            <a href="#">Ссылка 3</a>
                        </div>
                    </div>

                    {{--<a class="o-btn bgs-green o-hvr" href="#">+ Добавить сотрудника</a>--}}
                    <a class="o-btn bgs-green o-hvr" href="{{route('admin.users.create')}}">+ Добавить сотрудника</a>
                </div>
                @if(Route::current()->getName() == 'admin.tabel')
                    <div class="dash-panel-block type-transactions">
                        <span class="txt-label">Выплата {{sizeof($users)}} сотрудникам:</span>
                        <span class="txt-value">{{number_format($total_income, 0, ',', ' ')}} руб</span>
                    </div>
                @endif
            @endif
        </div>

        @yield('main')

        @include('admin.footer')

    </div>

</body>
</html>
