<script src="/assets/admin_new/js/jquery.min.js"></script>

<script src="/assets/admin_new/js/moment/moment.min.js"></script>

<script src="/assets/admin_new/js/chosen/chosen.jquery.min.js"></script>
<script src="/assets/admin_new/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/admin_new/js/daterangepicker/daterangepicker.js"></script>
<script src="/assets/admin_new/js/tooltipster/tooltipster.bundle.min.js"></script>
<script src="/assets/admin_new/js/jquery.maskedinput.min.js"></script>
<script src="/assets/admin_new/js/jquery.simplePagination.js"></script>

<script src="/assets/admin_new/js/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/admin_new/js/jquery.modal/jquery.modal.min.js"></script>
<script src="/assets/admin_new/js/dash-notify.js"></script>
{{--<script src="/assets/admin_new/js/air-datepicker/js/datepicker.min.js"></script>--}}
<script src="/assets/admin_new/js/air-datepicker/js/datepicker.js?v=44"></script>

<script src="/assets/admin_new/js/dash-init.js?v=45"></script>
{{--<script src="/assets/admin_new/js/dash-init-copy.js?v=44"></script>--}}
<script src="/assets/admin_new/js/dash-select-chosen.js?v=58"></script>

@if( Route::current()->getName() != 'admin.users.show' && Route::current()->getName() != 'admin.users.create' )
    <script src="/assets/admin_new/js/page-kp.js?v=45"></script>
@endif

@if( (strpos(Route::current()->getName(), 'admin.order_companies') !== false )
        || Route::current()->getName() == 'admin.settings' )
    <script src="/assets/admin_new/js/page-company.js"></script>
@endif

@if(strpos(Route::current()->getName(), 'admin.order_contacts') !== false )
    <script src="/assets/admin_new/js/page-contact.js"></script>
@endif

@if(strpos(Route::current()->getName(), 'admin.user') !== false )
    <script src="/assets/admin_new/js/page-worker.js?v=48"></script>
@endif

<script src="/assets/admin_new/js/turn.js?v=48"></script>
{{--<script src="/assets/admin_new/js/project.js?45"></script>--}}
<script src="/assets/admin_new/js/autocomplete.js?v=44"></script>
<script src="/assets/admin_new/js/js600.js?v=46"></script>
<script src="/assets/admin_new/js/common.js?v=48"></script>

{{--@if( Route::current()->getName() == "admin.orders.create" || Route::current()->getName() == "admin.orders.edit" )
    <script src="/assets/admin_new/js/order.js?44"></script>
@endif--}}
<script src="/assets/admin_new/js/order.js?v=44"></script>



