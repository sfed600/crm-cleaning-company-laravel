@extends('admin.layout')
@section('main')
    {{--<script src="/assets/admin/js/calculator.js?45"></script>--}}

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.products.index')}}">Товары / Услуги</a>
            </li>
            <li class="active">Редактирование</li>
        </ul><!-- /.breadcrumb -->
    </div>

    {{--<div class="page-content order-edit" data-tab="{{$request->input('tab')}}">--}}
    <div class="page-content">

        <div class="page-header">
            <h1>
                Товары / Услуги
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Редактирование
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                {{--<li @if( !$request->input('tab') or $request->input('tab') == 'offer') class="active" @endif>--}}
                <li class="active">
                    <a data-toggle="tab" href="#product" aria-expanded="true">
                        <i class="green ace-icon fa fa-home bigger-120"></i>
                        Товар / Услуга
                    </a>
                </li>

                {{--<li>
                    <a data-toggle="tab" href="#calculator" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Калькулятор
                    </a>
                </li>--}}

            </ul>

            <div class="tab-content">
                {{--<div id="offer" class="tab-pane @if( !$request->input('tab') or $request->input('tab') == 'offer') active @endif">--}}
                <div id="product" class="tab-pane active">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <form class="form-horizontal" role="form" action="{{route('admin.products.update', $product->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <input name="company_id" type="hidden" value="{{Auth::user()->company_id}}">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-2"> Наименование </label>
                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-2" name="name" placeholder="Наименование" value="{{ old('name', $product->name) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-222"> Полное наименование </label>
                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-222" name="full_name" placeholder="Полное наименование" value="{{ old('full_name', $product->full_name) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                {{--<div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-8"> Статус </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Offer::$statuses as $key => $status)
                                                <option value="{{$key}}" @if((old() && old('status')==$key) || (!old() &&  $key==$offer->status)) selected @endif>{{$status['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>--}}

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-88"> Вид </label>
                                    <div class="col-sm-8">
                                        <select name="tip" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Product::$tips as $key => $val)
                                                <option value="{{$key}}" @if((old() && old('tip')==$key) || (!old() &&  $key == $product->tip)) selected @endif>{{$val['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-22"> Цена </label>
                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-22" name="price" placeholder="Цена" value="{{ old('price', $product->price) }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8"> НДС </label>
                                    <div class="col-sm-8">
                                        <select name="nds" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Product::$nds as $key => $val)
                                                <option value="{{$key}}" @if((old() && old('nds')==$key) || (!old() &&  $key == $product->nds)) selected @endif>{{$val['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-8"> Ед. изм. </label>
                                    <div class="col-sm-8">
                                        <select name="measure" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Product::$measures as $key => $val)
                                                <option value="{{$key}}" @if((old() && old('measure')==$key) || (!old() &&  $key == $product->measure)) selected @endif>{{$val['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-322"> Артикул </label>
                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-322" name="articul" placeholder="Артикул" value="{{ old('articul', $product->articul) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label no-padding-right" for="form-field-note"> Коментарий </label>
                                    <div class="col-sm-9">
                                        <textarea name="comment" class="col-sm-12">{{old('comment', $product->comment)}}</textarea>
                                    </div>
                                    {{--<div class="col-sm-9">
                                        <input type="text" id="form-field-note" name="comment" placeholder="Коментарий" value="{{old('comment')}}" class="col-sm-12">
                                    </div>--}}
                                </div>

                                {{--<div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-5"> Ответсвенный </label>
                                    <div class="col-sm-9">
                                        <select name="user_id" class="chosen-select chosen-autocomplite form-control" id="form-field-5" data-url="{{route('admin.users.search', ['block' => 'Management'])}}" data-placeholder="Начните ввод...">
                                           @if($offer->user_id)
                                           <option value="{{$offer->user_id}}" selected>{{$offer->user->fio()}}</option>
                                           @else
                                           <option value=""></option>
                                           @endif
                                        </select>
                                    </div>
                                </div>--}}

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-success" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Сохранить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-warning button-apply" type="button" data-action="{{route('admin.products.update', ['id' => $product->id, 'route' => 'back'])}}">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Применить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="{{route('admin.products.index')}}">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Назад
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>

                {{--<div id="calculator" class="tab-pane">
                    @include('admin.offers.calculator')
                </div>--}}

            </div>
        </div>

    </div>

@stop

