<div class="worker-reviews-wrap">

    <div class="w-reviews-items" data-addreviews-form="added">
        {{--{{dd($reviews)}}--}}
        @foreach ($reviews as $review)
            <div class="w-reviews-item clearfix" data-wreviews="item">
                <div class="w-reviews-item-ava" style="background-image:url(img/blank/avatar.png);"></div>
                <div class="w-reviews-item-profile">
                    <div class="w-reviews-item-profile-name">
                        <span class="w-reviews-item-profile-txt">{{$review->author->fio()}}</span>
                        <span class="w-reviews-item-profile-group">({{$review->author->group->name}})</span>
                    </div>
                    <div class="w-reviews-item-profile-company">{{$review->author->company->name}}</div>
                    <div class="w-reviews-item-profile-date">{{date('d.m.Y H:i', strtotime($review->created_at))}}</div>
                </div>
                {{--<div class="w-reviews-item-rating">
                    <div class="w-reviews-rating-icon type-up"></div>
                </div>--}}
                <div class="w-reviews-item-content">
                    <div class="w-reviews-item-message">
                        <div class="w-reviews-item-message-inline" data-wreviews="edited">{{$review->review}}</div>
                   <span class="w-reviews-item-btns">
                   <span class="w-reviews-item-btn type-edit" data-wreviews="btn_edit" data-review_id="{{$review->id}}"></span>
                   <span class="w-reviews-item-btn type-remove" data-wreviews="btn_delete" data-review_id="{{$review->id}}"></span>
                </span>
                    </div>
                    <div class="w-reviews-item-order">(Заказ №<a href="/admin/orders/{{$review->order->id}}/edit">{{$review->order->number}}</a>)</div>
                </div>
            </div>
        @endforeach
    </div>

    <div data-addreviews-toggles="wrap">
        <div class="w-addreviews type-preview clearfix" data-addreviews-toggles="hide">
            <div class="w-addreviews-left"></div>
            <div class="w-addreviews-right">
                <div class="w-addreviews-button">
                    <div class="o-btn o-hvr" data-addreviews-toggles="btn">Добавить отзыв</div>
                </div>
            </div>
        </div>
        <div class="w-addreviews type-toggle clearfix display-none" data-addreviews-toggles="show" data-addreviews-form="wrap">
            <div class="w-addreviews-left">
                <div class="op-input type-btns" data-op-input="item">
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <input name="addreviews_order_id" type="text" placeholder="Заказ №" data-ntitle="Заполните поле: Заказ №" value="" data-op-input="get" id="js-addreviews-order-search" class="js-ajax-valid" autocomplete="off">
                </div>
            </div>
            <div class="w-addreviews-right">
                <div class="w-addreviews-content clearfix">
                    {{--<div class="w-addreviews-rating">
                        <label class="w-addreviews-rating-item">
                            <input type="radio" name="addreviews_rating" value="up">
                            <span class="w-addreviews-rating-item-brdr"><span class="w-reviews-rating-icon type-up"></span></span>
                        </label>
                        <label class="w-addreviews-rating-item">
                            <input  type="radio" name="addreviews_rating" value="down">
                            <span class="w-addreviews-rating-item-brdr"><span class="w-reviews-rating-icon type-down"></span></span>
                        </label>
                    </div>--}}

                    <div class="w-addreviews-message">
                        <div class="op-input type-btns" data-op-input="item">
                            <textarea name="addreviews_message" placeholder="Текст Вашего отзыва..." data-ntitle="Заполните поле: Текст Вашего отзыва..." data-autoheight="true" data-op-input="get"></textarea>
                        </div>
                    </div>
                </div>

                <div class="w-addreviews-button">
                        <div class="o-btn o-hvr" data-addreviews-form="submit">Добавить</div>
                </div>
            </div>
        </div>
    </div>


</div>



<script type="text/template" id="tpl-addreviews-item">
    <div class="w-reviews-item clearfix" data-wreviews="item">
        <div class="w-reviews-item-ava" style="background-image:url([photo]);"></div>
        <div class="w-reviews-item-profile">
            <div class="w-reviews-item-profile-name">
                <span class="w-reviews-item-profile-txt">[name]</span>
                <span class="w-reviews-item-profile-group">[group]</span>
            </div>
            <div class="w-reviews-item-profile-company">[company]</div>
            <div class="w-reviews-item-profile-date">[date]</div>
        </div>
        <div class="w-reviews-item-rating">
            <div class="w-reviews-rating-icon type-[rating]"></div>
        </div>
        <div class="w-reviews-item-content">
            <div class="w-reviews-item-message">
                <div class="w-reviews-item-message-inline" data-wreviews="edited">[message]</div>
            <span class="w-reviews-item-btns">
               <span class="w-reviews-item-btn type-edit" data-wreviews="btn_edit"></span>
               <span class="w-reviews-item-btn type-remove" data-wreviews="btn_delete"></span>
            </span>
            </div>
            <div class="w-reviews-item-order">(Заказ №<a href="[order_url]">[order_id]</a>)</div>
        </div>
    </div>
</script>