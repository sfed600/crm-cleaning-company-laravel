<div class="kp-order-form-wrap">
    {{--@if(Route::current()->getName() == 'admin.users.create')--}}
    @if($action == 'create')
        <form class="form-horizontal user" role="form" action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{csrf_token()}}">
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="company_id" value="{{old('company_id', @$user->company_id)}}">
    @elseif($action == 'show')
        <form form-user class="form-horizontal user" role="form" action="{{route('admin.users.update', $user->id)}}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input name="_token" type="hidden" value="{{csrf_token()}}">
            <input type="hidden" name="user_id" value="{{$user->user_id}}">
            <input type="hidden" name="company_id" value="{{old('company_id', @$user->company_id)}}">

        @if (old('phones'))
            @foreach(old('phones') as $key => $phone)
                @if (old('phone_ids')[$key])
                    <input type="hidden" name="phone_ids[]" value="{{old('phone_ids')[$key]}}">
                @endif
            @endforeach
        @else
            @forelse($user->phones as $key => $phone)
                <input type="hidden" name="phone_ids[]" value="{{$phone->id}}">
            @empty
            @endforelse
        @endif
        @endif
        <input type="hidden" name="user_self_id" value="{{@$user->id}}">
        {{--{{dump(old('city_work'))}}--}}
        <input type="hidden" name="str_city_work" value="{{old('str_city_work')}}">

        @if($action == 'create')
            <input type="hidden" name="fio" value="{{old('fio')}}">
        @elseif($action == 'show')
            <input type="hidden" name="fio" value="{{old('fio', (isset($user)) ? @$user->fio() : '')}}">
            {{--<input type="hidden" name="fio" @if($action == 'show') value="{{$user->fio()}}" @endif>--}}
        @endif

        @if($action == 'create')
            <input type="hidden" name="group_id" value="{{old('group_id', $groups['Сотрудник'])}}">
        @elseif($action == 'show')
            <input type="hidden" name="group_id" @if($action == 'show') value="{{old('group_id', $user->group->id)}}" @endif>
        @endif

        @if($action == 'create')
            <input type="hidden" name="profession_id" value="{{old('profession_id', $professions['Водитель'])}}">
        @elseif($action == 'show')
            <input type="hidden" name="profession_id" value="{{old('profession_id', @$user->profession_id)}}">
        @endif

        <div class="kp-order-form-row clearfix type-fulltime-worker">
    <span class="o-input-box-replaced brdr-gray">
    <input type="checkbox" name="staff" tabindex="0" data-item-check="current" @if(@$user->staff) checked @endif>
    <span class="o-input-box-replaced-check"></span>
    </span>
            <span class="txt-label">Штатный сотрудник (поставьте галочку, если сотрудник оформлен в вашу компанию и работает только у вас)</span>
        </div>

        <div class="p-worker-heads clearfix">
            <div class="p-worker-heads-fields">
                <div class="p-worker-work-info-cols">
                    {{--@if($action == 'show')--}}
                    <div class="p-worker-work-info-col col-1">
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Рейтинг:</div>
                            <div class="p-worker-work-info-item-box">@if($action == 'show') {{@$user->rating()}} @endif</div>
                        </div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Выходов:</div>
                            <div class="p-worker-work-info-item-box">{{@$user->orders_cnt}}</div>
                        </div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Выходов в вашу компанию:</div>
                            <div class="p-worker-work-info-item-box">{{@$cnt_turns_u_company}}</div>
                        </div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Опозданий:</div>
                            <div class="p-worker-work-info-item-box">{{@$user->orders_late}}</div>
                        </div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Неявок:</div>
                            <div class="p-worker-work-info-item-box">{{@$user->orders_nocome}}</div>
                        </div>
                    </div>
                    {{--@endif--}}

                    <div class="p-worker-work-info-col col-2">
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Дата крайнего выхода:</div>
                            <div class="p-worker-work-info-item-box">@if(null !== @$user && null !== @$user->days()->max('date')) {{date('d.m.Y', strtotime(@$user->days()->max('date')))}} @endif</div>
                        </div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Дата он-лайн:</div>
                            <div class="p-worker-work-info-item-box">@if(null !== @$user->last_visit->time_point) {{date('d.m.Y H:i', strtotime(@$user->last_visit->time_point))}} @endif</div>
                        </div>

                        <div class="p-worker-work-info-item type-empty"></div>
                        <div class="p-worker-work-info-item type-empty"></div>
                        <div class="p-worker-work-info-item type-inline">
                            <div class="op-input-label">Тип работы:</div>
                            <div class="p-worker-work-info-item-box">
                                <div class="op-input-box">
                                    <div class="o-form-select-chosen style-minimal js-select-schosen type-typework">
                                        <select name="sex" data-width="100%" data-placeholder="..." data-select-single="true">
                                            <option>Квартиры еженедельные</option>
                                            <option>Квартиры еженедельные2</option>
                                            <option>Квартиры еженедельные3</option>
                                            <option>Квартиры еженедельные4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-worker-work-info-col col-3">
                        <div class="p-worker-work-info-item type-inline type-sex">
                            <div class="op-input-label">Пол:</div>
                            <div class="p-worker-work-info-item-box">
                                <div class="op-input-box">
                                    <div class="o-form-select-chosen style-minimal js-select-schosen">
                                        <select name="gender" data-width="100%" data-placeholder="..." data-select-single="true">
                                            <option value="male" @if((old() && old('gender') == 'male') || (!old() && @$user->gender=='male')) selected @endif>Муж</option>
                                            <option value="female" @if((old() && old('gender') == 'female') || (!old() && @$user->gender=='female')) selected @endif>Жен</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-worker-heads-logo">
                <div class="p-worker-heads-photo-block type-uploads" data-worker-block-photo="wrap">
                    <div class="p-worker-heads-nophoto">
                        <label class="p-worker-heads-photo-add">
                            {{--<input type="file" data-worker-block-photo="upload" id="worker-block-photo-uploads">--}}
                            <input name="img" type="file" value="{{ old('img') }}" data-worker-block-photo="upload" id="worker-block-photo-uploads">
                        </label>
                    </div>
                    @if( empty($user->img) )
                        <a class="p-worker-heads-photo" style="background-image:url(/img/test.png)" href="/img/test.png" data-fancybox="gallery" data-worker-block-photo="image"></a>
                    @else
                        <a class="p-worker-heads-photo" style="background-image:url(/assets/imgs/users/{{$user->img}})" href="/assets/imgs/users/{{$user->img}}" data-fancybox="gallery" data-worker-block-photo="image"></a>
                    @endif
                </div>
            </div>
        </div>

        @php
            if( null == @$cities_work && null !== old('str_city_work')) {
                $old_cities_work = array();
                parse_str(old('str_city_work'), $old_cities_work);

                $cities_work = (object) array(
                    'district_work' => @$old_cities_work['district_work'],
                    'region_work' => @$old_cities_work['region_work'],
                    'city_work' => @$old_cities_work['city_work']
                );
            }
        @endphp

        <div class="p-worker-work-info-item type-regionwork">
            <div class="op-input-label">Регионы работы сотрудника:</div>
            <div class="op-input-box">
                <!-- Если выбран регион, то необходимо добавлять класс regioncheck-control-show к .regionwork-control-link -->
                <div class="regionwork-control-link" data-regioncheck-control>
        <span class="pseudo-link" data-regioncheck-open="#regioncheck">
           <span class="txt-label">Настроить</span>
           <span data-regioncheck-counter>({{sizeof(@$cities_work->city_work)}})</span>
        </span>
                    <span class="pseudo-link type-cancel" data-regioncheck-reset="#regioncheck">Сбросить</span>
                </div>
            </div>
        </div>
        {{--{{dd($cities_work)}}--}}
        <div class="display-none">
            <div class="regionwork-box" id="regioncheck">
                <div class="regionwork-box-list" data-regioncheck-wrap>
                    <div class="regionwork-box-quickly">
                        <div class="regionwork-box-quickly-title">Быстрый выбор:</div>
                        <div class="regionwork-box-quickly-item">
                            <span class="pseudo-link" data-regioncheck-quickly="#input-region-moscow">Москва и область</span>
                        </div>
                        <div class="regionwork-box-quickly-item">
                            <span class="pseudo-link" data-regioncheck-quickly="#input-region-sanktpiter">Санкт-Петербург и область</span>
                        </div>
                    </div>
                    <ul>
                        @php
                            /*if( null == @$cities_work && null !== old('city_work')) {
                                $old_cities_work = array();
                                parse_str(old('city_work'), $old_cities_work);

                                $cities_work = (object) array(
                                    'district_work' => $old_cities_work['district_work'],
                                    'region_work' => $old_cities_work['region_work'],
                                    'city_work' => $old_cities_work['city_work']
                                );
                            }*/

                            foreach($work_cities as $district) {
                                if( $district->region == null && $district->district == null ) {
                                    (isset($cities_work->district_work) && in_array($district->id, $cities_work->district_work)) ? $checked_district = ' checked ' : $checked_district = '';
                                    echo '<li>
                                        <div class="btn-regionwork-tree"></div>
                                        <label>
                                            <span class="o-input-box-replaced brdr-gray">
                                                <input type="checkbox" name="district_work[]" data-regioncheck="tree" value="'.$district->id.'"  '.$checked_district.'>
                                                <span class="o-input-box-replaced-check"></span>
                                            </span>
                                            <span>'.$district->name.'</span>
                                        </label>';
                                        foreach($work_cities as $region) {
                                            if( $region->region == null && $region->district == $district->id) {
                                                (isset($cities_work->region_work) && in_array($region->id, $cities_work->region_work)) ? $checked_region = ' checked ' : $checked_region = '';
                                                echo '
                                                    <ul>
                                                        <li>
                                                            <div class="btn-regionwork-tree"></div>
                                                            <label>
                                                                <span class="o-input-box-replaced brdr-gray">
                                                                <input type="checkbox" name="region_work[]" data-regioncheck="tree" value="'.$region->id.'" ';
                                                                /*if($region->id == 42 || $region->id == 43) {
                                                                    echo 'id="input-region-moscow"';
                                                                }*/
                                                                echo $checked_region.'>
                                                                <span class="o-input-box-replaced-check"></span></span>
                                                                <span>'.$region->name.'</span>
                                                            </label>
                                                            <ul>';
                                                            foreach($work_cities as $city) {
                                                                if( $city->region == $region->id && $city->district == null) {
                                                                (isset($cities_work->city_work) && in_array($city->id, $cities_work->city_work)) ? $checked_city = ' checked ' : $checked_city = '';
                                                                    echo '
                                                                        <li>
                                                                            <label>
                                                                                <span class="o-input-box-replaced brdr-gray">
                                                                                <input type="checkbox" name="city_work[]" data-regioncheck="city" value="'.$city->id.'"'.$checked_city.'>
                                                                                <span class="o-input-box-replaced-check"></span></span>
                                                                                <span>'.$city->name.'</span>
                                                                            </label>
                                                                        </li>';
                                                                }
                                                            }
                                                        echo '</ul></li>
                                                    </ul>';
                                            }
                                        }
                                        echo '</li>';
                                }
                            }
                        @endphp
                    </ul>
                    <div class="regionwork-box-bottom">
                        <div class="o-btn bgs-gray o-hvr" data-regioncheck-cancel>Отмена</div>
                        <div class="o-btn bgs-blue o-hvr" data-regioncheck-choose>Выбрать</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix">
            <div class="kp-order-box-toggle-fields-col-item">
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">Рабочий телефон:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input update-ajax data-jsmask="tel" type="text" placeholder="..." name="phone[{{@$user->phones[0]->id}}]" @if(@$user->phones) value="{{@$user->phones[0]->phone}}" @endif>
                        <input type="hidden" name="old_phone[{{@$user->phones[0]->id}}]" @if(@$user->phones) value="{{@$user->phones[0]->phone}}" @endif>
                    </div>
                </div>
            </div>
            <div class="kp-order-box-toggle-fields-col-item">
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">Мобильный:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input update-ajax data-jsmask="tel" type="text" placeholder="..." name="phone[{{@$user->phones[1]->id}}]" @if(@$user->phones) value="{{@$user->phones[1]->phone}}" @endif>
                        <input type="hidden" name="old_phone[{{@$user->phones[1]->id}}]" @if(@$user->phones) value="{{@$user->phones[1]->phone}}" @endif>
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix">
            <div class="kp-order-box-toggle-fields-col-item">
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">E-mail:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input type="email" name="email" placeholder="..." value="{{ old('email', @$user->email) }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-addressworker">
            <div class="kp-order-form-labinput">
                <div class="op-input-label">Адрес проживания:</div>
                <div class="kp-order-form-labinput-box">
                    <div class="op-input type-btns" data-op-input="item" data-openside>
                        <div class="op-input-control-btn" data-op-input="search">
                            <span class="icon-input-ctrl-search"></span>
                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                        </div>
                        {{--<input name="address" type="text" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" value="">--}}
                        <input id="suggest" name="address" type="text" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" value="{{ old('address', @$user->address) }}">
                        {{--<input id="suggest" type="text" name="address" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" value="{{ old('address', @$contact->address) }}">--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-addressdetail">
            <div class="kp-order-form-justify">
                <div class="kp-order-form-justify-ins">
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Квартира:</div>
                        <div class="op-input type-btns name_input-apartment" data-op-input="item">
                            <input type="text" placeholder="..." name="apartment" value="{{ old('apartment', @$user->apartment)}}" data-op-input="get" class="js-ajax-valid">
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Подъезд:</div>
                        <div class="op-input type-btns name_input-todrive" data-op-input="item">
                            <input type="text" placeholder="..." name="porch" value="{{ old('porch', @$user->porch)}}" data-op-input="get" class="js-ajax-valid">
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Этаж:</div>
                        <div class="op-input type-btns name_input-level" data-op-input="item">
                            <input type="text" placeholder="..." name="floor" value="{{ old('floor', @$user->floor)}}" data-op-input="get" class="js-ajax-valid">
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Код домофона:</div>
                        <div class="op-input type-btns name_input-code" data-op-input="item">
                            <input type="text" placeholder="..." name="domofon" value="{{ old('domofon', @$user->domofon)}}" data-op-input="get" class="js-ajax-valid">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-metro">
            <div class="op-input-label">Ближайшее метро:</div>
            <div class="op-input-box">
                <div class="o-form-select-chosen style-minimal js-select-schosen type-metro">
                    <select name="metro_id" data-width="100%" data-placeholder="@if( null !== @$user->metro ) {{@$user->metro->name}} @else Метро не выбрано @endif">
                        <option></option>
                        @foreach(\App\Models\Metro::all() as $metro)
                            <option value="{{$metro->id}}" @if(@$user->metro_id == $metro->id) selected @endif>{{$metro->name}}</option>
                        @endforeach
                        {{--<option>Авиамоторная</option>--}}
                    </select>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-htextarea">
            <div class="op-input-label">Примечание:</div>
            <div class="op-input type-btns" data-op-input="item">
                <textarea name="note" placeholder="..." data-autoheight="true" data-op-input="get">{{ old('note', @$user->note)}}</textarea>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-birthday">
            <div class="op-input-label">День рождения:</div>
            <label class="op-input type-icon">
                @if(intval(@$user->bithdate) > 0)
                    <input type="text" placeholder="..." name="bithdate" value="{{ old('bithdate', $user->datePicker())}}" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid" data-calendar-birthday="#birthday-result">
                @else
                    <input type="text" placeholder="..." name="bithdate" value="{{ old('bithdate')}}" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid" data-calendar-birthday="#birthday-result">
                @endif
                <span class="op-input-icon-date"></span>
            </label>

            <div class="worker-birthday-result">
                Возраст <span class="worker-birthday-result-value" id="birthday-result">@if($action == 'show') {{@$user->age()}} @endif</span>
            </div>
        </div>


        <div class="kp-order-form-row clearfix type-citizenship">
            <div class="op-input-label">Гражданство:</div>
            <div class="op-input-box">
                <div class="o-form-select-chosen style-minimal js-select-schosen type-citizenship">
                    <select name="citizenship" data-width="100%" data-placeholder="..." data-select-single="true">
                        <option></option>
                        @foreach(\App\User::$citizenships as $citizenship)
                            <option value="{{$citizenship}}" @if((old() && old('citizenship')==$citizenship) || (!old() && mb_strtolower(@$user->citizenship) == mb_strtolower($citizenship)) ) selected="selected" @endif>{{$citizenship}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>


        <div class="kp-order-form-row clearfix type-pasport">
            <div class="op-input-label">Паспортные данные:</div>
            <div class="op-input type-btns" data-op-input="item">
                <textarea placeholder="..." data-autoheight="true" data-op-input="get" name="passport">{{ old('passport', @$user->passport) }}</textarea>
            </div>
        </div>

        <div class="kp-order-form-row clearfix">
            <div class="p-worker-card-col" data-input-card="wrap">
                <div class="op-input-label">
        <span class="o-input-box-replaced">
            <input type="checkbox" tabindex="0" data-item-check="current" @if( null !== @$sberbank ) checked @endif>
            <span class="o-input-box-replaced-check"></span>
        </span>
                    Карта сбербанка:
                </div>
                <div class="op-input type-btns" data-op-input="item">
                    <input type="text" placeholder="..." name="sberbank" value="{{@$sberbank->number}}" data-op-input="get" data-length="sberbank" data-ntitle="Карта сбербанка" @if( null == @$sberbank ) readonly @endif>
                </div>
            </div>
            <div class="p-worker-card-col" data-input-card="wrap">
                <div class="op-input-label">
        <span class="o-input-box-replaced brdr-gray">
            <input type="checkbox" tabindex="0" data-item-check="current" @if( null !== @$tinkoff ) checked @endif>
            <span class="o-input-box-replaced-check"></span>
        </span>
                    Карта Тинькофф:
                </div>
                <div class="op-input type-btns" data-op-input="item">
                    <input type="text" placeholder="..." name="tinkoff" value="{{@$tinkoff->number}}" data-op-input="get" data-length="tinkoff" data-ntitle="Карта Тинькофф" @if( null == @$tinkoff ) readonly @endif >
                </div>
            </div>
        </div>

        <div class="kp-order-boxdata clearfix" style="display: block">
            <div class="kp-order-boxdata-title">
                <div class="op-input-label">Компания</div>
                <div class="op-input type-btns" data-op-input="item">
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <div class="op-input-control-btn" data-op-input="add" id="js-kp-order-boxdata-company-addnew" {{--onclick="create_order_company();"--}}>
                        <span class="icon-input-ctrl-add"></span>
                        <span class="icon-input-ctrl-add-h btn-hover"></span>
                    </div>
                    <input context="contact" type="text" placeholder="..." data-op-input="get" id="js-boxdata-user-company-search">
                </div>
            </div>

            <script type="text/template" id="js-kp-order-boxdata-company-template">
                <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>
                    <input type="hidden" data-name="id">

                    <div class="kp-order-box-toggle-fields-row type-company">
                        <div class="op-input type-btns type-company" data-op-input="item">
                            {{--<input type="text" placeholder="..." data-op-input="get" data-name="name" value="">--}}
                            <input type="text" placeholder="..." value="">
                        </div>
                    </div>

                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Сайт:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            {{--<input name="site" type="text" placeholder="..." data-op-input="get" data-name="site" value="{{@$user->company->site}}" readonly>--}}
                            <input type="text" placeholder="..." value="{{@$user->company->site}}" readonly>
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Телефон:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            {{--<input name="phone" data-jsmask="tel" type="text" placeholder="..." data-name="phone" value="{{@$user->company->phone}}" readonly>--}}
                            <input data-jsmask="tel" type="text" placeholder="..." value="{{@$user->company->phone}}" readonly>
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">E-mail:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            {{--<input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="{{@$user->company->email}}" readonly>--}}
                            <input type="text" placeholder="..." data-op-input="get" value="{{@$user->company->email}}" readonly>
                        </div>
                    </div>
                </div>
            </script>

            <div id="js-kp-order-boxdata-company-insert">
                @if(isset($user->company))
                    <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>
                        <div class="kp-order-box-toggle-fields-row type-company">
                            <div class="op-input type-btns type-company" data-op-input="item">
                                {{--<input type="text" placeholder="..." data-op-input="get" data-name="name" value="{{$user->company->name}}">--}}
                                <input type="text" value="{{$user->company->name}}" readonly>
                            </div>

                            {{--<div class="op-input-control-btn" data-op-input="clear" style="cursor:pointer; z-index: 600;">
                                <span class="icon-input-ctrl-clear"></span>
                                <span class="icon-input-ctrl-clear-h btn-hover"></span>
                            </div>--}}

                        </div>

                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Сайт:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                {{--<input name="site" type="text" placeholder="..." data-op-input="get" data-name="www" value="{{@$user->company->site}}" readonly>--}}
                                <input type="text" value="{{@$user->company->site}}" readonly>
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Телефон:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                {{--<input name="phone" data-jsmask="tel" type="text" placeholder="..." value="{{@$user->company->phone}}" readonly>--}}
                                <input type="text" placeholder="..." value="{{@$user->company->phone}}" readonly>
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">E-mail:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                {{--<input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="{{@$user->company->email}}" readonly>--}}
                                <input type="text" placeholder="..." value="{{@$user->company->email}}" readonly>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-person">
            <div class="kp-order-form-labinput">
                <div class="op-input-label">Ответственный:</div>
                <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                    {{--<div class="op-input type-btns" data-op-input="item">
                        <input type="text" placeholder="..." value="" data-user_block="Management" data-op-input="person-search">
                    </div>--}}

                    @if( Route::current()->getName() == "admin.users.show" )
                        @if( @$user->user_id > 0 )
                            <div class="kp-order-form-person-item" data-persons="item">
                                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.\App\User::find($user->user_id)->img}})"></div>
                                <div class="kp-order-form-person-item-name">{{\App\User::find($user->user_id)->fio()}}</div>
                                {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{$company->user->id}}"></div>--}}
                            </div>
                        @else
                            <div class="kp-order-form-person-item" data-persons="item">
                                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.Auth::user()->img}})"></div>
                                <div class="kp-order-form-person-item-name">{{Auth::user()->fio()}}</div>
                            </div>
                        @endif
                    @elseif( Route::current()->getName() == "admin.users.create" )
                        <div class="kp-order-form-person-item" data-persons="item">
                            <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.Auth::user()->img}})"></div>
                            <div class="kp-order-form-person-item-name">{{Auth::user()->fio()}}</div>
                            {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{Auth::user()->id}}"></div>--}}
                        </div>
                    @endif

                </div>
            </div>
        </div>


        <div class="page-kp-form-bottom-button type-right">
            {{--<span class="page-kp-form-button bgs-green">Сохранить</span>--}}
            <input class="page-kp-form-button bgs-green" value="Сохранить" type="submit">
            {{--<span class="page-kp-form-button bgs-orange" onclick="aply_user_changes();">Обновить</span>--}}
        </div>

    </form>
    {{--@endif--}}
</div>