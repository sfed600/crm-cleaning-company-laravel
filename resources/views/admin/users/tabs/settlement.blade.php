<div class="worker-calcs-wrap">
    {{--{{dd($dates)}}--}}
    <div class="worker-calcs-range-choise">
        <div class="worker-calcs-range-choise-item @if( @$filters['period'] == 'current_week') current @endif" onclick="user_calculation_period('current_week', this);">текущая неделя</div>
        <div class="worker-calcs-range-choise-item @if( @$filters['period'] == 'last_week') current @endif" onclick="user_calculation_period('last_week', this);">прошлая неделя</div>
        <div class="worker-calcs-range-choise-item @if( @$filters['period'] == 'current_month' || !@$filters['period']) current @endif" onclick="user_calculation_period('current_month', this);">текущий месяц</div>
        <div class="worker-calcs-range-choise-item @if( @$filters['period'] == 'last_month') current @endif" onclick="user_calculation_period('last_month', this);">прошлый месяц</div>
        <div class="worker-calcs-range-choise-item-input">
            {{--<input type="text" value="03.03.2018-03.04.2018">--}}
            {{--<input type="text" class="datepicker" name="f[daterange]" value="{{@$filters['daterange']}}">--}}
            <input type="text" class="datepicker" name="f[daterange]" value="@if(null !== app('request')->input('daterange')) {{ app('request')->input('daterange') }} @else {{@$filters['daterange']}} @endif">
        </div>
    </div>


    <div class="dash-content">
        <div class="dash-table table-worker-calcs" data-item-check-worker="wrap">
            <table>
                <colgroup>
                    @can('income', new App\User())
                        <col class="tbl-check">
                    @endcan
                    <col class="tbl-date">
                    <col class="tbl-shifts">
                    <col class="tbl-charged">
                    <col class="tbl-paid">
                    <col class="tbl-order">
                    <col class="tbl-typepay">
                    <col class="tbl-comment">
                </colgroup>
                <thead>
                <tr>
                    <td class="tbl-check">
                        @can('income', new App\User())
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check-worker="all-current" data-worker-calcs="allow"><span class="o-input-box-replaced-check"></span></label>
                        @endcan
                    </td>
                    <td class="tbl-date">Дата</td>
                    <td class="tbl-shifts">Кол-во<br> смен</td>
                    <td class="tbl-charged">Начислено</td>
                    <td class="tbl-paid">Выплачено</td>
                    <td class="tbl-order">№ заказа</td>
                    <td class="tbl-typepay">Вид<br> оплаты</td>
                    <td class="tbl-comment">Комментарий</td>
                </tr>
                </thead>
                <tbody>
                @can('income', new App\User())
                    <tr class="tbl-addheader" id="insert-worker-calcs-row">
                        <td class="tbl-check"><span class="btn-worker-calcs-addrow o-hvr" data-addtbl-worker="#tpl-worker-calcs-row" data-addtbl-worker-insert="#insert-worker-calcs-row"></span></td>
                        <td class="tbl-date"><span class="worker-calcs-addrow-input"><input type="text" data-jsmask="date" data-kpcalendar="date" data-input="date"></span></td>
                        <td class="tbl-shifts"></td>
                        <td class="tbl-charged"><span class="worker-calcs-addrow-input"><input type="text" data-jsmask="num_charged" data-input="charged" data-worker-copyvalueto="#input-worker-paid"></span></td>
                        <td class="tbl-paid"><span class="worker-calcs-addrow-input"><input type="text" data-jsmask="num_charged" data-input="paid" id="input-worker-paid"></span></td>
                        <td class="tbl-order"></td>
                        <td class="tbl-typepay">
                            <div class="o-form-select-chosen style-minimal js-select-schosen">
                                <select data-select-single="true" data-placeholder="Выберите" data-input="typepay">
                                    <option></option>
                                    <option value="advance">Аванс</option>
                                    <option value="penalty">Штраф</option>
                                    <option value="bonus">Бонус</option>
                                </select>
                            </div>
                        </td>
                        <td class="tbl-comment"><span class="worker-calcs-addrow-input"><input type="text" data-input="comment"></span></td>
                    </tr>
                @endcan

                {{--@foreach($dates->sortBy('date') as $date)--}}
                @foreach($dates as $date)
                    <tr data-worker-calcs="item" data-status="nopaid" data-incomes_id="{{@$date->id}}">
                        @can('income', new App\User())
                            <td class="tbl-check">
                                @if( intval($date->amount) != intval($date->outcome) )
                                    <label class="o-input-box-replaced">
                                        <input type="checkbox" name="items[]" tabindex="0" data-item-check-worker="current" data-worker-calcs="allow">
                                        <span class="o-input-box-replaced-check"></span>
                                    </label>
                                @endif
                            </td>
                        @endcan
                        <td class="tbl-date">{{date('d.m.Y', strtotime($date->date))}}</td>
                        <td class="tbl-shifts">1</td>
                        {{--<td class="tbl-charged"><span data-worker-calcs="value">{{ ($date->amount > 0) ? $date->amount : $date->outcome}}</span></td>--}}
                        <td class="tbl-charged"><span data-worker-calcs="value">{{ $date->amount }}</span></td>
                        <td class="tbl-paid">
                            <span class="js-worker-edit" data-input-title="Выплачено" data-input-name="paid" data-jsmask="num_charged">
                                <span class="input-value-empty-dotts"></span>
                                {{$date->outcome}}
                            </span>
                        </td>
                        {{--<td class="tbl-paid"><span>{{$date->outcome}}</span></td>--}}
                        <td class="tbl-order">
                            @if( isset($date->order_id) )
                                <a href="{{route('admin.orders.edit', $date->order_id)}}">#{{$date->order_number}}</a>
                            @endif
                        </td>
                        <td class="tbl-typepay">
                            {{--<div class="o-form-select-chosen style-minimal js-select-schosen">
                                <select data-select-single="true" data-placeholder="Выберите" data-input="typepay" data-worker-calcs="typepay">
                                    <option></option>
                                    <option value="wages" @if(!$date->type || $date->type == 'wages') selected @endif>Зарплата</option>
                                    <option value="penalty" @if($date->type == 'penalty') selected @endif>Штраф</option>
                                    <option value="bonus" @if($date->type == 'bonus') selected @endif>Бонус</option>
                                    <option value="advance" @if($date->type == 'advance') selected @endif>Аванс</option>
                                </select>
                            </div>--}}
                            @if(isset($date->type)) {{\App\Models\UserIncome::$types[$date->type]}} @else Зарплата @endif
                        </td>
                        {{--<td class="tbl-comment"><span class="js-worker-edit" data-input-title="Комментарий"><span class="input-value-empty-dotts"></span></span></td>--}}
                        <td class="tbl-comment"><span>{{$date->comment}}</span></td>
                    </tr>
                @endforeach

                @if(!empty($dates))
                    <tr class="table-worker-calcs-total">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{@$total_amount}}</td>
                        <td>{{@$total_outcome}}</td>
                        <td></td>
                        <td class="table-worker-calcs-total-label">Итого:</td>
                        <td>{{@$total_amount - @$total_outcome}}</td>
                    </tr>
                @endif
                <tr class="table-worker-calcs-total">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="table-worker-calcs-total-label">Баланс:</td>
                    <td>{{$balance}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="worker-calcs-bottom-topaid display-none" data-worker-calcs="total">
        <div class="worker-calcs-bottom-topaid-total">Вы хотите выплатить <span data-worker-calcs="total_counter"></span> заказ(а) на сумму <span data-worker-calcs="total_sum"></span> руб.</div>
        <div class="worker-calcs-bottom-topaid-button">
            <button class="o-btn" onclick="payment_transaction();">Выплатить</button>
        </div>
    </div>
</div>

<script type="text/template" id="tpl-worker-calcs-row">
    {{--<tr data-worker-calcs="item" data-status="{item_status}" data-btn-worker-loadr>--}}
    <tr data-worker-calcs="item" data-status="nopaid" data-btn-worker-loadr>
        <td class="tbl-check">
            <label class="o-input-box-replaced">
                <input type="checkbox" name="items[]" tabindex="0" data-item-check-worker="current" data-worker-calcs="allow">
                <span class="o-input-box-replaced-check"></span>
            </label>
        </td>
        <td class="tbl-date">{date}</td>
        <td class="tbl-shifts">1</td>
        <td class="tbl-charged"><span data-worker-calcs="value" data-jsmask="num_charged">{charged}</span></td>
        <td class="tbl-paid"><span class="js-worker-edit" data-input-title="Выплачено" data-input-name="paid">{paid}</span></td>
        {{--<td class="tbl-order"><a href="#">{id}</a></td>--}}
        <td class="tbl-order">
            {{--<a href="#"></a>--}}
        </td>
        <td class="tbl-typepay">
            <div class="o-form-select-chosen style-minimal js-select-schosen">
                <select data-select-single="true" data-placeholder="Не выбрано" data-input="typepay" data-worker-calcs="typepay">{typepay}</select>
            </div>
        </td>
        <td class="tbl-comment"><span data-worker-calcs="value" class="js-worker-edit" data-input-title="Комментарий">{comment}</span></td>
    </tr>
</script>