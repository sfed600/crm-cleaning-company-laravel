@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.users.index')}}">Сотрудники</a>
            </li>
            <li class="active">Взаимозачеты сотрудника</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Сотрудники
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Взаимозачеты
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">

                <div class="">
                    <div id="user-profile-2" class="user-profile ace-thumbnails">
                        <div class="tabbable">
                            @include('admin.users.profile', ['user' => $user])

                            <ul class="nav nav-tabs padding-18">
                                <li class="">
                                    <a href="{{route('admin.users.show', $user->id)}}">
                                        <i class="green ace-icon glyphicon glyphicon-refresh bigger-120"></i>
                                        Взаимозачеты
                                    </a>
                                </li>
                                @can('income', new App\User())
                                <li class="">
                                    <a href="{{route('admin.users.outcome', $user->id)}}">
                                        <i class="orange ace-icon glyphicon glyphicon-repeat bigger-120"></i>
                                        Выплаты
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{route('admin.users.income', $user->id)}}" >
                                        <i class="blue ace-icon glyphicon glyphicon-euro bigger-120"></i>
                                        Зарплата
                                    </a>
                                </li>
                                @endcan
                                <li class="active">
                                    <a href="{{route('admin.users.comments', $user->id)}}" >
                                        <i class="blue ace-icon fa fa-comments bigger-120"></i>
                                        Отзывы
                                    </a>
                                </li>
                            </ul>

                            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                                    <div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													<i class="ace-icon fa fa-comment blue"></i>
													Отзывы
												</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main no-padding">
													<div class="dialogs ace-scroll"><div class="scroll-track scroll-active" style="display: block; height: 300px;"><div class="scroll-bar" style="height: 262px; top: 0px;"></div></div><div class="scroll-content" style="max-height: 300px;">
													    @foreach($comments as $comment)
														<div class="itemdiv dialogdiv">
															<div class="user">
																<img alt="Alexa's Avatar" src="/assets/admin/avatars/avatar2.png">
															</div>
															<div class="body">
																<div class="time">
																	<i class="ace-icon fa fa-clock-o"></i>
																	<span class="green">{{$comment->dateHuman()}}</span>
																</div>

																<div class="name">
																	<a href="#">{{$comment->userAdd->fio()}}</a>
																</div>
																<div class="text">{!! nl2br($comment->comment) !!}</div>

															</div>
														</div>
														@endforeach
														</div>
													</div></div>

													<form class="form-horizontal" method="post" action="{{route('admin.users.comments.store', $user->id)}}">
													    <input name="_token" type="hidden" value="{{csrf_token()}}">

														<div class="form-actions">
															<div>
																<textarea name="comment" placeholder="Оставьте свой отзыв"  class="form-control" class="col-sm-8" style="width: 100%"></textarea>

																<div class="space space-2"></div>

																<span class="input-group-btn">
																	<button class="btn btn-sm btn-info no-radius" type="submit">
																		<i class="ace-icon fa fa-share"></i>
																		Отправить
																	</button>
																</span>
															</div>
														</div>
													</form>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div>
                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
