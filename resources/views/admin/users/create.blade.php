@extends('admin.layout')
@section('main')
    @include('admin.message')

    {{--for ymap--}}
    <script src="/assets/admin_new/js/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=onLoad"></script>
    <script src="/assets/admin_new/js/ymaps.js?v=48"></script>
    <script>
        function onLoad (ymaps) {
            var suggestView = new ymaps.SuggestView('suggest');
        }
    </script>

    {{--{{dd($professions)}}--}}
    <div class="dash-container">
        <div class="dash-kp-line type-fleft">
            <ul class="dash-kp-line-list" data-kp-step="wrap">
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Водитель']}}'); $('[name=\'group_id\']').val('{{$groups['Сотрудник']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Водитель']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-cadetblue"></span><span class="btn-bgs bgs-hover bgs-cadetblue"></span><span class="txt-label">Водитель</span></span></li>
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Бригадир']}}'); $('[name=\'group_id\']').val('{{$groups['Сотрудник']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Бригадир']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-lightcherrycraiola"></span><span class="btn-bgs bgs-hover bgs-lightcherrycraiola"></span><span class="txt-label">Бригадир</span></span></li>
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Уборщик']}}'); $('[name=\'group_id\']').val('{{$groups['Сотрудник']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Уборщик']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-yellowivory"></span><span class="btn-bgs bgs-hover bgs-yellowivory"></span><span class="txt-label">Уборщик</span></span></li>
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Менеджер']}}'); $('[name=\'group_id\']').val('{{$groups['Модератор']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Менеджер']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-slateblue"></span><span class="btn-bgs bgs-hover bgs-slateblue"></span><span class="txt-label">Менеджер</span></span></li>
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Оператор']}}'); $('[name=\'group_id\']').val('{{$groups['Модератор']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Оператор']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-rawguard"></span><span class="btn-bgs bgs-hover bgs-rawguard"></span><span class="txt-label">Оператор</span></span></li>
                <li><span onclick="$('[name=\'profession_id\']').val('{{$professions['Руководитель']}}'); $('[name=\'group_id\']').val('{{$groups['Адмиинистратор']}}');" class="dash-kp-line-btn @if(@$user->profession_id == $professions['Руководитель']) current @endif" data-kp-step="btn"><span class="btn-bgs bgs-lightcherrycraiola"></span><span class="btn-bgs bgs-hover bgs-lightcherrycraiola"></span><span class="txt-label">Руководитель</span></span></li>
            </ul>
        </div>

        <div class="dash-content">
            <div id="dash-kp">
                <ul class="dash-kp-nav">
                    <li><a href="#" data-tab=".dash-wrap" data-findtab="kp-tabbox-main" class="current">Основное</a></li>
                    {{--<li><a href="#" data-tab=".dash-wrap" data-findtab="kp-tabbox-calculations">Взаиморасчеты</a></li>
                    <li><a href="#">Статистика</a></li>
                    <li><a href="#" data-tab=".dash-wrap" data-findtab="kp-tabbox-reviews">Отзывы</a></li>--}}
                </ul>


                <div class="dash-kp-content">
                    <div class="dash-kp-col col-left">
                        <div class="o-tabs-box kp-tabbox-main current" data-tab-box>
                            @include('admin.users.tabs.main')
                        </div>
                        {{--<div class="o-tabs-box kp-tabbox-calculations" data-tab-box>
                            @include('admin.users.tabs.settlement')
                        </div>
                        <div class="o-tabs-box kp-tabbox-reviews" data-tab-box>
                            @include('admin.users.tabs.reviews')
                        </div>--}}
                    </div>
                    <div class="dash-kp-col col-right">
                        <div class="o-tabs-box kp-tabbox-main kp-tabbox-calculations kp-tabbox-reviews current" data-tab-box>
                            <div class="kp-system-messages-control">
                                <label class="kp-system-messages-control-item">
                                    <span class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></span>
                                    <span class="txt-label">Скрыть системные сообщения</span>
                                </label>
                            </div>

                            <div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа</div>
                            </div>

                            <div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">
                                    03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа
                                    <div class="kp-history-item-box">
                                        <div class="kp-history-item-box-heads">03.08.2017 15:53 Олег Клочков</div>
                                        СДЛорвдмалвадомивжлмовждмдвлаьмидлвьидлваиавдивэваиваливдлаи
                                        ваиваиваиваиваиваиваиваиваивчыиачвичвичвичв
                                        чвамивчаичваичвичвичвяивчиаяваияви
                                    </div>
                                </div>
                            </div>

                            <div class="kp-note-form">
                                <div class="kp-note-form-infos">
                                    <div class="kp-note-form-infos-text">Нет запланированных встреч, рекомендуем добавить</div>
                                    <div class="kp-note-form-infos-angle"></div>
                                </div>
                                <label class="kp-note-form-text">
                                    <textarea required></textarea>
                                    <span class="kp-note-form-text-notes"><a href="#">Примечание:</a> Введите текст</span>
                                </label>
                            </div>
                            <div class="kp-note-form-empty"></div>
                        </div>

                        {{--<div class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                            <iframe src="https://yandex.ua/map-widget/v1/-/CBuEIOuF9C" width="560" height="400" frameborder="1" allowfullscreen="true"></iframe>
                        </div>--}}
                        <div style="top:50px" class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>

                        <div class="kp-worker-position" id="side-worker-calculations">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>
@stop