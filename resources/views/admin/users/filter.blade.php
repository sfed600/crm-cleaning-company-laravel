<form id="users_filter_form" class="dash-search" data-dash-search="wrap" method="GET" action="{{route('admin.users.index')}}">
    <input type="hidden" name="f[perpage]" value="{{@$filters["perpage"]}}">
    <div class="dash-search-option">{{--{{dd($filters["fio"])}}--}}
        {{--@if( isset($filters["profession"]) && !empty($filters["profession"]) )
            <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                {{$filters["profession"]}}
                <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'profession', 'personal');"></span>
            </span>
        @endif--}}
        @if(isset($filters))
            @if( isset($filters["profession"]) && intval($filters["profession"]) > 0 )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{@$professions[intval(@$filters['profession']) - 1]->name}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'profession', 'users');"></span>
                </span>
            @endif
            @if( isset($filters["fio"]) && !empty($filters["fio"]) )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{@$filters['fio']}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'id', 'users');"></span>
                </span>
            @endif
            {{--@if( isset($filters["phone"]) && $filters["phone"] != '' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{@$filters['phone']}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'phone', 'tabel');"></span>
                </span>
            @endif--}}
        @endif
    </div>
    <div class="dash-search-form">
        <div class="dash-search-form-input"><input type="text" placeholder="+ поиск" data-dash-search="input" data-layer="yes"><div class="dash-search-form-hover"></div></div>
        <label class="dash-search-form-btn type-submit"><input type="submit" value=" "></label>
        <label class="dash-search-form-btn type-remove"><input type="reset" value=" "></label>
    </div>
    <div class="dash-search-drop" data-dash-search="drop">
        <div class="dash-search-drop-ins">
            {{--<div class="dash-search-drop-left">
                <ul class="dash-search-drop-list">
                    <li onclick="set_tabel_filter_period('yesterday', this);" @if( @$filters['period'] == 'yesterday') class="current" @endif>Вчера</li>
                    <li onclick="set_tabel_filter_period('today', this);" @if( @$filters['period'] == 'today') class="current" @endif>Сегодня</li>
                    <li onclick="set_tabel_filter_period('current_week', this);" @if( @$filters['period'] == 'current_week') class="current" @endif>Текущая неделя</li>
                    <li onclick="set_tabel_filter_period('last_week', this);" @if( @$filters['period'] == 'last_week') class="current" @endif>Прошлая неделя</li>
                    <li onclick="set_tabel_filter_period('current_month', this);" @if( @$filters['period'] == 'current_month') class="current" @endif>Текущий месяц</li>
                    <li onclick="set_tabel_filter_period('last_month', this);" @if( @$filters['period'] == 'last_month') class="current" @endif>Прошлый месяц</li>
                </ul>
            </div>--}}
            <div class="dash-search-drop-right">
                <div class="dash-search-drop-form">
                    {{--<div class="o-form-row">
                        <div class="o-form-row-label">Специальность</div>
                        <div class="o-form-row-input">
                            <select class="selectpicker" data-width="100%" name="f[profession]">
                                <option value="">-- Не выбрана --</option>
                                @if(null !==@$professions)
                                    @forelse(@$professions as $profession)
                                        <option value="{{$profession->id}}" @if(isset($filters['profession']) &&  @$filters['profession']== $profession->id) selected="selected" @endif>{{$profession->name}}</option>
                                    @empty
                                    @endforelse
                                @endif
                            </select>
                        </div>
                    </div>--}}

                    <div class="o-form-row">
                        <div class="o-form-row-label">ФИО</div>
                        <div class="o-form-row-input">
                            <select id="user_id" class="chosen-select chosen-autocomplite-base col-sm-8 chosen-select chosen-autocomplite-base" data-url="{{route('admin.users.search')}}" data-placeholder="Начните ввод..." data-width="100%" name="f[id]">
                                <option value="{{$filters['id'] or ''}}">{{$filters['fio'] or ' '}}</option>
                            </select>
                        </div>
                    </div>

                    {{--<div class="o-form-row">
                        <div class="o-form-row-label">Телефон</div>
                        <div class="o-form-row-input">
                            <input type="text" data-jsmask="tel" name="f[phone]" value="{{@$filters['phone']}}">
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Выбор периода</div>
                        <div class="o-form-row-input">
                            <input type="text" class="datepicker" name="f[daterange]" value="{{@$filters['daterange']}}">
                        </div>
                    </div>--}}

                    <div class="o-form-bottom">
                        <input type="submit" value="Найти">
                        <input type="reset" value="Сбросить" onclick="reset_filter('users')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

