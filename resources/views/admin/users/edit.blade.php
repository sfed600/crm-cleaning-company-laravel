@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.users.index')}}">Сотрудники</a>
            </li>
            <li class="active">Редактирование сотрудника</li>
        </ul><!-- /.breadcrumb -->
    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Сотрудники
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Редактирование
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal user" role="form" action="{{route('admin.users.update', $user->id)}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">

                   <div class="form-group">
                       <label class="col-sm-3 control-label no-padding-right" for="form-field-20"> Группа </label>
                       <div class="col-sm-9">
                           <select name="group_id" id="form-field-20">
                               <option value="">--Не выбрана--</option>
                               @foreach($groups as $group)
                               @if($group->name == 'SuperAdmin')
                                   @can('superAdmin', new App\User())
                                   <option value="{{$group->id}}" data-group="{{$group->name}}" data-block="{{$group->block->name}}" @if((old() && old('group_id')==$group->id) || (!old() && $user->group_id==$group->id))selected="selected"@endif>{{$group->name_rus}} ({{$group->block->name_rus}})</option>
                                   @endcan
                               @else
                                    <option value="{{$group->id}}" data-group="{{$group->name}}" data-block="{{$group->block->name}}" @if((old() && old('group_id')==$group->id) || (!old() && $user->group_id==$group->id))selected="selected"@endif>{{$group->name_rus}} ({{$group->block->name_rus}})</option>
                               @endif
                               @endforeach
                           </select>
                       </div>
                   </div>

                   @can('index', new App\Models\Company())
                   <div id="company" class="form-group" style="display: none;">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-201"> Компания </label>
                        <div class="col-sm-9">
                            <select name="company_id" id="form-field-201">
                                <option value="">--Не выбрана--</option>
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}" @if((old() && old('company_id')==$company->id) || (!old() && $user->company_id==$company->id))selected="selected"@endif>{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endcan

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-32"> Специальности </label>
                        <div class="col-sm-9">
                            <select multiple="" name="professions[]" class="chosen-select form-control tag-input-style" id="form-field-32" data-placeholder="Выберите специальность...">
                                @foreach($professions as $profession)
                                    <option value="{{$profession->id}}" @if((old() && old('professions') && in_array($profession->id, old('professions'))) || (!old() && $user->professions->count() && $user->professions->find($profession->id)))selected="selected"@endif>{{$profession->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-10"> Фамилия </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-10" name="surname" placeholder="Фамилия" value="{{ old('surname', $user->surname) }}" class="col-sm-12">
                        </div>
                    </div>

                   <div class="form-group">
                       <label class="col-sm-3 control-label no-padding-right" for="form-field-0"> Имя </label>
                       <div class="col-sm-9">
                           <input type="text" id="form-field-0" name="name" placeholder="Имя" value="{{ old('name', $user->name) }}" class="col-sm-12">
                       </div>
                   </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-11"> Отчество </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-11" name="patronicname" placeholder="Отчество" value="{{ old('patronicname', $user->patronicname) }}" class="col-sm-12">
                        </div>
                    </div>


                   <div class="form-group">
                       <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> E-mail </label>
                       <div class="col-sm-9">
                           <input type="email" id="form-field-1" name="email" placeholder="E-mail" value="{{ old('email', $user->email) }}" class="col-sm-12">
                       </div>
                   </div>

                   <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> &nbsp;</label>
                        <div class="col-xs-12 col-sm-4">
                        <div class="widget-box collapsed">
                            <div class="widget-header">
                                <h4 class="widget-title">Изменить пароль</h4>

                                <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                        <i class="ace-icon fa fa-chevron-down"></i>
                                    </a>

                                </div>
                            </div>

                            <div class="widget-body" style="display: none; height: 148px">
                                <div class="widget-main">
                                    <div>
                                        <label for="form-field-8">Пароль</label>

                                        <input type="password" id="form-field-2" name="password" placeholder="Пароль" value="" class="col-sm-12">
                                    </div>

                                    <div>
                                        <label for="form-field-9">Повторите пароль</label>

                                        <input type="password" id="form-field-3" name="password_confirmation" placeholder="Повторите пароль" value="" class="col-sm-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-12"> Пол </label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input name="gender" type="radio" class="ace" value="male" @if((old() && old('gender') == 'male') || (!old() && $user->gender=='male')) checked="checked" @endif>
                                    <span class="lbl"> Мужчина</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="gender" type="radio" class="ace" value="female" @if((old() && old('gender') == 'female') || (!old() && $user->gender=='female')) checked="checked" @endif>
                                    <span class="lbl"> Женщина</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-13"> Фото </label>
                        <div class="col-sm-9">
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="myTab">
                                    @if($user->img)
                                        <li class="active">
                                            <a data-toggle="tab" href="#field-img-now" aria-expanded="false">
                                                Текущее
                                            </a>
                                        </li>
                                    @endif
                                    <li @if(!$user->img) class="active" @endif>
                                        <a data-toggle="tab" href="#field-img-new" aria-expanded="true">
                                            Новое
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    @if($user->img)
                                        <div id="field-img-now" class="tab-pane fade active in">
                                            <ul class="ace-thumbnails clearfix">
                                                <li>
                                                    <a href="/{{$user->getImgPath().$user->img}}"  data-rel="colorbox" class="cboxElement">
                                                        <img  src="/{{$user->getImgPreviewPath().$user->img}}">
                                                    </a>
                                                    <div class="tools">
                                                        <a href="{{route('admin.image.crop', ['img' => '/'.$user->getImgPath().$user->img, 'preview' => '/'.$user->getImgPreviewPath().$user->img, 'width' => $user->getPreviewSize('width'), 'height' => $user->getPreviewSize('height')])}}" title="Изменить">
                                                            <i class="ace-icon glyphicon glyphicon-camera"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                    <div id="field-img-new" class="tab-pane fade @if(!$user->img) active in @endif">
                                        <input name="img" type="file" class="img-drop" accept="image/*" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="auto" @if(!$user->professions->count() || !$user->professions->where('name','Водитель')->count()) style="display: none" @endif>
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-151"> Автомобиль </label>
                        <div class="col-sm-9">
                            <select name="auto_id" id="form-field-151">
                                <option value="">--Не выбрано--</option>
                                @foreach($autos as $auto)
                                    <option value="{{$auto->id}}" @if((old() && old('auto_id')==$auto->id) || (!old() && $user->auto_id == $auto->id) ) selected="selected" @endif>{{$auto->number}} ({{$auto->model->name}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-14"> Кредитная карта </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-14" name="card" placeholder="Кредитная карта" value="{{ old('card', $user->card) }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-15"> Гражданство </label>
                        <div class="col-sm-9">
                            <select name="citizenship" id="form-field-15">
                                <option value="">--Не выбрано--</option>
                                @foreach(\App\User::$citizenships as $citizenship)
                                    <option value="{{$citizenship}}" @if((old() && old('citizenship')==$citizenship) || (!old() && mb_strtolower($user->citizenship) == mb_strtolower($citizenship)) ) selected="selected" @endif>{{$citizenship}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-151"> Паспорт(серия, номер) </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-151" name="passport" placeholder="Паспорт" value="{{ old('passport', $user->passport) }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-16"> Дата рождения</label>
                        <div class="col-sm-9">
                            <div class="input-group col-sm-4">
                                <input class=" form-control date-picker" name="bithdate" value="{{old('bithdate', $user->datePicker())}}" id="form-field-16" type="text" data-date-format="dd.mm.yyyy" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-24"> Телефоны </label>
                        <div class="col-sm-9">
                            <div class="dynamic-input">
                                @if (old('phones'))
                                    @foreach(old('phones') as $key => $phone)
                                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                                            @if (old('phone_ids')[$key])
                                                <input type="hidden" name="phone_ids[]" value="{{old('phone_ids')[$key]}}">
                                            @endif
                                            <input class="col-sm-12 input-mask" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phones[]" value="{{$phone}}" placeholder="Телефон">
                                            <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                                <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                                            </a>
                                        </div>
                                    @endforeach
                                @else
                                    @forelse($user->phones as $key => $phone)
                                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                                            <input type="hidden" name="phone_ids[]" value="{{$phone->id}}">
                                            <input class="col-sm-12 input-mask" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phones[]" value="{{$phone->phone}}" placeholder="Телефон">
                                            <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                                <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                                            </a>
                                        </div>
                                    @empty
                                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                                            <input class="col-sm-12 input-mask" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phones[]" placeholder="Телефон">
                                            <a href="" class="input-group-addon plus">
                                                <i class="glyphicon glyphicon-plus bigger-110"></i>
                                            </a>
                                        </div>
                                    @endforelse
                                @endif
                            </div>
                        </div>
                    </div>


                   <div class="form-group">
                       <label class="col-sm-3 control-label no-padding-right" for="form-field-5"> Активность </label>
                       <div class="col-sm-9">
                           <label>
                               <input type="hidden" name="status" value="0">
                               <input name="status" @if ((old() && old('status')) || (empty(old()) && $user->status) ) checked="checked" @endif   value="1" class="ace ace-switch ace-switch-4 btn-empty" type="checkbox">
                               <span class="lbl"></span>
                           </label>
                       </div>
                   </div>

                   <div class="clearfix form-actions">
                       <div class="col-md-offset-3 col-md-9">
                           <button class="btn btn-success" type="submit">
                               <i class="ace-icon fa fa-check bigger-110"></i>
                               Сохранить
                           </button>
                           &nbsp; &nbsp; &nbsp;
                           <button class="btn" type="reset">
                               <i class="ace-icon fa fa-undo bigger-110"></i>
                               Обновить
                           </button>
                           &nbsp; &nbsp; &nbsp;
                           <a class="btn btn-info" href="{{route('admin.users.index')}}">
                               <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                               Назад
                           </a>

                       </div>
                   </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
