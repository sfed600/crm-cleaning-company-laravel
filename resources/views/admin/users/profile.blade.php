<div class="tab-content no-border padding-24">
    <div id="home" class="tab-pane active">
        <div class="row">
            <div class="col-xs-12 col-sm-3 center">
                @if($user->img)
                <span class="profile-picture">
                    <a href="/{{$user->getImgPath().$user->img}}"  data-rel="colorbox" class="cboxElement">
                        <img class="editable img-responsive" src="/{{$user->getImgPreviewPath().$user->img}}"  />
                    </a>
                </span>
                @else
                <span class="profile-picture">
                    <img class="editable img-responsive" src="/assets/admin/images/user.jpeg"  />
                </span>
                @endif

                <div class="space space-4"></div>

                @can('index', new App\User())
                <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-sm btn-block btn-primary">
                    <i class="ace-icon fa fa-pencil bigger-110"></i>
                    <span class="bigger-110">Редактировать</span>
                </a>
                @endcan

                <div class="space space-4"></div>

                <div class="infobox-container">
                        <div class="infobox infobox-green" style="width: auto;">
                            <div class="infobox-icon">
                                <i class="ace-icon fa fa-bar-chart-o"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{$user->rating()}}</span>
                                <div class="infobox-content">Рейтинг</div>
                            </div>
                        </div>

                        <div class="infobox infobox-blue" style="width: auto;">
                            <div class="infobox-icon">
                                <i class="ace-icon fa fa-gavel"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{$user->orders_cnt}}</span>
                                <div class="infobox-content">{{Lang::choice('Выход|Выхода|Выходов', $user->orders_cnt, [], 'ru')}}</div>
                            </div>
                        </div>

                        <div class="infobox infobox-pink" style="width: auto;">
                            <div class="infobox-icon">
                                <i class="ace-icon glyphicon glyphicon-time"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{$user->orders_late}}</span>
                                <div class="infobox-content">{{Lang::choice('Опоздание|Опоздания|Опозданий', $user->orders_late, [], 'ru')}}</div>
                            </div>
                        </div>

                        <div class="infobox infobox-red" style="width: auto;">
                            <div class="infobox-icon">
                                <i class="ace-icon fa fa-bolt"></i>
                            </div>

                            <div class="infobox-data">
                                <span class="infobox-data-number">{{$user->orders_nocome}}</span>
                                <div class="infobox-content">{{Lang::choice('Не явка|Не явки|Не явок', $user->orders_nocome, [], 'ru')}}</div>
                            </div>
                        </div>
                    </div>

            </div>

            <div class="col-xs-12 col-sm-9">
                <h4 class="blue">
                    <span class="middle" id="user-fio">{{$user->fio()}}</span>
                    @if($user->fio())
                        <button class="btn btn-minier copy-clipboard" data-id="user-fio" title="Скопировать в буфер"><i class="ace-icon fa fa-copy  bigger-110 icon-only"></i></button>
                    @endif

                </h4>

                <div class="profile-user-info">
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Группа </div>

                        <div class="profile-info-value">
                            <span>{{$user->group->name_rus}} ({{$user->group->block->name_rus}})</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Компания </div>

                        <div class="profile-info-value">
                            <span>@if($user->company_id ) {{$user->company->name}} @endif</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Специальности </div>

                        <div class="profile-info-value">
                            @foreach($user->professions as $profession)
                                <span class="label label-info arrowed-in-right arrowed">{{$profession->name}}</span>
                            @endforeach
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Пол </div>

                        <div class="profile-info-value">
                            <span>{{$user->genderName()}}</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Телефоны </div>

                        <div class="profile-info-value">
                            @foreach($user->phones as $phone)
                                <span>{{$phone->phone}}</span>
                            @endforeach
                        </div>
                    </div>


                    <div class="profile-info-row">
                        <div class="profile-info-name"> Возраст </div>

                        <div class="profile-info-value">
                            <span>{{$user->age()}}</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> День рождения </div>

                        <div class="profile-info-value">
                            <span>{{$user->datePicker()}}</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Гражданство </div>

                        <div class="profile-info-value">
                            <span>{{$user->citizenship}}</span>
                        </div>
                    </div>

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Паспорт </div>

                        <div class="profile-info-value">
                            <span>{{$user->passport}}</span>
                        </div>
                    </div>

                    @if($user->professions->count() && $user->professions->where('name','Водитель')->count())
                    <div class="profile-info-row">
                        <div class="profile-info-name"> Автомобиль </div>

                        <div class="profile-info-value">
                            <span id="credit-card">@if($user->auto_id) {{$user->auto->number}} ({{$user->auto->model->name}}) @endif</span>
                        </div>
                    </div>
                    @endif

                    <div class="profile-info-row">
                        <div class="profile-info-name"> Кредитная карта </div>

                        <div class="profile-info-value">
                            <span id="credit-card">{{$user->card}}</span>
                            @if($user->card)
                            <button class="btn btn-minier copy-clipboard" data-id="credit-card" title="Скопировать в буфер"><i class="ace-icon fa fa-copy  bigger-110 icon-only"></i></button>
                            @endif
                        </div>
                    </div>

                </div>


            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="space-20"></div>

    </div><!-- /#home -->
</div>