@extends('admin.layout')
@section('main')
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="dash-container">
        <div class="dash-content">
            <div class="dash-table" data-item-check="wrap">
                <table>
                    <colgroup>
                        <col class="tbl-check">
                        <col class="tbl-number">
                        <col class="tbl-title">
                        <col class="tbl-contact">
                        <col class="tbl-budget">
                        <col class="tbl-tasks">
                        <col class="tbl-typework">
                        <col class="tbl-typepay">
                        <col class="tbl-datestart">
                        <col class="tbl-datecreate">
                    </colgroup>
                    <thead>
                    <tr>
                        <td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                        </td>
                        <td>
                            {{--<a class="tbl-heads-link" href="#">ФИО</a>--}}
                            ФИО
                        </td>
                        <td>Телефон</td>
                        <td>
                            {{--<a class="tbl-heads-link" href="#">Рейтинг</a>--}}
                            Рейтинг
                            <a href="javascript:void(0);" sort="rating">
                                <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="rating-asc"></i>
                                <i class="fa fa-fw fa-sort-down" sort="rating-desc"></i>
                            </a>
                        </td>
                        <td>Выходов</td>
                        <td>Опозданий</td>
                        <td>Неявок</td>
                        <td>Баланс</td>
                        <td>
                            {{--<a class="tbl-heads-link type-up" href="#">Дата активности</a>--}}
                            Дата активности
                        </td>
                        <td>
                            {{--<a class="tbl-heads-link" href="#">Дата создания</a>--}}
                            Дата создания
                        </td>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                            </td>
                            <td><a href="{{route('admin.users.show', $user->id)}}">{{$user->fio()}}</a></td>
                            <td>
                                @foreach($user->phones as $phone)
                                    {{$phone->phone}}<br>
                                @endforeach
                            </td>
                            <td>{{$user->rating()}}</td>
                            <td>{{$user->orders_cnt}}</td>
                            <td>{{$user->orders_late}}</td>
                            <td>{{$user->orders_nocome}}</td>
                            <td class="tbl-col-balance">
                                {{--{{number_format((!empty($user['income']) ? $user['income'] : 0) - (!empty($user['outcome']) ? $user['outcome'] : 0), 0, ',', ' ')}}--}}
                                {{number_format($user->balance($filters), 0, ',', ' ')}}
                            </td>
                            <td>
                                @if($user->days->max('date'))
                                    {{date('d.m.Y', strtotime($user->days->max('date')))}}
                                @endif
                            </td>
                            <td>{{date('d.m.Y h:i', strtotime($user->created_at))}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    {{--<div class="paginator-label">Показано {{sizeof($users)}} из {{@$count_users}}</div>--}}
                    <div class="paginator-label">Показано {{sizeof($users)}} из {{$total_users_count}}</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'users');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'users');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'users');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'users');">100</a></li>
                        </ul>
                    </div>

                    {!! $users->render() !!}
                </div>
            </div>

            <div class="dash-container-layer"></div>
        </div>

@stop