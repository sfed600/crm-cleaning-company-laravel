<div class="dash-panel-heads">
   <span class="dash-panel-heads-label">
      <span class="op-input">
          @if(Route::current()->getName() == 'admin.users.show')
            <input id="user_head_fio" type="text" placeholder="..."  value="{{old('fio', (isset($user)) ? @$user->fio() : '')}}">
          @else
            <input id="user_head_fio" type="text" placeholder="..."  value="{{old('fio')}}">
          @endif
      </span>
      <a href="{{route('admin.users.index')}}" class="dash-panel-heads-button"></a>
   </span>
</div>


<div class="dash-panel-block type-btns">
    <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
        <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
        <div class="dash-panel-dropdown-drop">
            <a href="#">Ссылка 1</a>
            <a href="#">Ссылка 2</a>
            <a href="#">Ссылка 3</a>
        </div>
    </div>

    <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
        <div class="o-btn bgs-green o-hvr" data-dropped="btn" data-layer="yes" onclick="window.location.href = '{{route('admin.users.create')}}';">
            + Добавить сотрудника
        </div>
        {{--<div class="dash-panel-dropdown-drop">
            <a href="#">Ссылка 1</a>
            <a href="#">Ссылка 2</a>
        </div>--}}
    </div>
</div>
<div class="dash-panel-block type-transactions">
    <span class="txt-label">Баланс:</span>
           <span class="txt-value">
              <span class="op-input"><input type="text" placeholder="..." value=""></span>
              руб.</span>
</div>