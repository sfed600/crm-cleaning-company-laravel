@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.users.index')}}">Сотрудники</a>
            </li>
            <li class="active">Выплаты сотрудника</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Сотрудники
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Выплаты
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">

                <div class="">
                    <div id="user-profile-2" class="user-profile ace-thumbnails">
                        <div class="tabbable">
                            @include('admin.users.profile', ['user' => $user])

                            <ul class="nav nav-tabs padding-18">
                                <li class="">
                                    <a href="{{route('admin.users.show', $user->id)}}">
                                        <i class="green ace-icon glyphicon glyphicon-refresh bigger-120"></i>
                                        Взаимозачеты
                                    </a>
                                </li>

                                <li class="active">
                                    <a href="{{route('admin.users.outcome', $user->id)}}">
                                        <i class="orange ace-icon glyphicon glyphicon-repeat bigger-120"></i>
                                        Выплаты
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{route('admin.users.income', $user->id)}}" >
                                        <i class="blue ace-icon glyphicon glyphicon-euro bigger-120"></i>
                                        Зарплата
                                    </a>
                                </li>
                            </ul>

                            <div id="dynamic-table_wrapper" class="dataTables_wrapper no-footer">
                                <div class="row">
                                    <div class=" col-sm-12">
                                        <div class="col-xs-12 col-sm-5">
                                            <div class="widget-box collapsed">
                                                <div class="widget-header">
                                                    <h4 class="widget-title">Добавить выплату</h4>

                                                    <div class="widget-toolbar">
                                                        <a href="#" data-action="collapse">
                                                            <i class="ace-icon fa fa-chevron-down"></i>
                                                        </a>

                                                    </div>
                                                </div>

                                                <div class="widget-body" style="display: none; ">
                                                    <div class="widget-main">
                                                        <form class="form-horizontal" method="post" action="{{route('admin.users.outcome.store', $user->id)}}">
                                                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-8"> Дата </label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input class=" form-control date-picker" name="date" value="{{old('date', date('d.m.Y'))}}" id="form-field-8" type="text" data-date-format="dd.mm.yyyy" />
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar bigger-110"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-9"> Сумма </label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" id="form-field-9" name="outcome" placeholder="Сумма" value="{{old('outcome')}}" class="col-sm-12">
                                                                            <span class="input-group-addon">
                                                                            <i class="fa fa-rub bigger-110"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-10"> Вид оплаты </label>
                                                                <div class="col-sm-9">
                                                                    <select name="type">
                                                                           <option>--Не выбран--</option>
                                                                           @foreach(\App\Models\UserIncome::$types as $type => $name)
                                                                           <option value="{{$type}}" @if(old() && old('type')==$type)selected="selected"@endif>{{$name}}</option>
                                                                           @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-11"> Описание </label>
                                                                <div class="col-sm-9">
                                                                    <textarea name="comment" class="col-sm-9">{{old('comment')}}</textarea>
                                                                </div>
                                                            </div>


                                                            <div class="clearfix form-actions">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                <button class="btn btn-success" type="submit">
                                                                   <i class="ace-icon fa fa-check bigger-110"></i>
                                                                   Сохранить
                                                               </button>
                                                               </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                @if($outcomes->count())
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead class="thin-border-bottom">
                                                        <tr>
                                                            <th>Дата</th>
                                                            <th>Выплата</th>
                                                            <th>Вид оплаты</th>
                                                            <th>Описание</th>
                                                            <th class="hidden-480"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($outcomes as $outcome)
                                                        <tr>
                                                            <td>{{$outcome->datePicker()}}</td>
                                                            <td>{{$outcome->outcome}} руб.</td>
                                                            <td>{{$outcome->typeName()}}</td>
                                                            <td>{{$outcome->comment}}</td>
                                                            <td class="hidden-480">
                                                                <form method="POST" action='{{route('admin.users.outcome.delete', $outcome->id)}}' style="display:inline;">
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                                    <button class="btn btn-xs btn-danger action-delete" type="button" style="border-width: 1px;">
                                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
