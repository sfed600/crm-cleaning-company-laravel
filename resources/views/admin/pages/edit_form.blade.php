<form class="form-horizontal" role="form" action="{{route('admin.pages.update', $page->id)}}" method="POST">
    <input type="hidden" name="_method" value="PUT">
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-0"> Системное (URL) </label>
        <div class="col-sm-9">
            <input type="text" id="form-field-0" name="name" placeholder="Название" value="{{ old('name', $page->name) }}" class="col-sm-5">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Системное (URL) </label>
        <div class="col-sm-9">
            <input type="text" id="form-field-1" name="sysname" placeholder="sysname" value="{{ old('sysname', $page->sysname) }}" class="col-sm-5">
            <span class="help-button" data-rel="popover" data-trigger="hover" data-placement="left" data-content="Используется в качестве части адреса страницы." title="">?</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Содержание </label>
        <div class="col-sm-9">
            <textarea class="ck-editor" id="editor2" name="content">{{ old('content', $page->content) }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-4"> Дополнительные данные </label>
        <div class="col-sm-9">
            <div class="dynamic-input">
                @if (old('vars'))
                    @foreach(old('vars') as $key => $var)
                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                            @if (old('var_ids')[$key])
                                <input type="hidden" name="var_ids[]" value="{{old('var_ids')[$key]}}">
                            @endif
                            <input class="col-sm-3" class="form-control" type="text" name="vars[]" value="{{$var}}" placeholder="Имя">
                            <input class="col-sm-9"class="form-control" type="text" name="values[]" value="{{old('values')[$key]}}" placeholder="Значение">
                            <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                            </a>
                        </div>
                    @endforeach
                @else
                    @forelse($page->vars as $key => $var)
                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                            <input type="hidden" name="var_ids[]" value="{{$var->id}}">
                            <input class="col-sm-3" class="form-control" type="text" name="vars[]" value="{{$var->var}}" placeholder="Имя">
                            <input class="col-sm-9"class="form-control" type="text" name="values[]" value="{{$var->value}}" placeholder="Значение">
                            <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                            </a>
                        </div>
                    @empty
                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                            <input class="col-sm-3" class="form-control" type="text" name="vars[]" placeholder="Имя">
                            <input class="col-sm-9"class="form-control" type="text" name="values[]" placeholder="Значение">
                            <a href="" class="input-group-addon plus">
                                <i class="glyphicon glyphicon-plus bigger-110"></i>
                            </a>
                        </div>
                    @endforelse
                @endif
            </div>
        </div>
    </div>

    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            <button class="btn btn-info" type="submit">
                <i class="ace-icon fa fa-check bigger-110"></i>
                Сохранить
            </button>

            &nbsp; &nbsp; &nbsp;
            <button class="btn" type="reset">
                <i class="ace-icon fa fa-undo bigger-110"></i>
                Отменить
            </button>
        </div>
    </div>

</form>