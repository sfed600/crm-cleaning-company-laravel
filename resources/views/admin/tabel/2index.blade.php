@extends('admin.layout')
@section('main')
    <div class="dash-container">
        <div class="dash-content">

            <div class="dash-table-report">
                <a class="tbl-report-navs left" href="#"></a>
                <a class="tbl-report-navs right" href="#"></a>
                <table>
                    <colgroup>
                        <col class="tbl-col-person">
                        <col class="tbl-col-shifts">
                        <col class="tbl-col-charged">
                        <col class="tbl-col-balance">
                        @foreach($days as $day => $day_o)
                            <col class="tbl-col-day">
                        @endforeach
                    </colgroup>
                    <tbody>
                    <tr class="table-heads">
                        <th class="tbl-col-person">Сотрудник</th>
                        <th class="tbl-col-shifts">Смены</th>
                        <th class="tbl-col-charged">Начислено</th>
                        <th class="tbl-col-balance">Остаток</th>
                        @foreach($days as $day => $day_o)
                            <th @if($day_o['weekday']==1) class="workday" @else class="freeday" @endif>
                                <span class="tbl-report-hday"><span class="tbl-report-day">{{$day}}</span> {{mb_strtoupper($day_o['name'], 'UTF-8')}}</span>
                            </th>
                        @endforeach
                    </tr>

                    @if(!empty($users))
                        @forelse($users as $user)
                            <tr>
                                <td class="tbl-col-person">
                                    {{$user['user']->surname.' '.$user['user']->name.' '.$user['user']->patronicname}}
                                    <div class="tbl-box-hint">
                                        <div class="tbl-box-hint-ins">
                                            {{--<div class="tbl-box-hint-person-name">{{$user['user']->surname.' '.$user['user']->name.' '.$user['user']->patronicname}}</div>--}}
                                            @if(@$user['user']->phone1 != @$user['user']->phone2)
                                                <div class="tbl-box-hint-person-item">{{@$user['user']->phone1}}</div>
                                                <div class="tbl-box-hint-person-item">{{@$user['user']->phone2}}</div>
                                            @else
                                                <div class="tbl-box-hint-person-item">{{@$user['user']->phone1}}</div>
                                            @endif
                                            {{--@foreach($user["phones"] as $phone)
                                                <div class="tbl-box-hint-person-item">{{@$phone->phone}}</div>
                                            @endforeach--}}
                                        </div>
                                    </div>
                                </td>
                                <td class="tbl-col-shifts">{{$user['sum']}}</td>
                                <td class="tbl-col-charged">{{number_format($user['income'], 0, ',', ' ')}}</td>
                                <td class="tbl-col-balance">{{number_format((!empty($user['income']) ? $user['income'] : 0) - (!empty($user['outcome']) ? $user['outcome'] : 0), 0, ',', ' ')}}</td>

                                @foreach($days as $day => $day_o)
                                    @php
                                        $night = false;
                                        $_day = false;
                                        $_income = false;
                                        if(!empty($user['days']) && !empty($user['days'][$day])) {
                                            $_income = true;
                                            foreach($user['days'][$day]['items'] as $order) {
                                                if($order->nigth == 1) {
                                                    $night = true;
                                                } else if($order->nigth == 0) {
                                                    $_day = true;
                                                }
                                            }
                                        }
                                    @endphp
                                    <td style="padding: 0;" class="freeday {{--workday--}} @if($_income) current @endif @if($night && $_day) type-duble @endif">
                                        @if($_income)
                                            {{$user['days'][$day]['sum_cef']}}
                                            <div class="tbl-box-hint">
                                                <div class="tbl-box-hint-ins">
                                                    @foreach($user['days'][$day]['items'] as $order)
                                                        @if($order->nigth==1)
                                                            <div class="tbl-box-hint-item clr-purple" style="cursor: pointer;" onclick="window.location.href = '{{route('admin.orders.edit', $order->order_id)}}';"><span class="txt-label">Ночь:</span> Заказ № {{$order->number}}</div>
                                                        @else
                                                            <div class="tbl-box-hint-item" style="cursor: pointer;" onclick="window.location.href = '{{route('admin.orders.edit', $order->order_id)}}';"><span class="txt-label">День:</span> Заказ № {{$order->number}}</div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @empty
                        @endforelse
                    @endif

                    <tr>
                        <td class="tbl-col-total">Итого:</td>
                        <td class="tbl-col-shifts">{{$total_smen}}</td>
                        <td class="tbl-col-charged">{{number_format($total_income, 0, ',', ' ')}}</td>
                        <td class="tbl-col-balance">{{number_format($total_income - $total_outcome, 0, ',', ' ')}}</td>

                        @foreach($days as $day => $day_o)
                            <td @if($day_o['weekday']==1) class="workday" @else class="freeday" @endif>
                                {{--{{$day_o['cnt'] or ''}}--}}
                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    <div class="paginator-label">Показано @if($filters['perpage'] > $total_users) {{$total_users}} @else {{$filters['perpage']}} @endif из {{@$total_users}} сотрудников</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'tabel');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'tabel');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'tabel');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'tabel');">100</a></li>
                        </ul>
                    </div>

                    <div class="pages-list js-paginator-report"></div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>

    <script>
        function ready() {
            tabel_paginator_init({{$total_users}}, {{$filters['perpage'] or 10}}, {{$filters['page'] or 1}});
        }

        document.addEventListener("DOMContentLoaded", ready);

    </script>

@stop