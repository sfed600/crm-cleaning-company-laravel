{{--{{dump($filters)}}--}}
<div id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <table class="main-header">
            <tr>
                @if(Auth::user()->company)
                    <td class="header-logo">
                        <a href="{{route('admin.main')}}" class="navbar-brand">
                            <div>
                                <span style="font-family: Lato Reqular;!important;">{{Auth::user()->company->name}}</span>
                                <span style="font-family: Lato Reqular;!important;">Клининговая компания</span>
                            </div>
                        </a>
                    </td>
                @else
                    <td width="10%"></td>
                @endif
                <td class="header-title" style="font-weight: 300; font-family: Lato Reqular;!important;">Табель</td>
                <td width="30%">
                    <div class="header-search">
                        <div class="filters_btns">
                            @if(isset($filters))
                                <button type="button">
                                    {{$filters["daterange"]}}
                                    <i class="ace-icon fa fa-times" onclick="exclude_from_filter(this, 'daterange');"></i>
                                </button>
                                @if( isset($filters["profession"]) && intval($filters["profession"]) > 0 )
                                    {{--<button class="btn btn-info btn-xs" type="button">--}}
                                    <button type="button">
                                        {{$professions[intval(@$filters['profession']) - 1]->name}} {{--&nbsp;--}}
                                        <i class="ace-icon fa fa-times" onclick="exclude_from_filter(this, 'profession');"></i>
                                    </button>
                                @endif
                                {{--@if( $filters["date"] != '' )
                                    <button class="btn btn-info btn-xs" type="button" data-filter="date">
                                        {{@$filters['date']}} &nbsp;
                                        <i class="ace-icon fa fa-times" onclick="update_filters_result(this);"></i>
                                    </button>
                                @endif--}}
                                @if( isset($filters["surname"]) && $filters["surname"] != '' )
                                    <button type="button">
                                        {{@$filters['surname']}} &nbsp;
                                        <i class="ace-icon fa fa-times" onclick="exclude_from_filter(this, 'surname');"></i>
                                    </button>
                                @endif
                                @if( isset($filters["phone"]) && $filters["phone"] != '' )
                                    <button type="button" data-filter="phone">
                                        {{@$filters['phone']}} &nbsp;
                                        <i class="ace-icon fa fa-times" onclick="exclude_from_filter(this, 'phone');"></i>
                                    </button>
                                @endif
                            @endif
                        </div>
                        <input id="search_field" type="text" class="form-control" placeholder="+поиск" style="border: 0; width: 100%; font-family: Lato Reqular;">
                        <i class="ace-icon glyphicon glyphicon-search" style="position: relative; left: -20px; top:10px;"></i>
                        <i class="fa fa-times-thin fa-2x reset_tabel_filter" aria-hidden="true" onclick="reset_tabel_filter();"></i>
                    </div>
                </td>
                <td class="header-info">
                    <span style="font-family: Lato Reqular;!important;">Выплата сотрудникам {{number_format($total_income, 0, ',', ' ')}} руб.</span>
                </td>
                <td class="header-btn1" style="border-left: 1px solid #ebebeb;">
                    <button style="border: 0; background-color: #fff;">...</button>
                    <div class="ibox-tools" style="float: none;">
                        <a href="{{route('admin.users.create')}}" class="navbar-add-btn">
                            <i class="fa fa-plus"></i>
                            Добавить сотрудника
                        </a>
                    </div>
                </td>
                <td class="header-right">
                    <img src="/img/bell.png" />
                    <div class="notes-alert">0</div>
                    {{--<span>Здравствуйте, {{ Auth::user()->name }}</span>--}}
                    <div class="btn-group">
                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img class="round-img" src="/{{Auth::user()->getImgPreviewPath().Auth::user()->img}}"  />
                            <span class="caret"></span>
                        </a>--}}
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            @if(Auth::user()->img)
                                <img class="round-img" src="/{{Auth::user()->getImgPreviewPath().Auth::user()->img}}"  />
                            @else
                                <span>{{ Auth::user()->name }}</span>
                            @endif
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" style="left: -40px; font-family: Lato Reqular;">
                            <li>
                                <a href="{{route('index')}}">
                                    <i class="ace-icon fa fa-home"></i>
                                    Главная
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin.users.show', Auth::user()->id)}}">
                                    <i class="ace-icon fa fa-user"></i>
                                    Профиль
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Выход
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>

    </div><!-- /.navbar-container -->
</div>