@extends('admin.layout')
@section('main')

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>
            <li class="active">Табель</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Табель
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Выполненные работы по месяцам
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">
                <div>
                    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">

                            <!-- FILTERS -->
                            <div class="row">
                                <form method="GET" action="{{route('admin.tabel')}}">
                                    <div class="row">
                                        @can('company', new App\Models\Tabel())
                                            <div class="col-xs-3" style="width:22%">
                                                <label>Компания:
                                                    <select name="f[company]" class="form-control input-sm">
                                                        @foreach($companies as $company)
                                                            <option value="{{$company->id}}" @if (isset($filters['company']) &&  $filters['company']== $company->id) selected="selected" @endif>{{$company->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </label>
                                            </div>
                                        @endcan
                                        <div class="col-xs-4" style="width:30%">
                                            <div>
                                                <label>Дата:
                                                    <div class="input-group">
                                                        <input class="form-control date-picker-month" name="f[date]" value="{{$filters['date'] or ''}}" type="text">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-calendar bigger-110"></i>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-4" style="width:26%">
                                            <label>Специальность:
                                                <select name="f[profession]" class="form-control input-sm">
                                                    <option value="">--Не выбрана--</option>
                                                    @foreach($professions as $profession)
                                                        <option value="{{$profession->id}}" @if (isset($filters['profession']) &&  $filters['profession']== $profession->id) selected="selected" @endif>{{$profession->name}}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                        <div class="col-xs-3" style="width:21%">
                                            <div class="dataTables_length">
                                                <a class="btn  btn-xs" href="{{route('admin.tabel')}}?refresh=1">
                                                    Сбросить
                                                    <i class="ace-icon glyphicon glyphicon-refresh  align-top bigger-125 icon-on-right"></i>
                                                </a>

                                                <button class="btn btn-info btn-xs" type="submit">
                                                    Фильтровать
                                                    <i class="ace-icon glyphicon glyphicon-search  align-top bigger-125 icon-on-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div style="overflow-x: scroll;">
                            <table id="simple-table" class="table table-striped table-bordered table-hover ace-thumbnails">
                                <thead>
                                <tr style="background-image: linear-gradient(to bottom,#f8f8f8 0,#6caef2 100%);color: #1d70f8;">
                                    <th nowrap>Сотрудник</th>
                                    <th class="center">Рабочих<br>смен</th>
                                    <th>Начислено</th>
                                    <th>Остаток</th>
                                    @foreach($days as $day => $day_o)
                                    <th class="center" @if($day_o['weekday']==1) style="color: #000000" @endif>{{$day}}<br>{{$day_o['name']}}</th>
                                    @endforeach
                                </tr>
                                </thead>

                                <tbody>
                                    @if(!empty($users))
                                    @forelse($users as $user)
                                        <tr @if($user->trashed()) style="background-color: #F6CECE" @endif>
                                            <td nowrap>
                                                @if($user->trashed())
                                                    <i class="ace-icon ace-icon fa fa-user"></i>
                                                    {{$user->fio()}}
                                                @else
                                                <a href="{{route('admin.users.show', $user->id)}}">
                                                    <i class="ace-icon ace-icon fa fa-user"></i>
                                                    {{$user->fio()}}
                                                </a>
                                                @endif
                                            </td>
                                            <td class="center">{{$user->orders->sum('cef')}}</td>
                                            <td class="center">{{$user->orders->sum('amount')}}</td>
                                            <td class="center"></td>
                                            @foreach($days as $day_o)
                                                <td class="center">
                                                   <span>{{$user->days->filter(function ($day) use ($day_o) {return $day->date == $day_o['date'];})->count() ?: ''}}</span>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @empty
                                    @endforelse
                                    @endif
                                </tbody>
    {{--
    <tfoot>
        <tr>
            <td nowrap colspan="2"><strong>Итого</strong></td>
            <td class="center">{{$total_income}}</td>
            <td class="center">{{$total_income - $total_outcome}}</td>
            @foreach($days as $day => $day_o)
            <td class="center">
                <span>{{$day_o['cnt'] or ''}}</span>
            </td>
            @endforeach
        </tr>
    </tfoot>
    --}}
                            </table>
                            </div>
                        </div><!-- /.row -->
                    </div>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->


@stop