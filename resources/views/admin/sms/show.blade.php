@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.sms.index')}}">SMS</a>
            </li>
            <li class="active">Детали сообщения</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                SMS
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Детали
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">

                <div class="">
                    <div id="user-profile-2" class="user-profile ace-thumbnails">
                        <div class="tabbable">
                            <div class="tab-content no-border padding-24">
                                <div id="home" class="tab-pane active">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-9">
                                            <div class="profile-user-info">
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> Получатели </div>

                                                    <div class="profile-info-value">
                                                        @foreach($sms->users as $user)
                                                            <span><a href="{{route('admin.users.show', $user->id)}}">{{$user->fio()}}</a></span>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> Текст </div>

                                                    <div class="profile-info-value">
                                                        <span>{{$sms->text}}</span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> Дата </div>

                                                    <div class="profile-info-value">
                                                        <span>{{$sms->created_at}}</span>
                                                    </div>
                                                </div>


                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> Статус </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <i class="ace-icon glyphicon @if($sms->result) glyphicon-ok green @else glyphicon-remove red @endif bigger-120"></i>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name"> Ошибка </div>

                                                    <div class="profile-info-value">
                                                        <span>{{$sms->error}}</span>
                                                    </div>
                                                </div>

                                            </div>


                                        </div><!-- /.col -->
                                    </div><!-- /.row -->

                                    <div class="space-20"></div>

                                </div><!-- /#home -->
                            </div>

                        </div>
                    </div>
                </div>


            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
