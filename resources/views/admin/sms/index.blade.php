@extends('admin.layout')
@section('main')

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>
            <li class="active">SMS</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                SMS
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Список всех сообщений
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">
                <div>
                    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                        <!-- PAGE CONTENT BEGINS -->
                        <div class="row">

                            <table id="simple-table" class="table table-striped table-bordered table-hover ace-thumbnails">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Получатели</th>
                                    <th>Текст</th>
                                    <th class="col-sm-1">Статус</th>
                                    <th>Ошибка</th>
                                    <th>Дата</th>
                                    <th></th>
                                </tr>
                                </thead>

                                <tbody>
                                @forelse($sms as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>
                                            @foreach($item->users as $user)
                                            <a href="{{route('admin.users.show', $user->id)}}">{{$user->fio()}}</a>,
                                            @endforeach
                                        </td>
                                        <td>{{$item->text}}</td>
                                        <td class="col-sm-1 center"><i class="ace-icon glyphicon @if($item->result) glyphicon-ok green @else glyphicon-remove red @endif bigger-120"></i></td>
                                        <td>{{$item->error}}</td>
                                        <td>{{$item->created_at}}</td>
                                        <td>
                                            <a class="btn btn-xs btn-success" href="{{route('admin.sms.show', $item->id)}}">
                                                <i class="ace-icon glyphicon glyphicon-search bigger-120"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <p>Нет смс</p>
                                @endforelse
                                </tbody>
                            </table>

                            <div class="row" style="border-bottom:none;">
                                <div class="col-xs-6">
                                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                        {!! $sms->render() !!}
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.row -->
                    </div>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.page-content -->


@stop