<div class="dash-nav">
    <ul class="dash-nav-list">
        <li>
            <a href="{{route('index')}}">
                <span class="dash-nav-icon"><span class="ic-nav-01"></span></span>
                <span class="dash-nav-label">Главная</span>
            </a>
        </li>
        {{--<li>
            <a href="{{route('admin.orders.index')}}">
                <span class="dash-nav-icon"><span class="ic-nav-02"></span></span><span class="dash-nav-label">Заказы</span>
            </a>
        </li>--}}
        <li>
            <a href="#">
                <span class="dash-nav-icon"><span class="ic-nav-02"></span></span><span class="dash-nav-label">Заказы</span>
            </a>
            <div class="dash-nav-drop">
                <ul class="dash-nav-drop-list">
                    <li><a href="{{route('admin.offers.index')}}">Лиды</a></li>
                    <li><a href="{{route('admin.orders.index')}}">Специальные работы</a></li>
                    <li><a href="#">Ежедневное обслуживание офисов</a></li>
                    <li><a href="#">Еженедельное обслуживание квартир</a></li>
                </ul>
            </div>
        </li>
        <li><a href="#"><span class="dash-nav-icon"><span class="ic-nav-03"></span></span><span class="dash-nav-label">Документы</span></a>
            <div class="dash-nav-drop">
                <ul class="dash-nav-drop-list">
                    <li><a href="{{route('admin.offers.index')}}">Предложения</a></li>
                    <li><a href="{{route('admin.invoices.index')}}">Счета</a></li>
                    <li><a href="#">Товары / услуги</a></li>
                    <li><a href="#">Договора</a></li>
                </ul>
            </div>
        </li>
        <li>
            <a href="{{route('admin.calendar')}}">
                <span class="dash-nav-icon"><span class="ic-nav-04"></span></span><span class="dash-nav-label">Календарь</span>
            </a>
            {{--<div class="dash-nav-drop">
                <ul class="dash-nav-drop-list">
                    <li><a href="#">Предложения</a></li>
                    <li><a href="#">Счета</a></li>
                    <li><a href="#">Товары / услуги</a></li>
                    <li><a href="#">Договора</a></li>
                </ul>
            </div>--}}
        </li>
        <li>
            <a href="{{route('admin.tabel')}}"><span class="dash-nav-icon"><span class="ic-nav-05"></span></span><span class="dash-nav-label">Табель</span></a>
        </li>
        <li><a href="{{route('admin.users.index')}}"><span class="dash-nav-icon"><span class="ic-nav-06"></span></span><span class="dash-nav-label">Сотрудники</span></a></li>
        <li>
            <a href="#">
                <span class="dash-nav-icon">
                    <span class="ic-nav-07"></span>
                </span>
                <span class="dash-nav-label">Контрагенты</span>
            </a>
            <div class="dash-nav-drop">
                <ul class="dash-nav-drop-list">
                    <li><a href="{{route('admin.order_contacts.index')}}">Контакты</a></li>
                    <li><a href="{{route('admin.order_companies.index')}}">Компании</a></li>
                </ul>
            </div>
        </li>
        <li><a href="#"><span class="dash-nav-icon"><span class="ic-nav-08"></span></span><span class="dash-nav-label">Отчеты</span></a></li>
        <li><a href="#"><span class="dash-nav-icon"><span class="ic-nav-09"></span></span><span class="dash-nav-label">Служебные разделы</span></a></li>
    </ul>
    <div class="dash-nav-layer"></div>
</div>