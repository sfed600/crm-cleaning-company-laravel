<div class="kp-order-form-wrap">

    <form class="form-horizontal" role="form" action="{{route('admin.companies.update', $company->id)}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
        <input name="_token" type="hidden" value="{{csrf_token()}}">

        <input name="typ" type="hidden" value="{{$company->typ}}">
        <input name="name" type="hidden" value="{{$company->name}}">
        <input name="inn" type="hidden" value="{{$company->inn}}">

        <div class="p-company-heads clearfix">
            <div class="p-company-heads-fields">
                <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap">

                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Сайт:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input type="text" placeholder="..." data-op-input="get" data-name="www" name="site" value="{{ old('site', $company->site) }}">
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Телефон:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input type="text" placeholder="..." data-op-input="get" data-jsmask="tel" data-name="tel" name="phone" value="{{ old('phone', $company->phone) }}">
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">E-mail:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input type="text" placeholder="..." data-op-input="get" data-name="email" name="email" value="{{ old('email', $company->email) }}">
                        </div>
                    </div>

                    {{--<div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Поле 1:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input type="text" placeholder="..." data-op-input="get" value="">
                            </div>
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Поле 2:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input type="text" placeholder="..." data-op-input="get" value="">
                            </div>
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Поле 3:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input type="text" placeholder="..." data-op-input="get" value="">
                            </div>
                        </div>
                    </div>

                    <div class="kp-order-box-toggle-fields-adds">
                        <div class="adds-down-btn" data-addfieldbox-tgl-show="btn">
                            <span class="txt-label">Еще</span>
                            <div class="adds-down-btn-icon">
                                <span class="icon-input-ctrl-down"></span>
                                <span class="icon-input-ctrl-down-h btn-hover"></span>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="p-company-heads-right">

                <div class="p-company-heads-logo-block type-uploads" data-contact-block-logo="wrap">
                    <div class="p-company-heads-nologo">
                        <label class="p-company-heads-logo-add">
                            <input name="logo" type="file" data-contact-block-logo="upload" accept="image/*">
                        </label>
                    </div>
                    <span class="p-company-heads-logo" data-contact-block-logo="image">
                        {{--<img src="img/blank/page-company-logo-lizing.png" alt="">--}}
                        <img src="/assets/imgs/companies/logo/{{$company->logo}}">
                    </span>
                    <div class="p-company-heads-logo-control">
                        <div class="op-input-control-btn" data-contact-block-logo="remove"><span class="icon-input-ctrl-clear"></span><span class="icon-input-ctrl-clear-h btn-hover"></span></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="kp-order-form-row clearfix type-address">
            <div class="kp-order-form-labinput">
                <div class="op-input-label">Адрес:</div>
                <div class="kp-order-form-labinput-box">
                    <div class="op-input type-btns" data-op-input="item" data-openside>
                        <div class="op-input-control-btn" data-op-input="search">
                            <span class="icon-input-ctrl-search"></span>
                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                        </div>
                        <input type="text" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" name="address" value="{{ old('address', $company->address) }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-addressdetail">
            <div class="kp-order-form-justify">
                <div class="kp-order-form-justify-ins">
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Строение:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="building" value="{{ old('building', $company->building)}}">
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Корпус:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="housing" value="{{ old('housing', $company->housing)}}">
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Офис:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="office" value="{{ old('office', $company->office) }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-note">
            <div class="op-input-label">Примечание:</div>
            <div class="op-input">
                <textarea placeholder="..." data-autoheight="true" name="comments">{{ old('comments', $company->comments) }}</textarea>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-person">
            <div class="kp-order-form-labinput">
                <div class="op-input-label">Ответственный:</div>
                <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                    {{--<div class="op-input type-btns" data-op-input="item">
                        <div class="op-input-control-btn" data-op-input="add_person">
                            <span class="icon-input-ctrl-down"></span>
                            <span class="icon-input-ctrl-down-h btn-hover"></span>
                        </div>
                        <div class="op-input-control-btn" data-op-input="search">
                            <span class="icon-input-ctrl-search"></span>
                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                        </div>
                        <input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">
                    </div>--}}

                    <div class="kp-order-form-person-item" data-persons="item">
                        {{--<div class="kp-order-form-person-item-ava" style="background-image:url(img/blank/avatar.png)"></div>--}}
                        <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$company->user_avatar}})"></div>
                        <div class="kp-order-form-person-item-name">{{$company->user_surname.($company->user_name ? ' '.$company->user_name : '').($company->user_patronicname ? ' '.$company->user_patronicname : '')}}</div>
                        {{--<div class="kp-order-form-person-item-remove" data-persons="remove"></div>--}}
                    </div>
                </div>
            </div>
        </div>

        <script type="text/template" id="kp-order-person-additem">
            <div class="kp-order-form-person-item" data-persons="item">
                <div class="kp-order-form-person-item-ava" style="background-image:url([avatar])"></div>
                <div class="kp-order-form-person-item-name">[name]</div>
                <div class="kp-order-form-person-item-remove" data-persons="remove"></div>
            </div>
        </script>

        <div class="page-kp-form-bottom-button type-right">
            {{--<span class="page-kp-form-button bgs-green">Сохранить</span>--}}
            <button class="page-kp-form-button bgs-green" type="submit">Сохранить</button>
            <span class="page-kp-form-button bgs-orange">Обновить</span>
        </div>

    </form>
</div>