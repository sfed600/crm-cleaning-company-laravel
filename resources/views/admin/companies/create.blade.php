@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.companies.index')}}">Компании</a>
            </li>
            <li class="active">Добавление компании</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Компании
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Добавление
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal" role="form" action="{{route('admin.companies.store')}}" method="POST" enctype="multipart/form-data">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-0"> Название </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-0" name="company" placeholder="Название" value="{{ old('company') }}" class="col-sm-5">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> ИНН </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-1" name="inn" placeholder="ИНН" value="{{ old('inn') }}" class="col-sm-5">
                        </div>
                    </div>

                    <h3 class="row header smaller lighter purple">
                        <span class="col-sm-6"> Администратор </span>
                    </h3>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-10"> Фамилия </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-10" name="surname" placeholder="Фамилия" value="{{ old('surname') }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-0"> Имя </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-0" name="name" placeholder="Имя" value="{{ old('name') }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-11"> Отчество </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-11" name="patronicname" placeholder="Отчество" value="{{ old('patronicname') }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> E-mail </label>
                        <div class="col-sm-9">
                            <input type="email" id="form-field-1" name="email" placeholder="E-mail" value="{{ old('email') }}" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Пароль </label>
                        <div class="col-sm-9">
                            <input type="password" id="form-field-2" name="password" placeholder="Пароль" value="" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-3"> Повторите пароль </label>
                        <div class="col-sm-9">
                            <input type="password" id="form-field-3" name="password_confirmation" placeholder="Повторите пароль" value="" class="col-sm-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-12"> Пол </label>
                        <div class="col-sm-9">
                            <div class="radio">
                                <label>
                                    <input name="gender" type="radio" class="ace" value="male" @if(old() && old('gender') == 'male') checked="checked" @endif>
                                    <span class="lbl"> Мужчина</span>
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="gender" type="radio" class="ace" value="female" @if((old() && old('gender') == 'female') || !old()) checked="checked" @endif>
                                    <span class="lbl"> Женщина</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-17"> Телефоны </label>
                        <div class="col-sm-9">
                            <div class="dynamic-input">
                                @if (old('phones'))
                                    @foreach(old('phones') as $key => $phone)
                                        <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                                            <input class="col-sm-12 input-mask" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phones[]" value="{{$phone}}" placeholder="Телефон">
                                            <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                                <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                                            </a>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="input-group dynamic-input-item" style="margin-bottom:5px;">
                                        <input class="col-sm-12 input-mask" data-mask="+7(999) 999-99-99" class="form-control" type="text" name="phones[]" placeholder="Телефон">
                                        <a href="" class="input-group-addon plus">
                                            <i class="glyphicon glyphicon-plus bigger-110"></i>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-success" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Сохранить
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Обновить
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <a class="btn btn-info" href="{{route('admin.companies.index')}}">
                                <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                Назад
                            </a>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
