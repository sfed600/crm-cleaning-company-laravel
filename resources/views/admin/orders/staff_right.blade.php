<div data-temp_id="{{$rand or $order->number+$day_number}}" class="kp-tab-personal-right" @if( !isset($rand)) style="display: none;"@endif>
    <div class="kp-personal-joinplace clearfix">
        <div class="kp-personal-joinplace-metro">
            <div class="o-form-row-input {{--js-select-schosen--}}">
                {{--<select class="select-chosen2" name="metro" data-width="100%" data-placeholder="Выбрать метро">
                    <option>Выбрать метро</option>
                    <option>Авиамоторная</option>
                    <option>Автозаводская</option>
                    <option>Академическая</option>
                </select>--}}
                <div class="op-input-control-btn" data-op-chosen="clear" onclick="clear_input(this);">
                    <span class="icon-input-ctrl-clear"></span>
                    <span class="icon-input-ctrl-clear-h btn-hover"></span>
                </div>
                <div class="op-input-control-btn">
                    <span class="icon-input-ctrl-down"></span>
                    <span class="icon-input-ctrl-down-h btn-hover"></span>
                </div>
                <div class="op-input-control-btn" data-op-input="search">
                    <span class="icon-input-ctrl-search"></span>
                    <span class="icon-input-ctrl-search-h btn-hover"></span>
                </div>

                {{--<input name="metro_id[{{$length or $day_number + 1}}]" type="text" placeholder="Выбрать метро" value="" data-op-input="metro" data-url="/admin/metro">--}}
                <input {{--name="metro_id[]"--}} type="text" placeholder="Выбрать метро" value="{{@$day->metro->name}}" data-op-input="metro" data-day="{{$length or $day_number}}">
                <input type="hidden" name="metro_id[{{@$day_number}}]" data-metro_id="{{@$day_number}}" value="{{@$day->metro->id}}">
            </div>
        </div>

        <div class="kp-personal-joinplace-date">
            <div class="kp-order-form-date-item fleft">
                <div class="kp-order-form-date-label">Дата начала:</div>
                <div class="kp-order-form-date-input">
                    <div class="op-input type-icon">
                        {{--<input name="dates[{{$length or $day_number + 1}}]" type="text" placeholder="..." value="" data-jsmask="date">--}}
                        <input name="dates[]" type="text" placeholder="..." value="@if( isset($day) ){{date('d.m.Y', strtotime($day->date))}} @else {{date('d.m.Y')}} @endif" data-jsmask="date" data-kpcalendar="date">
                        <div class="op-input-icon-date"></div>
                    </div>
                </div>
            </div>

            <div class="kp-order-form-date-item fright">
                <div class="kp-order-form-date-input">
                    <div class="op-input type-icon">
                        {{--<input name="times[{{$length or $day_number + 1}}]" type="text" placeholder="..." value="" data-jsmask="time">--}}
                        <input name="times[]" type="text" placeholder="..." value="{{@$day->time}}" data-jsmask="time" data-kpcalendar="time">
                        <div class="op-input-icon-time"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kp-personal-joinplace-address">
            <label class="kp-personal-joinplace-address-check">
                <span class="txt-label">Адрес</span>
                <span class="o-input-box-replaced">
                    <input type="checkbox" name="addr" tabindex="0" value="1">
                    <span class="o-input-box-replaced-check"></span>
                </span>
            </label>
        </div>
        <div class="kp-personal-joinplace-daynight">
            <div class="kp-calc-form-cols-item">
                <div class="navlist-merge type-fuses">
                    <div class="navlist-merge-item">
                        <label class="checks-options-btn">
                            @if(isset($day))
                                <input type="radio" name="nigth[{{$length or $day_number}}]" value="0" @if($day->nigth != 1) checked @endif>
                                {{--<input type="radio" name="nigth[]" value="1" @if($day->nigth == 0) checked @endif>--}}
                            @elseif(!isset($day))
                                <input type="radio" name="nigth[{{$length or $day_number}}]" value="0" checked>
                                {{--<input type="radio" name="nigth[]" value="1" checked>--}}
                            @endif
                            <span class="checks-options-btn-label curr-orange" onclick="turn_day_time('День');">День</span>
                        </label>
                    </div>
                    <div class="navlist-merge-item">
                        <label class="checks-options-btn">
                            @if(isset($day))
                                <input type="radio" name="nigth[{{$length or $day_number}}]" value="1" @if($day->nigth == 1) checked @endif>
                                {{--<input type="radio" name="vid_yborki[]" value="2" @if($day->nigth == 1) checked @endif>--}}
                            @elseif(!isset($day))
                                <input type="radio" name="nigth[{{$length or $day_number}}]" value="1">
                                {{--<input type="radio" name="nigth[]" value="2">--}}
                            @endif
                            <span class="checks-options-btn-label curr-orange" onclick="turn_day_time('Ночь');">Ночь</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-personal-sms">
        <div class="kp-personal-sms-message">
            {{--<textarea>Заказ № 8833. Метро: Новослободская в 8:00. Водитель Неклега 89255143401
    Леонте А, БалабайА, Шиян В</textarea>--}}
            @if(isset($day))
                <textarea>Заказ № {{$order->number}}. Метро: {{@$day->metro->name}} в {{$day->time}}. Водитель @forelse($day->drivers as $user_day) {{$user_day->user->surname.' '.@$user_day->user->phones()->first()->phone.' '}} @empty @endforelse
                    Сотрудники @foreach($day->workers as $user_key => $user_day) {{$user_day->user->surname.' '}} @endforeach
                </textarea>
            @else
                <textarea></textarea>
            @endif
        </div>
        <div class="kp-personal-sms-btns">
            <span class="kp-personal-sms-button bgs-orange">Отправить смс</span>
            <span class="kp-personal-sms-button bgs-gray">Отмена заказа</span>
        </div>
    </div>

    <div class="kp-personal-rate">
        <div class="kp-personal-checkeds">
            <div class="kp-personal-checkeds-label">Оценка бригады:</div>
            <label class="checks-options-btn">
                <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="0" checked>
                <span class="checks-options-btn-label curr-orange">Без оценки</span>
            </label>
            <label class="checks-options-btn">
                <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="1">
                <span class="checks-options-btn-label curr-orange">1</span>
            </label>
            <label class="checks-options-btn">
                <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="2">
                <span class="checks-options-btn-label curr-orange">2</span>
            </label>
            <label class="checks-options-btn">
                <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="3">
                <span class="checks-options-btn-label curr-orange">3</span>
            </label>
            <label class="checks-options-btn">
                <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="4">
                <span class="checks-options-btn-label curr-orange">4</span>
            </label>
            <label class="checks-options-btn">
                    <input onclick="ocenka_brigady(this);" type="radio" name="ocenka_brigady[{{$length or $day_number}}]" value="5">
                <span class="checks-options-btn-label curr-orange">5</span>
            </label>
        </div>
    </div>


    <div class="kp-personal-person-items-box {{--type-disabled--}}" data-brigadier-driver="wrap">
        <div class="kp-personal-person-items-header">
                <span class="o-input-box-replaced">
                    <input onclick="select_all_turn_users(this);" type="checkbox" name="allow_brigadier_driver" tabindex="0" value="1" data-brigadier-driver="check">
                    <span class="o-input-box-replaced-check"></span>
                </span>
            <span class="txt-label">Бригадир / Водитель</span>
        </div>
        <div class="kp-personal-person-items type-driver">
            <div class="kp-personal-person-row row-heads">
                <div class="kp-personal-person-col col-check"></div>
                <div class="kp-personal-person-col col-person"></div>
                <div class="kp-personal-person-col col-typer"></div>
                <div class="kp-personal-person-col col-pay">Оплата</div>
                <div class="kp-personal-person-col col-rating">Оценка</div>
                <div class="kp-personal-person-col col-checkgeneral">Главный</div>
                <div class="kp-personal-person-col col-comment">Комментарий:</div>
            </div>

            @if(isset($day->drivers))
                @forelse($day->drivers as $user_key => $user_day)
                    @if($user_day->user)
                        <input name="user_ids_v[{{$day_number + 1}}][]" value="{{$user_day->id}}" type="hidden" user_ids_v="{{$user_day->id}}"/>
                        <div class="kp-personal-person-row">
                            <div class="kp-personal-person-col col-check">
                                    <span class="o-input-box-replaced">
                                        <input type="checkbox" name="person_item[]" tabindex="0" value="1">
                                        <span class="o-input-box-replaced-check"></span>
                                    </span>
                            </div>
                            <div class="kp-personal-person-col col-person">
                                <div class="kp-personal-person-profile">
                                    <div class="op-input type-btns" data-op-input="item">
                                        <div class="op-input-control-btn" data-op-input="add_person">
                                            <span class="icon-input-ctrl-down"></span>
                                            <span class="icon-input-ctrl-down-h btn-hover"></span>
                                        </div>
                                        <div class="op-input-control-btn" data-op-input="search">
                                            <span class="icon-input-ctrl-search"></span>
                                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                                        </div>
                                        {{--<input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">--}}
                                        {{--<input name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">--}}
                                        <input user_id="{{$user_day->user->id}}" data-name="user_id_v[{{$day_number + 1}}][]" type="text" placeholder="..." value="{{$user_day->user->fio()}}" data-op-input="get" data-user_block="Workers">
                                    </div>

                                    {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-typer">
                                <div class="kp-personal-person-typer">
                                    <div class="person-type-checks-bord">
                                        <div class="person-type-checks">
                                            <div class="person-type-check-item">
                                                <label class="person-type-check-btn">
                                                    <input type="radio" name="fouls_v[{{$day_number + 1}}][{{$user_key}}]" value="late" @if($user_day->late == 1) checked @endif>
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-time"></span>
                                        <span class="icon-check-checked ic-time-checked"></span>
                                     </span>
                                                </label>
                                            </div>
                                            <div class="person-type-check-item">
                                                <label class="person-type-check-btn">
                                                    <input type="radio" name="fouls_v[{{$day_number + 1}}][{{$user_key}}]" value="nocome" @if($user_day->nocome == 1) checked @endif>
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-flash"></span>
                                        <span class="icon-check-checked ic-flash-checked"></span>
                                     </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kp-personal-person-plusminus">
                                        <div class="kp-personal-person-plusminus-ins">
                                            <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'driver');">+</div>
                                            <div user_ids_v="{{$user_day->id}}" class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'driver');">-</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-pay">
                                <div class="op-input" data-op-input="item">
                                    {{--<input name="amount_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" data-jsmask="num_pay">--}}
                                    <input name="amount_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" value="{{$user_day->amount ? $user_day->amount : ''}}" data-jsmask="num_pay">
                                    {{--<input name="amount_v[{{$day_number + 1}}][]" value="{{$user_day->amount ? $user_day->amount : ''}}" type="text" placeholder="Сумма" />--}}
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-rating">
                                <div class="kp-personal-checkeds">
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="0" @if(empty($user_day->rating)) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">Без оценки</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="1" @if($user_day->rating == 1) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">1</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="2" @if($user_day->rating == 2) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">2</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="3" @if($user_day->rating == 3) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">3</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="4" @if($user_day->rating == 4) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">4</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="5" @if($user_day->rating == 5) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">5</span>
                                    </label>
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-checkgeneral">
                                <span class="o-input-box-replaced">
                                    <input type="radio" name="main_v[{{$length or $day_number + 1}}]" tabindex="0" value="{{$user_day->user->id}}" @if($user_day->main) checked @endif>
                                    <span class="o-input-box-replaced-check"></span>
                                </span>
                            </div>
                            <div class="kp-personal-person-col col-comment">
                                <div class="op-input">
                                    <textarea name="comment_v[{{$length or $day_number + 1}}][{{$user_key}}]" placeholder="..." data-autoheight="true">{{$user_day->comment}}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif
                @empty
                    <div class="kp-personal-person-row">
                        <div class="kp-personal-person-col col-check">
                            <span class="o-input-box-replaced">
                                <input type="checkbox" name="person_item[]" tabindex="0" value="1">
                                <span class="o-input-box-replaced-check"></span>
                            </span>
                        </div>
                        <div class="kp-personal-person-col col-person">
                            <div class="kp-personal-person-profile">
                                <div class="op-input type-btns" data-op-input="item">
                                    <div class="op-input-control-btn" data-op-input="add_person">
                                        <span class="icon-input-ctrl-down"></span>
                                        <span class="icon-input-ctrl-down-h btn-hover"></span>
                                    </div>
                                    <div class="op-input-control-btn" data-op-input="search">
                                        <span class="icon-input-ctrl-search"></span>
                                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                                    </div>
                                    {{--<input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">--}}
                                    {{--<input name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">--}}
                                    <input data-name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">
                                </div>

                                {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-typer">
                            <div class="kp-personal-person-typer">
                                <div class="person-type-checks-bord">
                                    <div class="person-type-checks">
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls_v[{{$day_number + 1}}][0]" value="late">
                              <span class="person-type-check-btn-label">
                        <span class="icon-check ic-time"></span>
                        <span class="icon-check-checked ic-time-checked"></span>
                     </span>
                                            </label>
                                        </div>
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls_v[{{$day_number + 1}}][0]" value="nocome">
                              <span class="person-type-check-btn-label">
                        <span class="icon-check ic-flash"></span>
                        <span class="icon-check-checked ic-flash-checked"></span>
                     </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="kp-personal-person-plusminus">
                                    <div class="kp-personal-person-plusminus-ins">
                                        <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'driver');">+</div>
                                        <div class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'driver');">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-pay">
                            <div class="op-input" data-op-input="item">
                                <input name="amount_v[{{$day_number + 1}}][0]" type="text" placeholder="..." data-op-input="get" data-jsmask="num_pay">
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-rating">
                            <div class="kp-personal-checkeds">
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][0]" value="0" checked>
                                    <span class="checks-options-btn-label curr-orange">Без оценки</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][0]" value="1">
                                    <span class="checks-options-btn-label curr-orange">1</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][0]" value="2">
                                    <span class="checks-options-btn-label curr-orange">2</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][0]" value="3">
                                    <span class="checks-options-btn-label curr-orange">3</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][1]" value="4">
                                    <span class="checks-options-btn-label curr-orange">4</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$day_number + 1}}][0]" value="5">
                                    <span class="checks-options-btn-label curr-orange">5</span>
                                </label>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-checkgeneral">
                <span class="o-input-box-replaced">
                    <input type="radio" name="main_v[{{$day_number + 1}}]" tabindex="0" value="1">
                    <span class="o-input-box-replaced-check"></span>
                </span>
                        </div>
                        <div class="kp-personal-person-col col-comment">
                            <div class="op-input">
                                <textarea name="comment_v[{{$day_number + 1}}][0]" placeholder="..." data-autoheight="true"></textarea>
                            </div>
                        </div>
                    </div>
                @endforelse
            @else
                <div class="kp-personal-person-row">
                        <div class="kp-personal-person-col col-check">
                            <span class="o-input-box-replaced">
                                <input type="checkbox" name="person_item[]" tabindex="0" value="1">
                                <span class="o-input-box-replaced-check"></span>
                            </span>
                        </div>
                        <div class="kp-personal-person-col col-person">
                            <div class="kp-personal-person-profile">
                                <div class="op-input type-btns" data-op-input="item">
                                    <div class="op-input-control-btn" data-op-input="add_person">
                                        <span class="icon-input-ctrl-down"></span>
                                        <span class="icon-input-ctrl-down-h btn-hover"></span>
                                    </div>
                                    <div class="op-input-control-btn" data-op-input="search">
                                        <span class="icon-input-ctrl-search"></span>
                                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                                    </div>
                                    {{--<input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">--}}
                                    {{--<input name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">--}}
                                    <input data-name="user_id_v[{{$length + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">
                                </div>

                                {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-typer">
                            <div class="kp-personal-person-typer">
                                <div class="person-type-checks-bord">
                                    <div class="person-type-checks">
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls_v[{{$length + 1}}][0]" value="late">
                                      <span class="person-type-check-btn-label">
                                <span class="icon-check ic-time"></span>
                                <span class="icon-check-checked ic-time-checked"></span>
                             </span>
                                            </label>
                                        </div>
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls_v[{{$length + 1}}][0]" value="nocome">
                                      <span class="person-type-check-btn-label">
                                <span class="icon-check ic-flash"></span>
                                <span class="icon-check-checked ic-flash-checked"></span>
                             </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="kp-personal-person-plusminus">
                                    <div class="kp-personal-person-plusminus-ins">
                                        <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'driver');">+</div>
                                        <div class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'driver');">-</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-pay">
                            <div class="op-input" data-op-input="item">
                                <input name="amount_v[{{$length + 1}}][0]" type="text" placeholder="..." data-op-input="get" data-jsmask="num_pay">
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-rating">
                            <div class="kp-personal-checkeds">
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length + 1}}][0]" value="0" checked>
                                    <span class="checks-options-btn-label curr-orange">Без оценки</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length + 1}}][0]" value="1">
                                    <span class="checks-options-btn-label curr-orange">1</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length + 1}}][0]" value="2">
                                    <span class="checks-options-btn-label curr-orange">2</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][0]" value="3">
                                    <span class="checks-options-btn-label curr-orange">3</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length + 1}}][0]" value="4">
                                    <span class="checks-options-btn-label curr-orange">4</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="brigadir_rating[{{$length + 1}}][0]" value="5">
                                    <span class="checks-options-btn-label curr-orange">5</span>
                                </label>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-checkgeneral">
                        <span class="o-input-box-replaced">
                            <input type="radio" name="main_v[{{$length + 1}}]" tabindex="0" value="1">
                            <span class="o-input-box-replaced-check"></span>
                        </span>
                        </div>
                        <div class="kp-personal-person-col col-comment">
                            <div class="op-input">
                                <textarea name="comment_v[{{$length + 1}}][0]" placeholder="..." data-autoheight="true"></textarea>
                            </div>
                        </div>
                    </div>
            @endif
        </div>
    </div>


    <div class="kp-personal-person-items-box">
        <div class="kp-personal-person-items-header"><span class="txt-label">Сотрудники</span></div>
        <div class="kp-personal-person-items cleaner-users">
            <div class="kp-personal-person-row row-heads">
                <div class="kp-personal-person-col col-check"></div>
                <div class="kp-personal-person-col col-person"></div>
                <div class="kp-personal-person-col col-typer">
                    <div class="navlist-merge-infos">
                        <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                    </div>
                </div>
                <div class="kp-personal-person-col col-pay">Оплата</div>
                <div class="kp-personal-person-col col-rating">Оценка</div>
                <div class="kp-personal-person-col col-comment">Комментарий:</div>
            </div>

            @if(isset($day->workers))
                @forelse($day->workers as $user_key => $user_day)
                    @if($user_day->user)
                        <input name="user_ids[{{$day_number + 1}}][]" value="{{$user_day->id}}" type="hidden" user_ids="{{$user_day->id}}" />
                        <div class="kp-personal-person-row">
                            <div class="kp-personal-person-col col-check">
                                    <span class="o-input-box-replaced">
                                        <input type="checkbox" name="sotrudnik_item[]" tabindex="0" value="1">
                                        <span class="o-input-box-replaced-check"></span>
                                    </span>
                            </div>
                            <div class="kp-personal-person-col col-person">
                                <div class="kp-personal-person-profile">
                                    <div class="op-input type-btns" data-op-input="item">
                                        <div class="op-input-control-btn" data-op-input="add_person">
                                            <span class="icon-input-ctrl-down"></span>
                                            <span class="icon-input-ctrl-down-h btn-hover"></span>
                                        </div>
                                        <div class="op-input-control-btn" data-op-input="search">
                                            <span class="icon-input-ctrl-search"></span>
                                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                                        </div>
                                        {{--<input name="user_id[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" class="js-personal-person" data-user_block="Workers">--}}
                                        <input user_id="{{$user_day->user->id}}" value="{{$user_day->user->fio()}}" data-name="user_id[{{$length or $day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" class="js-personal-person" data-user_block="Workers">
                                    </div>

                                    {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-typer">
                                <div class="kp-personal-person-typer">
                                    <div class="person-type-checks-bord">
                                        <div class="person-type-checks">
                                            <div class="person-type-check-item">
                                                <label class="person-type-check-btn">
                                                    <input type="radio" name="fouls[{{$day_number + 1}}][{{$user_key}}]" value="late" @if($user_day->late == 1) checked @endif>
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-time"></span>
                                        <span class="icon-check-checked ic-time-checked"></span>
                                     </span>
                                                </label>
                                            </div>
                                            <div class="person-type-check-item">
                                                <label class="person-type-check-btn">
                                                    <input type="radio" name="fouls[{{$day_number + 1}}][{{$user_key}}]" value="nocome" @if($user_day->nocome == 1) checked @endif>
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-flash"></span>
                                        <span class="icon-check-checked ic-flash-checked"></span>
                                     </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kp-personal-person-plusminus-ins">
                                        <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'cleaner');">+</div>
                                        <div user_ids="{{$user_day->id}}" class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'cleaner');">-</div>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-pay">
                                <div class="op-input" data-op-input="item">
                                    <input name="amount[{{$day_number + 1}}][{{$user_key}}]" type="text" placeholder="..." value="{{$user_day->amount ? $user_day->amount : ''}}" data-op-input="get" data-jsmask="num_pay">
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-rating">
                                <div class="kp-personal-checkeds">
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][{{$user_key}}]" value="0" @if(empty($user_day->rating)) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">Без оценки</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$length or $day_number + 1}}][{{$user_key}}]" value="1" @if($user_day->rating == 1) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">1</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][{{$user_key}}]" value="2" @if($user_day->rating == 2) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">2</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][{{$user_key}}]" value="3" @if($user_day->rating == 3) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">3</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][{{$user_key}}]" value="4" @if($user_day->rating == 4) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">4</span>
                                    </label>
                                    <label class="checks-options-btn">
                                        <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][{{$user_key}}]" value="5" @if($user_day->rating == 5) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">5</span>
                                    </label>
                                </div>
                            </div>
                            <div class="kp-personal-person-col col-comment">
                                <div class="op-input">
                                    <textarea name="comment[{{$day_number + 1}}][{{$user_key}}]" placeholder="..." data-autoheight="true">{{$user_day->comment}}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif
                @empty
                    <div class="kp-personal-person-row">
                        <div class="kp-personal-person-col col-check">
                        <span class="o-input-box-replaced">
                            <input type="checkbox" name="sotrudnik_item[]" tabindex="0" value="1">
                            <span class="o-input-box-replaced-check"></span>
                        </span>
                        </div>
                        <div class="kp-personal-person-col col-person">
                            <div class="kp-personal-person-profile">
                                <div class="op-input type-btns" data-op-input="item">
                                    <div class="op-input-control-btn" data-op-input="add_person">
                                        <span class="icon-input-ctrl-down"></span>
                                        <span class="icon-input-ctrl-down-h btn-hover"></span>
                                    </div>
                                    <div class="op-input-control-btn" data-op-input="search">
                                        <span class="icon-input-ctrl-search"></span>
                                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                                    </div>
                                    {{--<input name="user_id[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" class="js-personal-person" data-user_block="Workers">--}}
                                    <input data-name="user_id[{{$day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" class="js-personal-person" data-user_block="Workers">
                                </div>

                                {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-typer">
                            <div class="kp-personal-person-typer">
                                <div class="person-type-checks-bord">
                                    <div class="person-type-checks">
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls[{{$day_number + 1}}][0]" value="late">
                                  <span class="person-type-check-btn-label">
                            <span class="icon-check ic-time"></span>
                            <span class="icon-check-checked ic-time-checked"></span>
                         </span>
                                            </label>
                                        </div>
                                        <div class="person-type-check-item">
                                            <label class="person-type-check-btn">
                                                <input type="radio" name="fouls[{{$day_number + 1}}][0]" value="nocome">
                                  <span class="person-type-check-btn-label">
                            <span class="icon-check ic-flash"></span>
                            <span class="icon-check-checked ic-flash-checked"></span>
                         </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="kp-personal-person-plusminus-ins">
                                    <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'cleaner');">+</div>
                                    <div class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'cleaner');">-</div>
                                </div>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-pay">
                            <div class="op-input" data-op-input="item">
                                <input name="amount[{{$day_number + 1}}][0]" type="text" placeholder="..." value="" data-op-input="get" data-jsmask="num_pay">
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-rating">
                            <div class="kp-personal-checkeds">
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="0" checked>
                                    <span class="checks-options-btn-label curr-orange">Без оценки</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="1">
                                    <span class="checks-options-btn-label curr-orange">1</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="2">
                                    <span class="checks-options-btn-label curr-orange">2</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="3">
                                    <span class="checks-options-btn-label curr-orange">3</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="4">
                                    <span class="checks-options-btn-label curr-orange">4</span>
                                </label>
                                <label class="checks-options-btn">
                                    <input type="radio" name="sotrudnik_rating[{{$day_number + 1}}][0]" value="5">
                                    <span class="checks-options-btn-label curr-orange">5</span>
                                </label>
                            </div>
                        </div>
                        <div class="kp-personal-person-col col-comment">
                            <div class="op-input">
                                <textarea name="comment[{{$day_number + 1}}][0]" placeholder="..." data-autoheight="true"></textarea>
                            </div>
                        </div>
                    </div>
                @endforelse
            @else
                <div class="kp-personal-person-row">
                    <div class="kp-personal-person-col col-check">
                        <span class="o-input-box-replaced">
                            <input type="checkbox" name="sotrudnik_item[]" tabindex="0" value="1">
                            <span class="o-input-box-replaced-check"></span>
                        </span>
                    </div>
                    <div class="kp-personal-person-col col-person">
                        <div class="kp-personal-person-profile">
                            <div class="op-input type-btns" data-op-input="item">
                                <div class="op-input-control-btn" data-op-input="add_person">
                                    <span class="icon-input-ctrl-down"></span>
                                    <span class="icon-input-ctrl-down-h btn-hover"></span>
                                </div>
                                <div class="op-input-control-btn" data-op-input="search">
                                    <span class="icon-input-ctrl-search"></span>
                                    <span class="icon-input-ctrl-search-h btn-hover"></span>
                                </div>
                                {{--<input name="user_id[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" class="js-personal-person" data-user_block="Workers">--}}
                                <input data-name="user_id[{{$length + 1}}][]" type="text" placeholder="..." data-op-input="get" class="js-personal-person" data-user_block="Workers">
                            </div>

                            {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
                        </div>
                    </div>
                    <div class="kp-personal-person-col col-typer">
                        <div class="kp-personal-person-typer">
                            <div class="person-type-checks-bord">
                                <div class="person-type-checks">
                                    <div class="person-type-check-item">
                                        <label class="person-type-check-btn">
                                            <input type="radio" name="fouls[{{$length + 1}}][0]" value="late">
                                  <span class="person-type-check-btn-label">
                            <span class="icon-check ic-time"></span>
                            <span class="icon-check-checked ic-time-checked"></span>
                         </span>
                                        </label>
                                    </div>
                                    <div class="person-type-check-item">
                                        <label class="person-type-check-btn">
                                            <input type="radio" name="fouls[{{$length + 1}}][0]" value="nocome">
                                  <span class="person-type-check-btn-label">
                            <span class="icon-check ic-flash"></span>
                            <span class="icon-check-checked ic-flash-checked"></span>
                         </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-personal-person-plusminus-ins">
                                <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'cleaner');">+</div>
                                <div class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'cleaner');">-</div>
                            </div>
                        </div>
                    </div>
                    <div class="kp-personal-person-col col-pay">
                        <div class="op-input" data-op-input="item">
                            <input name="amount[{{$length + 1}}][0]" type="text" placeholder="..." value="" data-op-input="get" data-jsmask="num_pay">
                        </div>
                    </div>
                    <div class="kp-personal-person-col col-rating">
                        <div class="kp-personal-checkeds">
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="0" checked>
                                <span class="checks-options-btn-label curr-orange">Без оценки</span>
                            </label>
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="1">
                                <span class="checks-options-btn-label curr-orange">1</span>
                            </label>
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="2">
                                <span class="checks-options-btn-label curr-orange">2</span>
                            </label>
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="3">
                                <span class="checks-options-btn-label curr-orange">3</span>
                            </label>
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="4">
                                <span class="checks-options-btn-label curr-orange">4</span>
                            </label>
                            <label class="checks-options-btn">
                                <input type="radio" name="sotrudnik_rating[{{$length + 1}}][0]" value="5">
                                <span class="checks-options-btn-label curr-orange">5</span>
                            </label>
                        </div>
                    </div>
                    <div class="kp-personal-person-col col-comment">
                        <div class="op-input">
                            <textarea name="comment[{{$length + 1}}][0]" placeholder="..." data-autoheight="true"></textarea>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>


    <div class="kp-personal-person-bottom">
        <div class="page-kp-form-bottom-button">
            <div class="page-kp-form-bottom-button-right">
                <span data-temp_id="{{$rand or $order->number+$day_number}}" class="page-kp-form-button bgs-red" onclick="turn_delete(this);">Удалить смену</span>
            </div>
        </div>
    </div>

    <div class="page-kp-form-bottom-button type-left">
        <button type="button" class="page-kp-form-button bgs-green" onclick="submit_staff_order_form();">Сохранить</button>
        <button type="button" onclick="$('#staff-order-form').prepend('<input type=\'hidden\' name=\'route\' value=\'back\'>'); submit_staff_order_form();" class="page-kp-form-button bgs-orange">Применить</button>
    </div>
</div>

