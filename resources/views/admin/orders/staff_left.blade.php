<div class="kp-tab-personal-left">
    <div class="dash-content">
        {{--@if(sizeof($turns) > 0)--}}
            <div class="dash-table" data-item-check="wrap">
                <table>
                    <thead>
                    <tr>
                        <td>№</td>
                        <td>Смена</td>
                        <td>Дата</td>
                        <td>Человек</td>
                        <td>Сумма</td>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($turns as $key=>$turn)
                            {{--<tr data-btn-personal-loadr="" data-temp_id="{{$order->number+$key}}" onclick="turn_view({{$order->number+$key}});">--}}
                            <tr data-btn-personal-loadr="" data-temp_id="{{$order->number+$key}}" onclick="turn_view(this);">
                                <td>{{$key+1}}</td>
                                <td>@if($turn->nigth == 0)День@elseНочь@endif</td>
                                <td>{{date('d.m.Y', strtotime($turn->date))}}</td>
                                <td>{{$turn->users_count}}</td>
                                <td>{{$turn->sum_amount}}</td>
                            </tr>
                        @empty
                        @endforelse

                        <tr>
                            <td></td>
                            <td></td>
                            <td id="total_turns_users">Итого:</td>
                            <td></td>
                            <td id="total_turns_sum">Итого:</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        {{--@endif--}}
    </div>


    {{--<div class="kp-order-form-row clearfix type-person">
        <div class="kp-order-form-labinput">
            <div class="op-input-label">Ответственный:</div>
            <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                <div class="op-input type-btns" data-op-input="item">
                    <div class="op-input-control-btn" data-op-input="add_person">
                        <span class="icon-input-ctrl-down"></span>
                        <span class="icon-input-ctrl-down-h btn-hover"></span>
                    </div>
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">
                </div>

                <div class="kp-order-form-person-item" data-persons="item">
                    <div class="kp-order-form-person-item-ava" style="background-image:url(img/blank/avatar.png)"></div>
                    <div class="kp-order-form-person-item-name">Джон Джонсон</div>
                    <div class="kp-order-form-person-item-remove" data-persons="remove"></div>
                </div>
                <div class="kp-order-form-person-item" data-persons="item">
                    <div class="kp-order-form-person-item-ava" style="background-image:url(img/blank/avatar.png)"></div>
                    <div class="kp-order-form-person-item-name">Джон Джонсон</div>
                    <div class="kp-order-form-person-item-remove" data-persons="remove"></div>
                </div>

            </div>
        </div>
    </div>--}}

</div>