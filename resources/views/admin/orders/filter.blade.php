<form id="orders_filter_form" class="dash-search" data-dash-search="wrap" method="GET" action="{{route('admin.orders.index')}}">
    <input type="hidden" name="f[perpage]" value="{{@$filters["perpage"]}}">
    <input type="hidden" name="f[status]" @if(isset($filters['status'])) value="{{$filters['status']}}" @endif>

    <div class="dash-search-option">
        {{--{{dd(session()->get('admin.orders.index.filters'))}}--}}
        {{--{{dd($filters['user_id'])}}--}}
        @if(isset($filters))
            @if (!empty($filters) && !empty($filters['user_id']))
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        {{\App\User::find($filters['user_id'])->fio()}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'user_id', 'orders');"></span>
                </span>
            @endif
            @if( isset($filters["status"]) && !empty($filters["status"]) )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{\App\Models\Order::$statuses[$filters["status"]]['name']}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'status', 'orders');"></span>
                </span>
            @endif

            @if( @$filters["sum"] > 0 )
                @if($filters["sum_compare"] == 'equal')
                    <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        {{$filters["sum"]}}
                        <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'orders');"></span>
                    </span>
                @elseif($filters["sum_compare"] == 'more')
                        <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                            >{{$filters["sum"]}}
                            <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'orders');"></span>
                    </span>
                @elseif($filters["sum_compare"] == 'less')
                    <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        <{{$filters["sum"]}}
                        <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'orders');"></span>
                    </span>
                @endif
            @elseif( @$filters["sum_min"] > 0 && @$filters["sum_max"] > 0 )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{$filters["sum_min"].' - '.$filters["sum_max"]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum_compare', 'orders');"></span>
                </span>
            @endif

            @if( !empty($filters["date_start"]) && $filters["date_start"] != 'any' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Начало - {{\App\Models\Order::$dates_filter[$filters["date_start"]]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'date_start', 'orders');"></span>
                </span>
            @endif

            @if( !empty($filters["date_created"]) && $filters["date_created"] != 'any' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Создан - {{\App\Models\Order::$dates_filter[$filters["date_created"]]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'date_created', 'orders');"></span>
                </span>
            @endif

            @if( !empty($filters["date_update"]) && $filters["date_update"] != 'any' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Изменено - {{\App\Models\Order::$dates_filter[$filters["date_update"]]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'date_update', 'orders');"></span>
                </span>
            @endif

            @if( !empty($filters["date_finish"]) && $filters["date_finish"] != 'any' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Окончание - {{\App\Models\Order::$dates_filter[$filters["date_finish"]]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'date_finish', 'orders');"></span>
                </span>
            @endif

            @if( !empty($filters["typ"]) )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Вид работ - @if($filters["typ"] == 82)ген. уборка @elseif($filters["typ"] == 98)послестрой @endif
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'typ', 'orders');"></span>
                </span>
            @endif

            @if( @$filters["payment"] != '' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Вид оплат - @if($filters["payment"] == "10")Безналичный @elseif($filters["payment"] == "0")Наличный @endif
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'payment', 'orders');"></span>
                </span>
            @endif

            @if( @$filters["pay_status"] != '' )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Статус оплаты - {{\App\Models\Order::$payStatuses[$filters["pay_status"]]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'pay_status', 'orders');"></span>
                </span>
            @endif

        @endif
    </div>
    <div class="dash-search-form">
        <div class="dash-search-form-input">
            <input type="text" placeholder="+ поиск" data-dash-search="input" data-layer="yes">
            <div class="dash-search-form-hover"></div>
        </div>
        <label class="dash-search-form-btn type-submit">
            <span class="icon-input-ctrl-search"></span>
            <span class="icon-input-ctrl-search-h btn-hover"></span>
            <input type="submit" value=" ">
        </label>
        <label class="dash-search-form-btn type-remove" onclick="reset_filter('orders');">
            <span class="icon-input-ctrl-clear"></span>
            <span class="icon-input-ctrl-clear-h btn-hover"></span>
            <input type="reset" value=" ">
        </label>
    </div>
    <div class="dash-search-drop" data-dash-search="drop">
        <div class="dash-search-drop-ins">
            <div class="dash-search-drop-left">
                <ul class="dash-search-drop-list">
                    <li onclick="set_orders_filter_status('new', this);" @if( @$filters['status'] == 'new') class="current" @endif>Новые</li>
                    <li onclick="set_orders_filter_status('my', this);" @if( @$filters['status'] == 'my') class="current" @endif>Мои сделки</li>
                    <li onclick="set_orders_filter_status('first', this);" @if( @$filters['status'] == 'first') class="current" @endif>Первичный контакт</li>
                    <li onclick="set_orders_filter_status('agreed', this);" @if( @$filters['status'] == 'agreed') class="current" @endif>Согласовано</li>
                    <li onclick="set_orders_filter_status('work', this);" @if( @$filters['status'] == 'work') class="current" @endif>В работе</li>
                    <li onclick="set_orders_filter_status('performed', this);" @if( @$filters['status'] == 'performed') class="current" @endif>Работы выполнены</li>
                    {{--<li onclick="set_orders_filter_status('not_paid', this);" @if( @$filters['status'] == 'not_paid') class="current" @endif>Не оплачено</li>--}}
                    <li onclick="set_orders_filter_status('final_positive', this);" @if( @$filters['status'] == 'final_positive') class="current" @endif>Успешно реализовано</li>
                    <li onclick="set_orders_filter_status('final_negative', this);" @if( @$filters['status'] == 'final_negative') class="current" @endif>Не реализовано</li>
                </ul>
            </div>
            <div class="dash-search-drop-right">
                <div class="dash-search-drop-form">
                    <div class="o-form-row">
                        <div class="o-form-row-label">Ответственный</div>
                        <div class="o-form-row-input">
                            {{--<select name="f[id]" id="filter_id" class="chosen-select chosen-autocomplite-base col-sm-8" data-url="{{route('admin.users.search')}}" data-placeholder="Начните ввод...">
                                <option value="{{$filters['id'] or ''}}">{{$filters['fio'] or ' '}}</option>
                            </select>--}}
                            {{--<select class="select-chosen" data-width="100%" name="f[user_id]">
                                <option>Точно</option>
                                <option>Без разницы</option>
                                <option>Нет</option>
                            </select>--}}
                            {{--{{dump($filters)}}--}}
                            <select id="user_id" class="chosen-select chosen-autocomplite-base col-sm-8 chosen-select chosen-autocomplite-base" data-url="{{route('admin.users.search')}}" data-group="management" data-placeholder="Начните ввод..." data-width="100%" name="f[user_id]">
                                <option value="{{$filters['user_id'] or ''}}">{{$filters['fio'] or ' '}}</option>
                            </select>
                        </div>
                    </div>
                    </div>

                    <div class="o-form-row">
                        <div class="o-form-row-label">Сумма</div>
                        <div class="o-form-row-sum">
                            <div class="o-form-row-sum-col col-type">
                                <div class="o-form-row-input">
                                    <select class="selectpicker" data-width="100%" name="f[sum_compare]">
                                        <option value="equal" @if(!@$filters['sum_compare'] || @$filters['sum_compare'] == 'equal') selected @endif>Точно</option>
                                        <option value="interval" @if(@$filters['sum_compare'] == 'interval') selected @endif>Диапазон</option>
                                        <option value="more" @if(@$filters['sum_compare'] == 'more') selected @endif>Больше чем</option>
                                        <option value="less" @if(@$filters['sum_compare'] == 'less') selected @endif>Меньше чем</option>
                                    </select>
                                </div>
                            </div>
                            <div class="o-form-row-sum-col col-value">
                                <div class="o-form-row-input">
                                    {{--<input type="text" placeholder="См. реализацию в битриксе">--}}
                                    @if( @$filters['sum_compare'] != 'interval' )
                                        <input type="text" name="f[sum]" value="{{@$filters['sum']}}" />
                                    @else
                                        {{--<input type="text" name="f[sum_min]" value="{{@$filters['sum_min']}}" />
                                        <input name="f[sum_max]" type="text" value="{{@$filters['sum_max']}}" />--}}
                                        <div class="o-form-row-sum-col col-value">
                                            <div class="o-form-row-input" style="display: flex;">
                                                <input type="text" name="f[sum_min]" style="width: 48%;" value="{{@$filters['sum_min']}}" />
                                                <div class="interval-dash">&nbsp;-&nbsp;</div>
                                                <input name="f[sum_max]" type="text" style="width: 48%;" value="{{@$filters['sum_max']}}" />
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Дата и время начала</div>
                        <div class="o-form-row-input">
                            <select class="selectpicker" data-width="100%" name="f[date_start]">
                                <option value="any" @if(@$filters['date_start'] == 'any') selected @endif>Любая дата</option>
                                <option value="yesterday" @if(@$filters['date_start'] == 'yesterday') selected @endif>Вчера</option>
                                <option value="today" @if(@$filters['date_start'] == 'today') selected @endif>Сегодня</option>
                                <option value="tomorrow" @if(@$filters['date_start'] == 'tomorrow') selected @endif>Завтра</option>
                                <option value="week" @if(@$filters['date_start'] == 'week') selected @endif>Текущая неделя</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Дата создания</div>
                        <div class="o-form-row-input">
                            <select class="selectpicker" data-width="100%" name="f[date_created]">
                                <option value="any" @if(@$filters['date_created'] == 'any') selected @endif>Любая дата</option>
                                <option value="yesterday" @if(@$filters['date_created'] == 'yesterday') selected @endif>Вчера</option>
                                <option value="today" @if(@$filters['date_created'] == 'today') selected @endif>Сегодня</option>
                                <option value="week" @if(@$filters['date_created'] == 'week') selected @endif>Текущая неделя</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Дата изменения</div>
                        <div class="o-form-row-input">
                            <select class="selectpicker" data-width="100%" name="f[date_update]">
                                <option value="any" @if(@$filters['date_update'] == 'any') selected @endif>Любая дата</option>
                                <option value="yesterday" @if(@$filters['date_update'] == 'yesterday') selected @endif>Вчера</option>
                                <option value="today" @if(@$filters['date_update'] == 'today') selected @endif>Сегодня</option>
                                <option value="week" @if(@$filters['date_update'] == 'week') selected @endif>Текущая неделя</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Вид работ</div>
                        <div class="o-form-row-input">
                            <select name="f[typ]" class="selectpicker" data-width="100%">
                                <option value="">Любой</option>
                                <option value="82" @if( @$filters["typ"] == "82") selected @endif>ген. уборка</option>
                                <option value="98" @if( @$filters["typ"] == "98") selected @endif>послестрой</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Вид оплат</div>
                        <div class="o-form-row-input">
                            <select name="f[payment]" class="selectpicker" data-width="100%">
                                <option value="">Любой</option>
                                <option value="0" @if( @$filters["payment"] == "0") selected @endif>Наличный</option>
                                <option value="10" @if( @$filters["payment"] == "10") selected @endif>Безналичный</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Статус оплаты</div>
                        <div class="o-form-row-input">
                            <select name="f[pay_status]" class="selectpicker" data-width="100%">
                                <option value="">Любой</option>
                                <option value="payed" @if( @$filters["payment"] == "payed") selected @endif>Оплачен</option>
                                <option value="nopayed" @if( @$filters["payment"] == "nopayed") selected @endif>Не оплачен</option>
                                <option value="partpay" @if( @$filters["payment"] == "partpay") selected @endif>Частично оплачен</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Дата и время окончания</div>
                        <div class="o-form-row-input">
                            <select class="selectpicker" data-width="100%" name="f[date_finish]">
                                <option value="any" @if(@$filters['date_finish'] == 'any') selected @endif>Любая дата</option>
                                <option value="yesterday" @if(@$filters['date_finish'] == 'yesterday') selected @endif>Вчера</option>
                                <option value="today" @if(@$filters['date_finish'] == 'today') selected @endif>Сегодня</option>
                                <option value="tomorrow" @if(@$filters['date_finish'] == 'tomorrow') selected @endif>Завтра</option>
                                <option value="week" @if(@$filters['date_finish'] == 'week') selected @endif>Текущая неделя</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-bottom">
                        <input type="submit" value="Найти">
                        <input type="reset" value="Сбросить" onclick="reset_filter('orders')">
                    </div>
                </div>
        </div>
    </div>
</form>