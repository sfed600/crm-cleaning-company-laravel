@extends('admin.layout')
@section('main')
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="dash-container">
        <div class="dash-content">
            <div class="dash-table" data-item-check="wrap">
                {{--<div>
                    <em>Into This</em>
                    <select data-placeholder="Choose a Country..." class="chosen-select" tabindex="2">
                        <option value=""></option>
                        <option value="United States">United States</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Aland Islands">Aland Islands</option>
                        <option value="Albania">Albania</option>
                    </select>
                </div>--}}
                <table>
                    <colgroup>
                        <col class="tbl-check">
                        <col class="tbl-number">
                        <col class="tbl-title">
                        <col class="tbl-contact">
                        <col class="tbl-budget">
                        <col class="tbl-tasks">
                        <col class="tbl-typework">
                        <col class="tbl-typepay">
                        <col class="tbl-datestart">
                        <col class="tbl-step">
                        <col class="tbl-pay">
                        <col class="tbl-responsible">
                        <col class="tbl-datecreate">
                    </colgroup>
                    <thead>
                    <tr>
                        <td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                        </td>
                        <td>№</td>
                        <td>Название</td>
                        <td>Контакт</td>
                        <td><a class="tbl-heads-link" href="#">Бюджет, руб</a></td>
                        <td>Задачи</td>
                        <td>Вид работ</td>
                        <td>Вид оплат</td>
                        <td><a class="tbl-heads-link type-up" href="#">Дата начала</a></td>
                        <td>Этап заказа</td>
                        <td>Оплата</td>
                        <td>Ответственный</td>
                        <td><a class="tbl-heads-link" href="#">Дата создания</a></td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                                </td>
                                <td>{{$order->number}}</td>
                                <td>
                                    <a href="{{route('admin.orders.edit', $order->id)}}">{{$order->name}}</a>
                                </td>
                                <td>
                                    {{@$order->contacts()->first()->name}}
                                    @if(isset($order->orderCompany))
                                        <span class="tbl-block-company">“{{$order->orderCompany->name}}”</span>
                                    @endif
                                </td>
                                <td>{{number_format($order->amount, 0, ',', ' ')}}</td>
                                <td>Подготовить коммерческое предложение</td>
                                <td>
                                    @if( isset($order->calculator) )
                                        @if(@get_object_vars(\GuzzleHttp\json_decode($order->calculator))['typ'] == 82)
                                            ген. уборка
                                        @else
                                            послестрой
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @if( isset($order->calculator) )
                                        @if(@get_object_vars(\GuzzleHttp\json_decode($order->calculator))['payment'] == 10)
                                            Безналичный
                                        @else
                                            Наличный
                                        @endif
                                    @endif
                                </td>
                                <td>{{date('d.m.Y', strtotime($order->date))}}</td>
                                <td>
                                   <span class="pseudo-select" data-pseudo="wrap">
                                       @php
                                           $bg_status_class = '';
                                           switch ($order->status) {
                                               case 'first':
                                                   $bg_status_class = 'bgs-first';
                                                   break;
                                               case 'agree':
                                                   $bg_status_class = 'bgs-agree';
                                                   break;
                                               case 'agreed':
                                                   $bg_status_class = 'bgs-agreed';
                                                   break;
                                               case 'confirmed':
                                                   $bg_status_class = 'bgs-confirmed';
                                                   break;
                                               case 'work':
                                                   $bg_status_class = 'bgs-work';
                                                   break;
                                               case 'performed':
                                                   $bg_status_class = 'bgs-performed';
                                                   break;
                                               case 'final_positive':
                                                   $bg_status_class = 'bgs-final_positive';
                                                   break;
                                               case 'final_negative':
                                                   $bg_status_class = 'bgs-final_negative';
                                                   break;
                                           }
                                       @endphp
                                       <span class="pseudo-select-txt o-btn {{$bg_status_class}}" data-pseudo="title" tabindex="0">{{$order->statusName()}}</span>
                                       <span class="pseudo-select-drop" data-pseudo="drop">
                                            <span data-order_id="{{$order->id}}" data-value="first" data-class="bgs-first" @if($order->status == 'first') class="current" @endif>Первичный контакт</span>
                                            <span data-order_id="{{$order->id}}" data-value="agree" data-class="bgs-agree" @if($order->status == 'agree') class="current" @endif>Согласовать</span>
                                            <span data-order_id="{{$order->id}}" data-value="agreed" data-class="bgs-agreed" @if($order->status == 'agreed') class="current" @endif>Согласованно</span>
                                            <span data-order_id="{{$order->id}}" data-value="confirmed" data-class="bgs-confirmed" @if($order->status == 'confirmed') class="current" @endif>Подтверждено</span>
                                            <span data-order_id="{{$order->id}}" data-value="work" data-class="bgs-work" @if($order->status == 'work') class="current" @endif>В работе</span>
                                            <span data-order_id="{{$order->id}}" data-value="performed" data-class="bgs-performed" @if($order->status == 'performed') class="current" @endif>Работы выполнены</span>
                                            <span data-order_id="{{$order->id}}" data-value="final_positive" data-class="bgs-final_positive" @if($order->status == 'final_positive') class="current" @endif>Успешно реализовано</span>
                                            <span data-order_id="{{$order->id}}" data-value="final_negative" data-class="bgs-final_negative" @if($order->status == 'final_negative') class="current" @endif>Не реализовано</span>
                                       </span>
                                   </span>
                                </td>
                                <td>
                                    @php
                                        $pay_status = 'nopayed';
                                        if( $order->invoice )
                                            $pay_status = $order->invoice->status;
                                        else
                                            $pay_status = $order->pay_status;
                                    @endphp
                                   <span class="pseudo-select" data-pseudo="wrap">
                                      <span class="pseudo-select-txt" data-pseudo="title" tabindex="0">{{\App\Models\Order::$payStatuses[$pay_status]}}</span>
                                      <span class="pseudo-select-drop" data-pseudo="drop">
                                         <span data-order_id="{{$order->id}}" data-value="payed" @if($pay_status == 'payed') class="current" @endif>Оплачен</span>
                                         <span data-order_id="{{$order->id}}" data-value="nopayed" @if($pay_status == 'nopayed') class="current" @endif>Не оплачен</span>
                                         <span data-order_id="{{$order->id}}" data-value="partpay" @if($pay_status == 'partpay') class="current" @endif>Частично оплачен</span>
                                      </span>
                                   </span>
                                </td>
                                <td>
                                    @if( @$filters['user_id'] > 0 )
                                        {{$order->responsible_users()->where('order_user.user_id', $filters['user_id'])->first()->fio()}}
                                    @elseif( @$filters['status'] == 'my' )
                                        {{Auth::user()->fio()}}
                                    @elseif($order->responsible_users()->count() > 0 )
                                        {{$order->responsible_users()->first()->fio()}}
                                    @endif
                                </td>
                                <td>{{date('d.m.Y H:i', strtotime($order->created_at))}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    <div class="paginator-label">Показано {{sizeof($orders)}} из {{$count_orders}}</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'orders');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'orders');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'orders');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'orders');">100</a></li>
                        </ul>
                    </div>

                    {!! $orders->render() !!}
                </div>
            </div>

        <div class="dash-container-layer"></div>
    </div>

@stop