@if(Route::current()->getName() == 'admin.orders.index')
    <div class="dash-panel-block type-btns">
        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
            <div class="dash-panel-dropdown-drop">
                <a href="#">Ссылка 1</a>
                <a href="#">Ссылка 2</a>
                <a href="#">Ссылка 3</a>
            </div>
        </div>

        <a class="o-btn bgs-green o-hvr" href="{{route('admin.orders.create')}}">+ Добавить заказ</a>
    </div>
    <div class="dash-panel-block type-transactions">
        <span class="txt-label">{{@$count_orders}} сделок:</span>
        <span class="txt-value">{{number_format(@$sum_paginate, 0, ',', ' ')}} руб</span>
    </div>
@elseif(Route::current()->getName() == 'admin.orders.create' || Route::current()->getName() == 'admin.orders.edit')
    <div class="dash-panel-block type-btns">
        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
            <div class="dash-panel-dropdown-drop">
                <a href="#">Ссылка 1</a>
                <a href="#">Ссылка 2</a>
                <a href="#">Ссылка 3</a>
            </div>
        </div>

        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div onclick="window.location.href = '/admin/orders/create';" class="o-btn bgs-green o-hvr order-navbar-btn" data-dropped="btn" data-layer="no">+ Добавить Заказ</div>
            <div class="dash-panel-dropdown-drop" style="display: none;">
                <a href="#">Предложение по ежедневной уборке</a>
                <a href="#">Предложение по спец.работам</a>
            </div>
        </div>
    </div>
    <div class="dash-panel-block type-transactions">
        <span class="txt-label">Сумма:</span>
               <span class="txt-value">
                  <span class="op-input">
                      <input name="calculator_amount" type="text" placeholder="..." value="{{ old('amount', @$order->amount) }}">
                  </span>
                  руб.</span>
    </div>
@endif