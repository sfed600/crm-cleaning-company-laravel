<div class="dash-content">
    <div class="dash-table" data-item-check="wrap">
        <table>
            <thead>
            <tr>
                <td>№</td>
                <td>Наименование</td>
                <td>Сумма</td>
                <td>Вид работ</td>
                <td>Этап сделки</td>
                <td>Ответственный</td>
                <td>Дата создания</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1232</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>ген. уборка</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-purple" data-pseudo="title" tabindex="0">КП отправлено</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple" class="current">КП отправлено</span>
                                   <span data-value="success" data-class="bgs-green">Сделка</span>
                                   <span data-value="success2" data-class="bgs-blue">Клиент думает</span>
                                   <span data-value="success3" data-class="bgs-orange">Заключение договора</span>
                                   <span data-value="confirmed" data-class="bgs-red">Перезвонить</span>
                                </span>
                             </span>
                </td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>2122</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td><a href="#">послестрой</a></td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-green" data-pseudo="title" tabindex="0">Сделка</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">КП отправлено</span>
                                   <span data-value="success" data-class="bgs-green" class="current">Сделка</span>
                                   <span data-value="success2" data-class="bgs-blue">Клиент думает</span>
                                   <span data-value="success3" data-class="bgs-orange">Заключение договора</span>
                                   <span data-value="confirmed" data-class="bgs-red">Перезвонить</span>
                                </span>
                             </span>
                </td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>2123</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>ген. уборка</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-blue" data-pseudo="title" tabindex="0">Клиент думает</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">КП отправлено</span>
                                   <span data-value="success" data-class="bgs-green">Сделка</span>
                                   <span data-value="success2" data-class="bgs-blue" class="current">Клиент думает</span>
                                   <span data-value="success3" data-class="bgs-orange">Заключение договора</span>
                                   <span data-value="confirmed" data-class="bgs-red">Перезвонить</span>
                                </span>
                             </span>
                </td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>4212</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>ген. уборка</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-orange" data-pseudo="title" tabindex="0">Заключение договора</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">КП отправлено</span>
                                   <span data-value="success" data-class="bgs-green">Сделка</span>
                                   <span data-value="success2" data-class="bgs-blue">Клиент думает</span>
                                   <span data-value="success3" data-class="bgs-orange" class="current">Заключение договора</span>
                                   <span data-value="confirmed" data-class="bgs-red">Перезвонить</span>
                                </span>
                             </span>
                </td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>5121</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>ген. уборка</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-red" data-pseudo="title" tabindex="0">Перезвонить</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">КП отправлено</span>
                                   <span data-value="success" data-class="bgs-green">Сделка</span>
                                   <span data-value="success2" data-class="bgs-blue">Клиент думает</span>
                                   <span data-value="success3" data-class="bgs-orange">Заключение договора</span>
                                   <span data-value="confirmed" data-class="bgs-red" class="current">Перезвонить</span>
                                </span>
                             </span>
                </td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="paginator-wrap">
        <div class="paginator-block fleft">
            <div class="paginator-label">Показано 5 из 1000 заказов</div>
        </div>
    </div>
</div>