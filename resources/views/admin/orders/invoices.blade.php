<div class="dash-content">
    <div class="dash-table" data-item-check="wrap">
        <table>
            <thead>
            <tr>
                <td>№</td>
                <td>Наименование</td>
                <td>Сумма</td>
                <td>Этап счета</td>
                <td>Дата счета</td>
                <td>Ответственный</td>
                <td>Дата создания</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1232</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-purple" data-pseudo="title" tabindex="0">Черновик</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple" class="current">Черновик</span>
                                   <span data-value="success" data-class="bgs-green">Не оплачен</span>
                                   <span data-value="success2" data-class="bgs-blue">Оплачен</span>
                                   <span data-value="success3" data-class="bgs-orange">Олачен частично</span>
                                   <span data-value="confirmed" data-class="bgs-red">Отменен</span>
                                </span>
                             </span>
                </td>
                <td>03.08.2017</td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>2122</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-green" data-pseudo="title" tabindex="0">Не оплачен</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">Черновик</span>
                                   <span data-value="success" data-class="bgs-green" class="current">Не оплачен</span>
                                   <span data-value="success2" data-class="bgs-blue">Оплачен</span>
                                   <span data-value="success3" data-class="bgs-orange">Олачен частично</span>
                                   <span data-value="confirmed" data-class="bgs-red">Отменен</span>
                                </span>
                             </span>
                </td>
                <td>03.08.2017</td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>2123</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-blue" data-pseudo="title" tabindex="0">Оплачен</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">Черновик</span>
                                   <span data-value="success" data-class="bgs-green">Не оплачен</span>
                                   <span data-value="success2" data-class="bgs-blue" class="current">Оплачен</span>
                                   <span data-value="success3" data-class="bgs-orange">Олачен частично</span>
                                   <span data-value="confirmed" data-class="bgs-red">Отменен</span>
                                </span>
                             </span>
                </td>
                <td>03.08.2017</td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>4212</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-orange" data-pseudo="title" tabindex="0">Олачен частично</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">Черновик</span>
                                   <span data-value="success" data-class="bgs-green">Не оплачен</span>
                                   <span data-value="success2" data-class="bgs-blue">Оплачен</span>
                                   <span data-value="success3" data-class="bgs-orange" class="current">Олачен частично</span>
                                   <span data-value="confirmed" data-class="bgs-red">Отменен</span>
                                </span>
                             </span>
                </td>
                <td>03.08.2017</td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            <tr>
                <td>5121</td>
                <td>Уборка квартиры</td>
                <td>1 245</td>
                <td>
                             <span class="pseudo-select" data-pseudo="wrap">
                                <span class="pseudo-select-txt o-btn bgs-red" data-pseudo="title" tabindex="0">Отменен</span>
                                <span class="pseudo-select-drop" data-pseudo="drop">
                                   <span data-value="first" data-class="bgs-purple">Черновик</span>
                                   <span data-value="success" data-class="bgs-green">Не оплачен</span>
                                   <span data-value="success2" data-class="bgs-blue">Оплачен</span>
                                   <span data-value="success3" data-class="bgs-orange">Олачен частично</span>
                                   <span data-value="confirmed" data-class="bgs-red" class="current">Отменен</span>
                                </span>
                             </span>
                </td>
                <td>03.08.2017</td>
                <td>Олег Клочков</td>
                <td>03.08.2017 14:09</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="paginator-wrap">
        <div class="paginator-block fleft">
            <div class="paginator-label">Показано 5 из 1000 заказов</div>
        </div>
    </div>
</div>