<input name="user_ids_v[{{$day_number + 1}}][]" value="{{$user_day->id}}" type="hidden" user_ids_v="{{$user_day->id}}"/>
<div class="kp-personal-person-row">
    <div class="kp-personal-person-col col-check">
                                    <span class="o-input-box-replaced">
                                        <input type="checkbox" name="person_item[]" tabindex="0" value="1">
                                        <span class="o-input-box-replaced-check"></span>
                                    </span>
    </div>
    <div class="kp-personal-person-col col-person">
        <div class="kp-personal-person-profile">
            <div class="op-input type-btns" data-op-input="item">
                <div class="op-input-control-btn" data-op-input="add_person">
                    <span class="icon-input-ctrl-down"></span>
                    <span class="icon-input-ctrl-down-h btn-hover"></span>
                </div>
                <div class="op-input-control-btn" data-op-input="search">
                    <span class="icon-input-ctrl-search"></span>
                    <span class="icon-input-ctrl-search-h btn-hover"></span>
                </div>
                {{--<input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-person-search">--}}
                {{--<input name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="" data-op-input="get" data-user_block="Workers">--}}
                <input user_id="{{$user_day->user->id}}" data-name="user_id_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." value="{{$user_day->user->fio()}}" data-op-input="get" data-user_block="Workers">
            </div>

            {{--<div class="kp-personal-person-subtitle">С872ЩУ764 (POLO)</div>--}}
        </div>
    </div>
    <div class="kp-personal-person-col col-typer">
        <div class="kp-personal-person-typer">
            <div class="person-type-checks-bord">
                <div class="person-type-checks">
                    <div class="person-type-check-item">
                        <label class="person-type-check-btn">
                            <input type="radio" name="fouls_v[{{$length or $day_number + 1}}][1]" value="late">
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-time"></span>
                                        <span class="icon-check-checked ic-time-checked"></span>
                                     </span>
                        </label>
                    </div>
                    <div class="person-type-check-item">
                        <label class="person-type-check-btn">
                            <input type="radio" name="fouls_v[{{$length or $day_number + 1}}][1]" value="nocome">
                                              <span class="person-type-check-btn-label">
                                        <span class="icon-check ic-flash"></span>
                                        <span class="icon-check-checked ic-flash-checked"></span>
                                     </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="kp-personal-person-plusminus">
                <div class="kp-personal-person-plusminus-ins">
                    <div class="kp-personal-person-plusminus-button" onclick="plus_turn_user_row(this, 'driver');">+</div>
                    <div user_ids_v="{{$user_day->id}}" class="kp-personal-person-plusminus-button" onclick="minus_turn_user_row(this, 'driver');">-</div>
                </div>
            </div>
        </div>
    </div>
    <div class="kp-personal-person-col col-pay">
        <div class="op-input" data-op-input="item">
            {{--<input name="amount_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" data-jsmask="num_pay">--}}
            <input name="amount_v[{{$length or $day_number + 1}}][]" type="text" placeholder="..." data-op-input="get" value="{{$user_day->amount ? $user_day->amount : ''}}" data-jsmask="num_pay">
            {{--<input name="amount_v[{{$day_number + 1}}][]" value="{{$user_day->amount ? $user_day->amount : ''}}" type="text" placeholder="Сумма" />--}}
        </div>
    </div>
    <div class="kp-personal-person-col col-rating">
        <div class="kp-personal-checkeds">
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="0" checked>
                <span class="checks-options-btn-label curr-orange">Без оценки</span>
            </label>
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="1">
                <span class="checks-options-btn-label curr-orange">1</span>
            </label>
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="2">
                <span class="checks-options-btn-label curr-orange">2</span>
            </label>
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="3">
                <span class="checks-options-btn-label curr-orange">3</span>
            </label>
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="4">
                <span class="checks-options-btn-label curr-orange">4</span>
            </label>
            <label class="checks-options-btn">
                <input type="radio" name="brigadir_rating[{{$length or $day_number + 1}}][1]" value="5">
                <span class="checks-options-btn-label curr-orange">5</span>
            </label>
        </div>
    </div>
    <div class="kp-personal-person-col col-checkgeneral">
                                <span class="o-input-box-replaced">
                                    <input type="radio" name="main_v[{{$length or $day_number + 1}}][]" tabindex="0" value="1" @if($user_day->main) checked @endif>
                                    <span class="o-input-box-replaced-check"></span>
                                </span>
    </div>
    <div class="kp-personal-person-col col-comment">
        <div class="op-input">
            <textarea name="comment[{{$length or $day_number + 1}}][]" placeholder="..." data-autoheight="true"></textarea>
        </div>
    </div>
</div>