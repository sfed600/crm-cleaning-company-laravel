@if( Route::current()->getName() == "admin.orders.edit" )
    <form id="order-form-main" class="form-horizontal" role="form" action="{{route('admin.orders.update', $order->id)}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="status" value="{{ old('status', @$order->status) }}">
        <input type="hidden" name="number" value="{{ old('number', @$order->number) }}">
        <input type="hidden" name="order_id" value="{{ old('order_id', $order->id) }}">
@elseif( Route::current()->getName() == "admin.orders.create" )
    <form id="order-form-main" class="form-horizontal" role="form" action="{{route('admin.orders.store')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="status" value="first">
        <input type="hidden" name="number" value="{{ old('number', $order_number) }}">
@endif

        <input name="_token" type="hidden" value="{{csrf_token()}}">
        <input type="hidden" name="calculator" value="{{@$order->calculator}}">
        <input type="hidden" name="amount" value="{{@$order->amount}}">
        <input type="hidden" name="contact_user_id" value="{{Auth::user()->id}}">
        {{--<input type="hidden" name="contact_id[]" @if( Route::current()->getName() == "admin.orders.edit" ) value="{{$order->contacts()->first()->id}}" @endif>--}}
        <input type="hidden" name="order_company_id" value="{{@$order->order_company_id}}">
        <input type="hidden" name="name" value="{{ old('name', @$order->name) }}">
        {{--<input type="hidden" name="status" value="{{ old('status', @$order->status) }}">--}}
        {{--<input type="hidden" name="number" value="{{ old('number', @$order->number) }}">--}}

        @if( Route::current()->getName() == "admin.orders.edit" )
            @forelse($order->responsible_users as $key => $user)
                <input type="hidden" name="user_id[]" value="{{$user->id}}" data_user_id="{{$user->id}}">
            @empty
            @endforelse
        @else
            <input type="hidden" name="user_id[]" value="{{Auth::user()->id}}" data_user_id="{{Auth::user()->id}}">
        @endif

        <div class="kp-order-form-wrap">
            @if( Route::current()->getName() == "admin.orders.edit" && !empty($date_periods) )
                @foreach(@$date_periods as $period)
                    <div class="kp-order-form-row clearfix" data-kporder-date="clone">
                        <div class="kp-order-form-date fleft">
                            <div class="kp-order-form-date-item fleft">
                                <div class="kp-order-form-date-label">Дата начала:</div>
                                <div class="kp-order-form-date-input">
                                    <div class="op-input type-icon">
                                        <input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date[]" value="@if( !empty($period->date_start) ) {{$period->date_start}} @else {{Carbon\Carbon::now()->format('d.m.Y')}} @endif" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid">
                                        <div class="op-input-icon-date"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="kp-order-form-date-item fright">
                                <div class="kp-order-form-date-input">
                                    <div class="op-input type-icon">
                                        {{--<input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="time_start[]" value="{{(strlen($period->time_start)==5) ? $period->time_start : '0'.$period->time_start}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">--}}
                                        <input type="text" placeholder="..." name="time_start[]" value="{{(strlen($period->time_start)==5) ? $period->time_start : '0'.$period->time_start}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">
                                        <div class="op-input-icon-time"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kp-order-form-date fleft">
                            <div class="kp-order-form-date-item fleft">
                                <div class="kp-order-form-date-label">Дата окончания:</div>
                                <div class="kp-order-form-date-input">
                                    <div class="op-input type-icon">
                                        {{--<input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date_finish[]" value="{{$period->date_finish}}" data-jsmask="date">--}}
                                        <input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date_finish[]" value="@if( !empty($period->date_finish) ) {{$period->date_finish}} @else {{Carbon\Carbon::now()->format('d.m.Y')}} @endif" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid">
                                        <div class="op-input-icon-date"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="kp-order-form-date-item fright">
                                <div class="kp-order-form-date-input">
                                    <div class="op-input type-icon">
                                        {{--<input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="time_finish[]" value="{{$period->time_finish}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">--}}
                                        <input type="text" placeholder="..." name="time_finish[]" value="{{$period->time_finish}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">
                                        <div class="op-input-icon-time"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="kp-personal-person-plusminus fright">
                            <div class="kp-personal-person-plusminus-ins">
                                <div class="kp-personal-person-plusminus-button" onclick="plus_order_period_row(this);">+</div>
                                <div class="kp-personal-person-plusminus-button" onclick="minus_order_period_row(this);">-</div>
                            </div>
                        </div>

                    </div>
                @endforeach
            @else
                <div class="kp-order-form-row clearfix" data-kporder-date="clone">
                    <div class="kp-order-form-date fleft">
                        <div class="kp-order-form-date-item fleft">
                            <div class="kp-order-form-date-label">Дата начала:</div>
                            <div class="kp-order-form-date-input">
                                <div class="op-input type-icon">
                                    {{--<input type="text" placeholder="..." name="date[]" onkeyup="save_order_periods(this, 'change');" value="{{old('date[]', Carbon\Carbon::now()->format('d.m.Y'))}}" data-jsmask="date">--}}
                                    <input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date[]" value="@if( Route::current()->getName() == "admin.orders.edit") {{date('d.m.Y', strtotime($order->date))}} @else {{old('date[]')}} @endif" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid">
                                    <div class="op-input-icon-date"></div>
                                </div>
                            </div>
                        </div>

                        <div class="kp-order-form-date-item fright">
                            <div class="kp-order-form-date-input">
                                <div class="op-input type-icon">
                                    {{--<input type="text" placeholder="..." name="time_start[]" value="{{old('time_start[]', '09:00')}}" />--}}
                                    <input type="text" placeholder="..." {{--onkeyup="save_order_periods(this, 'change');"--}} name="time_start[]" value="{{old('time_start[]', '09:00')}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">
                                    <div class="op-input-icon-time"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kp-order-form-date fleft">
                        <div class="kp-order-form-date-item fleft">
                            <div class="kp-order-form-date-label">Дата окончания:</div>
                            <div class="kp-order-form-date-input">
                                <div class="op-input type-icon">
                                    {{--{{dump((int)($order->date))}}--}}
                                    {{--<input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date_finish[]" value="{{old('date_finish[]', Carbon\Carbon::now()->format('d.m.Y'))}}" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid">--}}
                                    <input type="text" placeholder="..." onkeyup="save_order_periods(this, 'change');" name="date_finish[]" value="@if( Route::current()->getName() == "admin.orders.edit" && intval($order->date_finish) > 0) {{date('d.m.Y', strtotime($order->date_finish))}} @else {{old('date_finish[]')}} @endif" data-jsmask="date" data-kpcalendar="date" class="js-ajax-valid">
                                    <div class="op-input-icon-date"></div>
                                </div>
                            </div>
                        </div>

                        <div class="kp-order-form-date-item fright">
                            <div class="kp-order-form-date-input">
                                <div class="op-input type-icon">
                                    {{--<input type="text" placeholder="..." name="time_finish[]" value="{{old('time_finish[]', '18:00')}}">--}}
                                    <input type="text" placeholder="..." {{--onkeyup="save_order_periods(this, 'change');"--}} name="time_finish[]" value="{{old('time_finish[]', '18:00')}}" data-jsmask="time" data-kpcalendar="time" class="js-ajax-valid">
                                    <div class="op-input-icon-time"></div>
                                </div>
                            </div>
                        </div>

                        {{--<div class="kp-order-form-date-control">
                            <div class="kp-order-form-date-button" data-kporder-date="add">+</div>
                        </div>--}}
                    </div>

                    <div class="kp-personal-person-plusminus fright">
                        <div class="kp-personal-person-plusminus-ins">
                            <div class="kp-personal-person-plusminus-button" onclick="plus_order_period_row(this);">+</div>
                            <div class="kp-personal-person-plusminus-button" onclick="minus_order_period_row(this);">-</div>
                        </div>
                    </div>

                </div>
            @endif

            <div class="kp-order-form-row clearfix type-address">
                <div class="kp-order-form-labinput">
                    <div class="op-input-label">Адрес уборки:</div>
                    <div class="kp-order-form-labinput-box">
                        <div class="op-input type-btns" data-op-input="item" data-openside>
                            <div class="op-input-control-btn" data-op-input="search">
                                <span class="icon-input-ctrl-search"></span>
                                <span class="icon-input-ctrl-search-h btn-hover"></span>
                            </div>
                            <input id="suggest" type="text" name="address" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" value="{{ old('address', @$order->address) }}">
                            {{--<input type="text" id="suggest" class="input" placeholder="..." name="address" data-openside-map="#side-ya-map" value="{{ old('address', @$order->address) }}">--}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="kp-order-form-row clearfix type-addressdetail">
                <div class="kp-order-form-justify">
                    <div class="kp-order-form-justify-ins">
                        <div class="kp-order-form-justify-item">
                            <div class="op-input-label">Квартира:</div>
                            <div class="op-input">
                                <input type="text" placeholder="..." name="apartment" data-update-ajax value="{{old('apartment', @$order->apartment)}}">
                            </div>
                        </div>
                        <div class="kp-order-form-justify-item">
                            <div class="op-input-label">Подъезд:</div>
                            <div class="op-input">
                                <input type="text" placeholder="..." data-update-ajax name="porch" value="{{old('porch', @$order->porch)}}">
                            </div>
                        </div>
                        <div class="kp-order-form-justify-item">
                            <div class="op-input-label">Этаж:</div>
                            <div class="op-input">
                                <input type="text" placeholder="..." name="floor" data-update-ajax value="{{old('floor', @$order->floor)}}">
                            </div>
                        </div>
                        <div class="kp-order-form-justify-item">
                            <div class="op-input-label">Код домофона:</div>
                            <div class="op-input">
                                <input type="text" placeholder="..." name="domofon" data-update-ajax value="{{old('domofon', @$order->domofon)}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kp-order-form-row clearfix type-note">
                <div class="op-input-label">Примечание:</div>
                <div class="op-input">
                    <textarea placeholder="..." data-autoheight="true" name="note">{{old('note', @$order->note)}}</textarea>
                </div>
            </div>

            <div class="kp-order-boxdata clearfix">
                <div class="kp-order-boxdata-left">
                    <div class="kp-order-boxdata-title">
                        <div class="op-input-label">Введите ФИО контакта</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <div class="op-input-control-btn" data-op-input="search">
                                <span class="icon-input-ctrl-search"></span>
                                <span class="icon-input-ctrl-search-h btn-hover"></span>
                            </div>
                            <div class="op-input-control-btn" data-op-input="add" id="js-kp-order-boxdata-contacts-addnew">
                                <span class="icon-input-ctrl-add"></span>
                                <span class="icon-input-ctrl-add-h btn-hover"></span>
                            </div>
                            <input type="text" placeholder="..." value="" data-op-input="get" id="js-kp-order-boxdata-contacts-search">
                        </div>
                    </div>
                    <script type="text/template" id="js-kp-order-boxdata-contacts-template">
                        <div class="kp-order-box-toggle-fields type-contact type-person toggle-open" data-boxtoggle-fields="wrap" update-ajax-order_contact>
                            <input type="hidden" name="contact_ids[]" data-name="id">

                            <div class="kp-order-box-toggle-fields-txt" data-boxtoggle-fields="txt" div-data-name="name">Контакт</div>

                            <div class="kp-order-box-toggle-drop" data-boxtoggle-fields="drop" data-addfieldbox-tgl-show="wrap" style="display: block;">
                                <div style="top: 13%; right: 15px;" class="op-input-control-btn" data-order_contact_href>
                                    <a href="#">...</a>
                                </div>

                                <div class="kp-order-box-toggle-fields-row type-fio">
                                    <div class="op-input type-btns type-person-fio" data-op-input="item">
                                        {{--<div class="op-input-control-btn" data-op-input="search">
                                            <span class="icon-input-ctrl-search"></span>
                                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                                        </div>--}}
                                        <input type="text" placeholder="..." data-op-input="get" data-name="name" name="contact_names[]" value="">
                                    </div>
                                </div>

                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Рабочий телефон:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" data-name="tel" name="tel[]" data-jsmask="tel">
                                    </div>
                                </div>
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Мобильный:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" data-name="mobile" name="mobile[]" data-jsmask="tel">
                                    </div>
                                </div>
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">E-mail:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" data-name="email" name="email[]" value="">
                                    </div>
                                </div>
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Должность:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" data-name="post" name="post[]" value="">
                                    </div>
                                </div>
                                <div style="top: 90%;" class="op-input-control-btn" data-order-company="remove" {{--order_company_id--}}>
                                    <span class="icon-input-ctrl-clear"></span>
                                    <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                </div>
                            </div>
                        </div>
                    </script>

                    <div id="js-kp-order-boxdata-contacts-insert">
                        @if( isset($order) )
                            @forelse($order->contacts as $key => $item)
                                <div class="kp-order-box-toggle-fields type-contact type-person" data-boxtoggle-fields="wrap" {{--update-ajax-order_contact--}}>
                                    <input type="hidden" name="contact_ids[]" value="{{$item->id}}">

                                    <div class="kp-order-box-toggle-fields-txt" data-boxtoggle-fields="txt" div-data-name="name">{{$item->name}}</div>

                                    <div class="kp-order-box-toggle-drop" data-boxtoggle-fields="drop" data-addfieldbox-tgl-show="wrap" style="display: none;">
                                        <div style="top: 13%; right: 15px;" class="op-input-control-btn" data-contact_href contact_id="{{$item->id}}">
                                            <a href="/admin/order_contacts/{{$item->id}}/edit">...</a>
                                        </div>

                                        <div class="kp-order-box-toggle-fields-row type-fio">
                                            <div class="op-input type-btns type-person-fio" data-op-input="item">
                                                {{--<div class="op-input-control-btn" data-op-input="search">
                                                    <span class="icon-input-ctrl-search"></span>
                                                    <span class="icon-input-ctrl-search-h btn-hover"></span>
                                                </div>--}}
                                                <input type="text" placeholder="..." data-op-input="get" name="contact_names[]" value="{{@$item->name}}">
                                            </div>
                                        </div>

                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Рабочий телефон:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input data-jsmask="tel" type="text" placeholder="..." name="tel[]" value="{{@$item->phones()->where('type', 'work')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Мобильный:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input data-jsmask="tel" type="text" placeholder="..." name="mobile[]" value="{{@$item->phones()->where('type', 'mobile')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">E-mail:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." name="email[]" value="{{@$item->emails()->first()->email}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Должность:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." name="post[]" value="{{@$item->post}}">
                                            </div>
                                        </div>
                                        <div style="top: 90%;" class="op-input-control-btn" {{--data-op-input="clear"--}} data-contact="remove" contact_id="{{$item->id}}">
                                            <span class="icon-input-ctrl-clear"></span>
                                            <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        @else
                            {{--{{dump(\App\Models\OrderContact::find(117)->name)}}--}}
                            {{--если пришли из карточки контакта подставить контакт--}}
                            @php $item = \App\Models\OrderContact::find(app('request')->input('contact_id')); @endphp
                            @if(isset($item))
                                <div class="kp-order-box-toggle-fields type-contact type-person toggle-open" data-boxtoggle-fields="wrap" update-ajax-order_contact>
                                    <input type="hidden" name="contact_ids[]" data-name="id" value="{{$item->id}}">

                                    <div class="kp-order-box-toggle-fields-txt" data-boxtoggle-fields="txt" div-data-name="name">Контакт</div>

                                    <div class="kp-order-box-toggle-drop" data-boxtoggle-fields="drop" data-addfieldbox-tgl-show="wrap" style="display: block;">
                                        <div style="top: 13%; right: 15px;" class="op-input-control-btn" data-contact_href contact_id="{{$item->id}}">
                                            <a href="/admin/order_contacts/{{$item->id}}/edit">...</a>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row type-fio">
                                            <div class="op-input type-btns type-person-fio" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" data-name="name" name="contact_names[]" value="{{@$item->name}}">
                                            </div>
                                        </div>

                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Рабочий телефон:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" data-name="tel" name="tel[]" data-jsmask="tel" value="{{@$item->phones()->where('type', 'work')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Мобильный:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" data-name="mobile" name="mobile[]" data-jsmask="tel" value="{{@$item->phones()->where('type', 'mobile')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">E-mail:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" data-name="email" name="email[]" value="{{@$item->emails()->first()->email}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Должность:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" data-name="post" name="post[]" value="{{@$item->post}}">
                                            </div>
                                        </div>
                                        <div style="top: 90%;" class="op-input-control-btn" {{--data-op-input="clear"--}} data-contact="remove" contact_id="{{$item->id}}">
                                            <span class="icon-input-ctrl-clear"></span>
                                            <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif

                        {{--old contacts session data--}}
                        @if( null !== old('contact_ids'))
                            @foreach( old('contact_ids') as $val )
                                @php
                                    $item = \App\Models\OrderContact::find($val);
                                @endphp
                                <div class="kp-order-box-toggle-fields type-contact type-person" data-boxtoggle-fields="wrap" {{--update-ajax-order_contact--}}>
                                    <input type="hidden" name="contact_ids[]" value="{{$item->id}}">

                                    <div class="kp-order-box-toggle-fields-txt" data-boxtoggle-fields="txt" div-data-name="name">{{$item->name}}</div>

                                    <div class="kp-order-box-toggle-drop" data-boxtoggle-fields="drop" data-addfieldbox-tgl-show="wrap" style="display: none;">
                                        {{--<div style="top: 8%; right: 15px;" class="kp-order-form-person-item-remove" data-contact="remove" contact_id="{{$item->id}}"></div>--}}
                                        <div style="top: 13%; right: 15px;" class="op-input-control-btn" data-contact_href contact_id="{{$item->id}}">
                                            <a href="{{route('order_contacts.edit', $item->id)}}"></a>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row type-fio">
                                            <div class="op-input type-btns type-person-fio" data-op-input="item">
                                                <input type="text" placeholder="..." data-op-input="get" name="contact_names[]" value="{{@$item->name}}">
                                            </div>
                                        </div>

                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Рабочий телефон:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input data-jsmask="tel" type="text" placeholder="..." name="tel[]" value="{{@$item->phones()->where('type', 'work')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Мобильный:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input data-jsmask="tel" type="text" placeholder="..." name="mobile[]" value="{{@$item->phones()->where('type', 'mobile')->first()->phone}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">E-mail:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." name="email[]" value="{{@$item->emails()->first()->email}}">
                                            </div>
                                        </div>
                                        <div class="kp-order-box-toggle-fields-row">
                                            <div class="op-input-label">Должность:</div>
                                            <div class="op-input type-btns" data-op-input="item">
                                                <input type="text" placeholder="..." name="post[]" value="{{@$item->post}}">
                                            </div>
                                        </div>
                                        <div style="top: 90%;" class="op-input-control-btn" {{--data-op-input="clear"--}} data-contact="remove" contact_id="{{$item->id}}">
                                            <span class="icon-input-ctrl-clear"></span>
                                            <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="kp-order-boxdata-right">
                    <div class="kp-order-boxdata-title">
                        <div class="op-input-label">Компания</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <div class="op-input-control-btn" data-op-input="search">
                                <span class="icon-input-ctrl-search"></span>
                                <span class="icon-input-ctrl-search-h btn-hover"></span>
                            </div>
                            <div class="op-input-control-btn" data-op-input="add" id="js-kp-order-boxdata-company-addnew" {{--onclick="create_order_company();"--}}>
                                <span class="icon-input-ctrl-add"></span>
                                <span class="icon-input-ctrl-add-h btn-hover"></span>
                            </div>
                            <input type="text" placeholder="..." data-op-input="get" id="js-kp-order-boxdata-company-search">
                        </div>
                    </div>

                    <script type="text/template" id="js-kp-order-boxdata-company-template">
                        <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>

                            <div class="kp-order-box-toggle-fields-row type-company">
                                <div class="op-input type-btns type-company" data-op-input="item">
                                    <input {{--name="name"--}} type="text" placeholder="..." data-op-input="get" data-name="name" value="">
                                </div>
                            </div>

                            <div style="top: 13%; right: -10px;" class="op-input-control-btn" data-order_company_href>
                                <a href="#">...</a>
                            </div>

                            <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">Сайт:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="site" type="text" placeholder="..." data-op-input="get" data-name="site" value="">
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">Телефон:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="phone" data-jsmask="tel" type="text" placeholder="..." data-name="phone">
                                </div>
                            </div>
                             <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">E-mail:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="">
                                </div>
                            </div>
                            <div style="top: 100%; right: -10px;" class="op-input-control-btn" data-order-company="remove">
                                <span class="icon-input-ctrl-clear"></span>
                                <span class="icon-input-ctrl-clear-h btn-hover"></span>
                            </div>
                        </div>
                    </script>

                    <div id="js-kp-order-boxdata-company-insert">
                        @if(isset($order->orderCompany))
                            <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>

                                <div class="kp-order-box-toggle-fields-row type-company">
                                    <div class="op-input type-btns type-company" data-op-input="item">
                                        <input {{--name="name"--}} type="text" placeholder="..." data-op-input="get" data-name="name" value="{{$order->orderCompany->name}}">
                                    </div>
                                </div>

                                {{--<div style="top: 13%; right: -10px;" class="op-input-control-btn" data-order-company="remove">
                                    <span class="icon-input-ctrl-clear"></span>
                                    <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                </div>--}}
                                <div style="top: 13%; right: -10px;" class="op-input-control-btn" data-order_company_href>
                                    <a href="/admin/order_companies/{{$order->orderCompany->id}}/edit">...</a>
                                </div>

                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Сайт:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input name="site" type="text" placeholder="..." data-op-input="get" data-name="www" value="{{$order->orderCompany->site}}">
                                    </div>
                                </div>
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Телефон:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input name="phone" data-jsmask="tel" type="text" placeholder="..." value="{{$order->orderCompany->phone}}">
                                    </div>
                                </div>
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">E-mail:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="{{$order->orderCompany->email}}">
                                    </div>
                                </div>
                                <div style="top: 100%; right: -10px;" class="op-input-control-btn" data-order-company="remove">
                                    <span class="icon-input-ctrl-clear"></span>
                                    <span class="icon-input-ctrl-clear-h btn-hover"></span>
                                </div>
                            </div>
                        @elseif( null !== old('order_company_id'))   {{--old company session data--}}
                            @php
                                $orderCompany = \App\Models\OrderCompany::find(old('order_company_id'));
                            @endphp
                        <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>

                            <div class="kp-order-box-toggle-fields-row type-company">
                                <div class="op-input type-btns type-company" data-op-input="item">
                                    <input {{--name="name"--}} type="text" placeholder="..." data-op-input="get" data-name="name" value="{{@$orderCompany->name}}">
                                </div>
                            </div>

                            <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">Сайт:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="site" type="text" placeholder="..." data-op-input="get" data-name="www" value="{{@$orderCompany->site}}">
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">Телефон:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="phone" data-jsmask="tel" type="text" placeholder="..." value="{{@$orderCompany->phone}}">
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-row">
                                <div class="op-input-label">E-mail:</div>
                                <div class="op-input type-btns" data-op-input="item">
                                    <input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="{{@$orderCompany->email}}">
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="kp-order-form-row clearfix type-person">
                <div class="kp-order-form-labinput">
                    <div class="op-input-label">Ответственный:</div>
                    <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                        <div class="op-input type-btns" data-op-input="item">
                            <div class="op-input-control-btn" data-op-input="add_person">
                                <span class="icon-input-ctrl-down"></span>
                                <span class="icon-input-ctrl-down-h btn-hover"></span>
                            </div>
                            <div class="op-input-control-btn" data-op-input="search">
                                <span class="icon-input-ctrl-search"></span>
                                <span class="icon-input-ctrl-search-h btn-hover"></span>
                            </div>
                            <input type="text" placeholder="..." value="" data-op-input="get" data-user_block="Management" id="js-kp-order-person-search">
                        </div>

                        @if( Route::current()->getName() == "admin.orders.edit" )
                            @forelse($order->responsible_users as $key => $user)
                                <div class="kp-order-form-person-item" data-persons="item">
                                    <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$user->img}})"></div>
                                    <div class="kp-order-form-person-item-name">{{$user->fio()}}</div>
                                    <div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{$user->id}}"></div>
                                </div>
                            @empty
                            @endforelse
                        @elseif( Route::current()->getName() == "admin.orders.create" )
                            <div class="kp-order-form-person-item" data-persons="item">
                                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.Auth::user()->img}})"></div>
                                <div class="kp-order-form-person-item-name">{{Auth::user()->fio()}}</div>
                                <div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{Auth::user()->id}}"></div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            <script type="text/template" id="kp-order-person-additem">
                <div class="kp-order-form-person-item" data-persons="item">
                    <div class="kp-order-form-person-item-ava" style="background-image:url([avatar])"></div>
                    <div class="kp-order-form-person-item-name">[name]</div>
                    <div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="[user_id]"></div>
                </div>
            </script>

            <div class="page-kp-form-bottom-button type-right">
                <label class="page-kp-form-bottom-check">
                    {{--<span class="txt-label">SMS отправлено клиенту 31.05.2018 в 10:56</span>--}}
                    <span class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></span>
                </label>

                {{--@if( Route::current()->getName() == "admin.orders.create" )--}}
                    <button type="button" class="page-kp-form-button bgs-green">Сохранить</button>
                    {{--<span class="page-kp-form-button bgs-orange" onclick="save_calculator_data('update');">Обновить</span>--}}
                {{--@endif--}}

            </div>
        </div>
    </form>

    <script type='text/javascript'>
        <?php
        $js_array = json_encode($countOrdersOnDate);
        echo "var plannedDateArray = ". $js_array . ";\n";
        ?>
    </script>


