{{--<script src="/assets/admin_new/js/calculator.js?45"></script>--}}
@php
    if( Route::current()->getName() == "admin.orders.create" ) {
        //Route::current()->getName() == "admin.orders.edit"
        $calculator = array();
        parse_str(session()->get('_old_input')["calculator"], $calculator);
    }
@endphp
{{--{{dd($calculator)}}--}}

<form id="calculator-form">
    <input type="hidden" name="order_id" value="{{@$order->id}}">
    <div class="kp-calc-section">
        <div class="kp-calc-block">
            <div class="kp-calc-block-heads clearfix">
                <div class="kp-calc-block-setting-btn">
                    <span class="icon-btn-setting-gear"></span>
                </div>
                <div class="kp-calc-block-title">Базовая информация</div>
            </div>
            <div class="kp-calc-block-content">
                {{--{{dump($calculator['two_sides_metrov'])}}--}}
                <ul class="navlist-options">
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="radio" name="base_info" calculator="base_info"  value="apartment" @if('apartment' == @$calculator['base_info']) checked @endif>
                            <span class="checks-options-btn-label">Квартира</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="radio" name="base_info" calculator="base_info" value="house" @if('house' == @$calculator['base_info']) checked @endif>
                            <span class="checks-options-btn-label">Дом/коттедж</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="radio" name="base_info" calculator="base_info" value="office" @if('office' == @$calculator['base_info']) checked @endif>
                            <span class="checks-options-btn-label">Офис</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="radio" name="base_info" calculator="base_info" value="shop" @if('shop' == @$calculator['base_info']) checked @endif>
                            <span class="checks-options-btn-label">Производственное помещение</span>
                        </label>
                    </li>
                    {{--<li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="base_info2[]" value="dry-cleaning" @if(@in_array('dry-cleaning', $calculator['base_info2'])) checked @endif>
                            <span class="checks-options-btn-label">Химчистка</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="base_info2[]" value="front" @if(@in_array('front', $calculator['base_info2'])) checked @endif>
                            <span class="checks-options-btn-label">Мойка окон, фасада</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="base_info2[]" value="other_work" @if(@in_array('other_work', $calculator['base_info2'])) checked @endif>
                            <span class="checks-options-btn-label">Прочие работы</span>
                        </label>
                    </li>--}}
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="dry_cleaning" calculator="dry_cleaning" {{--value="dry-cleaning"--}} @if( @$calculator['total_cover'] > 0 || @$calculator['total_garniture'] > 0  ) checked @endif>
                            <span class="checks-options-btn-label">Химчистка</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="front" calculator="front" {{--value="front"--}} @if( isset($calculator['front']) || @$calculator['total-windows_and_jaluzi'] > 0 ) checked @endif>
                            <span class="checks-options-btn-label">Мойка окон, фасада</span>
                        </label>
                    </li>
                    <li class="navlist-options-item">
                        <label class="checks-options-btn">
                            <input type="checkbox" name="other_work" calculator="other_work" {{--value="other_work"--}} @if( isset($calculator['other_work'])) checked @endif>
                            <span class="checks-options-btn-label">Прочие работы</span>
                        </label>
                    </li>
                </ul>

                <div class="kp-calc-form-rows">
                    <div class="kp-calc-form-baseinfo-col type-area-room">
                        <div class="navlist-merge type-input">
                            <div class="navlist-merge-label">Площадь помещения:</div>
                            <div class="navlist-merge-item">
                                <div class="op-input">
                                    <input type="text" placeholder="..." name="area" calculator="area" value="{{@$calculator['area']}}">
                                </div>
                            </div>
                            <div class="navlist-merge-infos">м.кв.</div>
                        </div>
                    </div>
                    <div class="kp-calc-form-baseinfo-col type-degree-pollution">
                        <div class="navlist-merge type-btns">
                            <div class="navlist-merge-label">Степень загрязнения:</div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="0" name="degree" calculator="degree" @if( !isset($calculator['degree']) || $calculator['degree'] == 0 )checked @endif>
                                    <span class="checks-options-btn-label curr-orange">1</span>
                                </label>
                            </div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="5" name="degree" calculator="degree" @if( @$calculator['degree'] == 5 )checked @endif>
                                    <span class="checks-options-btn-label curr-orange">2</span>
                                </label>
                            </div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="10" name="degree" calculator="degree" @if( @$calculator['degree'] == 10 )checked @endif>
                                    <span class="checks-options-btn-label curr-orange">3</span>
                                </label>
                            </div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="15" name="degree" calculator="degree" @if( @$calculator['degree'] == 15 )checked @endif>
                                    <span class="checks-options-btn-label curr-orange">4</span>
                                </label>
                            </div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="20" name="degree" calculator="degree" @if( @$calculator['degree'] == 20 )checked @endif>
                                    <span class="checks-options-btn-label curr-orange">5</span>
                                </label>
                            </div>
                            <div class="navlist-merge-infos">
                                <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kp-calc-form-rows">
                    <div class="kp-calc-form-cols type-baseinfo-option">
                        <div class="kp-calc-form-cols-item">
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">Вид уборки:</div>
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" name="typ" calculator="typ" value="{{@$prices[1]}}" @if( !(isset($calculator['typ']) && @$prices[4] == @$calculator['typ']) ) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">ген. уборка</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" name="typ" calculator="typ" value="{{@$prices[4]}}" @if( isset($calculator['typ']) && @$prices[4] == @$calculator['typ'] ) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">послестрой</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                                </div>
                            </div>
                        </div>
                        <div class="kp-calc-form-cols-item">
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" value="0" name="furniture" calculator="furniture" @if( !isset($calculator['furniture']) || $calculator['furniture'] == 0 ) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">без мебели</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" value="10" name="furniture" calculator="furniture" @if( isset($calculator['furniture']) && $calculator['furniture'] > 0 ) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">с мебелью</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                                </div>
                            </div>
                        </div>
                        <div class="kp-calc-form-cols-item">
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">Высота потолков:</div>
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" value="0" name="ceiling_height" calculator="ceiling_height" @if( !isset($calculator['ceiling_height']) || $calculator['ceiling_height'] == 0) checked @endif>
                                        <span class="checks-options-btn-label curr-orange">&lt; 3 м.</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-item">
                                    <label class="checks-options-btn">
                                        <input type="radio" value="5" name="ceiling_height" calculator="ceiling_height" @if(isset($calculator['ceiling_height']) && $calculator['ceiling_height'] > 0)checked @endif>
                                        <span class="checks-options-btn-label curr-orange">&gt; 3 м.</span>
                                    </label>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kp-calc-block-bottom">
                <div class="kp-calc-block-total">
                    <div class="kp-calc-block-total-forcost">
                        Стоимость за 1 м.кв. <span price="{{@$prices[1]}}">{{@$prices[1]}}</span> руб.
                    </div>
                    <div class="kp-calc-block-total-sum">
                        {{--Итого: <span class="txt-value" name="total1">{{$calculator["total1"] or '0'}}</span> руб.--}}
                        Итого: <span class="txt-value" data_name="total1">{{$calculator["total1"] or '0'}}</span> руб.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-calc-section">
        <div class="kp-calc-block">
            <div class="kp-calc-block-content">

                <div class="kp-calc-window-cleaning-wrap clearfix">
                    <div class="kp-calc-window-cleaning-col fleft">
                        <div class="kp-calc-block-title">Мойка окон</div>

                        <div class="kp-calc-form-window-cleaning-table">
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">
                                    <span class="navlist-merge-label-txt">Указать количество створок:</span>
                                    <label class="checks-options-btn">
                                        {{--<input type="checkbox" calculator="windows" name="two_sides_stvorok" @if( !isset($calculator) || isset($calculator['two_sides_stvorok']) )checked @endif>
                                        <span class="checks-options-btn-label curr-blue">С двух сторон</span>--}}
                                        @if( isset($calculator['two_sides_stvorok']) )
                                            <input type="checkbox" calculator="windows" name="two_sides_stvorok" checked onclick="if( $(this).prop('checked') ) $(this).next().text('С 2-х сторон'); else $(this).next().text('С 1-ой стороны');">
                                            <span class="checks-options-btn-label curr-blue">С 2-х сторон</span>
                                        @else
                                            <input type="checkbox" calculator="windows" name="two_sides_stvorok" onclick="if( $(this).prop('checked') ) $(this).next().text('С 2-х сторон'); else $(this).next().text('С 1-ой стороны');">
                                            <span class="checks-options-btn-label curr-blue">С 1-ой стороны</span>
                                        @endif
                                    </label>
                                </div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="windows" price="{{@$prices[5]}}" name="windows-stvorok" value="{{$calculator['windows-stvorok'] or ''}}">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <div class="navlist-merge-sufix">шт.</div>
                                    <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                                </div>
                            </div>
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">
                                    <span class="navlist-merge-label-txt">Указать количество м.кв.:</span>
                                    <label class="checks-options-btn">
                                        {{--<input type="checkbox" calculator="windows" name="two_sides_metrov" @if( !isset($calculator) || isset($calculator['two_sides_metrov']) )checked @endif>
                                        <span class="checks-options-btn-label curr-blue">С двух сторон</span>--}}
                                        @if( isset($calculator['two_sides_metrov']) )
                                            <input type="checkbox" calculator="windows" name="two_sides_metrov" checked onclick="if( $(this).prop('checked') ) $(this).next().text('С 2-х сторон'); else $(this).next().text('С 1-ой стороны');">
                                            <span class="checks-options-btn-label curr-blue">С 2-x сторон</span>
                                        @else
                                            <input type="checkbox" calculator="windows" name="two_sides_metrov" onclick="if( $(this).prop('checked') ) $(this).next().text('С 2-х сторон'); else $(this).next().text('С 1-ой стороны');">
                                            <span class="checks-options-btn-label curr-blue">С 1-ой стороны</span>
                                        @endif
                                    </label>
                                </div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="windows" price="{{@$prices[7]}}" name="windows-metrov" value="{{$calculator['windows-metrov'] or ''}}">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <div class="navlist-merge-sufix">м.кв.</div>
                                </div>
                            </div>
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">Панорамное остелкние (моем только внутри):</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="windows" price="{{@$prices[6]}}" name="windows-panaram" value="{{$calculator['windows-panaram'] or ''}}">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <div class="navlist-merge-sufix">м.кв.</div>
                                    <span class="form-infos-btn" title="Подсказка при наведении!"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{--<script> windows_total(); </script>--}}

                    <div class="kp-calc-window-cleaning-col fright">
                        <div class="kp-calc-block-title">Жалюзи</div>

                        <div class="kp-calc-form-window-cleaning-table">
                            <div class="navlist-merge type-fuses">
                                <div class="navlist-merge-label">Указать кол-во створок окна<br>которые закрывают жалюзи:</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input type-postop">
                                        <input type="text" placeholder="..." name="jaluzi_shtuk" calculator="jaluzi" price="{{@$prices[8]}}" value="{{$calculator['jaluzi_shtuk'] or ''}}">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <div class="navlist-merge-sufix">шт.</div>
                                </div>
                            </div>
                            <div class="navlist-merge type-fuses type-minuspadd">
                                <div class="navlist-merge-label">Указать количество м.кв.:</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." name="jaluzi_metrov" calculator="jaluzi" price="{{@$prices[9]}}" value="{{$calculator['jaluzi_metrov'] or ''}}">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <div class="navlist-merge-sufix">м.кв.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kp-calc-block-bottom">
                <div class="kp-calc-block-total">
                    <div class="kp-calc-block-total-sum">
                        {{--Итого: <span class="txt-value" name="total-windows_and_jaluzi">{{$calculator["total-windows_and_jaluzi"] or '0'}}</span>--}}
                        Итого: <span class="txt-value" data_name="total_windows_and_jaluzi">{{$calculator["total-windows_and_jaluzi"] or '0'}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-calc-section type-drycleaning">
        <div class="kp-calc-drycleaning-wrap">
            <div class="kp-calc-drycleaning">
                <div class="kp-calc-drycleaning-col col-left">
                    <div class="kp-calc-block" data-calc-cleaning="wrap">
                        <div class="kp-calc-block-title">Химчистка мебели</div>
                        <div class="kp-calc-block-content">

                            <div class="kp-calc-table-cleaning">
                                <table>
                                    <tbody>
                                    <tr>
                                        <th class="col-num">#</th>
                                        <th class="col-info"></th>
                                        <th class="col-cost">Стоимость <br>за единицу</th>
                                        <th class="col-pcs">шт.</th>
                                        <th class="col-total">Сумма</th>
                                    </tr>
                                    <tr data-calc-cleaning="item">
                                        <td class="col-num">Диван 2 места</td>
                                        <td class="col-info"><span class="form-infos-btn" title="Подсказка при наведении!"></span></td>
                                        <td class="col-cost"><span data-calc-cleaning="cost" price="{{@$prices[10]}}">{{@$prices[10]}}</span></td>
                                        <td class="col-pcs">
                                            <div class="op-input">
                                                <input type="text" placeholder="..." data-calc-cleaning="input" calculator="garniture" name="garniture1" value="{{$calculator['garniture1'] or ''}}">
                                            </div>
                                        </td>
                                        <td class="col-total"><span data-calc-cleaning="total">0</span> руб.</td>
                                    </tr>
                                    <tr data-calc-cleaning="item">
                                        <td class="col-num">Диван 3 места</td>
                                        <td class="col-info"><span class="form-infos-btn" title="Подсказка при наведении!"></span></td>
                                        <td class="col-cost"><span data-calc-cleaning="cost" price="{{@$prices[11]}}">{{@$prices[11]}}</span></td>
                                        <td class="col-pcs">
                                            <div class="op-input">
                                                <input type="text" placeholder="..." data-calc-cleaning="input" calculator="garniture" name="garniture2" value="{{$calculator['garniture2'] or ''}}">
                                            </div>
                                        </td>
                                        <td class="col-total"><span data-calc-cleaning="total">0</span> руб.</td>
                                    </tr>
                                    <tr data-calc-cleaning="item">
                                        <td class="col-num">Диван угловой</td>
                                        <td class="col-info"><span class="form-infos-btn" title="Подсказка при наведении!"></span></td>
                                        <td class="col-cost"><span data-calc-cleaning="cost" price="{{@$prices[12]}}">{{@$prices[12]}}</span></td>
                                        <td class="col-pcs">
                                            <div class="op-input">
                                                <input type="text" placeholder="..." data-calc-cleaning="input" calculator="garniture" name="garniture3" value="{{$calculator['garniture3'] or ''}}">
                                            </div>
                                        </td>
                                        <td class="col-total"><span data-calc-cleaning="total">0</span> руб.</td>
                                    </tr>
                                    <tr data-calc-cleaning="item">
                                        <td class="col-num">Диван со спальным матрасом</td>
                                        <td class="col-info"><span class="form-infos-btn" title="Подсказка при наведении!"></span></td>
                                        <td class="col-cost"><span data-calc-cleaning="cost" price="{{@$prices[13]}}">{{@$prices[13]}}</span></td>
                                        <td class="col-pcs">
                                            <div class="op-input">
                                                <input type="text" placeholder="..." data-calc-cleaning="input" calculator="garniture" name="garniture4" value="{{$calculator['garniture4'] or ''}}">
                                            </div>
                                        </td>
                                        <td class="col-total"><span data-calc-cleaning="total">0</span> руб.</td>
                                    </tr>
                                    <tr data-calc-cleaning="item">
                                        <td class="col-num">Диван угловой со спальным матрасом</td>
                                        <td class="col-info"><span class="form-infos-btn" title="Подсказка при наведении!"></span></td>
                                        <td class="col-cost"><span data-calc-cleaning="cost" price="{{@$prices[14]}}">{{@$prices[14]}}</span></td>
                                        <td class="col-pcs">
                                            <div class="op-input">
                                                <input type="text" placeholder="..." data-calc-cleaning="input" calculator="garniture" name="garniture5" value="{{$calculator['garniture5'] or ''}}">
                                            </div>
                                        </td>
                                        <td class="col-total"><span data-calc-cleaning="total">0</span> руб.</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="kp-calc-block-bottom">
                            <div class="kp-calc-block-total">
                                <div class="kp-calc-block-total-sum">
                                    {{--Итого: <span class="txt-value" data-calc-cleaning="alltotal" name="total_garniture">{{$calculator["total_garniture"] or '0'}}</span> руб.--}}
                                    Итого: <span class="txt-value" data-calc-cleaning="alltotal" data_name="total_garniture">{{$calculator["total_garniture"] or '0'}}</span> руб.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kp-calc-drycleaning-col col-right">
                    <div class="kp-calc-block" data-calc-clean-carpets="wrap">
                        <div class="kp-calc-block-title">Химчистка ковров</div>
                        <div class="kp-calc-block-content">

                            <div class="kp-calc-drycleaning-rows type-mar">
                                <div class="kp-calc-drycleaning-title">Химчистка ковров:</div>
                                <div class="navlist-merge type-input">
                                    <div class="navlist-merge-item">
                                        <div class="op-input">
                                            <input type="hidden" price="{{@$prices[17]}}" name="price_kover" disabled>
                                            <input type="text" placeholder="..." calculator="cover" name="kover_shtuk" value="{{$calculator['kover_shtuk'] or ''}}" {{--data-calc-clean-carpets="500"--}}>
                                        </div>
                                    </div>
                                    <div class="navlist-merge-infos">шт.</div>
                                </div>
                            </div>
                            <div class="kp-calc-drycleaning-rows type-mar">
                                <div class="kp-calc-drycleaning-title">Химчистка коврового покрытия:</div>
                                <div class="navlist-merge type-input">
                                    <div class="navlist-merge-item">
                                        <div class="op-input">
                                            <input type="hidden" name="price_cover" price50="{{@$prices[19]}}" price75="{{@$prices[20]}}" price90="{{@$prices[21]}}" price120="{{@$prices[22]}}" price200="{{@$prices[23]}}" price300="{{@$prices[24]}}" price400="{{@$prices[18]}}" disabled>
                                            <input type="text" placeholder="..." calculator="cover" name="cover_metrov" value="{{$calculator['cover_metrov'] or ''}}" {{--data-calc-clean-carpets="600"--}}>
                                        </div>
                                    </div>
                                    <div class="navlist-merge-infos">м.кв.</div>
                                </div>
                            </div>
                            <div class="kp-calc-drycleaning-rows">
                                <div class="navlist-merge type-fuses">
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="himchistka_kovra" value="0" @if( !(isset($calculator['himchistka_kovra']) && $calculator['himchistka_kovra'] > 0) ) checked @endif {{--data-calc-clean-carpets="1500"--}}>
                                            <span class="checks-options-btn-label curr-orange">короткий ворс</span>
                                        </label>
                                    </div>
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="himchistka_kovra" value="30" {{--data-calc-clean-carpets="2500"--}} @if( isset($calculator['himchistka_kovra']) && $calculator['himchistka_kovra'] > 0 ) checked @endif>
                                            <span class="checks-options-btn-label curr-orange">длинный ворс</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-calc-drycleaning-rows">
                                <div class="kp-calc-drycleaning-title">Степень загрязнения:</div>
                                <div class="navlist-merge type-btns-inline">
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="degree_kover" calculator="cover" value="0" {{--data-calc-clean-carpets="100"--}} @if( !isset($calculator['degree_kover']) || $calculator['degree_kover'] == 0 ) checked @endif>
                                            <span class="checks-options-btn-label curr-orange">1</span>
                                        </label>
                                    </div>
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="degree_kover" calculator="cover" value="5" {{--data-calc-clean-carpets="200"--}} @if( isset($calculator['degree_kover']) && $calculator['degree_kover'] == 5 ) checked @endif>
                                            <span class="checks-options-btn-label curr-orange">2</span>
                                        </label>
                                    </div>
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="degree_kover" calculator="cover" value="10" {{--data-calc-clean-carpets="300"--}} @if( isset($calculator['degree_kover']) && $calculator['degree_kover'] == 10 ) checked @endif >
                                            <span class="checks-options-btn-label curr-orange">3</span>
                                        </label>
                                    </div>
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="degree_kover" calculator="cover" value="15" {{--data-calc-clean-carpets="400"--}} @if( isset($calculator['degree_kover']) && $calculator['degree_kover'] == 15 ) checked @endif>
                                            <span class="checks-options-btn-label curr-orange">4</span>
                                        </label>
                                    </div>
                                    <div class="navlist-merge-item">
                                        <label class="checks-options-btn">
                                            <input type="radio" name="degree_kover" calculator="cover" value="20" {{--data-calc-clean-carpets="500"--}} @if( isset($calculator['degree_kover']) && $calculator['degree_kover'] == 20 ) checked @endif>
                                            <span class="checks-options-btn-label curr-orange">5</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="kp-calc-block-bottom">
                            <div class="kp-calc-block-total">
                                <div class="kp-calc-block-total-sum">
                                    {{--Итого: <span class="txt-value" data-calc-clean-carpets="alltotal" name="total_cover">{{$calculator['total_cover'] or '0'}}</span> руб.--}}
                                    Итого: <span class="txt-value" data-calc-clean-carpets="alltotal" data_name="total_cover">{{$calculator['total_cover'] or '0'}}</span> руб.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-calc-section">
        <div class="kp-calc-block" data-calc-clean-other="wrap">
            <div class="kp-calc-block-title">Другие услуги</div>
            <div class="kp-calc-block-content">

                <div class="kp-calc-other-services clearfix">
                    <div class="kp-calc-other-services fleft">
                        <div class="navlist-merge-table">
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Мойка холодильникаизнутри</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." price="{{@$prices[30]}}" calculator="within" name="fridge-quantity" value="{{$calculator['fridge-quantity'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span data-calc-clean-other="cost" total="within" name="fridge-sum">{{@$prices[30]}}</span> руб.
                                </div>
                            </div>
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Мойка микроволновой печи изнутри</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." price="{{@$prices[31]}}" calculator="within" name="microwave-quantity" value="{{$calculator['microwave-quantity'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span data-calc-clean-other="cost" total="within" name="microwave-sum">{{@$prices[31]}}</span> руб.
                                </div>
                            </div>
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Мойка духовки изнутри</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." price="{{@$prices[32]}}" calculator="within" name="oven-quantity" value="{{$calculator['oven-quantity'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span data-calc-clean-other="cost" total="within" name="oven-sum">{{@$prices[32]}}</span> руб.
                                </div>
                            </div>
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Дополнительный человек</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input"><input type="text" placeholder="..." price="{{@$prices[27]}}" calculator="within" name="man-quantity" value="{{$calculator['man-quantity'] or ''}}" data-calc-clean-other="input"></div>
                                </div>
                                <div class="navlist-merge-infos">
                                    <span data-calc-clean-other="cost" total="within" name="man-sum">{{@$prices[27]}}</span> руб.
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="kp-calc-other-services fright">
                        <div class="navlist-merge-table">
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Доставка тары <span class="txt-sufix">Малой</span></div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="tura" name="tura_small_quantity" value="{{@$calculator['tura_small_quantity'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos"><span data-calc-clean-other="cost" calculator="tura">{{@$prices[28]}}</span> руб.</div>
                            </div>
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Доставка тары <span class="txt-sufix">Большой</span></div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="tura" name="tura_big_quantity" value="{{@$calculator['tura_big_quantity'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos"><span data-calc-clean-other="cost" calculator="tura">{{@$prices[29]}}</span> руб.</div>
                            </div>
                            <div class="navlist-merge type-input" data-calc-clean-other="item">
                                <div class="navlist-merge-label">Стоянка</div>
                                <div class="navlist-merge-item">
                                    <div class="op-input">
                                        {{--<input type="text" placeholder="..." value="" data-calc-clean-other="input">--}}
                                        <input type="text" placeholder="..." price="{{@$prices[25]}}" calculator="parking" name="parking" value="{{$calculator['parking'] or ''}}" data-calc-clean-other="input">
                                    </div>
                                </div>
                                <div class="navlist-merge-infos"><span data-calc-clean-other="cost">{{@$prices[25]}}</span> руб.</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="kp-calc-block-bottom">
                <div class="kp-calc-block-total">
                    <div class="kp-calc-block-total-sum">
                        {{--Итого: <span class="txt-value" data-calc-clean-other="alltotal" name="total_other_services">{{$calculator['total_other_services'] or '0'}}</span> руб.--}}
                        Итого: <span class="txt-value" data-calc-clean-other="alltotal" data_name="total_other_services">{{$calculator['total_other_services'] or '0'}}</span> руб.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-calc-section">
        <div class="kp-calc-block">
            <div class="kp-calc-block-content">

                <div class="kp-calc-addworks-table">
                    <table>
                        <colgroup>
                            <col class="col-addworks-title">
                            <col class="col-addworks-cost">
                            <col class="col-addworks-pcs">
                            <col class="col-addworks-total">
                            <col class="col-addworks-btn">
                        </colgroup>
                        <tbody id="kp-calc-addworks-table-setitem">
                        <tr>
                            <th class="col-addworks-title">Дополнительные работы</th>
                            <th class="col-addworks-pcs">Кол-во</th>
                            <th class="col-addworks-cost">Цена</th>
                            <th class="col-addworks-total"></th>
                            <th class="col-addworks-btn"></th>
                        </tr>
                        {{--@if(isset($calculator))--}}
                        @if(isset($calculator['additionally_price']))
                            @foreach($calculator['additionally_price'] as $key => $additionally_price )
                                <tr data-addworks-item>
                                    <td class="col-addworks-title">
                                        <div class="op-input type-btns" data-op-input="item">
                                            <div class="op-input-control-btn" data-op-input="search">
                                                <span class="icon-input-ctrl-search"></span>
                                                <span class="icon-input-ctrl-search-h btn-hover"></span>
                                            </div>
                                            <input type="text" placeholder="..." data-op-input="get" name="additionally_name[]" value="{{$calculator['additionally_name'][$key]}}">
                                        </div>
                                    </td>
                                    <td class="col-addworks-cost">
                                        <div class="op-input">
                                            <input type="text" placeholder="..." calculator="additionally" name="additionally_quantity[]" value="{{$calculator['additionally_quantity'][$key]}}">
                                        </div>
                                    </td>
                                    <td class="col-addworks-pcs">
                                        <div class="op-input">
                                            <input type="text" placeholder="..." calculator="additionally" name="additionally_price[]" value="{{$calculator['additionally_price'][$key]}}">
                                        </div>
                                    </td>
                                    <td class="col-addworks-total" data-name="additionally_sum_row">{{$calculator['additionally_quantity'][$key]*$calculator['additionally_price'][$key]}} руб.</td>
                                    <td class="col-addworks-btn">
                                        <div class="kp-calc-addworks-button" data-btn-addworks="remove_item" {{--onclick="total(); save_calculator_data(null, 'ajax');"--}}></div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr data-addworks-item>
                                <td class="col-addworks-title">
                                    <div class="op-input type-btns" data-op-input="item">
                                        <div class="op-input-control-btn" data-op-input="search">
                                            <span class="icon-input-ctrl-search"></span>
                                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                                        </div>
                                        <input type="text" placeholder="..." data-op-input="get" name="additionally_name[]">
                                    </div>
                                </td>
                                <td class="col-addworks-cost">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="additionally" name="additionally_quantity[]" {{--onchange="additionally_works(this);"--}}>
                                    </div>
                                </td>
                                <td class="col-addworks-pcs">
                                    <div class="op-input">
                                        <input type="text" placeholder="..." calculator="additionally" name="additionally_price[]" {{--onchange="additionally_works(this);"--}}>
                                    </div>
                                </td>
                                <td class="col-addworks-total" data-name="additionally_sum_row">0 руб.</td>
                                <td class="col-addworks-btn">
                                    <div class="kp-calc-addworks-button" data-btn-addworks="remove_item" {{--onclick="total(); save_calculator_data(null, 'ajax');"--}}></div>
                                </td>
                            </tr>
                        @endif

                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td colspan="4"><span class="kp-calc-addworks-table-addbutton" data-addtemplate="#kp-calc-addworks-table-additem" data-addtemplate-append="#kp-calc-addworks-table-setitem">Добавить доп. работу</span></td>
                        </tr>
                        </tfoot>
                    </table>
                    <script type="text/template" id="kp-calc-addworks-table-additem">
                        <tr data-addworks-item>
                            <td class="col-addworks-title">
                                <div class="op-input type-btns" data-op-input="item">
                                    <div class="op-input-control-btn" data-op-input="search">
                                        <span class="icon-input-ctrl-search"></span>
                                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                                    </div>
                                    <input type="text" placeholder="..." data-op-input="get" value="" name="additionally_name[]">
                                </div>
                            </td>
                            <td class="col-addworks-cost">
                                <div class="op-input">
                                    <input calculator="additionally" type="text" placeholder="..." value="" name="additionally_quantity[]" onchange="additionally_works(this);">
                                </div>
                            </td>
                            <td class="col-addworks-pcs">
                                <div class="op-input">
                                    <input calculator="additionally" type="text" placeholder="..." value="" name="additionally_price[]" onchange="additionally_works(this);">
                                </div>
                            </td>
                            <td class="col-addworks-total" data-name="additionally_sum_row">0 руб.</td>
                            <td class="col-addworks-btn">
                                <div class="kp-calc-addworks-button" data-btn-addworks="remove_item" {{--onclick="total(); save_calculator_data(null, 'ajax');"--}}></div>
                            </td>
                        </tr>
                    </script>
                </div>

            </div>
            <div class="kp-calc-block-bottom">
                <div class="kp-calc-block-total">
                    <div class="kp-calc-block-total-sum">
                        Итого: <span class="txt-value" data_name="total_additionally_works">{{@$calculator['total_additionally_works']}}</span> руб.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-calc-section">
        <div class="kp-calc-block">
            <div class="kp-calc-total">

                <div class="kp-calc-block-bottom-total-item clearfix">
                    <div class="kp-calc-block-bottom-total-item-left type-coefficient clearfix">
                        <div class="navlist-merge type-input fright">
                            <div class="navlist-merge-label">Надбавочный коэффициент % <span class="form-infos-btn" title="Подсказка при наведении!"></span></div>
                            <div class="navlist-merge-item">
                                <div class="op-input">
                                    <input type="text" placeholder="..." value="{{$calculator['surcharge'] or ''}}" name="surcharge" calculator>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kp-calc-block-bottom-total-sum">
                        Итого: <span class="txt-value" data_name="total">{{$calculator['total'] or ''}}</span> руб.
                    </div>
                </div>
                <div class="kp-calc-block-bottom-total-item clearfix">
                    <div class="kp-calc-block-bottom-total-item-left type-discount clearfix">
                        <div class="navlist-merge type-input fright">
                            <div class="navlist-merge-label">Скидка%</div>
                            <div class="navlist-merge-item">
                                <div class="op-input">
                                    <input type="text" placeholder="..." calculator name="discount" value="{{$calculator['discount'] or ''}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kp-calc-block-bottom-fulltotal-sum">
                        Итого со скидкой: <span class="txt-value" data_name="total-discount">{{$calculator['total-discount'] or ''}}</span> руб.
                    </div>
                </div>

                <div class="page-kp-form-bottom-button type-right">
                    {{--<button type="button" class="page-kp-form-button bgs-green" onclick="save_calculator_data('save');">Сохранить</button>
                    <span class="page-kp-form-button bgs-orange" onclick="save_calculator_data('update');">Обновить</span>--}}
                    <div class="page-kp-form-bottom-button-left">
                        <div class="navlist-merge type-fuses">
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="0" name="payment" calculator="payment" @if(!isset($calculator['payment']) || $calculator['payment'] == 0) checked @endif>
                                    <span class="checks-options-btn-label curr-orange">Наличный</span>
                                </label>
                            </div>
                            <div class="navlist-merge-item">
                                <label class="checks-options-btn">
                                    <input type="radio" value="10" name="payment" calculator="payment" @if( isset($calculator['payment']) && intval($calculator['payment']) > 0) checked @endif>
                                    <span class="checks-options-btn-label curr-orange">Безналичный</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    {{--{{dump($calculator)}}--}}
                    <div class="page-kp-form-bottom-button-right">
                        {{--@if( Route::current()->getName() == "admin.orders.create" )--}}
                            <button type="button" class="page-kp-form-button bgs-green" data-action="save_calculator_data" data-action_mode="ajax">Сохранить</button>
                            {{--<span class="page-kp-form-button bgs-orange" onclick="save_calculator_data('update');">Обновить</span>--}}
                        {{--@endif--}}
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>