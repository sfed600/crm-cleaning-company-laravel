@extends('admin.layout')
@section('main')
    {{--for ymap--}}
    <script src="/assets/admin_new/js/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=onLoad"></script>
    <script src="/assets/admin_new/js/ymaps.js?v=48"></script>
    <script>
        function onLoad (ymaps) {
            var suggestView = new ymaps.SuggestView('suggest');
        }
    </script>

    <script src="/assets/admin_new/js/calculator.js?44"></script>

    {{--@php
        $calculator = array();
        parse_str(session()->get('_old_input')["calculator"], $calculator);
        dd($calculator);
    @endphp--}}
    {{--{{dump(old('status'))}}--}}
    <div class="dash-container">
        <div class="dash-kp-line">
            <ul class="dash-kp-line-list" data-kp-step="wrap">
                <li><span class="@if(old('status') == 'first') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-orangelight"></span><span class="btn-bgs bgs-hover bgs-orangelight"></span><span class="txt-label">Первичный контакт</span></span></li>
                <li><span class="@if(old('status') == 'agree') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-orange"></span><span class="btn-bgs bgs-hover bgs-orange"></span><span class="txt-label">Согласовать</span></span></li>
                <li><span class="@if(old('status') == 'agreed') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-greenlight"></span><span class="btn-bgs bgs-hover bgs-greenlight"></span><span class="txt-label">Согласовано</span></span></li>
                <li><span class="@if(old('status') == 'confirmed') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-blue"></span><span class="btn-bgs bgs-hover bgs-blue"></span><span class="txt-label">Подтвержено</span></span></li>
                <li><span class="@if(old('status') == 'work') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-blue"></span><span class="btn-bgs bgs-hover bgs-blue"></span><span class="txt-label">В работе</span></span></li>
                <li><span class="@if(old('status') == 'performed') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-purple"></span><span class="btn-bgs bgs-hover bgs-purple"></span><span class="txt-label">Работы выполнены</span></span></li>
                <li><span class="@if(old('status') == 'final_positive') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-greendark"></span><span class="btn-bgs bgs-hover bgs-greendark"></span><span class="txt-label">Успешно реализовано</span></span></li>
                <li><span class="@if(old('status') == 'final_negative') current @endif dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-red"></span><span class="btn-bgs bgs-hover bgs-red"></span><span class="txt-label">Не реализовано</span></span></li>
            </ul>
        </div>

        @include('admin.message')

        <div class="dash-content">
            <div id="dash-kp">
                <ul class="dash-kp-nav">
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-main" @if(Request::get('tab') != 'calculator') class="current" @endif>Основное</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-calc" @if(Request::get('tab') == 'calculator') class="current" @endif>Калькулятор</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-offers">Предложения</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-invoices">Счета</a></li>
                    <li><a href="#">Договора</a></li>
                    <li><a href="#">Письма</a></li>
                    {{--<li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-staff">Персонал</a></li>--}}
                    <li><a href="#">Статистика</a></li>
                </ul>

                <div class="dash-kp-content">
                    <div class="dash-kp-col col-left">

                        <div class="o-tabs-box kp-tabbox-main @if(Request::get('tab') != 'calculator') current @endif" data-tab-box>
                            @include('admin.orders.main')
                        </div>

                        <div class="o-tabs-box kp-tabbox-calc @if(Request::get('tab') == 'calculator') current @endif" data-tab-box>
                            {{--@include('admin.orders.calculator_create')--}}
                            @include('admin.orders.calculator')
                        </div>

                        <div class="o-tabs-box kp-tabbox-offers" data-tab-box>
                            @include('admin.orders.offers')
                        </div>

                        <div class="o-tabs-box kp-tabbox-invoices" data-tab-box>
                            @include('admin.orders.invoices')
                        </div>

                        <div class="o-tabs-box kp-tabbox-staff" data-tab-box>
                            @include('admin.orders.staff_left')
                        </div>
                    </div>
                    <div class="dash-kp-col col-right">
                        <div class="o-tabs-box kp-tabbox-main kp-tabbox-calc current" data-tab-box>
                            <div class="kp-system-messages-control">
                                <label class="kp-system-messages-control-item">
                                    <span class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></span>
                                    <span class="txt-label">Скрыть системные сообщения</span>
                                </label>
                            </div>

                            {{--<div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа</div>
                            </div>

                            <div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">
                                    03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа
                                    <div class="kp-history-item-box">
                                        <div class="kp-history-item-box-heads">03.08.2017 15:53 Олег Клочков</div>
                                        СДЛорвдмалвадомивжлмовждмдвлаьмидлвьидлваиавдивэваиваливдлаи
                                        ваиваиваиваиваиваиваиваиваивчыиачвичвичвичв
                                        чвамивчаичваичвичвичвяивчиаяваияви
                                    </div>
                                </div>
                            </div>--}}

                            {{--<div class="kp-note-form">
                                <div class="kp-note-form-infos">
                                    <div class="kp-note-form-infos-text">Нет запланированных встреч, рекомендуем добавить</div>
                                    <div class="kp-note-form-infos-angle"></div>
                                </div>
                                <label class="kp-note-form-text">
                                    <textarea required></textarea>
                                    <span class="kp-note-form-text-notes"><a href="#">Примечание:</a> Введите текст</span>
                                </label>
                            </div>
                            <div class="kp-note-form-empty"></div>--}}
                        </div>

                        <div class="o-tabs-box kp-tabbox-offers" data-tab-box>
                            <div class="dash-kp-blank idesc">
                                <h2>КОММЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ<br>
                                    № 012018 / 2 от 19.01.2018</h2>

                                <p>Срок действия 10.05.2018</p>
                                <h2>Перечень товаров/услуг:</h2>
                                <table>
                                    <tbody>
                                    <tr>
                                        <th scope="col">№</th>
                                        <th scope="col">Наименование товара</th>
                                        <th scope="col">Кол-во</th>
                                        <th scope="col">ЕД.</th>
                                        <th scope="col">Цена, руб.</th>
                                        <th scope="col">Ставка НДС</th>
                                        <th scope="col">Сумма, руб.</th>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Химчистка ковролина</td>
                                        <td>1</td>
                                        <td>Услуга</td>
                                        <td>8 200.00</td>
                                        <td>18%</td>
                                        <td>8 200.00</td>
                                    </tr>
                                    <tr class="tr-total">
                                        <td colspan="6" align="right" class="td-total">В том числе НДС:</td>
                                        <td>1 250.85</td>
                                    </tr>
                                    <tr class="tr-total">
                                        <td colspan="6" align="right" class="td-total">Итого:</td>
                                        <td>8 200.00</td>
                                    </tr>
                                    </tbody>
                                </table>

                                <br>
                                <br>
                                <br>

                                <h2>Условия и комментарии:</h2>
                                <ol>
                                    <li>Химчистка с помощью роторной машины</li>
                                    <li>Промывка экстракторной машиной</li>
                                    <li>Сушка</li>
                                </ol>
                                <p>Предоплата 100%</p>

                                <h2>Контакты и реквизиты:</h2>
                                <div class="block-requisites">
                                    <h2>ООО &ldquo;Брайт БОКС&rdquo;</h2>
                                    <p>
                                        Адрес: ул. Угрешская, дом 2, строение 53,<br>
                                        помещение 403, Москва, Россия, 115088<br>
                                        Телефон: 7348358479<br>
                                        E-mail: jdfhvbjdh<br>
                                        ИНН: 32423525<br>
                                    </p>
                                </div>

                                <p><a class="btn-download-kp o-hvr" href="file.txt" download="Коммерческое предложение">Скачать коммерческое предложение</a></p>
                            </div>
                        </div>

                        <div class="o-tabs-box kp-tabbox-invoices" data-tab-box>
                            <img src="/assets/admin_new/img/blank/blank-account.jpg" alt="">
                        </div>

                        <div class="o-tabs-box kp-tabbox-staff" data-tab-box>
                            {{--@include('admin.orders.staff_right')--}}
                            <form class="form-horizontal order" role="form" action="{{route('admin.orders.staff', @$order->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">

                                <div class="page-kp-form-bottom-button type-left">
                                    <button type="submit" class="page-kp-form-button bgs-green">Сохранить</button>
                                    <button type="button" data-action="{{route('admin.orders.staff', ['id' => @$order->id, 'route' => 'back'])}}" class="page-kp-form-button bgs-orange">Применить</button>
                                </div>
                            </form>
                        </div>

                        <div class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                            <iframe src="https://yandex.ua/map-widget/v1/-/CBuEIOuF9C" width="560" height="400" frameborder="1" allowfullscreen="true"></iframe>
                        </div>
                        {{--<div class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>
@stop