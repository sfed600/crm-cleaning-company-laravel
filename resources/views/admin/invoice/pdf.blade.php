<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8" />
    <meta charset="UTF-8">
    <title>Счет {{$invoice->number}}</title>

    {{--<link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css" />
    <script src="/assets/admin/js/bootstrap.min.js"></script>--}}

    <style>
        .border-table {
            width: 100%;
            border-collapse: collapse;
        }

        .border-table, .border-table th, .border-table td {
            border: 1px solid black;
            /*text-align: center;*/
            color: #1d2122;
        }
    </style>
</head>
<body style="font-family: DejaVu Sans">
{{--{{dd(dump($company->getImgPreviewPath('stamp').$company->stamp))}}--}}

<div style="display: flex">
    <div style="float: left; width: 40%">
        @if( strlen($company->logo) > 0)
            {{--<img style="width: 100%;" src="http://{{$_SERVER['HTTP_HOST'].'/'.$company->getImgPath('logo').$company->logo}}">--}}
            <img style="width:100%" src="http://test.cleancrm.ru/assets/imgs/companies/logo/1524066833_jCBia.png">
        @endif
    </div>
    <div style="float: left; width: 60%; padding-left: 20px;">
        <b>
            {{mb_strtoupper($company->name)}}<br>
            Address:<br>
            Тел.:{{$company->phone}}
        </b>
    </div>
</div>
<div style="clear: both"></div>
<br>
<table class="border-table">
    <tr>
        <td>ИНН {{$company->inn}}</td>
        <td>КПП {{$company->kpp}}</td>
        <td style="border-bottom: none"></td>
        <td style="border-bottom: none"></td>
    </tr>
    <tr>
        <td colspan="2">
            Получатель<br>
            {{mb_strtoupper($company->name)}}
        </td>
        <td style="border-top: none">Сч. №</td>
        <td style="border-top: none">{{$company->checking_account}}</td>
    </tr>
    <tr>
        <td colspan="2">
            Банк получателя<br>
            {{mb_strtoupper($company->bank_name)}}
        </td>
        <td>БИК<br>Сч. №</td>
        <td>{{$company->bik}}<br>{{$company->cor_account}}</td>
    </tr>
</table>

<h2>Счет № {{$invoice->number}} от {{$invoice->date}}</h2>

<p>Платильщик..</p>

@if($invoice->products->count())
    <table class="border-table">
        <tr>
            {{--<td>№</td>--}}
            <td>Наименование товара</td>
            <td>Кол-во</td>
            <td>Ед.</td>
            <td>Цена, руб.</td>
            <td>Ставка НДС</td>
            <td>Сумма, руб.</td>
        </tr>
        @foreach($invoice->products as $key => $product)
            <tr>
                {{--<td>{{$product->id}}</td>--}}
                <td>{{$product->name}}</td>
                <td>1</td>
                <td>{{$product->measureName()}}</td>
                <td>{{$product->price}}</td>
                <td>{{$product->ndsName()}}</td>
                <td align="right">{{$product->price}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="5" style="text-align: right; border: none;">В том числе НДС:</td>
            <td align="right">{{$sumNDS}}</td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: right; border: none;">Итого:</td>
            <td align="right">{{$sumTotal}}</td>
        </tr>
    </table>
@endif

<p>
    Всего наименований {{$invoice->products->count()}}, на сумму {{--{{$offer->amount}}--}} {{$sumTotal}} руб.<br>
    {{--<b>Двадцать две тысячи восемсот рублей 00 копеек</b>--}}
    <b>{{$invoice->num2str($sumTotal)}}</b>
</p>

@if( strlen($company->stamp) > 0 )
    <div style="position: absolute; z-index: -5; left: 35px;">
        {{--<img style="height:180px;" src="http://{{$_SERVER['HTTP_HOST'].'/'.$company->getImgPath('stamp').$company->stamp}}">--}}
        <img style="height:150px;" src="http://test.cleancrm.ru/assets/imgs/companies/stamp/1524066711_0k1At.jpg">
    </div>
@endif
<table>
    <tr>
        <td>Генеральный директор</td>
        <td style="border-bottom: 1px solid black;">
            @if( strlen($company->signature) > 0 )
                {{--<img style="height:50px;" src="http://{{$_SERVER['HTTP_HOST'].'/'.$company->getImgPath('signature').$company->signature}}">--}}
                <img style="height:50px;" src="http://test.cleancrm.ru/assets/imgs/companies/signature/1524066833_7jrbM.jpg">
            @endif
        </td>
        <td>{{$company->director}}</td>
    </tr>
    <tr>
        <td>Главный бухгалтер</td>
        <td style="border-bottom: 1px solid black;">@if( strlen($company->signature) > 0 )
                {{--<img style="height:50px;" src="http://{{$_SERVER['HTTP_HOST'].'/'.$company->getImgPath('signature').$company->signature}}">--}}
                <img style="height:50px;" src="http://test.cleancrm.ru/assets/imgs/companies/signature/1524066833_7jrbM.jpg">
            @endif
        </td>
        <td>{{$company->buhgalter}}</td>
    </tr>
</table>

{{--<div class="row">
    <div class="col-lg-4 col-xs-4">
        @if( strlen($company->stamp) > 0 )
            --}}{{--<img style="width:100%" src="/assets/imgs/companies/stamp/preview/1524123017_qJrPZ.jpg">--}}{{--
            <img --}}{{--style="width:100%"--}}{{-- src="http://test.cleancrm.ru/assets/imgs/companies/stamp/1524066711_0k1At.jpg">
        @endif
    </div>
    <div class="col-lg-8 col-xs-8">
        --}}{{--{{dump($company)}}--}}{{--
    </div>
</div>--}}


</body>
</html>