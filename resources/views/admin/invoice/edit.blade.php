@extends('admin.layout')
@section('main')
    {{--<script src="/assets/admin/js/calculator.js?45"></script>--}}

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.invoices.index')}}">Счета</a>
            </li>
            <li class="active">Редактирование счета</li>
        </ul><!-- /.breadcrumb -->
    </div>

    {{--<div class="page-content order-edit" data-tab="{{$request->input('tab')}}">--}}
    <div class="page-content">

        <div class="page-header">
            <h1>
                Счета
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Редактирование
                </small>
            </h1>

            <div class="ibox-tools">
                <a class="btn" href="{{route('admin.invoices.pdf', ['id' => $invoice->id])}}">
                    {{--<a class="btn btn-info" href="/downloadPDF/{{$offer->id}}">--}}
                    <i class="ace-icon glyphicon glyphicon-download bigger-110"></i>
                    Скачать PDF
                    <i class="ace-icon glyphicon glyphicon-print bigger-110"></i>
                    Печать
                </a>
            </div>
            {{--<div class="ibox-tools">
                <a class="btn" href="#">
                    <i class="ace-icon glyphicon glyphicon-print bigger-110"></i>
                    Печать
                </a>
            </div>--}}
            <div class="ibox-tools">
                {{--<a class="btn" href="{{route('admin.invoices.email', ['id' => $invoice->id])}}">--}}
                <a class="btn" href="#">
                    <i class="ace-icon glyphicon glyphicon-envelope bigger-110"></i>
                    Отправить по email
                </a>
            </div>
            <div class="ibox-tools">
                <a class="btn" href="#">
                    {{--<a class="btn btn-info" href="/downloadPDF/{{$offer->id}}">--}}
                    <i class="ace-icon glyphicon glyphicon-download bigger-110"></i>
                    Ссылка
                </a>
            </div>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                {{--<li @if( !$request->input('tab') or $request->input('tab') == 'offer') class="active" @endif>--}}
                <li class="active">
                    <a data-toggle="tab" href="#invoice" aria-expanded="true">
                        <i class="green ace-icon fa fa-home bigger-120"></i>
                        Предложение
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#products" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Товары / услуги
                    </a>
                </li>

            </ul>

            <div class="tab-content">
                {{--<div id="offer" class="tab-pane @if( !$request->input('tab') or $request->input('tab') == 'offer') active @endif">--}}
                <div id="invoice" class="tab-pane active">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <form id="edit-invoice-form" class="form-horizontal" role="form" action="{{route('admin.invoices.update', $invoice->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                {{--<input name="order_id" type="hidden" value="{{$invoice->order->id}}">--}}

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1"> # Счета </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="number" placeholder="# Счета" value="{{ old('number', $invoice->number) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-22"> Сумма </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-22" name="amount" placeholder="Сумма" value="{{ old('amount', $invoice->order->amount) }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Дата счета</label>
                                    {{--<label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Время начала</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="date" value="{{old('date', $invoice->datePicker())}}" id="form-field-16" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <label class="col-sm-1" for="form-field-116"> Срок оплаты</label>
                                    {{--<label class="col-sm-1" for="form-field-116"> Время окончания</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="due_date" value="{{old('due_date', $invoice->dueDatePicker())}}" id="form-field-116" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-3"> Контакты </label>
                                    <div class="col-sm-8">
                                        <div class="dynamic-input">
                                            @forelse($invoice->order->contacts as $key => $contact)
                                                <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                                    <select disabled="disabled" name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        <option value="{{$contact->id}}">{{$contact->name}}</option>
                                                    </select>
                                                    <a href="" class="input-group-addon @if($key==0)plus @else minus @endif">
                                                        <i class="glyphicon glyphicon-@if($key==0)plus @else minus @endif bigger-110"></i>
                                                    </a>
                                                </div>
                                            @empty
                                                <div class="input-group dynamic-input-contact col-sm-1" style="margin-bottom:5px;">
                                                    <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        <option value=""></option>
                                                    </select>
                                                    <a href="" class="input-group-addon plus">
                                                        <i class="glyphicon glyphicon-plus bigger-110"></i>
                                                    </a>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-10"> Компания </label>
                                    <div class="col-sm-8">
                                        <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                            <select disabled name="order_company_id" class="chosen-select chosen-autocomplite form-control" id="form-field-order_company_id" data-url="{{route('admin.order_companies.search')}}" data-placeholder="Начните ввод...">
                                                @if( $invoice->order->orderCompany )
                                                    <option value="{{$invoice->order->orderCompany->id}}">{{$invoice->order->orderCompany->name}}</option>
                                                @else
                                                    <option value=""></option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-note"> Примечание </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-note" name="note" placeholder="Примечание" value="{{old('note', $invoice->note)}}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-8"> Статус </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Offer::$statuses as $key => $status)
                                                <option value="{{$key}}" @if((old() && old('status')==$key) || (!old() &&  $key==$invoice->order->status)) selected @endif>{{$status['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-5"> Ответсвенный </label>
                                    <div class="col-sm-9">
                                        <select name="user_id" class="chosen-select chosen-autocomplite form-control" id="form-field-5" data-url="{{route('admin.users.search', ['block' => 'Management'])}}" data-placeholder="Начните ввод...">
                                           @if($invoice->order->user_id)
                                           <option value="{{$invoice->order->user_id}}" selected>{{$invoice->order->user->fio()}}</option>
                                           @else
                                           <option value=""></option>
                                           @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-success" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Сохранить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-warning button-apply" type="button" data-action="{{route('admin.invoices.update', ['id' => $invoice->order->id, 'route' => 'back'])}}">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Применить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="{{route('admin.invoices.index')}}">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Назад
                                        </a>
                                        {{--&nbsp; &nbsp; &nbsp;
                                        <a class="btn" href="{{route('admin.invoices.pdf', ['id' => $invoice->id])}}">
                                            <i class="ace-icon glyphicon glyphicon-download bigger-110"></i>
                                            Скачать PDF
                                        </a>--}}
                                        {{--&nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="#">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Печать
                                        </a>--}}
                                    </div>
                                </div>

                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>

                <div id="products" class="tab-pane">
                    @include('admin.invoice.products')
                </div>

            </div>
        </div>

    </div>

@stop

