@extends('admin.layout')
@section('main')
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="dash-container">
        <div class="dash-content">
            <div class="dash-table" data-item-check="wrap">
                {{--<div>
                    <em>Into This</em>
                    <select data-placeholder="Choose a Country..." class="chosen-select" tabindex="2">
                        <option value=""></option>
                        <option value="United States">United States</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Aland Islands">Aland Islands</option>
                        <option value="Albania">Albania</option>
                    </select>
                </div>--}}
                <table>
                    <colgroup>
                        <col class="tbl-check">
                        <col class="tbl-number">
                        <col class="tbl-title">
                        <col class="tbl-contact">
                        <col class="tbl-number">
                        <col class="tbl-budget">
                        <col class="tbl-datecreate">
                        <col class="tbl-step">
                        <col class="tbl-pay">
                        <col class="tbl-responsible">
                        <col class="tbl-datecreate">
                    </colgroup>
                    <thead>
                    <tr>
                        <td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                        </td>
                        <td>№ счета</td>
                        <td>Название</td>
                        <td>Контакт</td>
                        <td>№ ЗАКАЗ</td>
                        <td><a class="tbl-heads-link" href="#">Бюджет, руб</a></td>
                        <td>Дата счета</td>
                        <td>Этап заказа</td>
                        <td>Оплата</td>
                        <td>Ответственный</td>
                        <td><a class="tbl-heads-link" href="#">Дата создания</a></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoices as $item)
                        <tr>
                            <td>
                                <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                            </td>
                            <td>{{$item->number}}</td>
                            <td>
                                <a href="{{route('admin.invoices.edit', $item->id)}}">Счет № {{$item->number}}</a>
                            </td>
                            <td>
                                {{@$item->order->contacts()->first()->name}}
                                @if(isset($item->order->orderCompany))
                                    <span class="tbl-block-company">“{{$item->order->orderCompany->name}}”</span>
                                @endif
                            </td>
                            <td>{{$item->order->number}}</td>
                            <td>{{number_format($item->order->amount, 0, ',', ' ')}}</td>
                            <td>
                                @if($item->due_date != "0000-00-00" && strlen($item->due_date) == 10)
                                    {{date('d.m.Y', strtotime($item->due_date))}}
                                @endif
                            </td>
                            <td>
                                   <span class="pseudo-select" data-pseudo="wrap">
                                       @php
                                           $bg_status_class = '';
                                           switch ($item->order->status) {
                                               case 'first':
                                                   $bg_status_class = 'bgs-first';
                                                   break;
                                               case 'agree':
                                                   $bg_status_class = 'bgs-agree';
                                                   break;
                                               case 'agreed':
                                                   $bg_status_class = 'bgs-agreed';
                                                   break;
                                               case 'confirmed':
                                                   $bg_status_class = 'bgs-confirmed';
                                                   break;
                                               case 'work':
                                                   $bg_status_class = 'bgs-work';
                                                   break;
                                               case 'performed':
                                                   $bg_status_class = 'bgs-performed';
                                                   break;
                                               case 'final_positive':
                                                   $bg_status_class = 'bgs-final_positive';
                                                   break;
                                               case 'final_negative':
                                                   $bg_status_class = 'bgs-final_negative';
                                                   break;
                                           }
                                       @endphp
                                       <span class="pseudo-select-txt o-btn {{$bg_status_class}}" data-pseudo="title" tabindex="0">{{$item->order->statusName()}}</span>
                                       <span class="pseudo-select-drop" data-pseudo="drop">
                                            <span data-order_id="{{$item->order->id}}" data-value="first" data-class="bgs-first" @if($item->order->status == 'first') class="current" @endif>Первичный контакт</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="agree" data-class="bgs-agree" @if($item->order->status == 'agree') class="current" @endif>Согласовать</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="agreed" data-class="bgs-agreed" @if($item->order->status == 'agreed') class="current" @endif>Согласованно</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="confirmed" data-class="bgs-confirmed" @if($item->order->status == 'confirmed') class="current" @endif>Подтверждено</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="work" data-class="bgs-work" @if($item->order->status == 'work') class="current" @endif>В работе</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="performed" data-class="bgs-performed" @if($item->order->status == 'performed') class="current" @endif>Работы выполнены</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="final_positive" data-class="bgs-final_positive" @if($item->order->status == 'final_positive') class="current" @endif>Успешно реализовано</span>
                                            <span data-order_id="{{$item->order->id}}" data-value="final_negative" data-class="bgs-final_negative" @if($item->order->status == 'final_negative') class="current" @endif>Не реализовано</span>
                                       </span>
                                   </span>
                            </td>
                            <td>
                                {{--<span class="pseudo-select" data-pseudo="wrap">
                                   <span class="pseudo-select-txt" data-pseudo="title" tabindex="0">Оплачен</span>
                                   <span class="pseudo-select-drop" data-pseudo="drop">
                                      <span data-value="payed" class="current">Оплачен</span>
                                      <span data-value="nopayed">Не оплачен</span>
                                      <span data-value="partpay">Частично оплачен</span>
                                   </span>
                                </span>--}}
                            </td>
                            <td>
                                @if($item->order->responsible_users()->count() > 0 )
                                    {{$item->order->responsible_users()->first()->fio()}}
                                @endif
                            </td>
                            <td>{{date('d.m.Y H:i', strtotime($item->created_at))}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    <div class="paginator-label">Показано {{sizeof($invoices)}} из {{$count_invoices}}</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'invoices');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'invoices');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'invoices');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'invoices');">100</a></li>
                        </ul>
                    </div>

                    {!! $invoices->render() !!}
                </div>
            </div>

            <div class="dash-container-layer"></div>
        </div>

@stop