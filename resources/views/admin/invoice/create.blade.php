@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.invoices.index')}}">Счета</a>
            </li>
            <li class="active">Добавление счета</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Счета
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Добавление
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#order" aria-expanded="true">
                        <i class="green ace-icon fa fa-home bigger-120"></i>
                        Счет
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#products" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Товары / услуги
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="order" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <form id="form-invoice" class="form-horizontal" role="form" action="{{route('admin.invoices.store')}}" method="POST" enctype="multipart/form-data">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <input name="order_id" type="hidden" value="{{$order->id}}">

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1"> # Счета </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="number" placeholder="# Счета" value="{{ old('number', $invoice_number) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-22"> Сумма </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-22" readonly value="{{ $order->amount }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Дата счета</label>
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="date" value="{{Carbon\Carbon::now()->format('d.m.Y')}}" id="form-field-16" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <label class="col-sm-1" for="form-field-116"> Срок оплаты</label>
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker" name="due_date" id="form-field-116" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-3"> Контакты </label>
                                    <div class="col-sm-8">
                                        <div class="dynamic-input">
                                            @forelse($order->contacts as $key => $contact)
                                                <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                                    <select disabled="disabled" name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        <option value="{{$contact->id}}">{{$contact->name}}</option>
                                                    </select>
                                                    <a href="" class="input-group-addon @if($key==0)plus @else minus @endif">
                                                        <i class="glyphicon glyphicon-@if($key==0)plus @else minus @endif bigger-110"></i>
                                                    </a>
                                                </div>
                                            @empty
                                                <div class="input-group dynamic-input-contact col-sm-1" style="margin-bottom:5px;">
                                                    <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        <option value=""></option>
                                                    </select>
                                                    <a href="" class="input-group-addon plus">
                                                        <i class="glyphicon glyphicon-plus bigger-110"></i>
                                                    </a>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-10"> Компания </label>
                                    <div class="col-sm-8">
                                        <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                            <select disabled name="order_company_id" class="chosen-select chosen-autocomplite form-control" id="form-field-order_company_id" data-url="{{route('admin.order_companies.search')}}" data-placeholder="Начните ввод...">
                                                @if( $order->orderCompany )
                                                    <option value="{{$order->orderCompany->id}}">{{$order->orderCompany->name}}</option>
                                                @else
                                                    <option value=""></option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-note"> Примечание </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-note" name="note" placeholder="Примечание" value="{{$invoice->note or $order->note}}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-8"> Статус </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Order::$statuses as $key => $status)
                                                <option value="{{$key}}" @if( $order->status == $key) ) selected @endif>{{$status['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-5"> Ответсвенный </label>
                                    <div class="col-sm-9">
                                        <select name="user_id" class="chosen-select chosen-autocomplite form-control" id="form-field-5" data-url="{{route('admin.users.search', ['block' => 'Management'])}}" data-placeholder="Начните ввод...">
                                              <option value="{{$order->user_id}}" selected>{{Auth::user()->find($order->user_id)->fio()}}</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-success" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Сохранить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="{{route('admin.orders.index')}}">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Назад
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                </div>

                <div id="products" class="tab-pane">
                    @include('admin.invoice.products')
                </div>

            </div>
        </div>

    </div>
@stop
