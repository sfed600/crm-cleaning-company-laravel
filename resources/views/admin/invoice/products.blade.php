<div class="row">
    <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
        <form id="products-form" class="form-horizontal" role="form" {{--action="{{route('admin.offers.update', $offer->id)}}" method="POST" enctype="multipart/form-data"--}}>
            {{--<input type="hidden" name="_method" value="PUT">--}}
            <input name="_token" type="hidden" value="{{csrf_token()}}">

            <div class="form-group">
                {{--<label class="col-sm-2 control-label no-padding-right" for="form-field-3"> Товары/Услуги</label>--}}
                <div class="col-sm-9">
                    <div class="dynamic-input">
                        @if( @$invoice->products )
                            @forelse($invoice->products as $key => $product)
                                <div class="input-group dynamic-input-contact" style="margin-bottom:5px;">
                                    {{--<select name="product_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                        <option value="{{$product->id}}" selected>{{$product->name}}</option>
                                    </select>--}}
                                    <select name="product_id[]" class="col-sm-12 form-control">
                                        <option></option>
                                        @foreach($products as $key => $item)
                                            <option value="{{$item->id}}" @if($product->id == $item->id) selected @endif >{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                        <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                                    </a>
                                    <a href="" class="input-group-addon plus">
                                        <i class="glyphicon glyphicon-plus bigger-110"></i>
                                    </a>
                                </div>
                            @empty
                                <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                    {{--<select name="product_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                        <option value=""></option>
                                    </select>--}}
                                    <select name="product_id[]" class="col-sm-12 form-control">
                                        <option></option>
                                        @foreach($products as $key => $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <a href="" class="input-group-addon plus">
                                        <i class="glyphicon glyphicon-plus bigger-110"></i>
                                    </a>
                                </div>
                            @endforelse
                        @else
                            <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                {{--<select name="product_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                    <option value=""></option>
                                </select>--}}
                                <select name="product_id[]" class="col-sm-12 form-control">
                                    <option></option>
                                    @foreach($products as $key => $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <a href="" class="input-group-addon plus">
                                    <i class="glyphicon glyphicon-plus bigger-110"></i>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

<script>
    $('form#form-invoice').submit(function(){
        /*alert('Форма foo отправлена на сервер.');
        return false;*/
        $('#products-form').find('[name="product_id[]"]').each(function () {
            $('form#form-invoice').prepend('<input name="product_id[]" type="hidden" value="' + this.value + '">');
        });
    });

    $('form#edit-invoice-form').submit(function(){
        /*alert('Форма foo отправлена на сервер.');
         return false;*/
        $('#products-form').find('[name="product_id[]"]').each(function () {
            $('form#edit-invoice-form').prepend('<input name="product_id[]" type="hidden" value="' + this.value + '">');
        });
    });

</script>