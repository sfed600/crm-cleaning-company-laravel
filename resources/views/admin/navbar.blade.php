<div id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <table class="main-header">
            <tr>
                @if(Auth::user()->company)
                    <td class="header-logo">
                        <a href="{{route('admin.main')}}" class="navbar-brand">
                            <div style="display: inline-grid;">
                                <span>{{Auth::user()->company->name}}</span>
                                <span>Клининговая компания</span>
                            </div>
                        </a>
                    </td>
                @else
                    <td width="10%"></td>
                @endif
                <td class="header-title"><h4>Табель</h4></td>
                <td width="30%">
                    <div class="header-search">
                        <button class="btn btn-info btn-xs" type="submit">
                            Текущий месяц &nbsp;
                            <i class="ace-icon fa fa-times" onclick="$(this).parent().remove();"></i>
                        </button>
                        {{--<button class="btn btn-info btn-xs" type="submit">
                            Исполнитель &nbsp;
                            <i class="ace-icon fa fa-times"></i>
                        </button>--}}
                        <input id="search_field" type="text" class="form-control" placeholder="+поиск" style="border: 0;">
                        {{--<i class="ace-icon glyphicon glyphicon-search" style="position: relative; left: -35px; top:10px;"></i>
                        <i class="fa fa-times-thin fa-2x" aria-hidden="true" style="position: relative; left: -30px; top:4px;"></i>--}}
                        <i class="ace-icon glyphicon glyphicon-search" style="position: relative; left: -20px; top:10px;"></i>
                        <i class="fa fa-times-thin fa-2x reset_tabel_filter" aria-hidden="true" onclick="reset_tabel_filter();"></i>
                        {{--<div style="display: flex;">
                            <i class="ace-icon glyphicon glyphicon-search" style="position: relative; left: -35px; top:10px;"></i>
                            <i class="fa fa-times-thin fa-2x" aria-hidden="true" style="position: relative; left: -30px; top:4px;"></i>
                        </div>--}}
                    </div>
                </td>
                <td class="header-info">
                    <span>Выплата сотрудникам 1 651 656 321$</span>
                </td>
                <td class="header-btn1">
                    <button class="btn btn-success btn-xs" type="submit">
                        <i class="fa fa-plus"></i>&nbsp;Добавить заказ
                        {{--<i class="ace-icon glyphicon glyphicon-search  align-top bigger-125 icon-on-right"></i>--}}
                    </button>
                </td>
                <td class="header-right">
                    <img src="/img/bell.png" />
                    <div class="notes-alert">0</div>
                    {{--<span>Здравствуйте, {{ Auth::user()->name }}</span>--}}
                    <div class="btn-group">
                        {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <img class="round-img" src="/{{Auth::user()->getImgPreviewPath().Auth::user()->img}}"  />
                            <span class="caret"></span>
                        </a>--}}
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            @if(Auth::user()->img)
                                <img class="round-img" src="/{{Auth::user()->getImgPreviewPath().Auth::user()->img}}"  />
                            @else
                                <span>{{ Auth::user()->name }}</span>
                            @endif
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{route('index')}}">
                                    <i class="ace-icon fa fa-home"></i>
                                    Главная
                                </a>
                            </li>
                            <li>
                                <a href="{{route('admin.users.show', Auth::user()->id)}}">
                                    <i class="ace-icon fa fa-user"></i>
                                    Профиль
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('/logout') }}">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Выход
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>

    </div><!-- /.navbar-container -->
</div>