<div class="kp-order-form-wrap">
    @if( Route::current()->getName() == "admin.order_contacts.edit" )
        <form id="order-contact-form" class="form-horizontal" role="form" action="{{route('admin.order_contacts.update', $contact->id)}}" method="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input name="_token" type="hidden" value="{{csrf_token()}}">

            <input name="typ" type="hidden" value="{{$contact->typ}}">
            <input name="user_id" type="hidden" value="{{$contact->user_id}}">
            <input name="order_company_id" type="hidden" value="{{@$contact->company->id}}">
            <input name="contact_id" type="hidden" value="{{$contact->id}}">

            <input type="hidden" name="old_tel" value="{{@$contact->phones()->where('type', 'work')->first()->phone}}">
            <input type="hidden" name="old_mobile" value="{{@$contact->phones()->where('type', 'mobile')->first()->phone}}">
    @else
        <form id="order-contact-form" class="form-horizontal" role="form" action="{{route('admin.order_contacts.store')}}" method="POST" enctype="multipart/form-data">
            <input name="_token" type="hidden" value="{{csrf_token()}}">
            <input name="contact_name" type="hidden">
            <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
            <input name="typ" type="hidden" value="customer">
            <input name="order_company_id" type="hidden">

            @if(Request::has('route'))
                <input name="route" type="hidden" value="{{Request::input('route')}}">
            @elseif(Request::has('route2'))
                <input name="route" type="hidden" value="{{Request::input('route2')}}">
            @endif

            @if(Request::has('order_name'))
                <input name="order_name" type="hidden" value="{{Request::input('order_name')}}">
            @endif

            @if(Request::has('amount'))
                <input name="amount" type="hidden" value="{{Request::input('amount')}}">
            @endif
    @endif

            <input type="hidden" name="name" value="{{old('name', @$contact->name)}}">

    @if( Route::current()->getName() == "admin.order_contacts.edit" )
        @if (old('phones'))
            @foreach(old('phones') as $key => $phone)
                <input type="hidden" name="phone_ids[{{$key}}]" value="{{old('phone_ids')[$key]}}">
            @endforeach
        @elseif($contact->phones->count())
            @foreach($contact->phones as $key => $phone)
                <input type="hidden" name="phone_ids[{{$phone->type}}]" value="{{$phone->id}}">
            @endforeach
        @endif
    @endif

            <div class="comp-contact-heads clearfix">
                <div class="comp-contact-heads-fields">
                    <div class="kp-order-box-toggle-fields type-person toggle-open" data-boxtoggle-fields="drop" data-addfieldbox-tgl-show="wrap">
                        <input type="hidden" name="contact_ids[]" value="{{@$contact->id}}">

                        <div class="kp-order-box-toggle-fields-str">
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Рабочий телефон:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        {{--<input update-ajax data-jsmask="tel" type="text" placeholder="..." name="tel" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'work')->first()->phone}}" @endif>--}}
                                        <input update-ajax data-jsmask="tel" type="text" placeholder="..." name="phones[work]" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'work')->first()->phone}}" @endif>
                                        <input type="hidden" name="old_phone[work]" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'work')->first()->phone}}" @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Мобильный:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        {{--<input update-ajax data-jsmask="tel" type="text" placeholder="..." name="mobile" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'mobile')->first()->phone}}" @endif>--}}
                                        <input update-ajax data-jsmask="tel" type="text" placeholder="..." name="phones[mobile]" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'mobile')->first()->phone}}" @endif>
                                        <input type="hidden" name="old_phone[mobile]" @if( isset($contact) ) value="{{@$contact->phones()->where('type', 'mobile')->first()->phone}}" @endif>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-str">
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">E-mail:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        {{--<input update-ajax type="text" placeholder="..." data-op-input="get" data-name="email" name="email" @if( isset($contact) ) value="{{@$contact->emails()->first()->email}}" @endif>--}}
                                        <input update-ajax type="text" placeholder="..." data-name="email" name="email" @if( isset($contact) ) value="{{@$contact->emails()->first()->email}}" @endif>
                                    </div>
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Должность:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input update-ajax type="text" placeholder="..." data-op-input="get" data-name="work-position" name="post" value="{{@$contact->post}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="kp-order-box-toggle-fields-str display-none" data-addfieldbox-tgl-show="field">
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Поле 1:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Поле 2:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-str display-none" data-addfieldbox-tgl-show="field">
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Поле 3:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="kp-order-box-toggle-fields-col-item">
                                <div class="kp-order-box-toggle-fields-row">
                                    <div class="op-input-label">Поле 4:</div>
                                    <div class="op-input type-btns" data-op-input="item">
                                        <input type="text" placeholder="..." data-op-input="get" value="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="kp-order-box-toggle-fields-adds">
                            <div class="adds-down-btn" data-addfieldbox-tgl-show="btn">
                                <span class="txt-label">Еще</span>
                                <div class="adds-down-btn-icon">
                                    <span class="icon-input-ctrl-down"></span>
                                    <span class="icon-input-ctrl-down-h btn-hover"></span>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>

                <div class="comp-contact-heads-logo">
                    <div class="comp-contact-heads-photo-block type-uploads" data-contact-block-photo="wrap">
                        <div class="comp-contact-heads-nophoto">
                            <label class="comp-contact-heads-photo-add">
                                <input name="photo" type="file" value="{{ old('photo') }}" data-contact-block-photo="upload" @if( Route::current()->getName() == "admin.order_contacts.edit" ) upload-ajax @endif>
                            </label>
                        </div>

                        @if( empty($contact->photo) )
                            <a class="comp-contact-heads-photo" style="background-image:url(/img/avatar_user_contact.jpg)" href="/img/test.png" data-fancybox="gallery" data-contact-block-photo="image"></a>
                        @else
                            <a class="comp-contact-heads-photo" style="background-image:url(/assets/imgs/contacts/{{$contact->photo}})" href="/assets/imgs/contacts/{{$contact->photo}}" data-fancybox="gallery" data-contact-block-photo="image"></a>
                        @endif

                        <div class="comp-contact-heads-photo-control">
                            <div class="op-input-control-btn" data-contact-block-photo="remove"><span class="icon-input-ctrl-clear"></span><span class="icon-input-ctrl-clear-h btn-hover"></span></div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="kp-order-form-row clearfix type-address">
            <div class="kp-order-form-labinput">
                <div class="op-input-label">Адрес:</div>
                <div class="kp-order-form-labinput-box">
                    <div class="op-input type-btns" data-op-input="item" data-openside>
                        <div class="op-input-control-btn" data-op-input="search">
                            <span class="icon-input-ctrl-search"></span>
                            <span class="icon-input-ctrl-search-h btn-hover"></span>
                        </div>
                        <input id="suggest" type="text" name="address" update-ajax placeholder="..." data-openside-map="#side-ya-map" value="{{ old('address', @$contact->address) }}">
                        {{--<input type="text" id="suggest" class="input" placeholder="..." name="address" data-openside-map="#side-ya-map" value="{{ old('address', @$order->address) }}">--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-addressdetail">
            <div class="kp-order-form-justify">
                <div class="kp-order-form-justify-ins">
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Квартира:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="apartment" value="{{ old('apartment', @$contact->apartment)}}" update-ajax>
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Подъезд:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="porch" value="{{ old('porch', @$contact->porch)}}" update-ajax>
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Этаж:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="floor" value="{{ old('floor', @$contact->floor)}}" update-ajax>
                        </div>
                    </div>
                    <div class="kp-order-form-justify-item">
                        <div class="op-input-label">Код домофона:</div>
                        <div class="op-input">
                            <input type="text" placeholder="..." name="domofon" value="{{ old('domofon', @$contact->domofon)}}" update-ajax>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="kp-order-form-row clearfix type-note">
            <div class="op-input-label">Примечание:</div>
            <div class="op-input">
                <textarea placeholder="..." data-autoheight="true" name="comments">{{old('comments', @$contact->comments)}}</textarea>
            </div>
        </div>

        <div class="kp-order-boxdata clearfix">
            <div class="kp-order-boxdata-title">
                <div class="op-input-label">Компания</div>
                <div class="op-input type-btns" data-op-input="item">
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <div class="op-input-control-btn" data-op-input="add" id="js-kp-order-boxdata-company-addnew" {{--onclick="create_order_company();"--}}>
                        <span class="icon-input-ctrl-add"></span>
                        <span class="icon-input-ctrl-add-h btn-hover"></span>
                    </div>
                    <input context="contact" type="text" placeholder="..." data-op-input="get" id="js-kp-order-boxdata-company-search">
                </div>
            </div>

            <script type="text/template" id="js-kp-order-boxdata-company-template">
                <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>
                    <input type="hidden" data-name="id">

                    <div class="kp-order-box-toggle-fields-row type-company">
                        <div class="op-input type-btns type-company" data-op-input="item">
                            <input type="text" placeholder="..." data-op-input="get" data-name="name" value="">
                        </div>
                    </div>

                    <div style="top: 13%; right: -10px;" class="op-input-control-btn" data-order-company="remove">
                        <span class="icon-input-ctrl-clear"></span>
                        <span class="icon-input-ctrl-clear-h btn-hover"></span>
                    </div>

                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Сайт:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input name="site" type="text" placeholder="..." data-op-input="get" data-name="site" value="">
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Телефон:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input name="phone" data-jsmask="tel" type="text" placeholder="..." data-name="phone">
                        </div>
                    </div>
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">E-mail:</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="">
                        </div>
                    </div>
                </div>
            </script>

            <div id="js-kp-order-boxdata-company-insert">
                @if(isset($contact->company))
                    <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap" data-update-ajax-order_company>
                        <div class="kp-order-box-toggle-fields-row type-company">
                            <div class="op-input type-btns type-company" data-op-input="item">
                                <input {{--name="name"--}} type="text" placeholder="..." data-op-input="get" data-name="name" value="{{$contact->company->name}}">
                            </div>

                            <div style="top: 13%; right: -10px;" class="op-input-control-btn" data-order-company="remove">
                                <span class="icon-input-ctrl-clear"></span>
                                <span class="icon-input-ctrl-clear-h btn-hover"></span>
                            </div>

                        </div>

                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Сайт:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input name="site" type="text" placeholder="..." data-op-input="get" data-name="www" value="{{$contact->company->site}}">
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">Телефон:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input name="phone" data-jsmask="tel" type="text" placeholder="..." value="{{$contact->company->phone}}">
                            </div>
                        </div>
                        <div class="kp-order-box-toggle-fields-row">
                            <div class="op-input-label">E-mail:</div>
                            <div class="op-input type-btns" data-op-input="item">
                                <input name="email" type="text" placeholder="..." data-op-input="get" data-name="email" value="{{$contact->company->email}}">
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        {{--@if( Route::current()->getName() == "admin.order_contacts.edit" )
            <div class="kp-order-form-row clearfix type-person">
                <div class="kp-order-form-labinput">
                    <div class="op-input-label">Ответственный:</div>
                    <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                        <div class="kp-order-form-person-item" data-persons="item">
                            <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$contact->user->img}})"></div>
                            <div class="kp-order-form-person-item-name">{{$contact->user->surname.($contact->user->name ? ' '.$contact->user->name : '').($contact->user->patronicname ? ' '.$contact->user->patronicname : '')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endif--}}

            <div class="kp-order-form-row clearfix type-person">
                <div class="kp-order-form-labinput">
                    <div class="op-input-label">Ответственный:</div>
                    <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                        <div class="op-input type-btns" data-op-input="item">
                            <input type="text" placeholder="..." value="" data-user_block="Management" data-op-input="person-search">
                        </div>

                        @if( Route::current()->getName() == "admin.order_contacts.edit" )
                            <div class="kp-order-form-person-item" data-persons="item">
                                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$contact->user->img}})"></div>
                                <div class="kp-order-form-person-item-name">{{@$contact->user->fio()}}</div>
                                {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{$company->user->id}}"></div>--}}
                            </div>
                        @elseif( Route::current()->getName() == "admin.order_contacts.create" )
                            <div class="kp-order-form-person-item" data-persons="item">
                                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.Auth::user()->img}})"></div>
                                <div class="kp-order-form-person-item-name">{{Auth::user()->fio()}}</div>
                                {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{Auth::user()->id}}"></div>--}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            <script type="text/template" id="kp-order-person-additem">
                <div class="kp-order-form-person-item" data-persons="item">
                    <div class="kp-order-form-person-item-ava" style="background-image:url([avatar])"></div>
                    <div class="kp-order-form-person-item-name">[name]</div>
                    {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="[user_id]"></div>--}}
                </div>
            </script>


        @if( Route::current()->getName() == "admin.order_contacts.create" )
            <div class="page-kp-form-bottom-button type-right">
                <button class="page-kp-form-button bgs-green" type="submit">Сохранить</button>
                {{--<button class="page-kp-form-button bgs-orange" type="reset">Обновить</button>--}}
            </div>
        @elseif( Route::current()->getName() == "admin.order_contacts.edit" )
            <script>
                var userClickedSave = false;
            </script>

            <div class="page-kp-form-bottom-button type-right" style="display:none">
                <button type="button" class="page-kp-form-button bgs-green" onclick="userClickedSave = true; $('#order-contact-form').submit();">Сохранить</button>
                {{--<button class="page-kp-form-button bgs-orange" type="reset">Обновить</button>--}}
            </div>
        @endif
    </form>
</div>