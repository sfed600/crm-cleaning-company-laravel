@extends('admin.layout')
@section('main')
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="dash-container">
        <div class="dash-content">
            <div class="dash-table" data-item-check="wrap">
                <table>
                    {{--<colgroup>
                        <col class="tbl-check">
                        <col class="tbl-number">
                        <col class="tbl-title">
                        <col class="tbl-contact">
                        <col class="tbl-budget">
                        <col class="tbl-tasks">
                        <col class="tbl-typepay">
                        <col class="tbl-datecreate">
                    </colgroup>--}}
                    <thead>
                    <tr>
                        <td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                        </td>
                        <td>
                            <a class="tbl-heads-link" href="#">ФИО</a>
                        </td>
                        <td><a class="tbl-heads-link" href="#">Компания</a></td>
                        <td>Телефон</td>
                        <td>
                            <a class="tbl-heads-link" href="#">Сумма</a>
                            {{--<a href="javascript:void(0);" sort="rating">
                                <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="rating-asc"></i>
                                <i class="fa fa-fw fa-sort-down" sort="rating-desc"></i>
                            </a>--}}
                        </td>
                        <td><a class="tbl-heads-link" href="#">Баланс</a></td>
                        <td><a class="tbl-heads-link" href="#">Email</a></td>
                        <td>
                            <a class="tbl-heads-link" href="#">Дата создания</a>
                        </td>
                        <td><a class="tbl-heads-link" href="#">Ответственный</a></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $item)
                        <tr>
                            <td>
                                <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                            </td>
                            <td>
                                <a href="{{route('admin.order_contacts.edit', $item->id)}}">{{$item->name}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.order_companies.edit', @$item->company->id)}}">{{@$item->company->name}}</a>
                            </td>
                            <td>
                                @foreach($item->phones as $phone)
                                    {{$phone->phone}}<br>
                                @endforeach
                            </td>
                            <td>{{number_format($item->orders->sum('amount'), 0, ',', ' ')}}</td>
                            <td>

                            </td>
                            <td>
                                @foreach($item->emails as $email)
                                    {{$email->email}}<br>
                                @endforeach
                            </td>
                            <td>
                                {{date('d.m.Y h:i', strtotime($item->created_at))}}
                            </td>
                            <td>
                                <a href="{{route('admin.users.edit', $item->user_id)}}">{{$item->user->fio()}}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    {{--<div class="paginator-label">Показано {{sizeof($users)}} из {{@$count_users}}</div>--}}
                    <div class="paginator-label">Показано {{sizeof($contacts)}} из {{@$total_contacts_count}}</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'order_contacts');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'order_contacts');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'order_contacts');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'order_contacts');">100</a></li>
                        </ul>
                    </div>

                    {!! $contacts->render() !!}
                </div>
            </div>

            <div class="dash-container-layer"></div>
        </div>

@stop