@extends('admin.layout')
@section('main')
    {{--for ymap--}}
    <script src="/assets/admin_new/js/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=onLoad"></script>
    <script src="/assets/admin_new/js/ymaps.js?v=48"></script>
    <script>
        function onLoad (ymaps) {
            var suggestView = new ymaps.SuggestView('suggest');
        }
    </script>

    <div class="dash-container">
        <div class="dash-kp-line type-fleft">
            <ul class="dash-kp-line-list" data-kp-step="wrap">
                <li>
                    <span class="dash-kp-line-btn current" data-kp-step="btn" onclick="$('[name=\'typ\']').val('customer');">
                        <span class="btn-bgs bgs-orangelight"></span>
                        <span class="btn-bgs bgs-hover bgs-orangelight"></span>
                        <span class="txt-label">Заказчик</span>
                    </span>
                </li>
                <li>
                    <span class="dash-kp-line-btn" data-kp-step="btn" onclick="$('[name=\'typ\']').val('provider');">
                        <span class="btn-bgs bgs-orange"></span>
                        <span class="btn-bgs bgs-hover bgs-orange"></span>
                        <span class="txt-label">Поставщик</span>
                    </span>
                </li>
                <li>
                    <span class="dash-kp-line-btn" data-kp-step="btn" onclick="$('[name=\'typ\']').val('contractor');">
                        <span class="btn-bgs bgs-greenlight"></span>
                        <span class="btn-bgs bgs-hover bgs-greenlight"></span>
                        <span class="txt-label">Подрядчик</span>
                    </span>
                </li>
            </ul>
        </div>

        <div class="dash-content">
            <div id="dash-kp">
                {{--<ul class="dash-kp-nav">
                    <li><a href="#" data-tab=".dash-wrap" data-findtab="kp-tabbox-main" class="current">Основное</a></li>
                    <li><a href="#" data-tab=".dash-wrap">Контакты</a></li>
                    <li><a href="#">Заказы</a></li>
                    <li><a href="#">Предложения</a></li>
                    <li><a href="#">Счета</a></li>
                    <li><a href="#">Договора</a></li>
                    <li><a href="#">Письма</a></li>
                    <li><a href="#">Реквизиты</a></li>
                    <li><a href="#">Статистика</a></li>
                </ul>--}}


                <div class="dash-kp-content">
                    <div class="dash-kp-col col-left">
                        <div class="o-tabs-box kp-tabbox-main current" data-tab-box>
                            @include('admin.message')
                            @include('admin.order_contacts.tab_main')
                        </div>
                    </div>
                    <div class="dash-kp-col col-right">
                        {{--<div class="o-tabs-box kp-tabbox-main kp-tabbox-contacts current" data-tab-box>
                            <div class="kp-system-messages-control">
                                <label class="kp-system-messages-control-item">
                                    <span class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></span>
                                    <span class="txt-label">Скрыть системные сообщения</span>
                                </label>
                            </div>

                            <div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа</div>
                            </div>

                            <div class="kp-history-item">
                                <div class="kp-history-item-date"><span>Август, 2017</span></div>
                                <div class="kp-history-item-message">
                                    03.08.2017 15:53 Олег Клочков Сделка создана: Название заказа
                                    <div class="kp-history-item-box">
                                        <div class="kp-history-item-box-heads">03.08.2017 15:53 Олег Клочков</div>
                                        СДЛорвдмалвадомивжлмовждмдвлаьмидлвьидлваиавдивэваиваливдлаи
                                        ваиваиваиваиваиваиваиваиваивчыиачвичвичвичв
                                        чвамивчаичваичвичвичвяивчиаяваияви
                                    </div>
                                </div>
                            </div>

                            <div class="kp-note-form">
                                <div class="kp-note-form-infos">
                                    <div class="kp-note-form-infos-text">Нет запланированных встреч, рекомендуем добавить</div>
                                    <div class="kp-note-form-infos-angle"></div>
                                </div>
                                <label class="kp-note-form-text">
                                    <textarea required></textarea>
                                    <span class="kp-note-form-text-notes"><a href="#">Примечание:</a> Введите текст</span>
                                </label>
                            </div>
                            <div class="kp-note-form-empty"></div>
                        </div>--}}

                        {{--<div class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                            <iframe src="https://yandex.ua/map-widget/v1/-/CBuEIOuF9C" width="560" height="400" frameborder="1" allowfullscreen="true"></iframe>
                        </div>--}}
                        <div style="top:50px" class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>
@stop
