@extends('admin.layout')
@section('main')
    {{--for ymap--}}
    <script src="/assets/admin_new/js/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=onLoad"></script>
    <script src="/assets/admin_new/js/ymaps.js?v=48"></script>
    <script>
        function onLoad (ymaps) {
            var suggestView = new ymaps.SuggestView('suggest');
        }
    </script>

    <div class="dash-container">
        <div class="dash-kp-line type-fleft">
            <ul class="dash-kp-line-list" data-kp-step="wrap">
                <li>
                    <span class="dash-kp-line-btn @if($contact->typ == 'customer') current @endif" data-kp-step="btn" onclick="$('[name=\'typ\']').val('customer');">
                        <span class="btn-bgs bgs-orangelight"></span>
                        <span class="btn-bgs bgs-hover bgs-orangelight"></span>
                        <span class="txt-label">Заказчик</span>
                    </span>
                </li>
                <li>
                    <span class="dash-kp-line-btn @if($contact->typ == 'provider') current @endif" data-kp-step="btn" onclick="$('[name=\'typ\']').val('provider');">
                        <span class="btn-bgs bgs-orange"></span>
                        <span class="btn-bgs bgs-hover bgs-orange"></span>
                        <span class="txt-label">Поставщик</span>
                    </span>
                </li>
                <li>
                    <span class="dash-kp-line-btn @if($contact->typ == 'contractor') current @endif" data-kp-step="btn" onclick="$('[name=\'typ\']').val('contractor');">
                        <span class="btn-bgs bgs-greenlight"></span>
                        <span class="btn-bgs bgs-hover bgs-greenlight"></span>
                        <span class="txt-label">Подрядчик</span>
                    </span>
                </li>
            </ul>
        </div>

        @include('admin.message')

        <div class="dash-content">
            <div id="dash-kp">
                <ul class="dash-kp-nav">
                    {{--<li @if( !$request->input('tab') || $request->input('tab') == 'company' ) class="active" @endif>--}}
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-main" @if(Request::get('tab') != 'staff') class="current" @endif>Основное</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-orders">Заказы</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-offers">Предложения</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-invoices">Счета</a></li>
                    <li><a href="#">Договора</a></li>
                    <li><a href="#">Письма</a></li>
                    <li><a href="#">Статистика</a></li>
                </ul>


                <div class="dash-kp-content">
                    <div class="dash-kp-col col-left">
                        <div class="o-tabs-box kp-tabbox-main @if(Request::get('tab') != 'staff') current @endif" data-tab-box>
                            @include('admin.order_contacts.tab_main')
                        </div>

                        <div class="o-tabs-box kp-tabbox-orders" data-tab-box>
                            @include('admin.order_contacts.orders')
                        </div>

                        <div class="o-tabs-box kp-tabbox-offers" data-tab-box>
                            @include('admin.order_contacts.offers')
                        </div>

                        <div class="o-tabs-box kp-tabbox-invoices" data-tab-box>
                            @include('admin.order_contacts.invoices')
                        </div>
                    </div>


                    <div class="dash-kp-col col-right">
                        <div class="o-tabs-box kp-tabbox-main kp-tabbox-contacts current" data-tab-box>
                            <div class="kp-system-messages-control">
                                <label class="kp-system-messages-control-item">
                                    <span class="o-input-box-replaced">
                                        <input type="checkbox" name="items[]" tabindex="0" data-item-check="current" onclick="$('.kp-history-item, .kp-note-form-empty, .kp-note-form').toggle();">
                                        <span class="o-input-box-replaced-check"></span>
                                    </span>
                                    <span class="txt-label">Скрыть системные сообщения</span>
                                </label>
                            </div>

                            @forelse($periods as $period)
                                <div class="kp-history-item">
                                    <div class="kp-history-item-date"><span>{{\App\Models\Order::$months[$period['month']].', '.$period['year']}}</span></div>
                                    {{--{{dd($logs)}}--}}
                                    @forelse($logs as $key=>$log)
                                        {{--{{dd($period['year'])}}--}}
                                        @if($period['year'] == $log->_year && $period['month'] == $log->_month)
                                            <div class="kp-history-item-message"><pre>{!!$log->note!!}</pre></div><br>
                                        @endif
                                    @empty
                                    @endforelse
                                </div>
                            @empty
                            @endforelse
                        </div>

                        <div class="kp-yamap-position" id="side-ya-map">
                            <div style="top:50px" class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>
                        {{--<div class="kp-yamap-position" id="side-ya-map">
                            <div class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>
@stop
