<div class="dash-table" data-item-check="wrap">
    @can('add', new App\Models\Order())
        <div class="ibox-tools" style="float: right;">
            <a href="{{route('admin.orders.create').'/?company_id='.$company->id}}" class="btn btn-success btn-xs">
                <i class="fa fa-plus"></i>
                Добавить заказ
            </a>
        </div>
    @endcan

    @if( !(sizeof($company_orders) > 0) )
        <p><i>Заказов пока нет...</i></p>
    @else
        <table>
                <colgroup>
                    {{--<col class="tbl-check">--}}
                    <col class="tbl-number">
                    <col class="tbl-title">
                    <col class="tbl-contact">
                    <col class="tbl-budget">
                    <col class="tbl-tasks">
                    <col class="tbl-typework">
                    <col class="tbl-typepay">
                    <col class="tbl-datestart">
                    <col class="tbl-step">
                    <col class="tbl-pay">
                    <col class="tbl-responsible">
                    <col class="tbl-datecreate">
                </colgroup>
                <thead>
                <tr>
                    {{--<td>
                        <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                    </td>--}}
                    <td>№</td>
                    <td>Название</td>
                    <td>Контакт</td>
                    <td><a class="tbl-heads-link" href="#">Бюджет, руб</a></td>
                    <td>Задачи</td>
                    <td>Вид работ</td>
                    <td>Вид оплат</td>
                    <td><a class="tbl-heads-link type-up" href="#">Дата начала</a></td>
                    <td>Этап заказа</td>
                    <td>Оплата</td>
                    <td>Ответственный</td>
                    <td><a class="tbl-heads-link" href="#">Дата создания</a></td>
                </tr>
                </thead>
                <tbody>
                @foreach($company_orders as $order)
                    <tr>
                        {{--<td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                        </td>--}}
                        <td>{{$order->number}}</td>
                        <td>
                            <a href="{{route('admin.orders.edit', $order->id)}}">{{$order->name}}</a>
                        </td>
                        <td>
                            {{@$order->contacts()->first()->name}}
                            @if(isset($order->orderCompany))
                                <span class="tbl-block-company">“{{$order->orderCompany->name}}”</span>
                            @endif
                        </td>
                        <td>{{number_format($order->amount, 0, ',', ' ')}}</td>
                        <td>Подготовить коммерческое предложение</td>
                        <td>
                            @if( isset($order->calculator) )
                                {{--{{dd($order->calculator)}}--}}
                                {{--{{dump(get_object_vars(\GuzzleHttp\json_decode($order->calculator))['typ'])}}--}}
                                @if(get_object_vars(\GuzzleHttp\json_decode($order->calculator))['typ'] == 82)
                                    ген. уборка
                                @else
                                    послестрой
                                @endif
                            @endif
                        </td>
                        <td>{{--Безналичный--}}</td>
                        <td>{{date('d.m.Y', strtotime($order->date))}}</td>
                        <td>
                                   <span class="pseudo-select" data-pseudo="wrap">
                                       @php
                                           $bg_status_class = '';
                                           switch ($order->status) {
                                               case 'first':
                                                   $bg_status_class = 'bgs-first';
                                                   break;
                                               case 'agree':
                                                   $bg_status_class = 'bgs-agree';
                                                   break;
                                               case 'agreed':
                                                   $bg_status_class = 'bgs-agreed';
                                                   break;
                                               case 'confirmed':
                                                   $bg_status_class = 'bgs-confirmed';
                                                   break;
                                               case 'work':
                                                   $bg_status_class = 'bgs-work';
                                                   break;
                                               case 'performed':
                                                   $bg_status_class = 'bgs-performed';
                                                   break;
                                               case 'final_positive':
                                                   $bg_status_class = 'bgs-final_positive';
                                                   break;
                                               case 'final_negative':
                                                   $bg_status_class = 'bgs-final_negative';
                                                   break;
                                           }
                                       @endphp
                                       <span class="pseudo-select-txt o-btn {{$bg_status_class}}" data-pseudo="title" tabindex="0">{{$order->statusName()}}</span>
                                       <span class="pseudo-select-drop" data-pseudo="drop">
                                            <span data-order_id="{{$order->id}}" data-value="first" data-class="bgs-first" @if($order->status == 'first') class="current" @endif>Первичный контакт</span>
                                            <span data-order_id="{{$order->id}}" data-value="agree" data-class="bgs-agree" @if($order->status == 'agree') class="current" @endif>Согласовать</span>
                                            <span data-order_id="{{$order->id}}" data-value="agreed" data-class="bgs-agreed" @if($order->status == 'agreed') class="current" @endif>Согласованно</span>
                                            <span data-order_id="{{$order->id}}" data-value="confirmed" data-class="bgs-confirmed" @if($order->status == 'confirmed') class="current" @endif>Подтверждено</span>
                                            <span data-order_id="{{$order->id}}" data-value="work" data-class="bgs-work" @if($order->status == 'work') class="current" @endif>В работе</span>
                                            <span data-order_id="{{$order->id}}" data-value="performed" data-class="bgs-performed" @if($order->status == 'performed') class="current" @endif>Работы выполнены</span>
                                            <span data-order_id="{{$order->id}}" data-value="final_positive" data-class="bgs-final_positive" @if($order->status == 'final_positive') class="current" @endif>Успешно реализовано</span>
                                            <span data-order_id="{{$order->id}}" data-value="final_negative" data-class="bgs-final_negative" @if($order->status == 'final_negative') class="current" @endif>Не реализовано</span>
                                       </span>
                                   </span>
                        </td>
                        <td>
                                   <span class="pseudo-select" data-pseudo="wrap">
                                      <span class="pseudo-select-txt" data-pseudo="title" tabindex="0">{{\App\Models\Order::$payStatuses[$order->pay_status]}}</span>
                                      <span class="pseudo-select-drop" data-pseudo="drop">
                                         <span data-order_id="{{$order->id}}" data-value="payed" @if($order->pay_status == 'payed') class="current" @endif>Оплачен</span>
                                         <span data-order_id="{{$order->id}}" data-value="nopayed" @if($order->pay_status == 'nopayed') class="current" @endif>Не оплачен</span>
                                         <span data-order_id="{{$order->id}}" data-value="partpay" @if($order->pay_status == 'partpay') class="current" @endif>Частично оплачен</span>
                                      </span>
                                   </span>
                        </td>
                        <td>
                            @if($order->responsible_users()->count() > 0 )
                                {{$order->responsible_users()->first()->fio()}}
                            @endif
                        </td>
                        <td>{{date('d.m.Y H:i', strtotime($order->created_at))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    @endif

</div>

