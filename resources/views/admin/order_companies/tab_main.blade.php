<div class="kp-order-form-wrap">

    <div class="p-company-heads clearfix">
        <div class="p-company-heads-fields">
            <div class="kp-order-box-toggle-fields type-company" data-addfieldbox-tgl-show="wrap">

                {{--<div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">Название:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input type="text" name="name" placeholder="..." value="{{ old('name', @$company->name) }}">
                    </div>
                </div>--}}
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">Сайт:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input update-ajax type="text" placeholder="..." data-op-input="get" data-name="www" name="site" value="{{ old('site', @$company->site) }}">
                    </div>
                </div>
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">Телефон:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input update-ajax type="text" placeholder="..." data-op-input="get" data-jsmask="tel" name="phone" value="{{ old('phone', @$company->phone) }}">
                    </div>
                </div>
                <div class="kp-order-box-toggle-fields-row">
                    <div class="op-input-label">E-mail:</div>
                    <div class="op-input type-btns" data-op-input="item">
                        <input update-ajax type="text" placeholder="..." data-op-input="get" name="email" value="{{ old('email', @$company->email) }}">
                    </div>
                </div>

                <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Ген.директор</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input update-ajax type="text" {{--id="form-field-13"--}} name="director" placeholder="..." value="{{ old('director', @$company->director) }}" {{--class="col-sm-12"--}}>
                        </div>
                    </div>
                </div>

                <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Ген.бухгалтер</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input update-ajax type="text" {{--id="form-field-14"--}} name="buhgalter" placeholder="..." value="{{ old('buhgalter', @$company->buhgalter) }}" {{--class="col-sm-12"--}}>
                        </div>
                    </div>
                </div>

                <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Сокращенное наименование компании</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input update-ajax type="text" id="form-field-7" name="short_company_name" placeholder="..." value="{{ old('short_company_name', @$company->short_company_name) }}" class="col-sm-12">
                        </div>
                    </div>
                </div>

                <div class="kp-order-box-toggle-fields-tgl" data-addfieldbox-tgl-show="field" style="display: none;">
                    <div class="kp-order-box-toggle-fields-row">
                        <div class="op-input-label">Полное наименование компании</div>
                        <div class="op-input type-btns" data-op-input="item">
                            <input update-ajax type="text" id="form-field-8" name="full_company_name" placeholder="" value="{{ old('full_company_name', @$company->full_company_name) }}" class="col-sm-12">
                        </div>
                    </div>
                </div>

                <div class="kp-order-box-toggle-fields-adds">
                    <div class="adds-down-btn" data-addfieldbox-tgl-show="btn">
                        <span class="txt-label">Еще</span>
                        <div class="adds-down-btn-icon">
                            <span class="icon-input-ctrl-down"></span>
                            <span class="icon-input-ctrl-down-h btn-hover"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="p-company-heads-right">

            <div class="p-company-heads-logo-block type-uploads" data-contact-block-logo="wrap">
                <div class="p-company-heads-nologo">
                    <label class="p-company-heads-logo-add">
                        <input type="file" data-contact-block-logo="upload">
                    </label>
                </div>
                <span class="p-company-heads-logo" data-contact-block-logo="image"><img src="/img/page-company-logo-lizing.png" alt=""></span>
                <div class="p-company-heads-logo-control">
                    <div class="op-input-control-btn" data-contact-block-logo="remove"><span class="icon-input-ctrl-clear"></span><span class="icon-input-ctrl-clear-h btn-hover"></span></div>
                </div>
            </div>
        </div>--}}
        <div class="p-company-heads-right">

            <div class="p-company-heads-logo-block type-uploads" data-contact-block-logo="wrap">
                <div class="p-company-heads-nologo">
                    <label class="p-company-heads-logo-add">
                        <input name="logo" type="file" value="{{ old('logo') }}" data-contact-block-logo="upload" accept="image/*" @if( Route::current()->getName() == "admin.order_companies.edit" ) upload-ajax @endif>
                    </label>
                </div>


                <span class="p-company-heads-logo" data-contact-block-logo="image">
                    @if( empty($company->logo) )
                        <img src="/img/ava_company.png" alt="">
                    @else
                        <img src="/assets/imgs/order_companies/{{$company->logo}}">
                    @endif
                </span>

                <div class="p-company-heads-logo-control">
                    <div class="op-input-control-btn" data-contact-block-logo="remove"><span class="icon-input-ctrl-clear"></span><span class="icon-input-ctrl-clear-h btn-hover"></span></div>
                </div>
            </div>
        </div>
    </div>


    {{--<div class="kp-order-form-row clearfix type-address">
        <div class="kp-order-form-labinput">
            <div class="op-input-label">Адрес уборки:</div>
            <div class="kp-order-form-labinput-box">
                <div class="op-input type-btns" data-op-input="item" data-openside>
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <input type="text" placeholder="..." data-op-input="get" data-openside-map="#side-ya-map" value="Московская область, Одинцовский район, город Одинцово, Ул. Комсомольская">
                </div>
            </div>
        </div>
    </div>--}}
    <div class="kp-order-form-row clearfix type-address">
        <div class="kp-order-form-labinput">
            <div class="op-input-label">Адрес уборки:</div>
            <div class="kp-order-form-labinput-box">
                <div class="op-input type-btns" data-op-input="item" data-openside>
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>
                    <input id="suggest" type="text" name="address" update-ajax placeholder="..." data-openside-map="#side-ya-map" value="{{ old('address', @$company->address) }}">
                    {{--<input type="text" id="suggest" class="input" placeholder="..." name="address" data-openside-map="#side-ya-map" value="{{ old('address', @$order->address) }}">--}}
                </div>
            </div>
        </div>
    </div>

    <div class="kp-order-form-row clearfix type-addressdetail">
        <div class="kp-order-form-justify">
            <div class="kp-order-form-justify-ins">
                <div class="kp-order-form-justify-item">
                    <div class="op-input-label">Строение:</div>
                    <div class="op-input">
                        <input update-ajax type="text" placeholder="..." name="building" value="{{old('building', @$company->building)}}">
                    </div>
                </div>
                <div class="kp-order-form-justify-item">
                    <div class="op-input-label">Корпус:</div>
                    <div class="op-input">
                        <input update-ajax type="text" placeholder="..." name="housing" value="{{old('housing', @$company->housing)}}">
                    </div>
                </div>
                <div class="kp-order-form-justify-item">
                    <div class="op-input-label">Офис:</div>
                    <div class="op-input">
                        <input update-ajax type="text" placeholder="..." name="office" value="{{ old('office', @$company->office) }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kp-order-form-row clearfix type-note">
        <div class="op-input-label">Примечание:</div>
        <div class="op-input">
            <textarea placeholder="..." data-autoheight="true" name="comments">{{ old('comments', @$company->comments) }}</textarea>
        </div>
    </div>

    <div class="kp-order-form-row clearfix type-person">
        <div class="kp-order-form-labinput">
            <div class="op-input-label">Ответственный:</div>
            <div class="kp-order-form-labinput-box" data-persons="wrap" id="kp-order-person-setitem">
                <div class="op-input type-btns" data-op-input="item">
                    {{--<div class="op-input-control-btn" data-op-input="add_person">
                        <span class="icon-input-ctrl-down"></span>
                        <span class="icon-input-ctrl-down-h btn-hover"></span>
                    </div>
                    <div class="op-input-control-btn" data-op-input="search">
                        <span class="icon-input-ctrl-search"></span>
                        <span class="icon-input-ctrl-search-h btn-hover"></span>
                    </div>--}}
                    {{--<input type="text" placeholder="..." value="" data-op-input="get" data-user_block="Management" id="js-kp-order-person-search">--}}
                    <input type="text" placeholder="..." value="" data-user_block="Management" data-op-input="person-search">
                </div>

                @if( Route::current()->getName() == "admin.order_companies.edit" )
                    <div class="kp-order-form-person-item" data-persons="item">
                        <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$company->user->img}})"></div>
                        <div class="kp-order-form-person-item-name">{{@$company->user->fio()}}</div>
                        {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{$company->user->id}}"></div>--}}
                    </div>
                @elseif( Route::current()->getName() == "admin.order_companies.create" )
                    <div class="kp-order-form-person-item" data-persons="item">
                        <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.Auth::user()->img}})"></div>
                        <div class="kp-order-form-person-item-name">{{Auth::user()->fio()}}</div>
                        {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{Auth::user()->id}}"></div>--}}
                    </div>
                @endif

            </div>
        </div>
    </div>

    <script type="text/template" id="kp-order-person-additem">
        <div class="kp-order-form-person-item" data-persons="item">
            <div class="kp-order-form-person-item-ava" style="background-image:url([avatar])"></div>
            <div class="kp-order-form-person-item-name">[name]</div>
            {{--<div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="[user_id]"></div>--}}
        </div>
    </script>

    {{--<div class="kp-order-form-row clearfix type-person">
        <div class="kp-order-form-labinput">
            <div class="op-input-label">Ответственный:</div>
            <select id="user_id" class="chosen-select chosen-autocomplite-base col-sm-8 chosen-select chosen-autocomplite-base" data-url="{{route('admin.users.search')}}" data-placeholder="Начните ввод..." data-width="100%" name="f[user_id]">
                <option value="{{$company->user->id or ''}}">{{$company->user->fio() or ' '}}</option>
            </select>
            <div class="kp-order-form-person-item" data-persons="item">
                <div class="kp-order-form-person-item-ava" style="background-image:url({{'/'.\App\User::$img_path_preview.$company->user->img}})"></div>
                <div class="kp-order-form-person-item-name" style="max-width: none;">{{$company->user->fio()}}</div>
                <div class="kp-order-form-person-item-remove" data-persons="remove" data-user_id="{{$company->user->id}}"></div>
            </div>
        </div>
    </div>--}}

</div>