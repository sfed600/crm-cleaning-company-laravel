@extends('admin.layout')
@section('main')
    {{--for ymap--}}
    <script src="/assets/admin_new/js/jquery.min.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=SuggestView&onload=onLoad"></script>
    <script src="/assets/admin_new/js/ymaps.js?45"></script>
    <script>
        function onLoad (ymaps) {
            var suggestView = new ymaps.SuggestView('suggest');
        }
    </script>

    <div class="dash-container">

        @include('admin.message')

        <div class="dash-content">
            <div id="dash-kp">
                {{--<ul class="dash-kp-nav">
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-main" @if(Request::get('tab') != 'staff') class="current" @endif>Основное</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-contact">Контакты</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-orders">Заказы</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-offers">Предложения</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-invoices">Счета</a></li>
                    <li><a href="#">Договора</a></li>
                    <li><a href="#">Письма</a></li>
                    <li><a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-requisites">Реквизиты</a></li>
                    <li><a href="#">Статистика</a></li>
                </ul>--}}

                <div class="dash-kp-content">
                    <div class="dash-kp-col col-left">
                        <form class="form-horizontal" role="form" action="{{route('admin.order_companies.store')}}" method="POST" enctype="multipart/form-data">
                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="name" value="{{old('name')}}">

                            @if(Request::has('route'))
                                <input name="route" type="hidden" value="{{Request::input('route')}}">
                            @endif

                            @if(Request::has('order_name'))
                                <input name="order_name" type="hidden" value="{{Request::input('order_name')}}">
                            @endif

                            @if(Request::has('contact_name'))
                                <input name="contact_name" type="hidden" value="{{Request::input('contact_name')}}">
                            @endif

                            @if(Request::has('amount'))
                                <input name="amount" type="hidden" value="{{Request::input('amount')}}">
                            @endif

                            <div class="o-tabs-box kp-tabbox-main @if(Request::get('tab') != 'staff') current @endif" data-tab-box>
                                @include('admin.order_companies.tab_main')
                            </div>

                            <div class="page-kp-form-bottom-button">
                                {{--<span class="page-kp-form-button bgs-green">Сохранить</span>--}}
                                <button class="page-kp-form-button bgs-green" type="submit">Сохранить</button>
                                <span class="page-kp-form-button bgs-orange">Обновить</span>
                            </div>

                        </form>
                    </div>

                    <div class="dash-kp-col col-right">
                        <div class="kp-yamap-position" id="side-ya-map">
                            <div style="top:50px" class="kp-yamap-position-btn-close"><span class="icon-btn-box-close"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
