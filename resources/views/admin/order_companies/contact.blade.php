<style>
    .dash-table>table>thead td {
        font-weight: 600;
        font-size: 12px;
        white-space: nowrap;
    }
</style>


<div class="dash-table" data-item-check="wrap">
    <table>
        <colgroup>
            {{--<col class="tbl-check">--}}
            <col class="tbl-number">
            <col class="tbl-title">
            <col class="tbl-contact">
            {{--<col class="tbl-budget">
            <col class="tbl-tasks">--}}
            <col class="tbl-typepay">
            <col class="tbl-datecreate">
        </colgroup>
        <thead>
        <tr>
            {{--<td>
                <label class="o-input-box-replaced">
                    <input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span>
                </label>
            </td>--}}
            <td>
                {{--<a class="tbl-heads-link" href="#">ФИО</a>--}}
                ФИО
            </td>
            <td>
                {{--<a class="tbl-heads-link" href="#">Должность</a>--}}
                Должность
            </td>
            {{--<td><a class="tbl-heads-link" href="#">Компания</a></td>--}}
            <td>Телефон</td>
            {{--<td>
                <a class="tbl-heads-link" href="#">Сумма</a>
            </td>
            <td><a class="tbl-heads-link" href="#">Баланс</a></td>--}}
            <td>
                {{--<a class="tbl-heads-link" href="#">Email</a>--}}
                Email
            </td>
            <td>
                {{--<a class="tbl-heads-link" href="#">Дата создания</a>--}}
                Дата создания
            </td>
            <td>
                {{--<a class="tbl-heads-link" href="#">Ответственный</a>--}}
                Ответственный
            </td>
        </tr>
        </thead>
        <tbody>
        @foreach($company_contacts as $item)
            <tr>
                {{--<td>
                    <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                </td>--}}
                <td>
                    <a href="{{route('admin.order_contacts.edit', $item->id)}}">{{$item->name}}</a>
                </td>
                <td>
                    {{$item->post}}
                </td>
                <td>
                    @foreach($item->phones as $phone)
                        {{$phone->phone}}<br>
                    @endforeach
                </td>
                {{--<td>{{number_format($item->orders->sum('amount'), 0, ',', ' ')}}</td>
                <td>

                </td>--}}
                <td>
                    @foreach($item->emails as $email)
                        {{$email->email}}<br>
                    @endforeach
                </td>
                <td>
                    {{date('d.m.Y h:i', strtotime($item->created_at))}}
                </td>
                <td>
                    <a href="{{route('admin.users.edit', $item->user_id)}}">{{$item->user->fio()}}</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>