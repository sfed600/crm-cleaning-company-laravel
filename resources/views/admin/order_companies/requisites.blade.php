{{--<div class="form-group">
    <div class="col-sm-12 form-heading">Реквизиты компании</div>
</div>--}}

<div class="col-sm-2">
    <button class="btn btn-xs" type="button" onclick="fill_by_inn();" style="float: right;">
        Заполнить по ИНН
    </button>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">ИНН</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="inn" placeholder="..." value="{{ old('inn', $company->inn) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">ОГРН</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="ogrn" placeholder="..." value="{{ old('ogrn', $company->ogrn) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">КПП</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="kpp" placeholder="..." value="{{ old('kpp', $company->kpp) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">Дата регистрации</div>
    <div class="kp-order-form-date-input">
        <div class="op-input type-icon">
            <input update-ajax type="text" placeholder="..." name="date_register" value="{{ old('date_register', $company->datePicker()) }}" data-jsmask="date" class="init-mask" im-insert="true">
            <div class="op-input-icon-date"></div>
        </div>
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">ОКПО</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="okpo" placeholder="..." value="{{ old('okpo', $company->okpo) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">ОКТМО</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="oktmo" placeholder="..." value="{{ old('oktmo', $company->oktmo) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">Наименование банка</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="bank_name" placeholder="..." value="{{ old('bank_name', $company->bank_name) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">БИК</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="bik" placeholder="..." value="{{ old('bik', $company->bik) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">Расчетный счет</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="checking_account" placeholder="..." value="{{ old('checking_account', $company->checking_account) }}">
    </div>
</div>

<div class="kp-order-box-toggle-fields-row">
    <div class="op-input-label">Кор. счет</div>
    <div class="op-input type-btns" data-op-input="item">
        <input update-ajax type="text" name="cor_account" placeholder="..." value="{{ old('cor_account', $company->cor_account) }}">
    </div>
</div>