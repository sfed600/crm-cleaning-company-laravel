@if(Route::current()->getName() == 'admin.order_companies.index')
    <div class="dash-panel-block type-btns">
        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
            <div class="dash-panel-dropdown-drop">
                <a href="#">Ссылка 1</a>
                <a href="#">Ссылка 2</a>
                <a href="#">Ссылка 3</a>
            </div>
        </div>

        <a class="o-btn bgs-green o-hvr" href="{{route('admin.order_companies.create')}}">+ Добавить компанию</a>
    </div>
    <div class="dash-panel-block type-transactions">
        <span class="txt-label">{{@$count_companies}} компаний:</span>
        <span class="txt-value">{{number_format(@$sum_paginate, 0, ',', ' ')}} руб</span>
    </div>
@elseif(Route::current()->getName() == 'admin.order_companies.edit')
    <div class="dash-panel-block type-btns">
        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
            <div class="dash-panel-dropdown-drop">
                <a href="#">Ссылка 1</a>
                <a href="#">Ссылка 2</a>
                <a href="#">Ссылка 3</a>
            </div>
        </div>

        <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
            <div class="o-tabs-box kp-tabbox-main current" data-tab-box>
                <div class="o-btn bgs-green o-hvr" data-dropped="btn" data-layer="yes" onclick="window.location.href = '{{route('admin.order_companies.create')}}';">+ Добавить компанию</div>
                <div class="dash-panel-dropdown-drop">
                    <a href="#">Ссылка 1</a>
                    <a href="#">Ссылка 2</a>
                </div>
            </div>
            <div class="o-tabs-box kp-tabbox-contacts" data-tab-box>
                <div class="o-btn bgs-green o-hvr" data-dropped="btn" data-layer="yes">+ Добавить контакт</div>
                <div class="dash-panel-dropdown-drop">
                    <a href="#">Ссылка 1</a>
                    <a href="#">Ссылка 2</a>
                </div>
            </div>
        </div>
    </div>
    <div class="dash-panel-block type-transactions">
        <span class="txt-label">Сумма:</span>
           <span class="txt-value">
              <span class="op-input"><input type="text" placeholder="..." value=""></span>
              руб.</span>
    </div>
@endif