@extends('admin.layout')
@section('main')
    <div class="dash-container">
        <div class="dash-kp-line type-fleft">
            <ul class="dash-kp-line-list" data-kp-step="wrap">
                <li>
                    <a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-calendar_plan">
                        <span class="dash-kp-line-btn @if($filters['tab'] == 'plan') current @endif" data-kp-step="btn">
                            <span class="btn-bgs bgs-gentlyolive"></span>
                            <span class="btn-bgs bgs-hover bgs-gentlyolive"></span>
                            <span class="txt-label">Специальные работы ПЛАН</span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#" data-tab="dash-kp" data-findtab="kp-tabbox-calendar_fact">
                        <span class="dash-kp-line-btn @if($filters['tab'] == 'fact') current @endif" data-kp-step="btn">
                            <span class="btn-bgs bgs-pastelgreen"></span>
                            <span class="btn-bgs bgs-hover bgs-pastelgreen"></span>
                            <span class="txt-label">Специальные работы ФАКТ</span>
                        </span>
                    </a>
                </li>
                <li><span class="dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-bluesteel"></span><span class="btn-bgs bgs-hover bgs-bluesteel"></span><span class="txt-label">Еженедельная квартиры</span></span></li>
                <li><span class="dash-kp-line-btn" data-kp-step="btn"><span class="btn-bgs bgs-heliotrope"></span><span class="btn-bgs bgs-hover bgs-heliotrope"></span><span class="txt-label">Еженедельная офисы</span></span></li>
            </ul>
        </div>

        <div class="dash-content">
            <div id="dash-kp">
                <div class="dash-kp-nav">
                    <div class="page-calendar-nav">
                        @php
                            setlocale(LC_ALL, "ru_RU.UTF-8");
                        @endphp

                        @if(isset($today))
                            <a class="button-page-calendar-prev" href="{{route('admin.calendar', ['f[date]' => $today->copy()->subMonth()->format('m.Y')])}}"><span></span></a>
                            {{--<span class="page-calendar-nav-label">{{$today->format('F Y')}}</span>--}}
                            <span class="page-calendar-nav-label">{{strftime("%B %Y", strtotime($today->format('d.m.Y')))}}</span>
                            <a class="button-page-calendar-next" href="{{route('admin.calendar', ['f[date]' => $today->copy()->addMonth()->format('m.Y')])}}"><span></span></a>
                        @elseif(isset($filters['daterange']))
                            <span class="page-calendar-nav-label">{{$filters['period_start']}} - {{$filters['period_end']}}</span>
                        @endif
                    </div>
                </div>

                <style>
                    thead .page-calendar-item{
                        width: 100%;
                        border-bottom:0px;
                        height: auto;
                    }

                    tr .page-calendar-item{
                        width: 100%;
                        /*border-bottom:solid 1px;*/
                        /*height: auto;*/
                    }

                    .page-calendar-items table td{width: 14%;}
                </style>

                <div class="dash-calendar-content">
                    <div class="dash-calendar-col col-left">
                        <div class="page-calendar o-tabs-box kp-tabbox-calendar_plan @if($filters['tab'] == 'plan' ) current @endif" data-tab-box>
                            <div class="page-calendar-items">

                                <table style="width: 100%;">
                                    <thead>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Пн</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Вт</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Ср</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Чт</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Пт</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname">Сб</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    <td>
                                        <div class="page-calendar-item" data-calendar-day>
                                            <div class="page-calendar-dayname" style="position: relative; right: 35px;">Вс</div>
                                            {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                        </div>
                                    </td>
                                    </thead>

                                        {{--<tr><td>{{dump($orders)}}</td></tr>--}}

                                    {{--1-я неделя--}}
                                    <tr>
                                        @if(array_keys($weeks[1])[0] == 'Sun')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @elseif(array_keys($weeks[1])[0] == 'Sat')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @elseif(array_keys($weeks[1])[0] == 'Fri')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @elseif(array_keys($weeks[1])[0] == 'Thu')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @elseif(array_keys($weeks[1])[0] == 'Wed')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @elseif(array_keys($weeks[1])[0] == 'Tue')
                                            <td><div class="page-calendar-item" data-calendar-day></div></td>
                                        @endif

                                        @foreach($weeks[1] as $key=>$day)
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-day"><span class="page-calendar-day-border"><span>{{$day->day}}</span></span></div>
                                                    <div class="page-calendar-list-block">
                                                        <ul class="page-calendar-list">
                                                            @php
                                                                foreach($periods as $item) {
                                                                    $arPeriod = explode('-', $item['period']);
                                                                    if( isset($arPeriod[0]) && isset($arPeriod[1]) ) {
                                                                        if( date('d.m.Y', strtotime($arPeriod[0])) <= $day->format('d.m.Y') && $day->format('d.m.Y') <= date('d.m.Y', strtotime($arPeriod[1])) ) {
                                                                            foreach($orders->where('id', $item['order_id']) as $order) {
                                                                                echo '<li>
                                                                                    <a href="'.route('admin.orders.edit', $order->id).'">
                                                                                        <span style="color:'.@\App\Models\Order::$statuses[$order->status]['color'].'" class="calendar-clr-blue">№'.$order->number.' ('. \App\Models\Calendar::count_order_days($order).')</span>
                                                                                    </a>
                                                                                </li>';
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                        </ul>
                                                    </div>
                                                    <div class="page-calendar-drop" data-calendar-drop> {!! $details[1][$key] !!}</div>
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                    {{--1-я неделя--}}

                                    @for ($i = 2; $i <= sizeof($weeks); $i++)
                                        {{--<tr>{{dump($weeks[$i])}}</tr>--}}
                                        <tr>
                                            @foreach($weeks[$i] as $key=>$day)
                                                <td>
                                                    <div class="page-calendar-item" data-calendar-day>
                                                        <div class="page-calendar-day"><span class="page-calendar-day-border"><span>{{$day->day}}</span></span></div>
                                                        <div class="page-calendar-list-block">
                                                            <ul class="page-calendar-list">
                                                                @php
                                                                    foreach($periods as $item) {
                                                                        $arPeriod = explode('-', $item['period']);
                                                                        if( isset($arPeriod[0]) && isset($arPeriod[1]) ) {
                                                                            if( date('d.m.Y', strtotime($arPeriod[0])) <= $day->format('d.m.Y') && $day->format('d.m.Y') <= date('d.m.Y', strtotime($arPeriod[1])) ) {
                                                                                foreach($orders->where('id', $item['order_id']) as $order) {
                                                                                    echo '<li>
                                                                                        <a href="'.route('admin.orders.edit', $order->id).'">
                                                                                            <span style="color:'.@\App\Models\Order::$statuses[$order->status]['color'].'" class="calendar-clr-blue">№'.$order->number.' ('. \App\Models\Calendar::count_order_days($order).')</span>
                                                                                        </a>
                                                                                    </li>';
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                @endphp
                                                            </ul>
                                                        </div>
                                                        <div class="page-calendar-drop" data-calendar-drop> {!! $details[$i][$key] !!}</div>
                                                    </div>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endfor

                                </table>

                            </div>
                            <a class="tbl-report-navs left" href="#"></a>
                            <a class="tbl-report-navs right" href="#"></a>
                            <div class="page-calendar-nosearch" data-calendar-day-nosearch><div class="calendar-day-entry">Нечего нет в этот день.</div></div>
                        </div>

                        <div class="page-calendar o-tabs-box kp-tabbox-calendar_fact @if(Request::get('tab') == 'fact' || $filters['tab'] == 'fact') current @endif" data-tab-box>
                            {{--@include('admin.order_contacts.orders')--}}
                            <div class="page-calendar">
                                <div class="page-calendar-items">

                                    <table style="width: 100%;">
                                        <thead>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Пн</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Вт</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Ср</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Чт</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Пт</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname">Сб</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="page-calendar-item" data-calendar-day>
                                                    <div class="page-calendar-dayname" style="position: relative; right: 35px;">Вс</div>
                                                    {{--<div class="page-calendar-day"><span class="page-calendar-day-border"><span>30</span></span></div>--}}
                                                </div>
                                            </td>
                                        </thead>

                                        {{--<tr><td>{{dump($turns)}}</td></tr>--}}

                                        {{--1-я неделя--}}
                                        <tr>
                                            @if(array_keys($weeks[1])[0] == 'Sun')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @elseif(array_keys($weeks[1])[0] == 'Sat')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @elseif(array_keys($weeks[1])[0] == 'Fri')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @elseif(array_keys($weeks[1])[0] == 'Thu')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @elseif(array_keys($weeks[1])[0] == 'Wed')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @elseif(array_keys($weeks[1])[0] == 'Tue')
                                                <td><div class="page-calendar-item" data-calendar-day></div></td>
                                            @endif

                                            @foreach($weeks[1] as $key=>$day)
                                                <td>
                                                    <div class="page-calendar-item" data-calendar-day>
                                                        <div class="page-calendar-day"><span class="page-calendar-day-border"><span>{{$day->day}}</span></span></div>
                                                        <div class="page-calendar-list-block">
                                                            <ul class="page-calendar-list">
                                                                @forelse($turns->where('date', $day->format('Y-m-d'))->whereIn('status', ['agreed', 'confirmed', 'work', 'performed', 'final_positive']) as $turn)
                                                                    <li>
                                                                        <a style="color:{{\App\Models\Order::$statuses[$turn->status]['color']}}" href="{{route('admin.orders.edit', $turn->order_id)}}">
                                                                            <span class="calendar-clr-blue">№{{$turn->number}} - день {{\App\Models\OrderDay::row_number($turn->id)}} ({{$turn->cnt}})</span>
                                                                        </a>
                                                                    </li>
                                                                @empty
                                                                @endforelse
                                                            </ul>
                                                        </div>
                                                        <div class="page-calendar-drop" data-calendar-drop> {!! $details[1][$key] !!}</div>
                                                    </div>
                                                </td>
                                            @endforeach
                                        </tr>
                                        {{--1-я неделя--}}

                                        @for ($i = 2; $i <= sizeof($weeks); $i++)
                                            {{--<tr>{{dump($weeks[$i])}}</tr>--}}
                                            <tr>
                                                @foreach($weeks[$i] as $key=>$day)
                                                    <td>
                                                        <div class="page-calendar-item" data-calendar-day>
                                                            <div class="page-calendar-day"><span class="page-calendar-day-border"><span>{{$day->day}}</span></span></div>
                                                            <div class="page-calendar-list-block">
                                                                <ul class="page-calendar-list">
                                                                    @forelse($turns->where('date', $day->format('Y-m-d')) as $turn)
                                                                        <li>
                                                                            <a style="color:{{\App\Models\Order::$statuses[$turn->status]['color']}}" href="{{route('admin.orders.edit', $turn->order_id)}}">
                                                                                <span class="calendar-clr-blue">№{{$turn->number}} - день {{\App\Models\OrderDay::row_number($turn->id)}} ({{$turn->cnt}})</span>
                                                                            </a>
                                                                        </li>
                                                                    @empty
                                                                    @endforelse
                                                                </ul>
                                                            </div>
                                                            <div class="page-calendar-drop" data-calendar-drop> {!! $details[$i][$key] !!}</div>
                                                        </div>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endfor

                                    </table>

                                </div>
                                <a class="tbl-report-navs left" href="#"></a>
                                <a class="tbl-report-navs right" href="#"></a>
                                <div class="page-calendar-nosearch" data-calendar-day-nosearch><div class="calendar-day-entry">Нечего нет в этот день.</div></div>
                            </div>
                        </div>
                    </div>

                    <div class="dash-calendar-col col-right">
                        <div class="dash-calendar-scroll">
                            <div class="calendar-day-item" id="js-calendar-day-dropinsert">
                                {{--<div class="calendar-day-item-date"><span>14 августа, 2018 - 14 уборок в этот день</span></div>
                                <div class="calendar-day-entry">
                                    <div class="calendar-day-entry-heads"><a href="#"><span class="calendar-clr-blue">Заказ №321 - день 1 из 5</span></a></div>
                                    <div class="calendar-day-entry-text">1) Уборка в загородном доме в Мелихино, 60м2, окна</div>
                                    <div class="calendar-day-entry-cur">День, 09:00 - Московская область (Мелихово)</div>
                                </div>
                                <div class="calendar-day-entry">
                                    <div class="calendar-day-entry-heads"><a href="#"><span class="calendar-clr-purple">Заказ №322 - день 1 из 2</span></a></div>
                                    <div class="calendar-day-entry-text">2) Уборка квартиры Аннино, пр. Загорского</div>
                                    <div class="calendar-day-entry-cur">День, 10:00 - Москва (Анино)</div>
                                </div>
                                <div class="calendar-day-entry">
                                    <div class="calendar-day-entry-heads"><a href="#"><span class="calendar-clr-green">Заказ №325 - день 1 из 1</span></a></div>
                                    <div class="calendar-day-entry-text">3) Уборка коттедж 200м2 август</div>
                                    <div class="calendar-day-entry-cur">Ночь, 18:00 - Московская область (Одинцово)</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dash-container-layer"></div>
    </div>
@stop