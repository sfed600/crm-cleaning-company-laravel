<form class="dash-search" data-dash-search="wrap">
    <input type="hidden" name="f[status]" @if(isset($filters['status'])) value="{{$filters['status']}}" @endif>
    <input type="hidden" name="f[tab]" @if(isset($filters['tab'])) value="{{$filters['tab']}}" @endif>
    <input type="hidden" name="f[date]" @if(isset($filters['date'])) value="{{$filters['date']}}" @endif>

    <div class="dash-search-option">
        {{--{{dd(session()->get('admin.orders.index.filters'))}}--}}
        {{--{{dump($filters)}}--}}
        @if( isset($filters["daterange"]) && !empty($filters["daterange"]) )
            <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                {{$filters["daterange"]}}
                <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'daterange', 'calendar');"></span>
            </span>
        @endif
        @if(isset($filters))
            @if (!empty($filters) && !empty($filters['user_id']))
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        {{\App\User::find($filters['user_id'])->fio()}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'user_id', 'calendar');"></span>
                </span>
            @endif
            @if( isset($filters["status"]) && !empty($filters["status"]) )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{\App\Models\Order::$statuses[$filters["status"]]['name']}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'status', 'calendar');"></span>
                </span>
            @endif

            @if( @$filters["sum"] > 0 )
                @if($filters["sum_compare"] == 'equal')
                    <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        {{$filters["sum"]}}
                        <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'calendar');"></span>
                    </span>
                @elseif($filters["sum_compare"] == 'more')
                    <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                            >{{$filters["sum"]}}
                        <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'calendar');"></span>
                    </span>
                @elseif($filters["sum_compare"] == 'less')
                    <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                        <{{$filters["sum"]}}
                        <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum', 'calendar');"></span>
                    </span>
                @endif
            @elseif( @$filters["sum_min"] > 0 && @$filters["sum_max"] > 0 )
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    {{$filters["sum_min"].' - '.$filters["sum_max"]}}
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'sum_compare', 'calendar');"></span>
                </span>
            @endif

            @if(@$filters['typ'] == 82)
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                ген. уборка
                <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'typ', 'calendar');"></span>
            </span>
            @elseif(@$filters['typ'] == 98)
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                послестрой
                <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'typ', 'calendar');"></span>
            </span>
            @endif

            @if(@$filters['payment'] == 10)
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Безналичный
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'payment', 'calendar');"></span>
                </span>
            {{--@elseif(@$filters['payment'] == '0')
                <span class="dash-search-option-item bgs-blue o-hvr" data-dash-search="option" data-layer="yes">
                    Наличный
                    <span class="dash-search-option-item-remove" data-dash-search="remove_option" onclick="exclude_from_filter(this, 'payment', 'calendar');"></span>
                </span>--}}
            @endif

        @endif
    </div>
    <div class="dash-search-form">
        <div class="dash-search-form-input">
            <input type="text" placeholder="Поиск заказов и уборок" data-dash-search="input" data-layer="yes"><div class="dash-search-form-hover"></div>
        </div>
        <label class="dash-search-form-btn type-submit">
            <span class="icon-input-ctrl-search"></span>
            <span class="icon-input-ctrl-search-h btn-hover"></span>
            <input type="submit" value=" ">
        </label>
        <label class="dash-search-form-btn type-remove">
            <span class="icon-input-ctrl-clear"></span>
            <span class="icon-input-ctrl-clear-h btn-hover"></span>
            <input type="reset" value=" ">
        </label>
    </div>
    <div class="dash-search-drop" data-dash-search="drop">
        <div class="dash-search-drop-ins">
            <div class="dash-search-drop-left">
                <ul class="dash-search-drop-list">
                    {{--<li>Новые</li>
                    <li>Мои сделки</li>
                    <li>Первичный контакт</li>
                    <li>Согласовано</li>
                    <li>В работе</li>
                    <li class="current">Работы выполнены</li>
                    <li>Не оплачено</li>
                    <li>Успешно реализовано</li>
                    <li>Не реализовано</li>--}}
                    <li onclick="set_orders_filter_status('new', this);" @if( @$filters['status'] == 'new') class="current" @endif>Новые</li>
                    <li onclick="set_orders_filter_status('my', this);" @if( @$filters['status'] == 'my') class="current" @endif>Мои сделки</li>
                    <li onclick="set_orders_filter_status('first', this);" @if( @$filters['status'] == 'first') class="current" @endif>Первичный контакт</li>
                    <li onclick="set_orders_filter_status('agreed', this);" @if( @$filters['status'] == 'agreed') class="current" @endif>Согласовано</li>
                    <li onclick="set_orders_filter_status('work', this);" @if( @$filters['status'] == 'work') class="current" @endif>В работе</li>
                    <li onclick="set_orders_filter_status('performed', this);" @if( @$filters['status'] == 'performed') class="current" @endif>Работы выполнены</li>
                    <li onclick="set_orders_filter_status('not_paid', this);" @if( @$filters['status'] == 'not_paid') class="current" @endif>Не оплачено</li>
                    <li onclick="set_orders_filter_status('final_positive', this);" @if( @$filters['status'] == 'final_positive') class="current" @endif>Успешно реализовано</li>
                    <li onclick="set_orders_filter_status('final_negative', this);" @if( @$filters['status'] == 'final_negative') class="current" @endif>Не реализовано</li>
                </ul>
            </div>
            <div class="dash-search-drop-right">
                <div class="dash-search-drop-form">
                    <div class="o-form-row">
                        <div class="o-form-row-label">Ответственный</div>
                        <div class="o-form-row-input">
                            <select id="user_id" class="chosen-select chosen-autocomplite-base col-sm-8 chosen-select chosen-autocomplite-base" data-url="{{route('admin.users.search')}}" data-placeholder="Начните ввод..." data-width="100%" name="f[user_id]">
                                <option value="{{$filters['user_id'] or ''}}">{{$filters['fio'] or ' '}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="o-form-row">
                        <div class="o-form-row-label">Сумма</div>
                        <div class="o-form-row-sum">
                            <div class="o-form-row-sum-col col-type">
                                <div class="o-form-row-input">
                                    <select class="selectpicker" data-width="100%" name="f[sum_compare]">
                                        <option value="equal" @if(!@$filters['sum_compare'] || @$filters['sum_compare'] == 'equal') selected @endif>Точно</option>
                                        <option value="interval" @if(@$filters['sum_compare'] == 'interval') selected @endif>Диапазон</option>
                                        <option value="more" @if(@$filters['sum_compare'] == 'more') selected @endif>Больше чем</option>
                                        <option value="less" @if(@$filters['sum_compare'] == 'less') selected @endif>Меньше чем</option>
                                    </select>
                                </div>
                            </div>
                            <div class="o-form-row-sum-col col-value">
                                <div class="o-form-row-input">
                                    {{--<input type="text" placeholder="См. реализацию в битриксе">--}}
                                    @if( @$filters['sum_compare'] != 'interval' )
                                        <input type="text" name="f[sum]" value="{{@$filters['sum']}}" />
                                    @else
                                        {{--<input type="text" name="f[sum_min]" value="{{@$filters['sum_min']}}" />
                                        <input name="f[sum_max]" type="text" value="{{@$filters['sum_max']}}" />--}}
                                        <div class="o-form-row-sum-col col-value">
                                            <div class="o-form-row-input" style="display: flex;">
                                                <input type="text" name="f[sum_min]" style="width: 48%;" value="{{@$filters['sum_min']}}" />
                                                <div class="interval-dash">&nbsp;-&nbsp;</div>
                                                <input name="f[sum_max]" type="text" style="width: 48%;" value="{{@$filters['sum_max']}}" />
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Вид работ</div>
                        <div class="o-form-row-input">
                            <select name="f[typ]" class="selectpicker" data-width="100%">
                                <option>&nbsp;</option>
                                <option value="82" @if(@$filters['typ'] == 82) selected @endif>Ген. уборка</option>
                                <option value="98" @if(@$filters['typ'] == 98) selected @endif>Послестрой</option>
                            </select>
                        </div>
                    </div>
                    <div class="o-form-row">
                        <div class="o-form-row-label">Вид оплат</div>
                        <div class="o-form-row-input">
                            <select name="f[payment]" class="selectpicker" data-width="100%">
                                <option>&nbsp;</option>
                                <option value="600" @if(@$filters['payment'] == 600) selected @endif>Наличный</option>
                                <option value="10" @if(@$filters['payment'] == 10) selected @endif>Безналичный</option>
                            </select>
                        </div>
                    </div>

                    <div class="o-form-row">
                        <div class="o-form-row-label">Выбор периода</div>
                        <div class="o-form-row-input">
                            <input type="text" class="datepicker" name="f[daterange]" value="{{@$filters['daterange']}}">
                        </div>
                    </div>

                    <div class="o-form-bottom">
                        <input type="submit" value="Найти">
                        <input type="reset" value="Сбросить" onclick="reset_filter('calendar')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>