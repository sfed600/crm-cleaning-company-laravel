<div class="dash-panel-block type-btns">
    <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
        <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
        <div class="dash-panel-dropdown-drop">
            <a href="#">Ссылка 1</a>
            <a href="#">Ссылка 2</a>
            <a href="#">Ссылка 3</a>
        </div>
    </div>

    <a class="o-btn bgs-green o-hvr" href="{{route('admin.orders.create')}}">+ Добавить заказ</a>
</div>
<div class="dash-panel-block type-transactions" style="text-align: center;">

    @if($filters['tab'] == 'fact')
        <span class="txt-value">{{sizeof($turns)}} уборок</span>
    @else
        <span class="txt-label">{{sizeof($orders)}} заказов:</span>
        <span class="txt-value">{{$cntDays}} уборок</span>
    @endif
</div>