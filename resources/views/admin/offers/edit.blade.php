@extends('admin.layout')
@section('main')
    {{--<script src="/assets/admin/js/calculator.js?45"></script>--}}

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.offers.index')}}">Предложения</a>
            </li>
            <li class="active">Редактирование предложения</li>
        </ul><!-- /.breadcrumb -->
    </div>

    {{--<div class="page-content order-edit" data-tab="{{$request->input('tab')}}">--}}
    <div class="page-content">

        <div class="page-header">
            <h1>
                Предложения
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Редактирование
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                {{--<li @if( !$request->input('tab') or $request->input('tab') == 'offer') class="active" @endif>--}}
                <li class="active">
                    <a data-toggle="tab" href="#offer" aria-expanded="true">
                        <i class="green ace-icon fa fa-home bigger-120"></i>
                        Предложение
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#products" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Товары / Услуги
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#calculator" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Калькулятор
                    </a>
                </li>

            </ul>

            <div class="tab-content">
                {{--<div id="offer" class="tab-pane @if( !$request->input('tab') or $request->input('tab') == 'offer') active @endif">--}}
                <div id="offer" class="tab-pane active">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <form id="edit-offer-form" class="form-horizontal" role="form" action="{{route('admin.offers.update', $offer->id)}}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1"> # Предложения </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="number" placeholder="# Заказа" value="{{ old('number', $offer->number) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-2"> Название </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-2" name="name" placeholder="Название" value="{{ old('name', $offer->name) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-222"> Площадь уборки</label>
                                    <div class="col-sm-2">
                                        <input type="text" id="form-field-222" name="area" value="{{ old('area', $offer->area) }}" class="col-sm-8">
                                        &nbsp;м.кв.
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-22"> Сумма </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-22" name="amount" placeholder="Сумма" value="{{ old('amount', $offer->amount) }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Дата начала</label>
                                    {{--<label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Время начала</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="date_start" value="{{old('date_start', $offer->datePicker())}}" id="form-field-16" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>

                                        {{--<div class="input-group" style="width: 40%">
                                            <input class="form-control time-picker" name="timestart" value="{{old('timestart', $offer->timestart)}}" type="text" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o bigger-110"></i>
                                            </span>
                                        </div>--}}
                                    </div>

                                    <label class="col-sm-1" for="form-field-116"> Дата окончания</label>
                                    {{--<label class="col-sm-1" for="form-field-116"> Время окончания</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="date_finish" value="{{old('date_finish', $offer->dateFinishPicker())}}" id="form-field-116" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>

                                        {{--<div class="input-group" style="width: 40%">
                                            <input class="form-control time-picker" name="timefinish" value="{{old('timefinish', $offer->timefinish)}}" type="text" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o bigger-110"></i>
                                            </span>
                                        </div>--}}
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-3"> Контакты</label>
                                    <div class="col-sm-9">
                                        <div class="dynamic-input">
                                        @if($offer->contacts->count())
                                            @foreach($offer->contacts as $key => $contact)
                                                <div class="input-group dynamic-input-contact" style="margin-bottom:5px;">
                                                    <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        <option value="{{$contact->id}}" selected>{{$contact->name}}</option>
                                                    </select>
                                                    <a href="" class="input-group-addon @if($key == 0)plus @else minus @endif">
                                                        <i class="glyphicon @if($key == 0)glyphicon-plus @else glyphicon-minus @endif bigger-110"></i>
                                                    </a>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                                <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                    <option value=""></option>
                                                </select>
                                                <a href="" class="input-group-addon plus">
                                                    <i class="glyphicon glyphicon-plus bigger-110"></i>
                                                </a>
                                            </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-10"> Компания </label>
                                    <div class="col-sm-9">
                                        <select name="order_company_id" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach($companies as $company)
                                                {{--<option value="{{$company->id}}" @if( intval($offer->order_company_id) == intval($company->id) ) selected="selected" @endif >{{$company->name}}</option>--}}
                                                {{--<option value="{{$company->id}}" @if( (old() && old('order_company_id')==$company->id) || (!old() && $offer->order_company_id) ) selected @endif>{{$company->name}}</option>--}}

                                                {{--@if( (old() && old('order_company_id')==$company->id) || (!old() && $offer->order_company_id) )
                                                    <option value="{{$company->id}}" selected >{{$company->name}}</option>
                                                @elseif( intval(@$offer->order_company_id) == intval($company->id) )
                                                    <option value="{{$company->id}}" selected="selected">{{$company->name}}</option>
                                                @endif--}}

                                                @if( isset($offer) )
                                                    @if( intval($offer->order_company_id) == intval($company->id) )
                                                        <option value="{{$company->id}}" selected="selected">{{$company->name}}</option>
                                                    @else
                                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$company->id}}" @if( (old() && old('order_company_id')==$company->id) || (!old() && $offer->order_company_id) ) selected @endif>{{$company->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-161"> Метро</label>
                                    <div class="col-sm-9">
                                        <select name="metro_id" class="chosen-select chosen-autocomplite form-control" id="form-field-161" data-url="{{route('admin.metro.search')}}" data-placeholder="Начните ввод...">
                                            <option value=""></option>
                                            @if(!empty($offer->metro_id))
                                                <option value="{{$offer->metro_id}}" selected>{{$offer->metro->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-162"> Адрес </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-162" name="address" placeholder="Адрес" value="{{ old('address', $offer->address) }}" class="col-sm-10">

                                        <label class="block col-sm-2"  title="Использовать в СМС">
                                            <input name="use_address" value="1" type="checkbox" class="ace input-lg" />
                                            <span class="lbl bigger-120"> </span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-apartment"> Квартира</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="apartment" value="{{old('apartment', $offer->apartment)}}" id="form-field-apartment" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-porch"> Подъезд</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="porch" value="{{old('porch', $offer->porch)}}" id="form-field-porch" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-floor"> Этаж</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="floor" value="{{old('floor', $offer->floor)}}" id="form-field-floor" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-domofon"> Код домофона</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="domofon" value="{{old('domofon', $offer->domofon)}}" id="form-field-domofon" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-note"> Примечание </label>
                                    {{--<div class="col-sm-10">
                                        <textarea name="note">{{old('note')}}</textarea>
                                    </div>--}}
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-note" name="note" placeholder="Примечание" value="{{old('note', $offer->note)}}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-8"> Статус </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Offer::$statuses as $key => $status)
                                                <option value="{{$key}}" @if((old() && old('status')==$key) || (!old() &&  $key==$offer->status)) selected @endif>{{$status['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-5"> Ответсвенный </label>
                                    <div class="col-sm-9">
                                        <select name="user_id" class="chosen-select chosen-autocomplite form-control" id="form-field-5" data-url="{{route('admin.users.search', ['block' => 'Management'])}}" data-placeholder="Начните ввод...">
                                           @if($offer->user_id)
                                           <option value="{{$offer->user_id}}" selected>{{$offer->user->fio()}}</option>
                                           @else
                                           <option value=""></option>
                                           @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-success" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Сохранить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-warning button-apply" type="button" data-action="{{route('admin.offers.update', ['id' => $offer->id, 'route' => 'back'])}}">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>
                                            Применить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="{{route('admin.offers.index')}}">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Назад
                                        </a>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn" href="{{route('admin.offers.pdf', ['id' => $offer->id])}}">
                                            <i class="ace-icon glyphicon glyphicon-download bigger-110"></i>
                                            Скачать PDF
                                        </a>
                                        {{--&nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="#">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Печать
                                        </a>--}}
                                    </div>
                                </div>

                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>

                <div id="products" class="tab-pane">
                    @include('admin.offers.products')
                </div>

                <div id="calculator" class="tab-pane">
                    @include('admin.offers.calculator')
                </div>

            </div>
        </div>

    </div>

@stop

