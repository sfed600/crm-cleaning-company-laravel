<style>
    .weekDays-selector input {
        display: none!important;
    }

    .weekDays-selector input[type=checkbox] + label {
        display: inline-block;
        border-radius: 6px;
        background: #dddddd;
        height: 40px;
        width: 30px;
        margin-right: 3px;
        line-height: 40px;
        text-align: center;
        cursor: pointer;
    }

    .weekDays-selector input[type=checkbox]:checked + label {
        background: #2AD705;
        color: #ffffff;
    }
</style>

<div class="row">
    <div class="col-xs-12">
        {{--{{dump(@$calculator)}}--}}
        <form id="calculator-offer-form">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">персонал</th>
                    <th scope="col">время с</th>
                    <th scope="col">время по</th>
                    <th scope="col">дни</th>
                    <th scope="col">кол-во</th>
                    <th scope="col">начислено</th>
                    <th scope="col">на руки</th>
                    <th scope="col">сумма</th>
                    <th scope="col"></th>
                </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>
                            <select class="form-control">
                                @foreach($professions as $profession)
                                    <option value="{{$profession->id}}">{{$profession->name}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <div class="input-group">
                                <input class="form-control time-picker" name="timestart" value="{{old('timestart', '09:00')}}" type="text" size="4" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <input class="form-control time-picker" name="timefinish" value="{{old('timefinish', '18:00')}}" type="text" size="4" />
                                <span class="input-group-addon">
                                    <i class="fa fa-clock-o bigger-110"></i>
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="weekDays-selector">
                                <input type="checkbox" id="weekday-mon" class="weekday" />
                                <label for="weekday-mon">M</label>
                                <input type="checkbox" id="weekday-tue" class="weekday" />
                                <label for="weekday-tue">T</label>
                                <input type="checkbox" id="weekday-wed" class="weekday" />
                                <label for="weekday-wed">W</label>
                                <input type="checkbox" id="weekday-thu" class="weekday" />
                                <label for="weekday-thu">T</label>
                                <input type="checkbox" id="weekday-fri" class="weekday" />
                                <label for="weekday-fri">F</label>
                                <input type="checkbox" id="weekday-sat" class="weekday" />
                                <label for="weekday-sat">S</label>
                                <input type="checkbox" id="weekday-sun" class="weekday" />
                                <label for="weekday-sun">S</label>
                            </div>
                        </td>
                        <td><input type="text" size="6"></td>
                        <td></td>
                        <td><input type="text" size="6"></td>
                        <td></td>
                        <td>
                            {{--<a href="javascript:void(0);" onclick="$(this).parents('tr').after('<tr>' + $(this).parents('tr').html() + '</tr>');">--}}
                            <a href="javascript:void(0);" onclick="add_row_offer_calculator(this);">
                                <i class="fa fa-plus" style="font-size: 22px;"></i>
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" colspan="7">Итого ФОТ:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="7">Налоги на ФОТ 30%:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="7">Итого ФОТ с налогами:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="8">
                            Во время отпуска человеку платятся отпускные, а на время его отсутствия
                            необходим человек для подмены, которому также нужно заплатить. Т.е. фактически
                            месяцев в году 12, а зарплат получается 13. Для получения среднемесячных затрат на з/п
                            умножаем ФОТ с налогами на 13 иделим на 12.
                        </td>
                    </tr>

                    <tr>
                        <td colspan="7" align="center">Среднемесячные затраты ФОТ с налогами:</td>
                        <td></td>
                    </tr>

                    <tr><td colspan="12">&nbsp;</td></tr>

                    <tr>
                        <td colspan="7">Затраты на химию, амортизацию инвенторя, курирующий менеджер,</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="3">Прибыль компании:</td>
                        <td><input type="text"></td>
                        <td colspan="3">&nbsp;</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td align="right" colspan="7">Итого:</td>
                        <td></td>
                    </tr>

                    <tr>
                        <td colspan="7">НДС 18%:</td>
                        <td></td>
                    </tr>

                    <tr><td colspan="12">&nbsp;</td></tr>

                    <tr>
                        <td colspan="7" align="center">Стоимость услуг, включая НДС:</td>
                        <td></td>
                    </tr>

                </tbody>
            </table>

        </form>
    </div>
</div>