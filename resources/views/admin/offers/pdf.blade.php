<!-- pdf.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8" />
    <meta charset="UTF-8">
    <title>{{$offer->name}}</title>

    {{--<link rel="stylesheet" href="/assets/admin/css/bootstrap.min.css" />
    <script src="/assets/admin/js/bootstrap.min.js"></script>--}}

    <style>
        .border-table {
            width: 100%;
            border-collapse: collapse;
        }

        .border-table, .border-table th, .border-table td {
            border: 1px solid black;
            /*text-align: center;*/
            color: #1d2122;
        }
    </style>
</head>
<body style="font-family: DejaVu Sans">
{{--{{dd(dump($company->getImgPreviewPath('stamp').$company->stamp))}}--}}

    <div style="float: left;">
        @if( strlen($company->logo) > 0)
            {{--<img style="width:350px;" src="http://{{$_SERVER['HTTP_HOST'].'/'.$company->getImgPath('logo').$company->logo}}">--}}
            <img style="width:350px;" src="http://test.cleancrm.ru/assets/imgs/companies/logo/1524066833_jCBia.png">
        @endif
    </div>
    <div style="float: right;">
        <b>
            {{mb_strtoupper($company->name)}}<br>
            {{--Address:<br>--}}
            Тел.:{{$company->phone}}
        </b>
    </div>

    <div style="clear: both"></div>
    <br>
    {{--{{dd($offer->date_start)}}--}}
    <h3>
        КОММЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ № {{$offer->number}} @if($offer->date_start) от {{date('d.m.Y', strtotime($offer->date_start))}} @endif
    </h3>

    @if($offer->products->count())
        <table class="border-table">
            <tr>
                {{--<td>№</td>--}}
                <td>Наименование товара</td>
                <td>Кол-во</td>
                <td>Ед.</td>
                <td>Цена, руб.</td>
                <td>Ставка НДС</td>
                <td>Сумма, руб.</td>
            </tr>
            @foreach($offer->products as $key => $product)
                <tr>
                    {{--<td>{{$product->id}}</td>--}}
                    <td>{{$product->name}}</td>
                    <td>1</td>
                    <td>{{$product->measureName()}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->ndsName()}}</td>
                    <td align="right">{{$product->price}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5" style="text-align: right; border: none;">В том числе НДС:</td>
                <td align="right">{{$sumNDS}}</td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right; border: none;">Итого:</td>
                <td align="right">{{$sumTotal}}</td>
            </tr>
        </table>
    @endif

    <p>
        Всего наименований {{$offer->products->count()}}, на сумму {{--{{$offer->amount}}--}} {{$sumTotal}} руб.<br>
        {{--<b>Двадцать две тысячи восемсот рублей 00 копеек</b>--}}
        <b>{{$offer->num2str($sumTotal)}}</b>
    </p>
    <br><br><br><br>

    <div style="float: left;">
        <b>{{mb_strtoupper($company->name)}}</b>
        <p>Тел:{{$company->phone}}</p>
    </div>
    <div style="float: right;">
        @if($order_company)
            <b>{{mb_strtoupper($order_company->name)}}</b>
            <p>
                Адрес:{{$order_company->address}}<br>
                Тел: {{$order_company->phone}}<br>
                Email: {{$order_company->email}}<br>
                ИНН: {{ ($order_company->inn > 0) ? $order_company->inn : '' }}
            </p>
        @endif

        @forelse($offer->contacts()->get() as $key=>$contact) {{--{{dd($contact)}}--}}
            Контактное лицо {{mb_strtoupper($contact->name)}}<br>
            Тел:
            @forelse($contact->phones as $key1=>$item)
                {{$item->phone}}<br>
            @empty
            @endforelse
            Email:
            @forelse($contact->emails as $key2=>$item)
                {{$item->email}}<br>
            @empty
            @endforelse
        @empty
        @endforelse
    </div>

</body>
</html>