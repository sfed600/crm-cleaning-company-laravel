@extends('admin.layout')
@section('main')
    <input name="_token" type="hidden" value="{{csrf_token()}}">

    <div class="dash-container">
        <div class="dash-content">
            <div class="dash-table" data-item-check="wrap">
                <table>
                    <colgroup>
                        <col class="tbl-check">
                        <col class="tbl-number">
                        <col class="tbl-title">
                        <col class="tbl-contact">
                        <col class="tbl-contact">
                        {{--<col class="tbl-budget">--}}
                        <col class="tbl-step">
                        {{--<col class="tbl-pay">--}}
                        <col class="tbl-responsible">
                        <col class="tbl-datecreate">
                        <col class="tbl-datecreate">
                    </colgroup>
                    <thead>
                    <tr>
                        <td>
                            <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="all-current"><span class="o-input-box-replaced-check"></span></label>
                        </td>
                        <td>№</td>
                        <td>Название</td>
                        <td>Контакт</td>
                        <td>Компания</td>
                        <td><a class="tbl-heads-link" href="#">Бюджет, руб</a></td>
                        <td>Статус</td>
                        {{--<td>Оплата</td>--}}
                        <td>Ответственный</td>
                        <td>Дата начала</td>
                        <td>Дата окончания</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($offers as $item)
                        <tr>
                            <td>
                                <label class="o-input-box-replaced"><input type="checkbox" name="items[]" tabindex="0" data-item-check="current"><span class="o-input-box-replaced-check"></span></label>
                            </td>
                            <td>{{$item->number}}</td>
                            <td>
                                <a href="{{route('admin.offers.edit', $item->id)}}">Предложение № {{$item->number}}</a>
                            </td>
                            <td>
                                {{@$item->contacts()->first()->name}}
                            </td>
                            <td>{{@$item->orderCompany->name}}</td>
                            <td>{{number_format($item->amount, 0, ',', ' ')}}</td>
                            <td>
                                   <span class="pseudo-select" data-pseudo="wrap">
                                       @php
                                           $bg_status_class = '';
                                           switch ($item->status) {
                                               case 'first':
                                                   $bg_status_class = 'bgs-first';
                                                   break;
                                               case 'agree':
                                                   $bg_status_class = 'bgs-agree';
                                                   break;
                                               case 'agreed':
                                                   $bg_status_class = 'bgs-agreed';
                                                   break;
                                               case 'confirmed':
                                                   $bg_status_class = 'bgs-confirmed';
                                                   break;
                                               case 'work':
                                                   $bg_status_class = 'bgs-work';
                                                   break;
                                               case 'performed':
                                                   $bg_status_class = 'bgs-performed';
                                                   break;
                                               case 'final_positive':
                                                   $bg_status_class = 'bgs-final_positive';
                                                   break;
                                               case 'final_negative':
                                                   $bg_status_class = 'bgs-final_negative';
                                                   break;
                                           }
                                       @endphp
                                       <span class="pseudo-select-txt o-btn {{$bg_status_class}}" data-pseudo="title" tabindex="0">{{$item->statusName()}}</span>
                                       <span class="pseudo-select-drop" data-pseudo="drop">
                                            <span data-order_id="{{$item->id}}" data-value="first" data-class="bgs-first" @if($item->status == 'first') class="current" @endif>Первичный контакт</span>
                                            <span data-order_id="{{$item->id}}" data-value="agree" data-class="bgs-agree" @if($item->status == 'agree') class="current" @endif>Согласовать</span>
                                            <span data-order_id="{{$item->id}}" data-value="agreed" data-class="bgs-agreed" @if($item->status == 'agreed') class="current" @endif>Согласованно</span>
                                            <span data-order_id="{{$item->id}}" data-value="confirmed" data-class="bgs-confirmed" @if($item->status == 'confirmed') class="current" @endif>Подтверждено</span>
                                            <span data-order_id="{{$item->id}}" data-value="work" data-class="bgs-work" @if($item->status == 'work') class="current" @endif>В работе</span>
                                            <span data-order_id="{{$item->id}}" data-value="performed" data-class="bgs-performed" @if($item->status == 'performed') class="current" @endif>Работы выполнены</span>
                                            <span data-order_id="{{$item->id}}" data-value="final_positive" data-class="bgs-final_positive" @if($item->status == 'final_positive') class="current" @endif>Успешно реализовано</span>
                                            <span data-order_id="{{$item->id}}" data-value="final_negative" data-class="bgs-final_negative" @if($item->status == 'final_negative') class="current" @endif>Не реализовано</span>
                                       </span>
                                   </span>
                            </td>
                            <td>
                                {{--@if($item->order->responsible_users()->count() > 0 )
                                    {{$item->order->responsible_users()->first()->fio()}}
                                @endif--}}
                                {{$item->user->fio()}}
                            </td>
                            <td>
                                @if($item->date_start)
                                    {{date('d.m.Y', strtotime($item->date_start))}}
                                @endif
                            </td>
                            <td>
                                @if($item->date_start)
                                    {{date('d.m.Y', strtotime($item->date_finish))}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="paginator-wrap">
                <div class="paginator-block fleft">
                    <div class="paginator-label">Показано {{sizeof($offers)}} из {{$count_offers}}</div>
                </div>
                <div class="paginator-block fright">
                    <div class="paginator-label">Количество записей</div>

                    <div class="pages-list">
                        <ul class="pages-list-ins">
                            <li @if($filters['perpage'] == 10) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(10, 'offers');">10</a></li>
                            <li @if($filters['perpage'] == 25) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(25, 'offers');">25</a></li>
                            <li @if($filters['perpage'] == 50) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(50, 'offers');">50</a></li>
                            <li @if($filters['perpage'] == 100) class="current" @endif><a href="javascript:void(0);" onclick="perpage_pagination(100, 'offers');">100</a></li>
                        </ul>
                    </div>

                    {!! $offers->render() !!}
                </div>
            </div>

            <div class="dash-container-layer"></div>
        </div>

@stop