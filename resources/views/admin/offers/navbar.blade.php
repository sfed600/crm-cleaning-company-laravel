<div class="dash-panel-block type-btns">
    <div class="dash-panel-dropdown dropdown-more" data-dropped="wrap">
        <div class="btn-dash-panel-more" data-dropped="btn" data-layer="yes">...</div>
        <div class="dash-panel-dropdown-drop">
            <a href="#">Ссылка 1</a>
            <a href="#">Ссылка 2</a>
            <a href="#">Ссылка 3</a>
        </div>
    </div>

    <a class="o-btn bgs-green o-hvr" href="{{route('admin.offers.create')}}">+ Добавить предложение</a>
</div>
<div class="dash-panel-block type-transactions">
    <span class="txt-label">{{$count_offers}} предложений:</span>
    <span class="txt-value">{{number_format($sum_paginate, 0, ',', ' ')}} руб</span>
</div>
