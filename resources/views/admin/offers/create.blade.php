@extends('admin.layout')
@section('main')
    <script src="/assets/admin/js/calculator.js?45"></script>

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.orders.index')}}">Предложения</a>
            </li>
            <li class="active">Добавление предложения</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Предложения
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Добавление
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a data-toggle="tab" href="#order" aria-expanded="true">
                        <i class="green ace-icon fa fa-home bigger-120"></i>
                        Предложение
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#calculator" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Калькулятор
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#products" aria-expanded="true">
                        <i class="green ace-icon fa fa-users bigger-120"></i>
                        Товары / Услуги
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="order" class="tab-pane fade active in">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <form id="form-order" class="form-horizontal" role="form" action="{{route('admin.offers.store')}}" method="POST" enctype="multipart/form-data">
                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                <input name="order_name" type="hidden">
                                <input type="hidden" name="calculator">

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-1"> # {{--Предложения--}} </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="number" placeholder="# Заказа" value="{{ old('number', $order_number) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-2"> Название </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-2" name="name" placeholder="Название" value="{{ old('name', $order_name) }}" class="col-sm-12 required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-222"> Площадь уборки</label>
                                    <div class="col-sm-2">
                                        <input type="text" id="form-field-222" name="area" value="{{ old('area', $request->input('area')) }}" class="col-sm-8">
                                        &nbsp;м.кв.
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-22"> Сумма </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-22" name="amount" placeholder="Сумма" value="{{ old('amount', $request->input('amount')) }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Дата начала</label>
                                    {{--<label class="col-sm-1 control-label no-padding-right" for="form-field-16"> Время начала</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker required" name="date_start" value="{{old('date_start', Carbon\Carbon::now()->format('d.m.Y'))}}" id="form-field-16" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>

                                        {{--<div class="input-group" style="width: 40%">
                                            <input class="form-control time-picker" name="timestart" value="{{old('timestart', '09:00')}}" type="text" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o bigger-110"></i>
                                            </span>
                                        </div>--}}
                                    </div>

                                    <label class="col-sm-1" for="form-field-116"> Дата окончания</label>
                                    {{--<label class="col-sm-1" for="form-field-116">Время окончания</label>--}}
                                    <div class="col-sm-4" style="display: flex;">
                                        <div class="input-group" style="width: 60%">
                                            <input class=" form-control date-picker" name="date_finish" value="{{old('date_finish', Carbon\Carbon::now()->format('d.m.Y'))}}" id="form-field-116" type="text" data-date-format="dd.mm.yyyy" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar bigger-110"></i>
                                            </span>
                                        </div>

                                        {{--<div class="input-group" style="width: 40%">
                                            <input class="form-control time-picker" name="timefinish" value="{{old('timefinish', '18:00')}}" type="text" />
                                            <span class="input-group-addon">
                                                <i class="fa fa-clock-o bigger-110"></i>
                                            </span>
                                        </div>--}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-3"> Контакты <span class="required">*</span></label>
                                    <div class="col-sm-8">
                                        <div class="dynamic-input">
                                            @if(old() && count(old('contact_id')) )
                                                @foreach(old('contact_id') as $key => $contact_id) {{--{{dd(dump(old('contact_id')))}}--}}
                                                    @if($contact_id > 0)
                                                        <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                                            <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                                <option value="{{$contact_id}}">{{\App\Models\OrderContact::find($contact_id)->name}}</option>
                                                            </select>
                                                            <a href="" class="input-group-addon @if($key==0)plus @else minus @endif">
                                                                <i class="glyphicon glyphicon-@if($key==0)plus @else minus @endif bigger-110"></i>
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @else
                                                <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                                    <select name="contact_id[]" class="chosen-select chosen-autocomplite form-control" id="form-field-3" data-url="{{route('admin.order_contacts.search')}}" data-placeholder="Начните ввод...">
                                                        @if($request->has('contact_id'))
                                                            <option value="{{$request->input('contact_id')}}">{{\App\Models\OrderContact::find($request->input('contact_id'))->name}}</option>
                                                        @else
                                                            <option value=""></option>
                                                        @endif
                                                    </select>
                                                    <a href="" class="input-group-addon plus">
                                                        <i class="glyphicon glyphicon-plus bigger-110"></i>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    {{--<div class="col-sm-1">
                                        <a class="btn btn-info btn-sm" href="javascript:void(0);" onclick="order_contacts_create('{{route('admin.order_contacts.create')}}?route=order', this);">
                                            <i class="ace-icon fa fa-plus bigger-110"></i>
                                            Создать
                                        </a>
                                    </div>--}}
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-10"> Компания </label>
                                    <div class="col-sm-8">
                                        <div class="input-group dynamic-input-contact col-sm-11" style="margin-bottom:5px;">
                                            <select name="order_company_id" class="chosen-select chosen-autocomplite form-control" id="form-field-order_company_id" data-url="{{route('admin.order_companies.search')}}" data-placeholder="Начните ввод...">
                                                @if( $order_company_id > 0 )
                                                    <option value="{{$order_company_id}}">{{\App\Models\OrderCompany::find($order_company_id)->name}}</option>
                                                @else
                                                    <option value=""></option>
                                                @endif
                                            </select>
                                        </div>

                                        {{--@if( $order_company_id > 0 )
                                            <select name="order_company_id" class="col-sm-12 form-control">
                                                @foreach($companies as $company)
                                                    <option value="{{$company->id}}" @if($order_company_id == $company->id ) selected @endif>{{$company->name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <select name="order_company_id" class="col-sm-12 form-control">
                                                <option></option>
                                                @foreach($companies as $company)
                                                    <option value="{{$company->id}}" @if((old() && old('order_company_id')==$company->id) || (!old() && $request->has('order_company_id') && $request->input('order_company_id')==$company->id )) selected @endif>{{$company->name}}</option>
                                                @endforeach
                                            </select>
                                        @endif--}}

                                    </div>
                                    {{--<div class="col-sm-1">
                                         <a class="btn btn-info btn-sm" href="javascript:void(0);" onclick="order_company_create('{{route('admin.order_companies.create')}}?route=order', this, 'order');">
                                            <i class="ace-icon fa fa-plus bigger-110"></i>
                                            Создать
                                        </a>
                                    </div>--}}
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-161"> Метро</label>
                                    <div class="col-sm-9">
                                        <select name="metro_id" class="chosen-select chosen-autocomplite form-control" id="form-field-161" data-url="{{route('admin.metro.search')}}" data-placeholder="Начните ввод...">
                                            <option value="">  </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-162"> Адрес </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-162" name="address" placeholder="Адрес" value="{{ old('address') }}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-apartment"> Квартира</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="apartment" value="{{old('apartment')}}" id="form-field-apartment" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-porch"> Подъезд</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="porch" value="{{old('porch')}}" id="form-field-porch" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-floor"> Этаж</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="floor" value="{{old('floor')}}" id="form-field-floor" type="text" />
                                    </div>

                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-domofon"> Код домофона</label>
                                    <div class="col-sm-1">
                                        <input class=" form-control" name="domofon" value="{{old('domofon')}}" id="form-field-domofon" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-note"> Примечание </label>
                                    {{--<div class="col-sm-10">
                                        <textarea name="note">{{old('note')}}</textarea>
                                    </div>--}}
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-note" name="note" placeholder="Примечание" value="{{old('note')}}" class="col-sm-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-8"> Статус </label>
                                    <div class="col-sm-9">
                                        <select name="status" class="col-sm-12 form-control">
                                            <option></option>
                                            @foreach(\App\Models\Order::$statuses as $key => $status)
                                                <option value="{{$key}}" @if((old() && old('status')==$key) || (!old() &&  $key=='first')) selected @endif>{{$status['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label no-padding-right" for="form-field-5"> Ответсвенный </label>
                                    <div class="col-sm-9">
                                        <select name="user_id" class="chosen-select chosen-autocomplite form-control" id="form-field-5" data-url="{{route('admin.users.search', ['block' => 'Management'])}}" data-placeholder="Начните ввод...">
                                              <option value="{{Auth::user()->id}}" selected>{{Auth::user()->fio()}}</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-success" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>
                                            Сохранить
                                        </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <a class="btn btn-info" href="{{route('admin.orders.index')}}">
                                            <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                            Назад
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div><!-- /.col -->
                    </div><!-- /.row -->

                </div>

                <div id="calculator" class="tab-pane">
                    @include('admin.offers.calculator')
                </div>

                <div id="products" class="tab-pane">
                    @include('admin.offers.products')
                </div>

            </div>
        </div>

    </div>
@stop
