@extends('admin.layout')
@section('main')

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.auto.index')}}">Автомобили</a>
            </li>
            <li class="active">Модели</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <ul class="nav nav-tabs padding-18">
            <li>
                <a href="{{route('admin.auto.index')}}">
                    Автомобили
                </a>
            </li>
            <li class="active">
                <a href="{{route('admin.auto_models.index')}}">
                    Модели
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="page-header">
                            <h1>
                                Автомобили
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Список всех моделей
                                </small>
                            </h1>
                        </div><!-- /.page-header -->

                        @include('admin.message')

                        <div class="row">
                            <div class="col-xs-12">

                                <div class="table-header">
                                    Список всех моделей
                                    <div class="ibox-tools">
                                        <a href="{{route('admin.auto_models.create')}}" class="btn btn-success btn-xs">
                                            <i class="fa fa-plus"></i>
                                            Добавить модель
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="row">

                                            <table id="simple-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Название</th>
                                                    <th></th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @forelse($models as $item)
                                                    <tr>
                                                        <td>
                                                            <a href="{{route('admin.auto_models.edit', $item->id)}}">{{$item->name}}</a>
                                                        </td>
                                                        <td>
                                                            <div class="hidden-sm hidden-xs btn-group">

                                                                <a class="btn btn-xs btn-info" href="{{route('admin.auto_models.edit', $item->id)}}">
                                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                                </a>
                                                                <form method="POST" action='{{route('admin.auto_models.destroy', $item->id)}}' style="display:inline;">
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                                    <button class="btn btn-xs btn-danger action-delete" type="button" style="border-width: 1px;">
                                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <p>Нет моделей</p>
                                                @endforelse
                                                </tbody>
                                            </table>

                                            <div class="row" style="border-bottom:none;">
                                                <div class="col-xs-6">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                                        {!! $models->render() !!}
                                                    </div>
                                                </div>
                                            </div>


                                        </div><!-- /.row -->
                                    </div>
                                </div>

                            </div><!-- /.col -->
                        </div><!-- /.row -->


            </div>
        </div>
    </div><!-- /.page-content -->


@stop