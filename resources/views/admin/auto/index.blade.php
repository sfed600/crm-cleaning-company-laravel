@extends('admin.layout')
@section('main')

    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>
            <li class="active">Автомобили</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <ul class="nav nav-tabs padding-18">
            <li  class="active">
                <a href="{{route('admin.auto.index')}}">
                    Автомобили
                </a>
            </li>
            <li>
                <a href="{{route('admin.auto_models.index')}}">
                    Модели
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active">
                <div class="page-header">
                            <h1>
                                Автомобили
                                <small>
                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                    Список всех автомобилей
                                </small>
                            </h1>
                        </div><!-- /.page-header -->

                        @include('admin.message')

                        <div class="row">
                            <div class="col-xs-12">

                                <div class="table-header">
                                    Список всех автомобилей
                                    <div class="ibox-tools">
                                        <a href="{{route('admin.auto.create')}}" class="btn btn-success btn-xs">
                                            <i class="fa fa-plus"></i>
                                            Добавить автомобиль
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                                        <!-- PAGE CONTENT BEGINS -->
                                        <div class="row">

                                            <form method="GET" action="{{route('admin.auto.index')}}">
                                                <input type="hidden" name="f[order]">
                                            </form>

                                            <table id="simple-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        Модель
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="model-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="model-desc"></i>
                                                    </th>
                                                    <th nowrap>
                                                        Гос. номер
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="number-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="number-desc"></i>
                                                    </th>
                                                    <th>
                                                        СТС №
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="sts-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="sts-desc"></i>
                                                    </th>
                                                    <th>
                                                        Дата постановки на учет
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="date_registration-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="date_registration-desc"></i>
                                                    </th>
                                                    <th nowrap>
                                                        Год выпуска
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="year-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="year-desc"></i>
                                                    </th>
                                                    <th>
                                                        VIN
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="vin-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="vin-desc"></i>
                                                    </th>
                                                    <th>
                                                        Свидетельство о регистрации
                                                    </th>
                                                    <th>
                                                        ПТС
                                                    </th>
                                                    <th>
                                                        Действие строховки
                                                        <i class="fa fa-fw fa-sort-up" style="position: absolute;" sort="date_insurance-asc"></i>
                                                        <i class="fa fa-fw fa-sort-down" sort="date_insurance-desc"></i>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @forelse($autos as $item)
                                                    <tr>
                                                        <td>{{$item->model->name}}</td>
                                                        <td>
                                                            <a href="{{route('admin.auto.edit', $item->id)}}">{{$item->number}}</a>
                                                        </td>
                                                        <td>{{$item->sts}}</td>
                                                        <td>{{$item->date_registration}}</td>
                                                        <td>{{$item->year}}</td>
                                                        <td>{{$item->vin}}</td>
                                                        <td>
                                                        @if($item->certificate)
                                                            <a href="/{{$item->getFilePath().$item->certificate}}" class="btn btn-xs btn-success">
                                                                <i class="ace-icon glyphicon glyphicon-picture bigger-120"></i>
                                                            </a>
                                                        @endif
                                                        </td>
                                                        <td>
                                                        @if($item->pts)
                                                            <a href="/{{$item->getFilePath().$item->pts}}" class="btn btn-xs btn-success">
                                                                <i class="ace-icon glyphicon glyphicon-picture bigger-120"></i>
                                                            </a>
                                                        @endif
                                                        </td>
                                                        <td>{{$item->date_insurance}}</td>
                                                        <td>
                                                            <div class="hidden-sm hidden-xs btn-group">

                                                                <a class="btn btn-xs btn-info" href="{{route('admin.auto.edit', $item->id)}}">
                                                                    <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                                </a>
                                                                <form method="POST" action='{{route('admin.auto.destroy', $item->id)}}' style="display:inline;">
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                                    <button class="btn btn-xs btn-danger action-delete" type="button" style="border-width: 1px;">
                                                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <p>Нет автомобилей</p>
                                                @endforelse
                                                </tbody>
                                            </table>

                                            <div class="row" style="border-bottom:none;">
                                                <div class="col-xs-6">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                                                        {!! $autos->render() !!}
                                                    </div>
                                                </div>
                                            </div>


                                        </div><!-- /.row -->
                                    </div>
                                </div>

                            </div><!-- /.col -->
                        </div><!-- /.row -->


            </div>
        </div>
    </div><!-- /.page-content -->


@stop