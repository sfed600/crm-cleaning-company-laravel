@extends('admin.layout')
@section('main')
    <div class="breadcrumbs" id="breadcrumbs">
        <script type="text/javascript">
            try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
        </script>

        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="{{route('admin.main')}}">Главная</a>
            </li>

            <li>
                <a href="{{route('admin.auto.index')}}">Автомобили</a>
            </li>
            <li class="active">Добавление автомобиля</li>
        </ul><!-- /.breadcrumb -->


    </div>

    <div class="page-content">

        <div class="page-header">
            <h1>
                Автомобили
                <small>
                    <i class="ace-icon fa fa-angle-double-right"></i>
                    Добавление
                </small>
            </h1>
        </div><!-- /.page-header -->

        @include('admin.message')

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form class="form-horizontal" role="form" action="{{route('admin.auto.store')}}" method="POST" enctype="multipart/form-data">
                    <input name="_token" type="hidden" value="{{csrf_token()}}">

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-201"> Модель </label>
                        <div class="col-sm-9">
                            <select name="model_id" id="form-field-201">
                                <option value="">--Не выбрана--</option>
                                @foreach($models as $model)
                                    <option value="{{$model->id}}" @if(old() && old('model_id')==$model->id) selected="selected" @endif>{{$model->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-0"> Гос. номер </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-0" name="number" placeholder="Гос. номер" value="{{ old('number') }}" class="col-sm-5">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Дата постановки на учет</label>
                        <div class="col-sm-9">
                            <div class="input-group col-sm-4">
                                <input class="form-control date-picker" name="date_registration" value="{{old('date_registration')}}" id="form-field-1" type="text" data-date-format="dd.mm.yyyy" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Год выпуска</label>
                        <div class="col-sm-9">
                            <div class="input-group col-sm-4">
                                <input class="form-control date-picker-year" name="year" value="{{old('year')}}" id="form-field-1" type="text" data-date-format="yyyy" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-10"> VIN </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-10" name="vin" placeholder="VIN" value="{{ old('vin') }}" class="col-sm-5">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> Свидетелельство о регистрации </label>
                        <div class="col-sm-4">
                            <input type="file" name="certificate" class="file-input-img col-sm-6"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> ПТС </label>
                        <div class="col-sm-4">
                            <input type="file" name="pts" class="file-input-img col-sm-6"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> Договор </label>
                        <div class="col-sm-4">
                            <input type="file" name="contract" class="file-input-img col-sm-6"  />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-15"> Действие строховки</label>
                        <div class="col-sm-9">
                            <div class="input-group col-sm-4">
                                <input class="form-control date-picker" name="date_insurance" value="{{old('date_insurance')}}" id="form-field-15" type="text" data-date-format="dd.mm.yyyy" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar bigger-110"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-11"> СТС № </label>
                        <div class="col-sm-9">
                            <input type="text" id="form-field-11" name="sts" placeholder="СТС №" value="{{ old('sts') }}" class="col-sm-5">
                        </div>
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-success" type="submit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Сохранить
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <button class="btn" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Обновить
                            </button>
                            &nbsp; &nbsp; &nbsp;
                            <a class="btn btn-info" href="{{route('admin.auto.index')}}">
                                <i class="ace-icon glyphicon glyphicon-backward bigger-110"></i>
                                Назад
                            </a>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
@stop
