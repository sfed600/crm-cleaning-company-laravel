<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    {!! Meta::render() !!}

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.jpg" type="image/x-icon" />

    <link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>
    @include('blocks.header')
    @yield('content')
    @include('blocks.footer')

    @widget('AdminPanelWidget')

    <div id="fade"></div>
    <link rel="stylesheet" type="text/css" href="/css/queries.css" />
    <!--[if lt IE 9]!><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <!--[if !IE]><!-->
    <script>
        if ( /*@cc_on!@*/ false) {
            document.documentElement.className += ' ie10';
        }
    </script>
    <!--<![endif]-->
    <!--[if IE 9]>
    <link rel="stylesheet" href="/css/ie9.css" type="text/css" />
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" href="/css/ie8.css" type="text/css" />
    <![endif]-->
    <link rel="stylesheet" href="/css/animate.css">

    <a href="#" class="scrollup">Наверх</a>
    <script src="/js/jquery.2.2.4.js"></script>
    <script src="/js/jquery.smoothscroll.js"></script>
    <script src="/slider/jquery.bxslider.js"></script>
    <script src="/slider/plugins/jquery.easing.1.3.js"></script>
    <script src="/slider/plugins/jquery.fitvids.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/js/wow.min.js"></script>

    <!-- lightgallery.css & lightgallery.js. -->
    <link rel="stylesheet" href="/js/lightgallery/css/lightgallery.css"/>
    <script src="/js/lightgallery/js/lightgallery.js"></script>

    <script type="text/javascript" src="/js/custom.js"></script>
    <script type="text/javascript" src="/js/script.js?2"></script>

    @include('blocks.counters')

</body>
</html>