<?php

use Illuminate\Database\Seeder;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\UserGroup::create(['name' => 'Admin',
                                      'name_rus' => 'Адмиинистратор',
                                      'descr' => 'Группа администратора, с полными провами']);
        App\Models\UserGroup::create(['name' => 'Moderator',
                                      'name_rus' => 'Модератор',
                                      'descr' => 'Группа модераторов, с ограниченным кол-вом прав']);
        App\Models\UserGroup::create(['name' => 'User',
                                      'name_rus' => 'Сотрудник',
                                      'descr' => 'Группа сотрудников компании']);
    }
}
