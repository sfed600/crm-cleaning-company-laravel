<?php

use Illuminate\Database\Seeder;

class UserBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\UserBlock::create(['name' => 'Management',
                                      'name_rus' => 'Сотрудники']);
        App\Models\UserBlock::create(['name' => 'Workers',
                                      'name_rus' => 'Персонал']);
        App\Models\UserBlock::create(['name' => 'SuperAdmin',
                                      'name_rus' => 'Супер Админ']);
    }
}
