<?php

use Illuminate\Database\Seeder;

class UserProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\UserProfession::create(['name' => 'Бригадир']);
        App\Models\UserProfession::create(['name' => 'Водитель']);
        App\Models\UserProfession::create(['name' => 'Дворник']);
        App\Models\UserProfession::create(['name' => 'Доп. сотрудник']);
        App\Models\UserProfession::create(['name' => 'Уборщик']);
        App\Models\UserProfession::create(['name' => 'Уборщица']);
        App\Models\UserProfession::create(['name' => 'Штатный сотрудник']);
    }
}
