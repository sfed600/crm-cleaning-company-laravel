<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserGroupSeeder::class);
        //$this->call(UserSeeder::class);
        //$this->call(UserProfessionSeeder::class);
        //$this->call(MetroSeeder::class);
        $this->call(UserBlockSeeder::class);
    }
}
