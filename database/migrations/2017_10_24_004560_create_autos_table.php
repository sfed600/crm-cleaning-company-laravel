<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            //$table->foreign('model_id')->references('id')->on('auto_models')->onDelete('set null');
            $table->string('number');
            $table->date('date_registration');
            $table->integer('year');
            $table->string('vin');
            $table->string('certificate');
            $table->string('pts');
            $table->string('contract');
            $table->date('date_insurance');
            $table->string('sts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('autos');
    }
}
