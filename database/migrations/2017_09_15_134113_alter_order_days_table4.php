<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDaysTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->boolean('nigth')->default(0)->after('timestart');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->dropColumn('nigth');
        });
    }
}
