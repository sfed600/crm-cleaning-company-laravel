<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTable6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('apartment', 4)->after('address');
            $table->string('porch', 4)->after('apartment');
            $table->string('floor', 3)->after('porch');
            $table->string('domofon', 5)->after('floor');
            $table->text('note')->after('domofon');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('apartment');
            $table->dropColumn('porch');
            $table->dropColumn('floor');
            $table->dropColumn('domofon');
            $table->dropColumn('note');

        });
    }
}
