<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTable5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('timestart')->default('9:00')->after('date');
            $table->date('date_finish')->after('timestart');
            $table->string('timefinish')->default('18:00')->after('date_finish');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('timestart');
            $table->dropColumn('date_finish');
            $table->dropColumn('timefinish');
        });
    }
}
