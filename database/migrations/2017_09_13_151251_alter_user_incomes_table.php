<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->enum('action', ['income', 'outcome'])->default('outcome')->after('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->dropColumn('action');
        });
    }
}
