<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderCompaniesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_companies', function (Blueprint $table) {
            $table->enum('typ', ['customer', 'provider', 'contractor'])->after('name')->nullable();
            $table->string('logo', 255)->after('typ')->nullable();
            $table->string('building', 5)->after('address')->nullable();
            $table->string('housing', 5)->after('building')->nullable();
            $table->string('office', 5)->after('housing')->nullable();
            //$table->integer('user_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_companies', function (Blueprint $table) {
            $table->dropColumn('typ');
            $table->dropColumn('logo');
            $table->dropColumn('building');
            $table->dropColumn('housing');
            $table->dropColumn('office');
        });
    }
}
