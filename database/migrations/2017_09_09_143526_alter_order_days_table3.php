<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDaysTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->integer('timestart')->default(0)->after('ours');
            $table->integer('timefinish')->default(0)->after('ours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->dropColumn('timestart');
            $table->dropColumn('timefinish');
        });
    }
}
