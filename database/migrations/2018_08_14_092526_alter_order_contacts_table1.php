<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderContactsTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_contacts', function (Blueprint $table) {
            $table->enum('typ', ['customer', 'provider', 'contractor'])->after('name');
            $table->string('photo', 255)->after('typ');
            $table->string('address', 255)->after('post');
            $table->string('apartment', 4)->after('address');
            $table->string('porch', 4)->after('apartment');
            $table->string('floor', 3)->after('porch');
            $table->string('domofon', 5)->after('floor');
            //$table->integer('user_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_contacts', function (Blueprint $table) {
            $table->dropColumn('typ');
            $table->dropColumn('photo');
            $table->dropColumn('address');
            $table->dropColumn('apartment');
            $table->dropColumn('porch');
            $table->dropColumn('floor');
            $table->dropColumn('domofon');
        });
    }
}
