<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->string('name');
            $table->integer('amount');
            $table->string('status');

            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('restrict');

            $table->date('date_start')->nullable();
            $table->string('timestart', 5);
            $table->date('date_finish')->nullable();
            $table->string('timefinish', 5);

            $table->integer('metro_id')->unsigned()->nullable()->default(null);
            $table->foreign('metro_id')->references('id')->on('metro')->onDelete('set null');
            $table->string('address');
                        
            $table->integer('order_company_id')->unsigned()->nullable();
            $table->foreign('order_company_id')->references('id')->on('order_companies')->onDelete('set null');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->string('apartment', 4);
            $table->string('porch', 4);
            $table->string('floor', 3);
            $table->string('domofon', 5);
            $table->text('note');

            $table->timestamps();

            //$table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offers');
    }
}
