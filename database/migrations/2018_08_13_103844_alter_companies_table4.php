<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('building', 4)->after('address');
            $table->string('housing', 4)->after('building');
            $table->string('office', 4)->after('housing');
            //$table->text('note')->after('office');

            $table->enum('typ', ['customer', 'provider', 'contractor'])->after('full_company_name');
            //$table->integer('user_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->after('typ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('building');
            $table->dropColumn('housing');
            $table->dropColumn('office');
            $table->dropColumn('typ');
            //$table->dropColumn('note');
        });
    }
}
