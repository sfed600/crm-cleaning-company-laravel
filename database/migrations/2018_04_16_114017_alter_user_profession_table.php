<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserProfessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_professions', function (Blueprint $table) {
            $table->integer('company_id')->unsigned()->nullable()->after('name');
            //$table->foreign('company_id')->references('id')->on('companies')->onDelete('restrict');
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_id');
    }
}
