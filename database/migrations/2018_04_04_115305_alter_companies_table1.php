<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('phone')->after('inn');
            $table->string('email')->after('phone');
            $table->string('site')->after('email');
            $table->text('comments')->after('site');
            $table->string('short_company_name')->after('comments');
            $table->string('full_company_name')->after('short_company_name');
            $table->string('ogrn')->after('full_company_name');
            $table->string('kpp')->after('ogrn');
            $table->date('date_register')->after('kpp');
            $table->string('okpo')->after('date_register');
            $table->string('oktmo')->after('okpo');
            $table->string('director')->after('oktmo');
            $table->string('buhgalter')->after('director');
            $table->string('company_dir_fio')->after('buhgalter');
            $table->string('bank_name')->after('company_dir_fio');
            $table->string('bik')->after('bank_name');
            $table->string('checking_account')->after('bik');
            $table->string('cor_account')->after('checking_account');
            //$table->integer('user_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('email');
            $table->dropColumn('site');
            $table->dropColumn('comments');
            $table->dropColumn('short_company_name');
            $table->dropColumn('full_company_name');
            $table->dropColumn('ogrn');
            $table->dropColumn('kpp');
            $table->dropColumn('date_register');
            $table->dropColumn('okpo');
            $table->dropColumn('oktmo');
            $table->dropColumn('director');
            $table->dropColumn('buhgalter');
            $table->dropColumn('company_dir_fio');
            $table->dropColumn('bank_name');
            $table->dropColumn('bik');
            $table->dropColumn('checking_account');
            $table->dropColumn('cor_account');
        });
    }
}
