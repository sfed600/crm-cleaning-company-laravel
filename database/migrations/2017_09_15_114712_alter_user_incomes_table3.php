<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterUserIncomesTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->float('amount_nigth')->nullable()->default(null)->after('amount');
            DB::statement('ALTER TABLE user_incomes MODIFY COLUMN amount double NULL DEFAULT NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->dropColumn('amount_nigth');
        });
    }
}
