<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');
            //$table->timestamps();
            
            /*$table->integer('service_id')->unsigned();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('restrict');*/
            
            $table->integer('company_id')->unsigned();
            //$table->foreign('company_id')->references('id')->on('companies')->onDelete('restrict');
            $table->foreign('company_id')->references('id')->on('companies');
            
            //$table->integer('price')->unsigned();
            $table->json('price')->nullable();
            
            $table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
    }
}
