<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDaysTable5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->integer('metro_id')->unsigned()->nullable()->default(null);
            $table->foreign('metro_id')->references('id')->on('metro')->onDelete('restrict');
            //$table->string('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_days', function (Blueprint $table) {
            $table->dropColumn('metro_id');
        });
    }
}
