<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTable4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('name');
            $table->integer('amount');
            $table->integer('order_company_id')->unsigned()->nullable();
            $table->foreign('order_company_id')->references('id')->on('order_companies')->onDelete('set null');
            $table->string('status');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('amount');
            $table->dropColumn('order_company_id');
            $table->dropColumn('status');
            $table->dropColumn('user_id');
        });
    }
}
