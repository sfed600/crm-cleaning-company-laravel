<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('staff');
            $table->json('work_regions')->nullable();

            $table->string('apartment', 4)->after('address');
            $table->string('porch', 4)->after('apartment');
            $table->string('floor', 3)->after('porch');
            $table->string('domofon', 5)->after('floor');
            $table->text('note')->after('domofon');

            $table->integer('metro_id')->unsigned()->nullable()->default(null);
            $table->foreign('metro_id')->references('id')->on('metro')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('staff');
            $table->dropColumn('work_regions');
            $table->dropColumn('apartment');
            $table->dropColumn('porch');
            $table->dropColumn('floor');
            $table->dropColumn('domofon');
            $table->dropColumn('note');
            $table->dropColumn('metro_id');
        });
    }
}
