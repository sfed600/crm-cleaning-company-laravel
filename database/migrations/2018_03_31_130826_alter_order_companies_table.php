<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_companies', function (Blueprint $table) {
            $table->string('bank_name');
            $table->string('bik');
            $table->string('checking_account');
            $table->string('cor_account');
            //$table->integer('user_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_companies', function (Blueprint $table) {
            $table->dropColumn('bank_name');
            $table->dropColumn('bik');
            $table->dropColumn('checking_account');
            $table->dropColumn('cor_account');
        });
    }
}
